// var request = require('request');
var express = require('express');
var socketio = require('socket.io');
var fs = require('fs');

var log = {
    all: true,
    debug: true,
    info: true,
};

var app = express();

app.get('/', function (req, res) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    res.header('Content-Type', 'application/json; charset=utf-8');
    var getResult = { result: '' };
    if (log.all || log.debug) console.log(req.query);

    if (req.query.type != undefined) {
        switch (req.query.type) {
            case 'count':
                getResult.result = io.engine.clientsCount;
                break;
        }
    }

    res.send(JSON.stringify(getResult));
});

var server = require('http').createServer(app);

server.listen(8001);

var io = socketio.listen(server);

io.set('transports', ['websocket', 'flashsocket', 'htmlfile', 'xhr-polling', 'jsonp-polling', 'polling']);
io.on('connection', function (socket) {
    console.log('socket connected ==>');
    //설정 저장
    socket.on('setup', function (data) {
        if (log.all || log.debug) console.log('setup', data.key);
        if (data.key == undefined || data.pageid == undefined) return;
        io.sockets.in('player_' + data.key).emit('setup', data);
    });

    //설정 접속
    socket.on('player', function (data) {
        if (log.all || log.debug) console.log('player', data);
        if (data.key == undefined || data.pageid == undefined) return;
        var getRoomName = 'player_' + data.key;
        socket.join(getRoomName);
        if (log.all || log.debug) console.log('player join', getRoomName, '접속자수 : ' + io.engine.clientsCount);
    });
});

setInterval(function () {
    if (log.all == true || log.info == true) {
        console.log('접속자수 : ', io.engine.clientsCount);
    }
}, 60 * 1000);
