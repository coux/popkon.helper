<?php
    $root_path = $_SERVER["DOCUMENT_ROOT"]."/";
	include $root_path."lib/common.php";
	
    $page_url = $_SERVER["HTTP_HOST"];
    $page_type = "setup";
    $page_key = isset($_GET["key"]) ? $_GET["key"] : "";
    $page_id = isset($_GET["id"]) && !empty($_GET["id"]) ? $_GET["id"] : "chat";
	$page_sub = isset($_GET["sub"]) && !empty($_GET["sub"]) ? $_GET["sub"] : "0";
	// $page_asp = isset($_GET["pcode"]) && !empty($_GET["pcode"]) ? $_GET["pcode"] : "";

	$get_key = "";
	$get_mngr = "";
	
	
    if(empty($page_key)) {
    	$page_key = isset($_SESSION["key"]) ? $_SESSION["key"] : "";
	}

	if(empty($page_key)) { 
        echo "ERROR:: 잘못된 페이지 요청 입니다. [NULL KEY]";
        die();
	}

	// if(empty($page_asp)) {
	// 	$page_asp = isset($_SESSION["asp"]) ? $_SESSION["asp"] : "";
	// }

	// if(empty($page_asp)) { 
    //     echo "ERROR:: 잘못된 페이지 요청 입니다. [NULL PCODE]";
    //     die();
	// }
	

	$test_id = isset($_GET["test"]) && !empty($_GET["test"]) ? $_GET["test"] : "";

	if(empty($test_id)) {
		$test_id = isset($_SESSION["test"]) ? $_SESSION["test"] : "";
	}
	$_SESSION["key"] = $page_key;
	$_SESSION["asp"] = $page_asp;
	$_SESSION["test"] = $test_id;
	
    $login_info = new stdClass;
    $login_info->idx = "";
	$login_info->idkey = "";
    $login_info->id = "";
    $login_info->name = "";
    $login_info->profile = "";
    $login_info->page = "";
	$login_info->url = "";
	$login_info->web = "";
	$login_info->svrKor = "";
	$login_info->svrEng = "";
	$login_info->coin = "";
	$login_info->asp = "";
	
	$user_info = new loginInfo();

	$login_data = $user_info->userInfo($page_key);
    if(isset($login_data->data->id)) {
		$login_info->id = $login_data->data->id;
		$login_info->name = $login_data->data->nickName;
		$login_info->profile = $login_data->data->pFileName;
		$login_info->web = $login_data->data->p_web;
		$login_info->svrKor = $login_data->data->svrName_kor;
		$login_info->svrEng = strtolower($login_data->data->svrName_eng);
		$login_info->coin = $login_data->data->coinName;
		$login_info->asp = $login_data->data->pcode;
		$login_info->key = urlencode($page_key);
	}
	else {
		$login_info->id = $test_id;
    	$login_info->name = "이름";
	}

	print_r($login_data);

	
	
	//TODO 회원 정보 변환 하기
    //로그인 체크
    $page_array = array("chat", "alert", "alertlist", "goal", "subtitle", "text", "banner", "timer", "update", "notice", "help", "faq", "bbs");
    if(in_array($page_id, $page_array) == false) {
        $page_id = "chat";
	}
	
    if(!empty($page_id) && !empty($login_info->id)) {
		$user_key = new userKey($popkon_keycode);
		$get_key = $user_key->encrypt($login_info->id);
		$get_playerurl = $user_key->encrypt($page_id."/".$login_info->id);
        $login_info->page = $site_domain."/page/".$get_playerurl;
        $login_info->url = $site_domain."/page/".$get_playerurl."/".$page_sub;
		$login_info->idkey = $get_key;
        // $login_result = $user_info->dbsave($login_info);
	
		//로그 저장
		$log_data = new stdClass;
		$log_data->idkey = $get_key;
		$log_data->id = $login_info->id;
		$log_data->name = $login_info->name;
		$log_data->type = "setup";
		$log_data->ip = $_SERVER["REMOTE_ADDR"];
		$log_data->agent = isset($_SERVER["HTTP_USER_AGENT"]) ? $_SERVER["HTTP_USER_AGENT"] : "";
		$log_data->referer = isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : "";
		
		// $user_info->dblog($log_data);
		//프리셋
		$get_presetdata = $user_info->presetload($get_key, $page_id);
		if(isset($get_presetdata) && count($get_presetdata) > 0) {
			$get_preset_check = false;
			foreach ($get_presetdata as $key => $value) {
				if(intval($value[0]) === intval($page_sub)) {
					$get_preset_check = true;
					break;
				}
			}
			
			if($get_preset_check === false) {
				header("Location: /".$page_id."/".$get_presetdata[0][0]);
				exit;
			}
		}
    }
?>

<!DOCTYPE html>
<html lang="ko" class="<?=$page_type?>">
<head>
    <title><?=$login_info->svrKor?> 방송도우미</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=0.5, minimum-scale=0.5, maximum-scale=1, user-scalable=yes">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    
    <!-- css -->
    <link rel="stylesheet" media="all" href="/resource/css/vendor/nouislider.min.css">
    <link rel="stylesheet" media="all" href="/resource/css/vendor/spectrum.css">
    <link rel="stylesheet" media="all" href="/resource/css/vendor/codemirror.min.css">
    <link rel="stylesheet" media="all" href="/resource/css/vendor/animate.min.css">
    <link rel="stylesheet" media="all" href="/resource/css/vendor/bannerslider.css?v=1">
    <link rel="stylesheet" media="all" href="/resource/css/vendor/summernote-lite.css">
    <link rel="stylesheet" media="all" href="/resource/css/vendor/flipclock.css">
    
    <!-- require -->
    <script src="/resource/js/vendor/require.js"></script>
    <script>
        var debug = null;
        var mngrcheck = "<?=$get_mngr;?>";
        var page = {};
        var sub = {};
        var pageType = "<?=$page_type?>";
        var pageKey = "<?=$get_key?>";
        var pageId = "<?=$page_id?>";
        var pageSub = "<?=$page_sub?>";
		var loginInfo = eval(<?=json_encode($login_info);?>);
	

        var nodeIp = "<?=$node_ip?>";
        var nodeUrl = "<?=$node_url?>";
        
        var requireArray = ["jquery", "socket.io", "noUiSlider", "spectrum", "notify", "Clipboard", "ajaxForm", "CodeMirror", "Sortable", "imgpreload", "twemoji",  "soundManager", "summernote", "FlipClock", "datejs", "sub"];
        requirejs.config({
            urlArgs:"ver=<?=$set_ver?>",
            baseUrl:"/resource/js",
            paths:{
                jquery:"vendor/jquery-3.3.1.min",
                "socket.io":"vendor/socket.io",
                CodeMirror:"vendor/codemirror.min",
                Clipboard:"vendor/clipboard.min",
                noUiSlider:"vendor/nouislider.min",
                spectrum:"vendor/spectrum",
                ajaxForm:"vendor/jquery.form.min",
                notify:"vendor/notify.min",
                imgpreload:"vendor/jquery.imgpreload.min",
                textillate:"vendor/jquery.textillate",
                lettering:"vendor/jquery.lettering",
                Sortable:"vendor/Sortable.min",
                ProgressBar:"vendor/progressbar.min",
                soundManager:"vendor/soundmanager2-nodebug-jsmin",
                summernote:"vendor/summernote-lite",
                summernotekr:"vendor/lang/summernote-ko-KR.min",
                FlipClock:"vendor/flipclock.min",
                datejs:"vendor/date.min",
                wowSlider:"vendor/bannerslider",
                bannerscript:"vendor/bannerscript",
                twemoji:"vendor/twemoji.min",
                sub:pageId
            },
            shim:{
                "socket.io":{
                    deps:[],
                    exports:"io"
                },
                noUiSlider:{
                    deps:["jquery"]
                },
                spectrum:{
                    deps:["jquery"]
                },
                notify:{
                    deps:["jquery"]
                },
                ajaxForm:{
                    deps:["jquery"]
                },
                imgpreload:{
                    deps:["jquery"]
                },
                textillate:{
                    deps:["jquery"]
                },
                lettering:{
                    deps:["jquery"]
                },
                Sortable:{
                    deps:["jquery"]
                },
                ProgressBar:{
                    deps:["jquery"]
                },
                soundManager:{
                    deps:["jquery"]
                },
                summernote:{
                    deps:["jquery"]
                },
                summernotekr:{
                    deps:["jquery", "summernote"]
                },
                FlipClock:{
                    deps:["jquery"]
                },
                wowSlider:{
                    deps:["jquery"]
                },
                bannerscript:{
                    deps:["jquery"]
                },
                twemoji:{ deps:["jquery"], exports:"twemoji" },
                sub:{
                    deps:["jquery", "socket.io"]
                }
            }
        });
    </script>
    
	<link rel="stylesheet" media="all" href="/resource/css/common.css?ver=<?=$set_ver?>">

    <script src="/resource/js/index.js?ver=<?=$set_ver?>"></script>
    <!--[if lt IE 10]>
        <script src="/resource/js/vendor/typedarray.js"></script>
    <![endif]-->
</head>
<body class="<?=$page_id?>">
    <div id="setup">
        <?
            if($page_id == "main") {
                include $root_path."lib/setup/main.php";
            }
            else if($page_id == "update" || $page_id == "help") {
                include $root_path."lib/setup/".$page_id.".php";
            }
            else {
                include $root_path."lib/setup/setup.php";
            }
        ?>
    </div>
</body>
</html>    
