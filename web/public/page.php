<?
	
    $root_path = $_SERVER["DOCUMENT_ROOT"]."/";
    include $root_path."lib/common.php";
    
    $page_url = $_SERVER["HTTP_HOST"];
	$page_type = "player";
    $page_id = isset($_GET["id"]) && !empty($_GET["id"]) ? $_GET["id"] : "chat";
	$page_sub = isset($_GET["sub"]) && !empty($_GET["sub"]) ? $_GET["sub"] : "0";
	$page_asp= isset($_GET["pcode"]) && !empty($_GET["pcode"]) ? $_GET["pcode"] : "";
	$player_key = isset($_GET["key"]) ? $_GET["key"] : "";
	
	$get_key = "";
    $get_mngr = isset($_GET["test"]) ? $_GET["test"] : "";
	
    $page_list = array();
    $login_info = new stdClass;
    $login_info->idx = "";
	$login_info->idkey = "";
    $login_info->id = "";
    $login_info->name = "";
    $login_info->profile = "";
    $login_info->page = "";
	$login_info->url = "";
	$login_info->web = "";
	$login_info->svrKor = "";
	$login_info->svrEng = "";
	$login_info->coin = "";
	$login_info->asp = "";

	
    if(!empty($player_key)) {
		$user_key = new userKey($popkon_keycode);
		$data = explode("/", $player_key);
	
		$get_page_array = $user_key->decrypt($data[0]);
		$get_array = explode("/", $get_page_array);

        if(count($get_array) > 1) {
            $page_id = $get_array[0];
			$page_list = array(array($page_id, $page_sub));
			$crypt = new crypt();
			$data = $crypt->encrypt($get_array[1].'/'.$page_asp);
			$user_info = new loginInfo();
			if(!empty($page_asp)){
				$login_data = $user_info->userInfo($data);
				$login_info->id = $login_data->data->id;
				$login_info->name = $login_data->data->nickName;
				$login_info->profile = $login_data->data->pFileName;
				$login_info->web = $login_data->data->p_web;
				$login_info->svrKor = $login_data->data->svrName_kor;
				$login_info->svrEng = strtolower($login_data->data->svrName_eng);
				$login_info->coin = $login_data->data->coinName;
				$login_info->asp = $login_data->data->pcode;
				$get_key = $user_key->encrypt($login_info->id);
			}
			
			//로그 저장
			$log_data = new stdClass;
			$log_data->idkey = $get_key;
			$log_data->id = $login_info->id;
			$log_data->name = $login_info->name;
			$log_data->type = $page_id;
			$log_data->ip = $_SERVER["REMOTE_ADDR"];
			$log_data->agent = isset($_SERVER["HTTP_USER_AGENT"]) ? $_SERVER["HTTP_USER_AGENT"] : "";
			$log_data->referer = isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : "";
			// $user_info->dblog($log_data);
        }
    }
    
    //페이지 체크
    $page_array = array("chat", "alert", "alertlist", "goal", "subtitle", "text", "banner", "timer");
    if(in_array ($page_id, $page_array) == false) {
        $page_id = "";
    }
    //if(isset($login_info->id) && $login_info->id === "darkyop") $get_mngr = "mngr";
?>

<!DOCTYPE html>
<html lang="ko" class="<?=$page_type?>">
<head>
    <meta name="robots" content="noindex, nofollow">
    <title><?=$login_info->svrKor?> 방송도우미</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">
	<meta Http-Equiv="Cache-Control" Content="no-cache">
	<meta Http-Equiv="Pragma" Content="no-cache">
	<meta Http-Equiv="Expires" Content="0">
	<meta Http-Equiv="Pragma-directive: no-cache">
	<meta Http-Equiv="Cache-directive: no-cache">
    
    <!-- css -->
    <link rel="stylesheet" media="all" href="/resource/css/vendor/nouislider.min.css">
    <link rel="stylesheet" media="all" href="/resource/css/vendor/spectrum.css">
    <link rel="stylesheet" media="all" href="/resource/css/vendor/codemirror.min.css">
    <link rel="stylesheet" media="all" href="/resource/css/vendor/animate.min.css">
    <link rel="stylesheet" media="all" href="/resource/css/vendor/bannerslider.css?v=1">
    <link rel="stylesheet" media="all" href="/resource/css/vendor/summernote-lite.css">
    <link rel="stylesheet" media="all" href="/resource/css/vendor/flipclock.css">
    
    <!-- require -->
    <script src="/resource/js/vendor/require.js"></script>
    <script>
        var debug = null;
        var mngrcheck = "<?=$get_mngr;?>";
        var player_key = "<?=$player_key;?>";
        var page = {};
        var sub = {};
        var io = {};
        var pageType = "<?=$page_type?>";
        var pageId = "<?=$page_id?>";
        var pageList = eval(<?=json_encode($page_list);?>);
        var pageKey = "<?=$get_key?>";
        var pageSub = "<?=$page_sub?>";
		var loginInfo = eval(<?=json_encode($login_info);?>);
	
        var nodeIp = "<?=$node_ip?>";
		var nodeUrl = "<?=$node_url?>";

        var requireArray = ["jquery", "socket.io", "noUiSlider", "spectrum", "notify", "Clipboard", "ajaxForm", "CodeMirror", "Sortable", "imgpreload", "twemoji", "soundManager", "FlipClock", "datejs", "sub"];
        requirejs.config({
            urlArgs:"ver=<?=$set_ver?>",
            baseUrl:"/resource/js",
            paths:{
                jquery:"vendor/jquery-3.3.1.min",
                "socket.io":"vendor/socket.io",
                CodeMirror:"vendor/codemirror.min",
                Clipboard:"vendor/clipboard.min",
                noUiSlider:"vendor/nouislider.min",
                spectrum:"vendor/spectrum",
                ajaxForm:"vendor/jquery.form.min",
                notify:"vendor/notify.min",
                imgpreload:"vendor/jquery.imgpreload.min",
                textillate:"vendor/jquery.textillate",
                lettering:"vendor/jquery.lettering",
                Sortable:"vendor/Sortable.min",
                ProgressBar:"vendor/progressbar.min",
                soundManager:"vendor/soundmanager2-nodebug-jsmin",
                summernote:"vendor/summernote-lite",
                summernotekr:"vendor/lang/summernote-ko-KR.min",
                FlipClock:"vendor/flipclock.min",
                datejs:"vendor/date.min",
                wowSlider:"vendor/bannerslider",
                bannerscript:"vendor/bannerscript",
                twemoji:"vendor/twemoji.min",
                sub:pageId
            },
            shim:{
                "socket.io":{
                    deps:[],
                    exports:"io"
                },
                noUiSlider:{
                    deps:["jquery"]
                },
                spectrum:{
                    deps:["jquery"]
                },
                notify:{
                    deps:["jquery"]
                },
                ajaxForm:{
                    deps:["jquery"]
                },
                imgpreload:{
                    deps:["jquery"]
                },
                textillate:{
                    deps:["jquery"]
                },
                lettering:{
                    deps:["jquery"]
                },
                Sortable:{
                    deps:["jquery"]
                },
                ProgressBar:{
                    deps:["jquery"]
                },
                soundManager:{
                    deps:["jquery"]
                },
                summernote:{
                    deps:["jquery"]
                },
                summernotekr:{
                    deps:["jquery", "summernote"]
                },
                FlipClock:{
                    deps:["jquery"]
                },
                wowSlider:{
                    deps:["jquery"]
                },
                bannerscript:{
                    deps:["jquery"]
                },
                twemoji:{ deps:["jquery"], exports:"twemoji" },
                sub:{
                    deps:["jquery", "socket.io"]
                }
            }
        });
        
    </script>
    
    <link rel="stylesheet" media="all" href="/resource/css/common.css?ver=<?=$set_ver?>">
	<script src="/resource/js/index.js?ver=<?=$set_ver?>"></script>
	<script>

		function jsQuery(data) {
			if(window.postMessage){
				window.postMessage('test');
			}
			if(window.cefQuery){
				window.cefQuery({ request: 'test' });
				document.body.innerHTML = '<div style="background:#fff;"><h1 style="color:white; font-size:40px;">hi</h1></div>';
			}
		}
	
	</script>
    <!--[if lt IE 10]>
        <script src="/resource/js/vendor/typedarray.js"></script>
    <![endif]-->
</head>
<body class="<?=$page_id?>">
    <div id="page">
        <? if(empty($get_key) || empty($page_id)) { ?>
            <div class="error_area">
                <p class="error_title">접속 오류</p>
                <div class="error_text">
                    <p>접속정보가 올바르지 않습니다. URL을 다시한번 확인해주세요.</p>
                </div>
            </div>
        <? } else {
                include $root_path."lib/player/player.php";
            }
        ?>
    </div>
</body>
</html>    
