sub.help = {
    init: function() {
        var lnbMenu = $(".lnb_area .help_menu .btn_menu").on("click", function() {
            lnbMenu.removeClass("active");
            $(".help_area .help_panel").removeClass("active").eq(lnbMenu.index($(this).addClass("active"))).addClass("active");
            return false;
        });
        $(".help_area .help_tab .btn_tab").on("click", function() {
            $(this).parent().find(".btn_tab").removeClass("active");
            $(this).closest(".help_panel").find(".tab_panel").removeClass("active").eq($(this).addClass("active").index()).addClass("active");
            return false;
        });
        
        $("#header .btn_top, .setup_btn .btn_top").on("click", function() {
            $("html, body").stop(true,true).animate({ scrollTop:0 }, 200);
            return false;
        });
        
        var getBody = $("body");
        var getSetupGroup = $(".setup_area .menu_panel");
        var getLnbMenu = $("#header .lnb.menu .menu");
        var getSaveBtn = $(".setup_btn");
        var getSaveCheck = getSaveBtn.length > 0;
        getLnbMenu.on("click", function() {
            var getIdx = getLnbMenu.index($(this));
            if(getSetupGroup.eq(getIdx).length > 0) {
                $("html,body").stop(true,true).animate({ scrollTop:getSetupGroup.eq(getIdx).offset().top - 136 }, 200, function() {
                    getLnbMenu.removeClass("active").eq(getIdx).addClass("active");
                });
            }
            return false;
        });
        function lnbScroll(idx) {
            getLnbMenu.removeClass("active");
            if(idx > -1) {
                getLnbMenu.eq(idx).addClass("active");
            }
        }
        function pageScroll() {
            var getScrollTop = $(window).scrollTop();
            var getDocHeight = $(window).height();
            var getScrollLeft = $(window).scrollLeft();
            if(getScrollTop > 23) {
                getBody.addClass("header_fixed");
            }
            else {
                getBody.removeClass("header_fixed");
            }
            var getIdx = -1;
            getSetupGroup.each(function(idx) {
                var getTop = $(this).offset().top;
                if(getScrollTop > getTop - 156) {
                    getIdx = idx;
                }
            });
            lnbScroll(getIdx);
            
            if(getSaveCheck === true) {
                var getSaveTop = getSaveBtn.offset().top + 82;
                if(getScrollTop + getDocHeight > getSaveTop) {
                    getBody.removeClass("save_fixed");
                }
                else {
                    getBody.addClass("save_fixed");
                }
            }
            
            $("body.header_fixed .content_area .lnb_area, body.save_fixed .setup_area .setup_btn .btn_wrap").css("transform", "translateX(-" + getScrollLeft + "px)");
        }
        $(window).on("scroll", function() {
            pageScroll();
        });
        pageScroll();
    }
}
