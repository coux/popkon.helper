define(['ProgressBar'], function (ProgressBar) {
    sub.goal = {
        opt: {
            main: null,
            use: true, //툴팁설정
            sound: true,

            idx: 0,
            prevtype: '',
            maxval: {},
        },
        sub: '',
        data: { font: 'Jeju Gothic', fontbold: false, progressani: true, colorstart: '#1d976c', colorend: '#93f9b9', opacity: '0', customstyle: false, customstylecss: '', typelist: ['star'], autoreset: false, startmin: '0', startmax: '100', text: '오늘의 {목표종류} 목표치', textin: '{목표값}{개} ({목표퍼센트})', style: 'bar', dir: 'width', border: '40', textonly: false, colorinner: '#dddddd', colortext: '#ffffff', colortextin: '#000000', addfonturl: '', addfontname: '', fontsize: '16', shadowsize: '2', colorshadow: 'rgba(0, 0, 0, 0.8)', bgtrans: 'none', colorbg: '#000000', bgimgzoom: false, image: {}, repeat: '10', item_goal_completeauto: false, item_goal_completeautoval: '100' },
        head:
            '.item_goal .goal_bar{background:#1d976c; background: -moz-linear-gradient(left, #1d976c 0%, #93f9b9 100%); background: -webkit-linear-gradient(left, #1d976c 0%, #93f9b9 100%); background: linear-gradient(to right, #1d976c 0%, #93f9b9 100%);}.item_goal .goal_box{margin-left:-20px;margin-top:-20px;width:40px;height:40px}.item_goal .goal_inner{background-color:#dddddd}.item_goal .text{color:#ffffff}.item_goal .goal_textin{color:#000000}.item_goal .text,.text_preview .item_goal{text-shadow:-1px -1px 0px rgba(0,0,0,0.8),-1px -0.5px 0px rgba(0,0,0,0.8),-1px 0px 0px rgba(0,0,0,0.8),-1px 0.5px 0px rgba(0,0,0,0.8),-1px 1px 0px rgba(0,0,0,0.8),-0.5px -1px 0px rgba(0,0,0,0.8),-0.5px -0.5px 0px rgba(0,0,0,0.8),-0.5px 0px 0px rgba(0,0,0,0.8),-0.5px 0.5px 0px rgba(0,0,0,0.8),-0.5px 1px 0px rgba(0,0,0,0.8),0px -1px 0px rgba(0,0,0,0.8),0px -0.5px 0px rgba(0,0,0,0.8),0px 0px 0px rgba(0,0,0,0.8),0px 0.5px 0px rgba(0,0,0,0.8),0px 1px 0px rgba(0,0,0,0.8),0.5px -1px 0px rgba(0,0,0,0.8),0.5px -0.5px 0px rgba(0,0,0,0.8),0.5px 0px 0.5px rgba(0,0,0,0.8),0.5px 0.5px 0.5px rgba(0,0,0,0.8),0.5px 1px 0.5px rgba(0,0,0,0.8),1px -1px 0px rgba(0,0,0,0.8),1px -0.5px 0px rgba(0,0,0,0.8),1px 0px 1px rgba(0,0,0,0.8),1px 0.5px 1px rgba(0,0,0,0.8),1px 1px 1px rgba(0,0,0,0.8)}.item_goal .text,.text_preview .item_goal{text-shadow:-1px -1px 0px rgba(0, 0, 0, 0.8),-1px -0.5px 0px rgba(0, 0, 0, 0.8),-1px 0px 0px rgba(0, 0, 0, 0.8),-1px 0.5px 0px rgba(0, 0, 0, 0.8),-1px 1px 0px rgba(0, 0, 0, 0.8),-0.5px -1px 0px rgba(0, 0, 0, 0.8),-0.5px -0.5px 0px rgba(0, 0, 0, 0.8),-0.5px 0px 0px rgba(0, 0, 0, 0.8),-0.5px 0.5px 0px rgba(0, 0, 0, 0.8),-0.5px 1px 0px rgba(0, 0, 0, 0.8),0px -1px 0px rgba(0, 0, 0, 0.8),0px -0.5px 0px rgba(0, 0, 0, 0.8),0px 0px 0px rgba(0, 0, 0, 0.8),0px 0.5px 0px rgba(0, 0, 0, 0.8),0px 1px 0px rgba(0, 0, 0, 0.8),0.5px -1px 0px rgba(0, 0, 0, 0.8),0.5px -0.5px 0px rgba(0, 0, 0, 0.8),0.5px 0px 0.5px rgba(0, 0, 0, 0.8),0.5px 0.5px 0.5px rgba(0, 0, 0, 0.8),0.5px 1px 0.5px rgba(0, 0, 0, 0.8),1px -1px 0px rgba(0, 0, 0, 0.8),1px -0.5px 0px rgba(0, 0, 0, 0.8),1px 0px 1px rgba(0, 0, 0, 0.8),1px 0.5px 1px rgba(0, 0, 0, 0.8),1px 1px 1px rgba(0, 0, 0, 0.8)}.item_goal.bgtrans_color{background-color:#000000}.item_goal.item_goal_bgimgzoom{background-size:100% 100%!important}',
        cls: 'item_box item_goal item_goal_progressani style_bar dir_width bgtrans_none',
        style: 'opacity: 1; font-size: 16px; font-family: "Jeju Gothic", 돋움;',
        loaddata: {},
        init: function (pid, psub) {
            sub.goal.opt.main = $('.item_goal');
            sub.goal.sub = psub;

            if (pageType == 'setup') {
                //칼러 선택
                $('.color_gradient .btn_color')
                    .each(function () {
                        var getColorArray = $(this).data('color').split(',');
                        var getStyle = 'background:' + getColorArray[0] + '; background: -moz-linear-gradient(left, ' + getColorArray[0] + ' 0%, ' + getColorArray[1] + ' 100%); background: -webkit-linear-gradient(left, ' + getColorArray[0] + ' 0%, ' + getColorArray[1] + ' 100%); background: linear-gradient(to right, ' + getColorArray[0] + ' 0%, ' + getColorArray[1] + ' 100%)';
                        $(this).attr('style', getStyle);
                    })
                    .on('click', function () {
                        var getColorArray = $(this).data('color').split(',');
                        $("[name='item_goal_colorstart']").val(getColorArray[0]).data('value', getColorArray[0]).trigger('change');
                        $("[name='item_goal_colorend']").val(getColorArray[1]).data('value', getColorArray[1]).trigger('change');
                        $("[name='item_goal_colorstart']").val(getColorArray[0]);
                        $("[name='item_goal_colorend']").val(getColorArray[1]);
                        return false;
                    });

                //종류 선택
                $('.goal_list .check').on('change', function () {
                    sub.goal.loaddata.typelist = [];
                    $('.goal_list .check:checked').each(function () {
                        sub.goal.loaddata.typelist.push(this.value);
                    });
                });
            } else {
                //저장된값 불러오기
                var getStorage = page.storage.load('goal_star');
                if (getStorage != null && getStorage.data != undefined && !isNaN(getStorage.data)) {
                    if (getStorage.time != undefined && !isNaN(getStorage.time)) {
                        var getNow = new Date().getTime();
                        var getTime = getNow - Number(getStorage.time);
                        if (getTime < 3600000) {
                            page.data.star = Number(getStorage.data);
                        }
                    }
                }

                var getStorageDoosan = page.storage.load('goal_doosan');
                if (getStorageDoosan != null && getStorageDoosan.data != undefined && !isNaN(getStorageDoosan.data)) {
                    if (getStorageDoosan.time != undefined && !isNaN(getStorageDoosan.time)) {
                        var getNow = new Date().getTime();
                        var getTime = getNow - Number(getStorageDoosan.time);
                        if (getTime < 3600000) {
                            page.data.doosan = Number(getStorageDoosan.data);
                        }
                    }
                }
            }

            //커스텀 그래프 설정
            sub.goal.circleProgress.init();

            //설정 불러오기
            $('#popup_mask').fadeIn(200);
            setTimeout(function () {
                page[pageType].load(pid, psub, function () {
                    if (pageType == 'setup') {
                        sub.goal.opt.idx = 0;
                        sub.goal.add();
                    }
                });
            }, 10);
        },
        add: function (data) {
            sub.goal.repeat();
            doTimeout(
                'goal_add',
                function () {
                    var chatOpt = sub.goal.opt;
                    var loadData = sub.goal.loaddata;
                    if (data != undefined && data.type != undefined && $.inArray(data.type, loadData.typelist) > -1) {
                        var getType = data.type;
                    } else {
                        var getIdx = chatOpt.idx;
                        var getTypeList = loadData.typelist;
                        chatOpt.idx += 1;
                        if (chatOpt.idx > getTypeList.length - 1) {
                            chatOpt.idx = 0;
                        }
                        var getType = getTypeList[getIdx];
                    }
                    var typeCheck = false;
                    if (chatOpt.prevtype != getType) {
                        typeCheck = true;
                    }
                    chatOpt.prevtype = getType;

                    if (page.data[getType] != undefined) {
                        var getVal = page.data[getType];
                        getVal = !isNaN(getVal) && Number(getVal) > -1 ? Number(getVal) : 0;
                        if (getVal < 0) getVal = 0;

                        var getMin = !isNaN(loadData.startmin) ? Number(loadData.startmin) : 0;
                        if (chatOpt.maxval[getType] != undefined) {
                            var getMax = Number(chatOpt.maxval[getType]);
                        } else {
                            var getMax = !isNaN(loadData.startmax) ? Number(loadData.startmax) : 100;
                        }
                        if (getMin >= getMax || getMax === 0) {
                            if (pageType == 'setup') {
                                page.msg('시작 값은 목표 값보다 작아야 하며, 목표 값은 0 보다 커야 합니다.', 'warn');
                            }
                            loadData.starmin = 0;
                            loadData.startmax = 100;
                            getMin = 0;
                            getMax = 100;
                        }
                        // if(getType === "star") {
                        // if(loadData.startmin !== undefined && !isNaN(loadData.startmin)) {
                        // getVal += Number(loadData.startmin);
                        // }
                        // }
                        if (loadData.completeauto === true && getVal >= getMax && loadData.completeautoval !== '' && !isNaN(loadData.completeautoval)) {
                            var getAutoVal = Number(loadData.completeautoval);
                            for (var i = getMax; getMax <= getVal; i += getAutoVal) {
                                getMax += getAutoVal;
                            }
                            chatOpt.maxval[getType] = getMax;
                        }

                        var getUnit = page.common.goal.unit[getType];
                        var getName = page.common.goal.name[getType];
                        var getPercent = parseInt((getVal * 100) / (getMax - 0), 10);
                        var getText = loadData.text
                            .replace('{목표종류}', getName)
                            .replace('{목표값}', page.comma(getVal))
                            .replace('{목표퍼센트}', getPercent + '%')
                            .replace('{목표최소값}', page.comma(0))
                            .replace('{목표최대값}', page.comma(getMax))
                            .replace('{개}', getUnit);
                        getText = page.common.replace(getText);
                        chatOpt.main.find('.goal_box .goal_text').html(page.common.br(getText));

                        var getTextIn = loadData.textin
                            .replace('{목표종류}', getName)
                            .replace('{목표값}', page.comma(getVal))
                            .replace('{목표퍼센트}', getPercent + '%')
                            .replace('{목표최소값}', page.comma(0))
                            .replace('{목표최대값}', page.comma(getMax))
                            .replace('{개}', getUnit);
                        getTextIn = page.common.replace(getTextIn);
                        chatOpt.main.find('.goal_box .goal_textin').html(page.common.br(getTextIn));
                        chatOpt.main.find('.goal_box .goal_start .value').html(page.comma(0));
                        chatOpt.main.find('.goal_box .goal_end .value').html(page.comma(getMax));
                        chatOpt.main.find('.goal_box .goal_start .val, .goal_box .goal_end .val').html(getUnit);

                        var getInner = chatOpt.main.find('.goal_box .goal_inner');
                        if (typeCheck === true || (data && data.repeat != undefined && data.repeat === true)) {
                            getInner.removeClass('animate').css('effect');
                            getInner.removeAttr('style');
                        }
                        doTimeout(
                            'goal_val',
                            function () {
                                getInner.addClass('animate');

                                //커스텀 스타일
                                if (loadData.style == 'bar') {
                                    sub.goal.circleProgress.load(false);
                                    if (loadData.dir == 'width') getInner.css('width', 100 - getPercent + '%');
                                    else if (loadData.dir == 'height') getInner.css('height', 100 - getPercent + '%'); //getInner.css("height", getPercent + "%");
                                } else {
                                    sub.goal.circleProgress.load(true, loadData.style, Number(getPercent / 100));
                                }
                            },
                            100
                        );
                    }
                },
                100
            );
        },
        repeat: function () {
            var chatOpt = sub.goal.opt;
            var loadData = sub.goal.loaddata;
            var getTime = !isNaN(loadData.repeat) ? Number(loadData.repeat) : 0;
            doTimeout('goal_repeat', false);
            if (getTime > 0) {
                doTimeout(
                    'goal_repeat',
                    function () {
                        sub.goal.add({ repeat: true });
                    },
                    getTime * 1000
                );
            }
        },
        circleProgress: {
            box: null,
            obj: null,
            init: function () {
                sub.goal.circleProgress.box = sub.goal.opt.main.find('.item_area');
                $(window).on('resize', function () {
                    doTimeout(
                        'goal_resize',
                        function () {
                            sub.goal.circleProgress.resize();
                        },
                        100
                    );
                });
            },
            load: function (type, category, value) {
                var chatOpt = sub.goal.opt;
                var loadData = sub.goal.loaddata;

                var getProcess = chatOpt.main.find('.goal_progress');
                if (type == true) {
                    var circleType = category;
                    if (circleType == 'bar') {
                        sub.goal.circleProgress.obj = null;
                        getProcess.empty();
                        return;
                    }

                    getProcess.empty();
                    var circleProgressWidth = !isNaN(loadData.border) ? Number(loadData.border) / 6.5 : 6;

                    if (pageType == 'setup') {
                        var circleColor = $("[name='item_goal_colorstart']").val();
                        var circleColorEnd = $("[name='item_goal_colorend']").val();
                        var circleColorInnter = $("[name='item_goal_colorinner']").val();
                    } else {
                        var circleColor = loadData.colorstart;
                        var circleColorEnd = loadData.colorend;
                        var circleColorInnter = loadData.colorinner;
                    }
                    var circleOpt = {
                        strokeWidth: circleProgressWidth,
                        trailWidth: circleProgressWidth,
                        easing: 'easeInOut',
                        duration: 1400,
                        color: circleColor,
                        trailColor: circleColorInnter,
                        from: { color: circleColor },
                        to: { color: circleColorEnd },
                        svgStyle: null,
                        step: function (state, bar) {
                            bar.path.setAttribute('stroke', state.color);
                        },
                    };

                    if (circleType == 'circle') {
                        sub.goal.circleProgress.obj = new ProgressBar.Circle(getProcess.get(0), circleOpt);
                    } else if (circleType == 'arc') {
                        sub.goal.circleProgress.obj = new ProgressBar.SemiCircle(getProcess.get(0), circleOpt);
                    } else if (circleType == 'arcr') {
                        sub.goal.circleProgress.obj = new ProgressBar.SemiCircle(getProcess.get(0), circleOpt);
                    } else if (circleType == 'heart') {
                        var heartPath = $('<svg xmlns="http://www.w3.org/2000/svg" viewBox="-5 -10 180 180">' + '<path fill-opacity="0" stroke-width="' + circleProgressWidth * 1.4 + '" stroke="' + circleOpt.trailColor + '" d="M83.7,27.3c33.8-41.9,85-26,85,21s-85,108.3-85,108.3S.5,93.5.5,46.5,52.3-18.1,86,24.2Z"/>' + '<path class="heart-path" fill-opacity="0" stroke-width="' + circleProgressWidth * 1.4 + '" stroke="' + circleOpt.color + '" d="M83.7,27.3c33.8-41.9,85-26,85,21s-85,108.3-85,108.3S.5,93.5.5,46.5,52.3-18.1,86,24.2Z"/></svg>');
                        var heartPath = $(
                            '<svg xmlns="http://www.w3.org/2000/svg" viewBox="70 15 510 455" style="padding-top:15px">' +
                                '<path fill-opacity="0" stroke-width="' +
                                circleProgressWidth * 3 +
                                '" stroke="' +
                                circleOpt.trailColor +
                                '" stroke-linejoin="round" stroke-miterlimit="4" d="m217.619186,24.642044c-137.447105,-2.471161 -264.638546,212.481247 105.925735,435.340683c0.406281,-0.73114 1.968781,-0.73114 2.375031,0c382.517975,-230.048676 234.665131,-451.640152 92.625702,-434.390625c-55.372711,6.724487 -81.503326,37.456451 -93.813263,63.88797c-12.309845,-26.431519 -38.440552,-57.163483 -93.81308,-63.88797c-4.438782,-0.539276 -8.866302,-0.870193 -13.300125,-0.950058z"/>' +
                                '<path class="heart-path" fill-opacity="0" stroke-width="' +
                                circleProgressWidth * 3 +
                                '" stroke="' +
                                circleOpt.color +
                                '" stroke-linejoin="round" stroke-miterlimit="4" d="m217.619186,24.642044c-137.447105,-2.471161 -264.638546,212.481247 105.925735,435.340683c0.406281,-0.73114 1.968781,-0.73114 2.375031,0c382.517975,-230.048676 234.665131,-451.640152 92.625702,-434.390625c-55.372711,6.724487 -81.503326,37.456451 -93.813263,63.88797c-12.309845,-26.431519 -38.440552,-57.163483 -93.81308,-63.88797c-4.438782,-0.539276 -8.866302,-0.870193 -13.300125,-0.950058z"/></svg>'
                        );

                        getProcess.html(heartPath);
                        sub.goal.circleProgress.obj = new ProgressBar.Path(heartPath.find('.heart-path').get(0), circleOpt);
                    } else if (circleType == 'stars') {
                        var heartPath = $('<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -6 200 200">' + '<path fill-opacity="0" stroke-linejoin="round" stroke-width="' + circleProgressWidth * 1.5 + '" stroke="' + circleOpt.trailColor + '" d="M 100 4 L 128.214 61.1672 L 191.301 70.3344 L 145.651 114.833 L 156.427 177.666 L 100 148 L 43.5726 177.666 L 54.3493 114.833 L 8.69857 70.3344 L 71.7863 61.1672 L 100 4 Z"/>' + '<path class="heart-path" fill-opacity="0" stroke-linejoin="round" stroke-width="' + circleProgressWidth * 1.5 + '" stroke="' + circleOpt.color + '" d="M 100 4 L 128.214 61.1672 L 191.301 70.3344 L 145.651 114.833 L 156.427 177.666 L 100 148 L 43.5726 177.666 L 54.3493 114.833 L 8.69857 70.3344 L 71.7863 61.1672 L 100 4 Z"/></svg>');

                        getProcess.html(heartPath);
                        sub.goal.circleProgress.obj = new ProgressBar.Path(heartPath.find('.heart-path').get(0), circleOpt);
                    }

                    if (value < 0) value = 0;
                    else if (value > 1) value = 1;

                    if (sub.goal.circleProgress.obj != null) {
                        var agent = navigator.userAgent.toLowerCase();
                        if (((navigator.appName == 'Netscape' && agent.indexOf('trident') !== -1) || agent.indexOf('msie') !== -1) && agent.indexOf('edge') === -1) {
                            sub.goal.circleProgress.obj.set(value);
                        } else {
                            sub.goal.circleProgress.obj.animate(value);
                        }

                        sub.goal.circleProgress.resize();
                    }
                } else {
                    if (sub.goal.circleProgress.obj != null) {
                        sub.goal.circleProgress.obj = null;
                        getProcess.empty();
                    }
                }
            },
            resize: function () {
                if (sub.goal.circleProgress.box != null) {
                    var getCircleWidth = sub.goal.circleProgress.box.width();
                    var getCircleHeight = sub.goal.circleProgress.box.height();
                    var getCircleSize = getCircleWidth > getCircleHeight ? getCircleHeight : getCircleWidth;
                    sub.goal.opt.main.find('.goal_circle').data('size', getCircleSize).css({ width: getCircleSize, height: getCircleSize });
                }
            },
        },
        reset: function () {
            page.data.star = 0;
            page.data.doosan = 0;
            if (sub.goal.loaddata.startmin !== undefined && !isNaN(sub.goal.loaddata.startmin)) {
                if (page.data.star < sub.goal.loaddata.startmin) page.data.star = Number(sub.goal.loaddata.startmin);
                if (page.data.doosan < sub.goal.loaddata.startmin) page.data.doosan = Number(sub.goal.loaddata.startmin);
            }
            page.storage.save('goal_star', page.data.star);
            page.storage.save('goal_doosan', page.data.doosan);
            sub.goal.add();
        },
    };
});
