define(["summernote", "summernotekr"], function(summernote) {
    sub.text = {
        opt: {
            main: null,
            use: true,   //툴팁설정
            sound: true,
            
            filteridnick: "",
            filterword: ""
        },
        sub:"",
        data: {"opacity":"0","font":"LotteMartDream","bgtrans":"none","colorbg":"#ffffff","bgimgzoom":false,"image":{},"text":""},
        head:'',
        cls:'',
        style:'',
        loaddata: {},
        init: function(pid, psub) {
            sub.text.opt.main = $(".item_text");
            sub.text.sub = psub;
            
            if(pageType == "setup") {
                $(".text_selectlist").find(".font_value").html("나눔고딕");
                var getFontlist = $(".text_selectlist").html();
                $("body").on("change", ".text_fontlist .select_font_text", function() {
                    $("#text_editor").summernote("fontName", this.value);
                });
                $(".text_selectlist").remove();
                var textFontlist = function (context) {
                    var ui = $.summernote.ui;
                    var button = ui.button({
                        contents: '<div class="text_fontlist">' + getFontlist + '</div>',
                        tooltip: '폰트 선택',
                        click: function () {
                        }
                    });
                    return button.render();
                }
                
                $("#text_editor").summernote({
                	focus: false,
                    lang: "ko-KR",
                    minHeight: 200,
                    toolbar: [
                        ['style', ['bold', 'italic', 'underline', 'clear']],
                        ['font', ['strikethrough', 'superscript', 'subscript']],
                        ['fontsize', ['fontsize']],
                        ['custom', ['fontlist']],
                        ['color', ['color']],
                        ['para', ['paragraph']],
                        ['height', ['height']]
                    ],
                    fontSizes: ["8", "10", "12", "14", "16", "18", "20", "22", "24", "26", "30", "34", "40", "46", "52", "60"],
                    buttons: {
                        fontlist: textFontlist
                    }
                });
                $("#text_editor").on("summernote.change", function() {
                    doTimeout("text_change", function() {
                        $(".preview_area .item_text").html(sub.text.save());
                    }, 100);
                });
            }
            
            //설정 불러오기
            $("#popup_mask").fadeIn(200);
            setTimeout(function() {
                page[pageType].load(pid, psub, function() {
                    if(pageType == "setup") {
                        $(".preview_area .item_text").html(sub.text.loaddata.text);
                        $("#text_editor").summernote("fontSize", 14);
                        $("#text_editor").summernote("fontName", "Nanum Gothic");
                        $("#text_editor").summernote("code", sub.text.loaddata.text);
                    }
                    else {
                        setInterval(function() {
                            $(".item_text").html(page.common.replace(sub.text.loaddata.text));
                        }, 10000);
                    }
                });
            }, 10);
        },
        add: function() {
            doTimeout("text_add", function(data) {
                var chatOpt = sub.text.opt;
                var loadData = sub.text.loaddata;
            }, 100);
        },
        save: function() {
            return $("#text_editor").summernote("code");
        }
    }
});