define(["FlipClock", "datejs"], function(FlipClock) {
	sub.timer = {
	    opt: {
	        main: null,
	        use: true,   //툴팁설정
	        sound: true,
	        
	        filteridnick: "",
	        filterword: ""
	    },
	    sub:"",
	    data: {"opacity":"0","clocktheme":"normal","customstyle":false,"customstylecss":"","bgtrans":"none","colorbg":"#000000","bgimgzoom":false,"image":{}},
	    head:'',
	    cls:'item_box item_timer clocktheme_normal',
	    style:'opacity: 1',
	    loaddata: {},
	    init: function(pid, psub) {
	        sub.timer.opt.main = $(".item_timer");
	        sub.timer.sub = psub;
	        
	        //설정 불러오기
	        $("#popup_mask").fadeIn(200);
	        setTimeout(function() {
	            page[pageType].load(pid, psub, function() {
	            	sub.timer.clock.load("!시간");
	            });
	        }, 10);
	    },
	    clock: {
            timer: null,
            load: function(msg) {
                var getMsg = $.trim(msg).split("/");
                var themeName = {"일반":"", "디지털":"digital", "네이버":"naver"};
                if(sub.debug) console.log("clock", msg);
                
                try {
                    if(getMsg[0] == "!시간") {
                        
                        if(getMsg.length > 1 && getMsg[1].indexOf("테마") > -1) {
                            if(getMsg.length > 2 && getMsg[2] !== "") {
                                $("#item_chat").removeClass("digital naver").addClass(themeName[getMsg[2]]);
                            }
                            return;
                        }
                        
                        if(sub.timer.clock.timer !== null) {
                            sub.timer.clock.timer.stop();
                            sub.timer.clock.timer.timer._destroyTimer();
                            sub.timer.clock.timer = null;
                        }
                        
                        var getTimeCheck = false;
                        var getTimeType = "";
                        var getTotalTime = 0;
                        
                        if(getMsg.length == 1) {
                            //현재시간
                            getTimeCheck = true;
                            getTimeType = "time";
                        }
                        else if(getMsg.length > 1 && getMsg[1] !== "") {
                            if(!isNaN(getMsg[1]) && Number(getMsg[1]) >= 0) {
                                getTimeCheck = true;
                                getTimeType = "start";
                                getTotalTime = Number(getMsg[1]);
                            }
                            if(getMsg[1].indexOf("업타임") > -1 || getMsg[1].indexOf("방송시간") > -1) {
                                getTimeCheck = true;
                                getTimeType = "start";
                                var getStartTime = "";
                                if(getMsg[1].indexOf("트위치업타임") > -1 || getMsg[1].indexOf("트위치방송시간") > -1) {
                                    getStartTime = player.twitch.datainfo.start;
                                }
                                else if(getMsg[1].indexOf("유튜브업타임") > -1 || getMsg[1].indexOf("유튜브방송시간") > -1) {
                                    getStartTime = player.youtube.datainfo.start;
                                }
                                else {
                                    getStartTime = player.afreecatv.datainfo.start;
                                }
                                
                                if(getStartTime !== "") {
                                    getTotalTime = parseInt((new Date() - Date.parse(getStartTime)) / 1000, 10);
                                }
                            }
                            else {
                                var getTimeStr = getMsg[1];
                                var getTime = { hour:0, min:0, sec:0 };
                                if(getTimeStr.indexOf("시간") == -1 && getTimeStr.indexOf("분") == -1 && getTimeStr.indexOf("초") == -1 && Number(getTimeStr) > 0) {
                                    getTime.sec = Number(getTimeStr);
                                }
                                else {
                                    if(getTimeStr.indexOf("시간") > -1) {
                                        var getTimeHour = getTimeStr.split("시간");
                                        getTimeStr = getTimeHour.length > 1 ? getTimeHour[1] : "";
                                        if(getTimeHour[0] !== "") {
                                            getTime.hour = Number(getTimeHour[0]) * 3600;
                                        }
                                    }
                                    if(getTimeStr.indexOf("분") > -1) {
                                        var getTimeHour = getTimeStr.split("분");
                                        getTimeStr = getTimeHour.length > 1 ? getTimeHour[1] : "";
                                        if(getTimeHour[0] !== "") {
                                            getTime.min = Number(getTimeHour[0]) * 60;
                                        }
                                    }
                                    if(getTimeStr.indexOf("초") > -1) {
                                        var getTimeHour = getTimeStr.split("초");
                                        if(getTimeHour[0] !== "") {
                                            getTime.sec = Number(getTimeHour[0]);
                                        }
                                    }
                                }
                                
                                getTotalTime = getTime.hour + getTime.min + getTime.sec;
                                if(getTotalTime > 0) {
                                    getTimeCheck = true;
                                    getTimeType = "countdown";
                                }
                            }
                        }
                        
                        if(getTimeCheck === true) {
                            if($("#timer").length == 0) {
                                sub.timer.opt.main.append('<div id="timer"><div class="timer_wrap timer_area"></div><div class="timer_theme timer_area"><div class="timer_box hour"></div><div class="timer_box min"></div><div class="timer_box sec"></div></div>');
                            }
                            var timer = $("#timer").attr("class", "");
                            var timerBox = $("#timer .timer_box");
                            var timeOpt = {};
                            if(getTimeType == "time") {
                                getTotalTime = 0;
                                timeOpt.clockFace = "TwentyFourHourClock";
                            }
                            else if(getTimeType == "start") {
                                timeOpt.countdown = false;
                            }
                            else if(getTimeType == "countdown") {
                                timeOpt.countdown = true;
                                timer.addClass("countdown t10");
                            }
                            timeOpt.callbacks = {
                            	start: function() {
                            		sub.timer.clock.resize();
                            		$(sub.timer.opt.main).addClass("active");
                            	},
                                interval: function() {
                                    if(this.countdown != undefined) {
                                        var getCurrentTime = new Date().clearTime().addSeconds(this.countdown === true ? (this.factory.original - this.count) : this.count);
                                    }
                                    else {
                                        var getCurrentTime = Date.parse(this.factory.original);
                                    }
                                    
                                    timerBox.eq(0).html(getCurrentTime.toString("HH")).end().eq(1).html(getCurrentTime.toString("mm")).end().eq(2).html(getCurrentTime.toString("ss"));
                                }
                            }
                            sub.timer.clock.timer = $("#timer .timer_wrap").FlipClock(getTotalTime, timeOpt);
                        }
                    }
                    else if(getMsg[0] == "!시간일시정지" || getMsg[0] == "!시간정지" || getMsg[0] == "!시간중지") {
                        //시간 일시정지
                        if(sub.timer.clock.timer !== null) {
                            sub.timer.clock.timer.stop();
                        }
                    }
                    else if(getMsg[0] == "!시간시작" || getMsg[0] == "!시간다시시작" || getMsg[0] == "!시간재생") {
                        //시간 일시정지 다시 시작
                        if(sub.timer.clock.timer !== null) {
                            sub.timer.clock.timer.start();
                        }
                    }
                    else if(getMsg[0] == "!시간삭제" || getMsg[0] == "!시간끝") {
                        if(sub.timer.clock.timer !== null) {
                            sub.timer.clock.timer.stop();
                            sub.timer.clock.timer.timer._destroyTimer();
                            sub.timer.clock.timer = null;
                        }
                        $("#timer").remove();
                        $("head .widget_timer").remove();
                    }
                }
                catch(error) {
                }
            },
            resize: function() {
            	var getRatio = $(sub.timer.opt.main).width() / 400;
	        	$(sub.timer.opt.main).find(".timer_area").css({ '-webkit-transform' : 'scale(' + getRatio + ')', '-moz-transform' : 'scale(' + getRatio + ')', '-ms-transform' : 'scale(' + getRatio + ')', '-o-transform' : 'scale(' + getRatio + ')', 'transform' : 'scale(' + getRatio + ')' });
	        	
            	doTimeout("timer_resize", function() {
            		sub.timer.clock.resize();
            	}, 1000);
            }
        },
	    add: function() {
	    },
	    save: function() {
	    }
	}
});
