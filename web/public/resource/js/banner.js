define(['Sortable', 'imgpreload', 'wowSlider', 'bannerscript'], function (Sortable, imgpreload, wowSlider) {
    sub.banner = {
        opt: {
            main: null,
            use: true, //툴팁설정
            sound: true,

            effectlist: 'fade,turn,shift,cube_over,louvers,lines,dribbles,parallax,glass_parallaxy,brick,basic,basic_linear,last,blinds,blur,book,bubbles,cube,domino,fly,page,rotate,seven,slices,stack,stack_vertical,tv',
            effectinterval: null,
        },
        sub: '',
        data: {
            effectlist: ['fade'],
            effectrandom: false,
            opacity: '0',
            customstyle: false,
            customstylecss: '',
            showtime: '10',
            duration: '0.5',
            image: {
                bannerimg: [
                    ['banner1.png', '//pic.popkontv.com/images/aspw/' + loginInfo.asp + '/banner/banner1.png'],
                    ['banner2.png', '//pic.popkontv.com/images/aspw/' + loginInfo.asp + '/banner/banner2.png'],
                ],
            },
        },
        head: '',
        cls: 'item_box item_banner',
        style: 'opacity: 1;',
        loaddata: {},
        init: function (pid, psub) {
            sub.banner.opt.main = $('.item_banner');
            sub.banner.sub = psub;

            if (pageType == 'setup') {
                var effectList = $('#effect_list');
                $.each(sub.banner.opt.effectlist.split(','), function (key, value) {
                    effectList.append('<li><a href="#" data-id="' + value + '" class="btns btn_effect">' + value + '</a></li>');
                });

                var effectListBtn = effectList.find('.btn_effect').on('click', function () {
                    if ($(this).hasClass('green')) {
                        if (effectList.find('.btn_effect.green').length < 2) return false;
                        $(this).removeClass('green');
                    } else {
                        $(this).addClass('green');
                    }
                    sub.banner.loaddata.effectlist = [];
                    $('#effect_list .btn_effect.green').each(function () {
                        sub.banner.loaddata.effectlist.push($(this).data('id'));
                    });
                    $("[name='item_banner_effectrandom']").prop('checked', false);
                    sub.banner.add();
                    return false;
                });

                //폼 추가 삭제
                var getDetailForm = $('#banner_list .banner_box').clone();
                $('#banner_list')
                    .on('click', '.btn_listadd', function () {
                        var getForm = getDetailForm.clone();
                        var getLastForm = $('#banner_list .banner_box').last();
                        var getVal = $('#banner_list .banner_box').length + 1;
                        getForm.find('.number').html(getVal + '.');
                        $('#banner_list').append(getForm);
                        return false;
                    })
                    .on('click', '.btn_listdel', function () {
                        if ($('#banner_list .banner_box').length > 1) {
                            $(this).closest('.banner_box').remove();
                            $.each($('#banner_list .banner_box'), function (key, value) {
                                $(this)
                                    .find('.number')
                                    .html(key + 1 + '.');
                            });
                            sub.banner.imglist();
                            sub.banner.add();
                        }
                        return false;
                    });

                //드래그 드롭
                new Sortable($('#banner_list').get(0), {
                    handle: '.banner_box',
                    forceFallback: true,
                    preventOnFilter: true,
                    filter: '.btn_listbox, .file_upload_box',
                    onUpdate: function () {
                        $.each($('#banner_list .banner_box'), function (key, value) {
                            $(this)
                                .find('.number')
                                .html(key + 1 + '.');
                        });
                        sub.banner.imglist();
                    },
                });
            } else {
            }

            //설정 불러오기
            $('#popup_mask').fadeIn(200);
            setTimeout(function () {
                page[pageType].load(pid, psub, function () {
                    sub.banner.add();
                });
            }, 10);
        },
        add: function () {
            doTimeout(
                'banner_add',
                function () {
                    var chatOpt = sub.banner.opt;
                    var loadData = sub.banner.loaddata;

                    var imgList = loadData.image && loadData.image.bannerimg ? loadData.image.bannerimg : [];
                    var effectList = loadData.effectlist;
                    var bannerTime = parseInt(loadData.showtime, 10) * 1000;
                    var delayTime = parseFloat(loadData.duration, 10) * 1000;

                    if (chatOpt.main.find('.wowslider-container1').length > 0) {
                        var getSlider = chatOpt.main.find('.wowslider-container1').get(0);
                        getSlider.wsStop();
                    }

                    var itemBanner = chatOpt.main.html('<div class="img_box"><i class="fa fa-picture-o" aria-hidden="true"></i><span class="text">배너 이미지를 선택해주세요.</span></div>');

                    if (imgList.length > 0) {
                        itemBanner.html('<div class="wowslider-container1"><div class="ws_images"><ul></ul></div></div>');
                        var getSlider = chatOpt.main.find('.wowslider-container1 ul');
                        console.log(imgList);
                        if (imgList.length == 1) {
                            imgList.push(imgList[0]);
                        }
                        var getImgList = [];
                        $.each(imgList, function (key, value) {
                            if (value.length > 1 && value[1] !== '') {
                                getSlider.append('<li><img src="' + value[1] + '" alt="" class="img"></li>');
                                getImgList.push(value[1]);
                            }
                        });
                        $.imgpreload(getImgList, function () {
                            // chatOpt.main.find('.wowslider-container1 img').each(function () {
                            //     $(this)[0].crossOrigin = 'anonymous';
                            // });
                            chatOpt.main.find('.wowslider-container1').wowSlider({
                                effect: loadData.effectlist.join(','),
                                prev: '<i class="fa fa-chevron-circle-left" aria-hidden="true"></i>',
                                next: '<i class="fa fa-chevron-circle-right" aria-hidden="true"></i>',
                                duration: delayTime,
                                delay: bannerTime,
                                width: '100%',
                                height: 'auto',
                                autoPlay: true,
                                autoPlayVideo: false,
                                playPause: false,
                                stopOnHover: false,
                                option: { loop: true },
                                bullets: 0,
                                caption: false,
                                captionEffect: '',
                                parallax: '',
                                controls: true,
                                controlsThumb: false,
                                responsive: 3,
                                fullScreen: true,
                                gestures: 0,
                                images: 0,
                            });
                        });
                    }
                },
                100
            );
        },
        imglist: function () {
            var loadData = sub.banner.loaddata;
            var imgList = [];
            $('#banner_list .file_upload_box.file_img .img_box').each(function () {
                var getUrl = $(this).data('url');
                if (getUrl && getUrl != '') {
                    imgList.push([$(this).data('img'), $(this).data('url')]);
                }
            });

            loadData.image.bannerimg = imgList;
        },
    };
});
