sub.subtitle = {
    opt: {
        main: null,
        use: true, //툴팁설정
        sound: true,

        filteridnick: '',
        filterword: '',
    },
    sub: '',
    data: { starmin: '1', startext: '{닉네임}님 {개수}개', font: 'Jeju Gothic', fontbold: false, opacity: '0', customstyle: false, customstylecss: '', startype: 'list', staralign: true, listunit: true, rankmedal: true, stargiftwidth: '150', autoreset: false, ranktext: '오늘의 후원 순위', align: 'left', halign: 'bottom', color: '#FFFFFF', colornick: '#ffc247', colornum: '#ffc247', addfonturl: '', addfontname: '', fontsize: '16', shadowsize: '2', colorshadow: 'rgba(0,0,0,0.8)', bgtrans: 'none', colorbg: '#000000', bgimgzoom: false, filteridnick: [], image: {} },
    head:
        '.item_subtitle .subtitle,.item_subtitle .star_title{color:#ffffff!important}.item_subtitle .subtitle .item_gift .gift_text p{color:#ffffff!important}.text_preview .item_subtitle{color:#ffffff!important}.item_subtitle .subtitle .nick{color:#ffc247!important}.item_subtitle .subtitle .item_gift .gift_text .nick{color:#ffc247!important}.text_preview .item_subtitle .nick{color:#ffc247!important}.item_subtitle .subtitle .value{color:#ffc247!important}.item_subtitle .subtitle .item_gift .gift_text .gift{color:#ffc247!important}.text_preview .item_subtitle .value{color:#ffc247!important}.item_subtitle .subtitle,.text_preview .item_subtitle{text-shadow:-1px -1px 0px rgba(0,0,0,0.8),-1px -0.5px 0px rgba(0,0,0,0.8),-1px 0px 0px rgba(0,0,0,0.8),-1px 0.5px 0px rgba(0,0,0,0.8),-1px 1px 0px rgba(0,0,0,0.8),-0.5px -1px 0px rgba(0,0,0,0.8),-0.5px -0.5px 0px rgba(0,0,0,0.8),-0.5px 0px 0px rgba(0,0,0,0.8),-0.5px 0.5px 0px rgba(0,0,0,0.8),-0.5px 1px 0px rgba(0,0,0,0.8),0px -1px 0px rgba(0,0,0,0.8),0px -0.5px 0px rgba(0,0,0,0.8),0px 0px 0px rgba(0,0,0,0.8),0px 0.5px 0px rgba(0,0,0,0.8),0px 1px 0px rgba(0,0,0,0.8),0.5px -1px 0px rgba(0,0,0,0.8),0.5px -0.5px 0px rgba(0,0,0,0.8),0.5px 0px 0.5px rgba(0,0,0,0.8),0.5px 0.5px 0.5px rgba(0,0,0,0.8),0.5px 1px 0.5px rgba(0,0,0,0.8),1px -1px 0px rgba(0,0,0,0.8),1px -0.5px 0px rgba(0,0,0,0.8),1px 0px 1px rgba(0,0,0,0.8),1px 0.5px 1px rgba(0,0,0,0.8),1px 1px 1px rgba(0,0,0,0.8)}.item_subtitle .subtitle,.text_preview .item_subtitle{text-shadow:-1px -1px 0px rgba(0, 0, 0, 0.8),-1px -0.5px 0px rgba(0, 0, 0, 0.8),-1px 0px 0px rgba(0, 0, 0, 0.8),-1px 0.5px 0px rgba(0, 0, 0, 0.8),-1px 1px 0px rgba(0, 0, 0, 0.8),-0.5px -1px 0px rgba(0, 0, 0, 0.8),-0.5px -0.5px 0px rgba(0, 0, 0, 0.8),-0.5px 0px 0px rgba(0, 0, 0, 0.8),-0.5px 0.5px 0px rgba(0, 0, 0, 0.8),-0.5px 1px 0px rgba(0, 0, 0, 0.8),0px -1px 0px rgba(0, 0, 0, 0.8),0px -0.5px 0px rgba(0, 0, 0, 0.8),0px 0px 0px rgba(0, 0, 0, 0.8),0px 0.5px 0px rgba(0, 0, 0, 0.8),0px 1px 0px rgba(0, 0, 0, 0.8),0.5px -1px 0px rgba(0, 0, 0, 0.8),0.5px -0.5px 0px rgba(0, 0, 0, 0.8),0.5px 0px 0.5px rgba(0, 0, 0, 0.8),0.5px 0.5px 0.5px rgba(0, 0, 0, 0.8),0.5px 1px 0.5px rgba(0, 0, 0, 0.8),1px -1px 0px rgba(0, 0, 0, 0.8),1px -0.5px 0px rgba(0, 0, 0, 0.8),1px 0px 1px rgba(0, 0, 0, 0.8),1px 0.5px 1px rgba(0, 0, 0, 0.8),1px 1px 1px rgba(0, 0, 0, 0.8)}.item_subtitle.bgtrans_color{background-color:#000000}.item_subtitle.item_subtitle_bgimgzoom{background-size:100% 100%!important}',
    cls: 'item_box item_subtitle startype_list item_subtitle_staralign item_subtitle_listunit item_subtitle_rankmedal align_left halign_bottom bgtrans_none',
    style: 'opacity: 1; font-size: 16px; font-family: "Jeju Gothic", 돋움;',
    loaddata: {},
    stardata: [],
    init: function (pid, psub) {
        sub.subtitle.opt.main = $('.item_subtitle');
        sub.subtitle.sub = psub;

        if (pageType == 'setup') {
        } else {
            //저장된값 불러오기
            var getStorage = page.storage.load('subtitle_star');
            if (getStorage != null && getStorage.data != undefined && typeof getStorage.data === 'object') {
                if (getStorage.time != undefined && !isNaN(getStorage.time)) {
                    var getNow = new Date().getTime();
                    var getTime = getNow - Number(getStorage.time);
                    if (getTime < 3600000) {
                        sub.subtitle.stardata = getStorage.data;
                    }
                }
            }
        }

        //설정 불러오기
        $('#popup_mask').fadeIn(200);
        setTimeout(function () {
            page[pageType].load(pid, psub, function () {
                if (pageType == 'setup') {
                    sub.subtitle.test();
                } else {
                    sub.subtitle.add();
                }
            });
        }, 10);
    },
    test: function () {
        //임시 데이터
        sub.subtitle.stardata = [
            { type: 'star', id: 'test', name: loginInfo.svrKor, value: 10 },
            { type: 'star', id: 'test1', name: loginInfo.svrKor + '1', value: 11 },
            { type: 'star', id: 'test2', name: loginInfo.svrKor + '2', value: 1 },
            { type: 'star', id: 'test3', name: loginInfo.svrKor + '3', value: 2 },
            { type: 'star', id: 'test1', name: loginInfo.svrKor + '1', value: 100 },
            { type: 'star', id: 'test2', name: loginInfo.svrKor + '2', value: 200 },
            { type: 'star', id: 'test3', name: loginInfo.svrKor + '3', value: 300 },
            { type: 'star', id: 'test7', name: loginInfo.svrKor + '7', value: 10 },
            { type: 'star', id: 'test8', name: loginInfo.svrKor + '8', value: 11 },
            { type: 'star', id: 'test9', name: loginInfo.svrKor + '9', value: 12 },
            { type: 'star', id: 'test7', name: loginInfo.svrKor + '7', value: 1000 },
            { type: 'star', id: 'test8', name: loginInfo.svrKor + '8', value: 500 },
            { type: 'star', id: 'test9', name: loginInfo.svrKor + '9', value: 100 },
            { type: 'star', id: 'test13', name: loginInfo.svrKor + '13', value: 10000 },
            { type: 'star', id: 'test14', name: loginInfo.svrKor + '14', value: 1000 },
        ];
        sub.subtitle.add();
    },
    add: function () {
        doTimeout(
            'subtitle_add',
            function (data) {
                var chatOpt = sub.subtitle.opt;
                var loadData = sub.subtitle.loaddata;

                chatOpt.main.find('.subtitle').empty();
                var getData = sub.subtitle.stardata;
                var getType = loadData.startype;
                var getMin = !isNaN(loadData.starmin) && Number(loadData.starmin) > 0 ? Number(loadData.starmin) : 1;
                var getText = getType != 'rank' ? loadData.startext : loadData.ranktext;
                getText = page.common.br(getText);

                var getStarData = {};
                var getStarList = [];
                var getStarArray = [];
                var getMaxData = { id: '', name: '', value: 0 };
                $.each(getData, function (key, value) {
                    if (!isNaN(value.value) && value.id !== '') {
                        var getCheck = true;
                        chatOpt.filteridnick.lastIndex = 0;
                        if (chatOpt.filteridnick !== '' && value.name !== '' && chatOpt.filteridnick.test(value.name)) {
                            getCheck = false;
                        }
                        chatOpt.filteridnick.lastIndex = 0;
                        if (getCheck === true && chatOpt.filteridnick !== '' && value.id !== '' && chatOpt.filteridnick.test(value.id)) {
                            getCheck = false;
                        }

                        if (getCheck === true) {
                            if (getStarData[value.id] == undefined) {
                                getStarData[value.id] = [key, value.value, { type: value.type, id: value.id, name: value.name, value: [value.value] }];
                            } else {
                                getStarData[value.id][1] += value.value;
                                getStarData[value.id][2].value.push(value.value);
                            }
                            getStarList.push([key, value.value, { type: value.type, id: value.id, name: value.name, value: value.value, starimg: value.starimg }]);
                            if (value.value > getMaxData.value) {
                                getMaxData = { type: value.type, id: value.id, name: value.name, value: value.value };
                            }
                        }
                    }
                });
                $.each(getStarData, function (key, value) {
                    getStarArray.push(value);
                });

                var sortIdx = (loadData.staralign === true && getType != 'recent') || (loadData.staralign === false && getType == 'rank') ? 1 : 0;
                getStarList
                    .sort(function (a, b) {
                        return a[sortIdx] - b[sortIdx];
                    })
                    .reverse();
                getStarArray
                    .sort(function (a, b) {
                        return a[sortIdx] - b[sortIdx];
                    })
                    .reverse();

                var getSubList = [];
                if (getType == 'list' || getType == 'add') {
                    $.each(getStarArray, function (key, value) {
                        var getListVal = 0;
                        if (value[1] >= getMin) {
                            if (getType == 'list') {
                                var getListArray = [];
                                $.each(value[2].value, function (keys, values) {
                                    getListArray.push('<span class="star">' + page.comma(values) + '</span>');
                                });
                                if (getListArray.length > 0) {
                                    getListVal = getListArray.join('');
                                } else return true;
                            } else {
                                getListVal = value[1];
                            }
                            getSubList.push('<li class="text">' + sub.subtitle.replace(getText, { type: value[2].type, id: value[2].id, name: value[2].name, value: getListVal }) + '</li>');
                        }
                    });
                } else if (getType == 'total') {
                    if (getMaxData.value >= getMin) {
                        getSubList = ['<li class="text">' + sub.subtitle.replace(getText, getMaxData) + '</li>'];
                    }
                } else if (getType == 'rank') {
                    $.each(getStarArray, function (key, value) {
                        if (value[1] >= getMin) {
                            getSubList.push('<tr><td class="rank"><span class="num">' + (key + 1) + '등</span><i class="fa fa-trophy" aria-hidden="true"></i></td><td class="nick">' + value[2].name + '</td><td class="val"><span class="value">' + page.comma(value[1]) + '</span>개</td></tr>');
                        }
                    });
                } else if (getType == 'recent') {
                    $.each(getStarList, function (key, value) {
                        if (value[1] >= getMin) {
                            getSubList.push('<li class="text">' + sub.subtitle.replace(getText, { id: value[2].id, name: value[2].name, value: value[1] }) + '</li>');
                        }
                    });
                    getSubList = getSubList.slice(0, 10);
                } else if (getType == 'giftimg') {
                    $.each(getStarList, function (key, value) {
                        if (value[2].value >= getMin) {
                            getSubList.push('<li class="gift_box"><img src="/resource/img/icon/blank.png" alt="" class="img"><div class="gift" style="background-image:url(' + page.common.img.starurl(value[2]) + ')"></div><div class="text">' + sub.subtitle.replace(getText, { type: value[2].type, id: value[2].id, name: value[2].name, value: value[2].value }) + '</div></li>');
                        }
                    });
                }

                if (getType == 'rank') {
                    chatOpt.main.find('.subtitle').html('<div class="rank_title">' + getText + '</div><table class="subtitle_list">' + getSubList.join('') + '</table>');
                } else {
                    chatOpt.main.find('.subtitle').html('<ul class="subtitle_list">' + getSubList.join('') + '</ul>');

                    if (getType == 'giftimg') {
                        var getGiftWidth = !isNaN(loadData.stargiftwidth) && loadData.stargiftwidth > 1 ? loadData.stargiftwidth : 150;
                        var getWidth = chatOpt.main.width() > 0 ? chatOpt.main.width() : $(window).width();
                        var getGiftSize = parseInt((getGiftWidth * 100) / chatOpt.main.width(), 10) - 1;
                        chatOpt.main.find('.gift_box').css('width', getGiftSize + '%');
                    } else if (getType == 'recent') {
                        var getRecentSpeed = getSubList.length * 2.5;
                        chatOpt.main.find('.subtitle_list').removeAttr('style');
                        chatOpt.main.find('.subtitle_list').attr('style', '-webkit-animation: subtitle_recent' + loadData.align + ' ' + getRecentSpeed + 's infinite linear;animation: subtitle_recent' + loadData.align + ' ' + getRecentSpeed + 's infinite linear');
                    }
                }
            },
            100
        );
    },
    replace: function (text, data) {
        return text
            .replace(/{닉네임}/gi, "<span class='nick'>" + data.name + '</span>')
            .replace(/{아이디}/gi, "<span class='nick'>" + page.common.mask(data.id) + '</span>')
            .replace(/{개수}/gi, "<span class='value'>" + (!isNaN(data.value) ? page.comma(data.value) : data.value) + '</span>');
    },
    reset: function () {
        sub.subtitle.stardata = [];
        page.storage.save('subtitle_star', sub.subtitle.stardata);
        sub.subtitle.add();
    },
};
