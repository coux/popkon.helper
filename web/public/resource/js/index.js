var popkonData = [
    {
        type: 'chat',
        data: {
            signId: loginInfo.svrEng,
            nickName: loginInfo.svrKor,
            roomLevel: 1,
            fanLevel: 0,
            memberSex: 1,
            gradeimg: 'pic.popkontv.com/images//www/live/assets/chatlist/icon_mc_woman.png',
            message: 'BJ 채팅 입니다',
        },
    },
    {
        type: 'chat',
        data: {
            signId: loginInfo.svrEng + 1,
            nickName: loginInfo.svrKor,
            roomLevel: 2,
            fanLevel: 0,
            memberSex: 1,
            gradeimg: '',
            message: '매니저 채팅 입니다',
        },
    },
    {
        type: 'star',
        data: {
            signId: loginInfo.svrEng + 2,
            nickName: loginInfo.svrKor,
            coinvalue: 100,
            cointype: 0,
            coinurl: 'pic.popkontv.com/images/aspw/' + loginInfo.asp + '/common/coin_100.png',
            msg: loginInfo.svrKor + '님이 ' + loginInfo.coin + ' 100개를 선물했습니다',
        },
    },
    {
        type: 'chat',
        data: {
            signId: loginInfo.svrEng + 3,
            nickName: loginInfo.svrKor,
            roomLevel: 0,
            fanLevel: 0,
            memberSex: 1,
            gradeimg: 'pic.popkontv.com/images/www/live/assets/chatlist/icon_fan_01.png',
            message: 'VIP 시청자 채팅 입니다',
        },
    },
    loginInfo.asp === 'P-00001' && {
        type: 'star',
        data: {
            signId: loginInfo.svrEng + 4,
            nickName: loginInfo.svrKor,
            coinvalue: 1275,
            cointype: 1,
            coinurl: 'pic.popkontv.com/images/aspw/' + loginInfo.asp + '/common/coin_s.png',
            msg: loginInfo.svrKor + '님이 스위트(' + loginInfo.coin + ' 1,275개) 선물하셨습니다.',
        },
    },
    {
        type: 'chat',
        data: {
            signId: loginInfo.svrEng + 5,
            nickName: loginInfo.svrKor,
            roomLevel: 0,
            fanLevel: 1,
            memberSex: 1,
            gradeimg: 'pic.popkontv.com/images/www/live/assets/chatlist/icon_fan_02.png',
            message: '골드 시청자 채팅입니다',
        },
    },
    {
        type: 'chat',
        data: {
            signId: loginInfo.svrEng + 6,
            nickName: loginInfo.svrKor,
            roomLevel: 0,
            fanLevel: 2,
            memberSex: 1,
            gradeimg: 'pic.popkontv.com/images/www/live/assets/chatlist/icon_fan_03.png',
            message: '실버 시청자 채팅입니다',
        },
    },
    {
        type: 'star',
        data: {
            signId: loginInfo.svrEng + 7,
            nickName: loginInfo.svrKor,
            coinvalue: 100,
            cointype: 0,
            coinurl: 'pic.popkontv.com/images/aspw/' + loginInfo.asp + '/common/coin_100.png',
            msg: loginInfo.svrKor + '님이 ' + loginInfo.coin + ' 100개를 선물했습니다',
        },
    },
    {
        type: 'chat',
        data: {
            signId: loginInfo.svrEng + 8,
            nickName: loginInfo.svrKor,
            roomLevel: 0,
            fanLevel: 3,
            memberSex: 1,
            gradeimg: 'pic.popkontv.com/images/www/live/assets/chatlist/icon_fan_04.png',
            message: '브론즈 시청자 채팅입니다',
        },
    },
    {
        type: 'star',
        data: {
            signId: loginInfo.svrEng + 9,
            nickName: loginInfo.svrKor,
            coinvalue: 1,
            cointype: 0,
            coinurl: 'pic.popkontv.com/images/aspw/' + loginInfo.asp + '/common/coin_default.png',
            msg: loginInfo.svrKor + '님이 ' + loginInfo.coin + ' 1개를 선물했습니다',
        },
    },
    { type: 'join', data: { msg: '열혈팬 님께서 입장하셨습니다.' } },
    {
        type: 'up',
        data: { recomcnt: 1, msg: loginInfo.svrKor + '님이 추천하셨습니다' },
    },
];

requirejs(requireArray, function ($, io, noUiSlider, spectrum, notify, Clipboard, ajaxForm, CodeMirror, Sortable, imgpreload, twemoji) {
    $(function () {
        //페이지 기본 설정
        page[pageType].init();
        // debug = page;
        if (mngrcheck === 'test') {
            setTimeout(function () {
                page.popkontest();
            }, 2000);
        }
    });

    page = {
        opt: {
            ver: '20200218',
            loaded: false,
            list: ['chat', 'alert', 'goal', 'subtitle', 'text', 'banner', 'timer'],
        },
        data: {
            connect: false,
            live: false,
            star: 0,
            doosan: 0,
            up: 0,
            view: 0,
            fan: 0,
            fav: 0,
            title: '',
            start: 0,
        },
        //셋업 설정
        setup: {
            socket: null,
            init: function () {
                //셋업 테스트 데이터
                page.data.star = 50;
                page.data.doosan = 50;
                page.data.up = 60;
                page.data.view = 70;
                page.data.fan = 80;
                page.data.fav = 90;

                //해시 메시지 체크
                page.hashmsg();

                //모듈 로딩 체크 후 페이지 실행
                if (sub[pageId] != undefined) {
                    sub[pageId].init(pageId, pageSub);
                }

                //팝업
                page.popup.init();

                if (pageId == 'main' || pageId == 'update' || pageId == 'help') {
                    if (pageId == 'update' || pageId == 'help') {
                        page.bbs.init();
                    }
                    return;
                }

                //공통
                page.setup.common();

                //저장
                page.setup.save();

                //소켓
                page.socket.init();
                console.log(page.socket);
            },
            common: function () {
                //프리셋
                page.setup.preset();

                //url 복사
                page.setup.urlcopy();

                //폰트
                page.setup.fontselect();

                //파일업로드
                page.setup.filemanager.init();

                //css 에디터
                page.setup.editor.init();

                //스크롤
                page.setup.scroll();

                //스타일 설정
                page.setup.style.init();

                //오디오
                page.audio.init([[pageId, pageSub]]);

                //미리보기 원격도구
                page.setup.control();
            },

            scroll: function () {
                $('#header .btn_top, .setup_btn .btn_top').on('click', function () {
                    $('html, body').stop(true, true).animate({ scrollTop: 0 }, 200);
                    return false;
                });

                var getBody = $('body');
                var getSetupGroup = $('.setup_area .menu_panel');
                var getLnbMenu = $('#header .lnb.menu .menu');
                var getSaveBtn = $('.setup_btn');
                var getSaveCheck = getSaveBtn.length > 0;
                getLnbMenu.on('click', function () {
                    var getIdx = getLnbMenu.index($(this));
                    if (getSetupGroup.eq(getIdx).length > 0) {
                        $('html,body')
                            .stop(true, true)
                            .animate(
                                {
                                    scrollTop: getSetupGroup.eq(getIdx).offset().top - 160,
                                },
                                200,
                                function () {
                                    getLnbMenu.removeClass('active').eq(getIdx).addClass('active');
                                }
                            );
                    }
                    return false;
                });
                function lnbScroll(idx) {
                    getLnbMenu.removeClass('active');
                    if (idx > -1) {
                        getLnbMenu.eq(idx).addClass('active');
                    }
                }
                function pageScroll() {
                    var getScrollTop = $(window).scrollTop();
                    var getDocHeight = $(window).height();
                    var getScrollLeft = $(window).scrollLeft();
                    if (getScrollTop > 1) {
                        getBody.addClass('header_fixed');
                    } else {
                        getBody.removeClass('header_fixed');
                    }
                    var getIdx = -1;
                    getSetupGroup.each(function (idx) {
                        var getTop = $(this).offset().top;
                        if (getScrollTop > getTop - 170) {
                            getIdx = idx;
                        }
                    });
                    lnbScroll(getIdx);

                    if (getSaveCheck === true) {
                        var getSaveTop = getSaveBtn.offset().top + 82;
                        if (getScrollTop + getDocHeight > getSaveTop) {
                            getBody.removeClass('save_fixed');
                        } else {
                            getBody.addClass('save_fixed');
                        }
                    }

                    $('body.header_fixed .content_area .lnb_area, body.save_fixed .setup_area .setup_btn .btn_wrap').css('transform', 'translateX(-' + getScrollLeft + 'px)');
                }
                $(window).on('scroll', function () {
                    pageScroll();
                });
                pageScroll();
            },
            preset: function () {
                //프리셋
                if ($('.preset_area').length == 0) return;

                $('.preset_area .btn_add').on('click', function () {
                    var getIdx = 0;
                    var getIdxList = [];
                    $('.preset_area .preset_list .btn_preset').each(function () {
                        getIdxList.push(Number($(this).data('idx')));
                    });
                    getIdxList.sort(function (a, b) {
                        return a - b;
                    });
                    var getLast = Number(getIdxList.slice(-1)) + 1;
                    getIdx = getLast;
                    for (var i = 0; i < getLast; i++) {
                        if ($.inArray(i, getIdxList) == -1) {
                            getIdx = i;
                            break;
                        }
                    }
                    var getName = '프리셋' + (getIdx > 0 ? getIdx : '');
                    var getPreset = '';
                    // var getPreset = $(".preset_area .select_preset").val();
                    // var getName = getPreset !== "" ? $(".preset_area .select_preset option:selected").text() : ("프리셋" + getIdx);
                    // if(getPreset !== "" && $(".preset_area .preset_list .btn_preset[data-preset='" + getName + "']").length > 0) {
                    // page.msg("이미 추가한 프리셋입니다.","warn");
                    // return false;
                    // }
                    page.setup.presetadd(getIdx, getName, '');

                    $('#popup_mask').fadeIn(200);
                    page.setup.presetsave(
                        function () {
                            if ($('.preset_list .preset_box').length > 1) {
                                $('.preset_area .preset_list .btn_preset').last().get(0).click();
                            } else {
                                $('#popup_mask').fadeOut(200);
                            }
                        },
                        getIdx,
                        getPreset
                    );
                    return false;
                });

                $('.preset_area .preset_list')
                    .on('click', '.btn_modify', function () {
                        var getPreset = $(this).closest('.preset_box');
                        if (!getPreset.hasClass('on')) {
                            getPreset.addClass('on');
                            var getBtn = $(this).closest('.preset_box').find('.btn_preset');
                            var text = getBtn.text();
                            var input = $('<input type="text" maxLength="10"/>').focus();
                            getBtn.data('name', text).text('');
                            getBtn.append(input);
                            input.val(text).focus();
                        }
                        return false;
                    })
                    .on('blur', '.btn_preset', function () {
                        $(this).closest('.preset_box').removeClass('on');
                        var getBtn = $(this);
                        var input = $('input', getBtn);

                        if ($.trim(input.val()) === '') {
                            getBtn.text(getBtn.data('name'));
                        } else {
                            getBtn.text(input.val());
                        }

                        input.remove();
                        page.setup.presetsave();
                    })
                    .on('click', '.btn_delete', function () {
                        var getPreset = $(this).closest('.preset_box');
                        var getBtn = getPreset.find('.btn_preset');
                        page.popup.confirm('프리셋 삭제', '<b>' + getBtn.text() + '</b> 프리셋을 삭제하시겠습니까?', function () {
                            getPreset.remove();

                            if (getBtn.hasClass('green') === true) {
                                $('#popup_mask').fadeIn(200);
                            }
                            page.setup.presetsave(function () {
                                if (getBtn.hasClass('green') === true) {
                                    if ($('.preset_list .preset_box').length > 0) {
                                        $('.preset_area .preset_list .btn_preset').eq(0).get(0).click();
                                    } else {
                                        $('#popup_mask').fadeOut(200);
                                    }
                                }
                            });
                        });
                        return false;
                    })
                    .on('keypress keyup keydown', '.btn_preset', function (e) {
                        if (e.keyCode != 8 && e.target.textContent.length > 16) {
                            return false;
                        }
                        if (e.keyCode == 13 || e.keyCode == 27) {
                            $(this).closest('.preset_box').removeClass('on');
                            var getBtn = $(this).blur();
                            if (e.keyCode == 27 || $.trim(getBtn.text()) === '') {
                                getBtn.html(getBtn.data('name'));
                            }
                            page.setup.presetsave();
                        }
                    });
                page.setup.presetsort();
            },
            presetadd: function (idx, name, preset) {
                var getUrl = '/' + pageId;
                if (idx !== '' && !isNaN(idx) && Number(idx) > 0) {
                    getUrl += '/' + idx;
                }
                var getName = name;
                var getPreset = $('<li class="preset_box"><a href="' + getUrl + '" data-idx="' + idx + '" class="btns btn_preset" >' + getName + '</a><a href="#" title="수정" class="btn_modify"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a><a href="#" title="삭제" class="btn_delete"><i class="fa fa-times" aria-hidden="true"></i></a></li>');
                $('.preset_area .preset_list .preset_listbox').append(getPreset);

                var getSub = pageSub === '' ? '0' : pageSub;
                $(".preset_area .preset_list .btn_preset[data-idx='" + getSub + "']").addClass('green');
            },
            presetsort: function () {
                new Sortable($('.preset_area .preset_list .preset_listbox').get(0), {
                    handle: '.preset_box',
                    forceFallback: true,
                    onUpdate: function () {
                        page.setup.presetsave();
                    },
                });
            },
            presetsave: function (func, idx, preset) {
                doTimeout(
                    'presetsave',
                    function () {
                        //프리셋
                        var presetList = [];
                        $('.preset_area .preset_listbox .btn_preset').each(function () {
                            // var getPreset = $(this).data("preset");
                            // getPreset = getPreset != undefined && getPreset !== "" ? getPreset : "";urlencode($page_key)
                            presetList.push([$(this).data('idx'), $.trim($(this).text()), loginInfo.page + '/' + $(this).data('idx') + '?pcode=' + loginInfo.asp, new Date().getTime()]);
                        });

                        $('.preset_area .url_area').toggle(presetList.length > 0);

                        var getForm = {
                            id: pageType,
                            type: 'preset',
                            key: pageKey,
                            page: pageId,
                            sub: pageSub,
                            data: JSON.stringify(presetList),
                        };
                        if (preset !== '') {
                            getForm.idx = idx;
                            getForm.preset = preset;
                        }

                        $.ajax({
                            type: 'POST',
                            url: '/lib/save.php',
                            dataType: 'json',
                            data: getForm,
                            success: function (data) {
                                if (func != undefined && typeof func == 'function') {
                                    func();
                                }
                            },
                            error: function () {
                                page.msg('저장중 오류가 발생했습니다. 관리자에게 문의해주세요.', 'error');
                                $('#popup_mask').fadeOut(200);
                            },
                        });
                    },
                    500
                );
            },

            //원격도구
            control: function () {
                if ($('.lnb_area').length == 0) return;
                $('.lnb_area .test_area .btn_test').on('click', function () {
                    var getCmd = $(this).data('type');

                    if (getCmd != null && getCmd !== '' && page.socket.isConnect()) {
                        var getVal = $('#test_star').val();
                        if (isNaN(getVal) || Number(getVal) < 1) getVal = 100;

                        if (getCmd == 'remove' && (pageId == 'goal' || pageId == 'subtitle')) {
                            page.popup.confirm('초기화', '현재 표시된 후원내용이 초기화됩니다.<br>초기화 하시겠습니까?', function () {
                                page.socket.obj.emit('setup', {
                                    type: 'control',
                                    cmd: getCmd,
                                    val: getVal,
                                    key: pageKey,
                                    pageid: pageId,
                                    pagesub: pageSub,
                                });
                            });
                        } else {
                            page.socket.obj.emit('setup', {
                                type: 'control',
                                cmd: getCmd,
                                val: getVal,
                                key: pageKey,
                                pageid: pageId,
                                pagesub: pageSub,
                            });
                        }

                        if (getCmd == 'star') {
                            var getTestData = {
                                type: 'star',
                                data: {
                                    id: 'popkontv',
                                    name: loginInfo.svrKor,
                                    star: getVal,
                                    startype: 0,
                                    starimg: 'pic.popkontv.com/images/aspw/' + loginInfo.asp + '/common/coin_default.png',
                                    msg: loginInfo.svrKor + '님이 ' + loginInfo.coin + ' ' + getVal + '개를 선물했습니다',
                                },
                            };
                            var getData = page.player.msg(getTestData);
                            if (page.player.typelist[pageId] != undefined && $.inArray(getData.type, page.player.typelist[pageId]) > -1) {
                                getData.chat = '안녕하세요 테스트 채팅입니다';
                                sub[pageId].add(getData);
                            }
                        } else if (getCmd == 'up') {
                            var getTestData = {
                                type: 'up',
                                data: {
                                    id: loginInfo.svrKor,
                                    name: loginInfo.svrKor,
                                    value: 1,
                                    msg: loginInfo.svrKor + '님이 추천하셨습니다',
                                },
                            };
                            var getData = page.player.msg(getTestData);
                            if (page.player.typelist[pageId] != undefined && $.inArray(getData.type, page.player.typelist[pageId]) > -1) {
                                sub[pageId].add(getData);
                            }
                        }
                    }
                    return false;
                });
            },

            urlcopy: function () {
                if ($('.url_area .btn_urlcopy').length == 0) return;

                var clipboard = new Clipboard('.url_area .btn_urlcopy');
                clipboard.on('success', function () {
                    //저장 여부 체크
                    var getCopyVal = $('#hp_url').val();
                    page.msg('URL이 클립보드에 복사되었습니다! \r\n복사된 URL을 확인 하시고, 사용법 페이지를 참고해주세요.', 'success');
                });

                $('#btn_page_url').on('click', function () {
                    $('.url_area .btn_urlcopy').trigger('click');
                    return false;
                });

                $('.btn_urlview').on('click', function () {
                    var getPopup = window.open($('#hp_url').val(), 'player', 'menubar=0,resizable=1,width=350,height=340');
                    try {
                        getPopup.onload = function () {
                            getPopup.focus();
                        };
                    } catch (error) {}
                    return false;
                });
            },
            fontselect: function () {
                //폰트 셀렉트 박스
                if ($('.select_font_list').length == 0) return;

                var fontSelectInterval = null;
                var fontSelect = $('.select_font_list')
                    .on('click', function (e) {
                        if (fontSelect.hasClass('active')) {
                            fontSelect.removeClass('active').find('.font_list').stop(true, true).fadeOut(200);
                            if ($(e.target).hasClass('font') || $(e.target).hasClass('over')) {
                                var getFontTarget = $(e.target).hasClass('over') ? $(e.target).parent() : $(e.target);
                                var getVal = $(getFontTarget).data('value');
                                if (getVal != undefined && getVal !== '') {
                                    fontSelectBox.val(getVal).trigger('change');
                                }
                            }
                        } else {
                            fontSelect.addClass('active').find('.font_list').stop(true, true).fadeIn(200);
                        }
                        return false;
                    })
                    .hover(
                        function () {
                            clearTimeout(fontSelectInterval);
                        },
                        function () {
                            clearTimeout(fontSelectInterval);
                            fontSelectInterval = setTimeout(function () {
                                fontSelect.removeClass('active').find('.font_list').stop(true, true).fadeOut(200);
                            }, 1000);
                        }
                    );
                var fontSelectBox = $('.select_font_text').on('change', function () {
                    var getFontValue = $(this).val();
                    fontSelect.find('li.on').removeClass('on');
                    fontSelect.find("li[data-value='" + getFontValue + "']").addClass('on');
                    var getFontName = $(this).find('option:selected').prop('selected', true).text();
                    fontSelect.find('.font_value').html(getFontName);
                    if (getFontValue == 'inputfont') {
                        $('.addfont_area').stop(true, true).slideDown(200);
                        page.addfont();
                    } else {
                        $('head .item_addfont').remove();
                        $('.addfont_area').stop(true, true).slideUp(200);
                    }
                });

                //추가 웹폰트 설정
                $('#btn_fontselect').on('click', function () {
                    var getFontUrl = $('#addfont_url');
                    var getFontName = $('#addfont_name');
                    var getFontUrlVal = getFontUrl.val();
                    var getFontNameVal = getFontName.val();
                    if (getFontUrlVal == '') {
                        getFontUrl.focus();
                        return false;
                    }
                    if (getFontNameVal == '') {
                        getFontName.focus();
                        return false;
                    }
                    if (getFontUrlVal.indexOf('http') == -1) {
                        getFontUrlVal = 'http://' + getFontUrlVal;
                    }
                    $.ajax({
                        url: '/lib/font.php?url=' + getFontUrlVal,
                        timeout: 5000,
                        success: function (data) {
                            page.addfont();
                        },
                        error: function () {
                            page.msg('URL의 웹폰트를 읽어 올 수 없습니다. URL이 올바른지 확인해주세요.', 'warn');
                        },
                    });
                    return false;
                });
            },
            filemanager: {
                init: function () {
                    if ($('#popup_file').length == 0) return;

                    var popupFile = $('#popup_file');
                    popupFile
                        .find('.upload_box')
                        .on('dragover dragenter', function () {
                            $(this).addClass('dragon');
                        })
                        .on('dragleave dragend drop', function () {
                            $(this).removeClass('dragon');
                        });

                    popupFile.find('.upload_box .input_upload').on('change', function () {
                        if (this.value != '') {
                            var sizeCheck = false;
                            var fileName = '';
                            // try{
                            var fileSize = $(this).get(0).files[0].size;
                            fileName = $(this).get(0).files[0].name.split('.').pop().toLowerCase();
                            if (fileName.indexOf('gif') > -1 || fileName.indexOf('png') > -1 || fileName.indexOf('jpeg') > -1 || fileName.indexOf('jpg') > -1 || fileName.indexOf('mp3') > -1 || fileName.indexOf('wav') > -1 || fileName.indexOf('ogg') > -1) {
                                sizeCheck = true;
                            }
                            if (sizeCheck === true && !isNaN(fileSize)) {
                                var getSize = parseInt(fileSize / 1024 / 1024, 10);
                                if (fileName.indexOf('gif') > -1 || fileName.indexOf('png') > -1 || fileName.indexOf('jpeg') > -1 || fileName.indexOf('jpg') > -1) {
                                    if (getSize > 8) sizeCheck = false;
                                } else if (fileName.indexOf('mp3') > -1 || fileName.indexOf('wav') > -1 || fileName.indexOf('ogg') > -1) {
                                    if (getSize > 4) sizeCheck = false;
                                } else sizeCheck = false;
                            }
                            // }
                            // catch(error) {
                            //
                            // }
                            if (sizeCheck == false) {
                                page.msg('8MB 이하의 GIF, PNG, JPG 이미지와 4MB 이하의 MP3, WAV, OGG 사운드만 업로드 가능합니다.', 'warn');
                                return false;
                            }

                            getPopupLoader.fadeIn(200);
                            getPopupFileForm.submit();
                        }
                    });

                    var getPopupLoader = $('#popup_file .popup_mask');
                    var getPopupFileForm = $('#popup_file_form').ajaxForm({
                        type: 'POST',
                        url: '/lib/setup/fileupload.php',
                        timeout: 30000,
                        async: true,
                        success: function (data) {
                            var getFileList = {};
                            if (data && data.list != undefined) {
                                getFileList = data.list;
                            }
                            page.setup.filemanager.list('my', data.mime, getFileList);
                        },
                        error: function (data) {
                            page.msg('파일 업로드에 실패하였습니다. 관리자에게 문의해주세요.', 'error');
                            getPopupLoader.fadeOut(200);
                        },
                        complete: function () {
                            popupFile.find('.upload_box .input_upload').val('');
                            getPopupLoader.fadeOut(200);
                        },
                    });

                    //팝업 리스트
                    popupFile.find('.file_list .btn_list').on('click', function () {
                        $('#popup_file_list .list_box.active').removeClass('active');
                        var getLoc = $(this).data('location');
                        var getType = $(this).data('type');
                        page.setup.filemanager.load(getLoc, getType);
                        return false;
                    });

                    //파일 제거
                    $('body').on('click', '.file_upload_box .upload_tooltip .btn_file_delete', function () {
                        page.setup.filemanager.del(this);
                        return false;
                    });

                    //파일 열기
                    $('body').on('click', '.file_upload_box .upload_tooltip .btn_file_upload', function () {
                        $('.file_upload_box.file_active').removeClass('file_active');
                        var getMime = $(this).closest('.file_upload_box').addClass('file_active').hasClass('file_img') ? 'img' : 'audio';
                        popupFile.data('mime', getMime).fadeIn(200, function () {
                            page.setup.filemanager.load('my', getMime);
                        });
                        return false;
                    });

                    //파일 선택
                    popupFile.find('.btn_select').on('click', function () {
                        page.setup.filemanager.select();
                        return false;
                    });

                    //팝업 닫기
                    popupFile.find('.btn_close, .btn_cancel').on('click', function () {
                        $('.file_upload_box.file_active').removeClass('file_active');
                        popupFile.fadeOut(200, function () {
                            $('#popup_file_list').empty();
                            page.audio.stop(pageId);
                        });
                        return false;
                    });

                    //파일 삭제
                    $('body')
                        .on('click', '#popup_file #popup_file_list .list_box .btn_delete', function () {
                            var getName = $(this).parent().data('name');
                            var getUrl = $(this).parent().data('url');
                            var getId = $(this).parent().data('id');
                            var getMime = $(this).parent().data('mime');
                            page.popup.confirm('파일삭제', '<b>' + getName + '</b> 파일을 삭제하시겠습니까?', function () {
                                $.ajax({
                                    type: 'POST',
                                    url: '/lib/setup/fileupload.php',
                                    data: {
                                        type: 'delete',
                                        key: pageKey,
                                        url: getUrl,
                                        id: getId,
                                    },
                                    success: function (data) {
                                        var getFileList = {};
                                        if (data && data.list != undefined) {
                                            getFileList = data.list;
                                        }
                                        page.setup.filemanager.list('my', getMime, getFileList);
                                    },
                                });
                            });
                            return false;
                        })
                        .on('click', '#popup_file #popup_file_list .list_box', function () {
                            $('#popup_file_list .list_box.active').removeClass('active');
                            $(this).addClass('active');

                            var getPlay = $(this).find('.btn_play');
                            if (getPlay.length > 0) {
                                var getBox = $(this);
                                var getSrc = $(this).data('url');
                                page.audio.play(pageId, getSrc);
                            }
                            return false;
                        })
                        .on('dblclick', '#popup_file #popup_file_list .list_box', function () {
                            $('#popup_file_list .list_box.active').removeClass('active');
                            $(this).addClass('active');

                            page.setup.filemanager.select();
                            return false;
                        });

                    //파일 사운드 재생
                    $('body').on('click', '.file_upload_box .img_box .btn_play', function () {
                        var getBox = $(this).closest('.img_box');
                        var getSrc = getBox.data('url');
                        page.audio.play(pageId, getSrc);
                        return false;
                    });

                    //파일 외부링크
                    $('body').on('click', '.file_upload_box .upload_tooltip .btn_file_link', function () {
                        var getBox = $(this).closest('.file_upload_box');
                        var getMime = getBox.hasClass('file_img') ? 'img' : 'audio';
                        var getLinkUrl = '';

                        var getImgBox = getBox.find('.upload_box .img_box');
                        var getImgUrl = getImgBox.data('url');
                        if (getImgUrl != undefined && getImgUrl != '') {
                            getLinkUrl = getImgUrl;
                        }
                        $('#popup_external .input_external').val(getLinkUrl);
                        $('#popup_external').removeClass('img audio').addClass(getMime).data({ type: getMime, box: getBox }).fadeIn(200);
                        return false;
                    });
                    $('#popup_external .btn_select').on('click', function () {
                        var getFileLink = $('#popup_external .input_external').val();
                        var getPopup = $('#popup_external');
                        if (getFileLink && getFileLink != '') {
                            if (getFileLink.indexOf('http://') == -1 && getFileLink.indexOf('https://') == -1 && getFileLink.indexOf('ftp://') == -1) {
                                getFileLink = 'http://' + getFileLink;
                            }
                            var getFileName = getFileLink.split('/').pop();
                            var getMime = getPopup.data('type');
                            var getBox = getPopup.data('box');
                            if (getMime == 'img') {
                                getBox
                                    .find('.upload_box')
                                    .addClass('upload')
                                    .find('.img_box')
                                    .data({
                                        img: getFileName,
                                        url: getFileLink,
                                    })
                                    .css('background-image', 'url(' + getFileLink + ')')
                                    .end()
                                    .find('.name')
                                    .html(getFileName);
                                if (pageId == 'alert') {
                                    sub.alert.stardata[0].detail.image = [getFileName, getFileLink];
                                    sub.alert.setupadd();
                                }
                            } else if (getMime == 'audio') {
                                getBox
                                    .find('.upload_box')
                                    .addClass('upload')
                                    .find('.img_box')
                                    .data({
                                        audio: getFileName,
                                        url: getFileLink,
                                    })
                                    .end()
                                    .find('.name')
                                    .html(getFileName);
                            }
                        }
                        getPopup.fadeOut(200);
                        return false;
                    });
                },
                list: function (type, mime, data) {
                    var popupFileList = $('#popup_file_list');
                    var popupFile = $('#popup_file');
                    popupFile.find('.file_list .btn_list').removeClass('active');
                    popupFile.find('.file_list .btn_list.' + type + '.' + mime).addClass('active');

                    popupFileList.html('<p class="no_file">파일이 없습니다. <br> 파일을 업로드하거나 기본 파일 리스트에서 선택해주세요.</p>').removeClass('default my').addClass(type);
                    var getFileList = [];

                    var audioUrl = '';
                    $.each(data, function (key, value) {
                        var getFileStr = '';
                        var getSize = '';
                        if (value.mime == mime) {
                            if (type == 'my') {
                                getSize = parseInt(value.size / 1000, 10) + 'KB';
                            }
                            if (mime == 'img') {
                                getFileStr = '<div data-url="' + value.url + '" data-name="' + value.name + '" data-mime="' + value.mime + '" data-id="' + key + '" class="list_box"><a title="파일 삭제" href="#" class="btn_delete"><i class="fa fa-trash" aria-hidden="true"></i></a><div class="list_icon" style="background-image:url(' + value.url + ')"></div><div class="list_info""><p class="name">' + value.name + '</p><p class="size">' + getSize + '</p></div></div>';
                            } else if (mime == 'audio') {
                                getFileStr = '<div data-url="' + value.url + '" data-name="' + value.name + '" data-mime="' + value.mime + '" data-id="' + key + '" class="list_box"><a title="파일 삭제" href="#" class="btn_delete"><i class="fa fa-trash" aria-hidden="true"></i></a><a title="사운드 재생" href="#" class="btn_play play"><i class="fa fa-play-circle-o" aria-hidden="true"></i></a><a title="사운드 멈춤" href="#" class="btn_play pause"><i class="fa fa-pause-circle-o" aria-hidden="true"></i></a><div class="list_icon"><i class="fa fa-music" aria-hidden="true"></i></div><div class="list_info""><p class="name">' + value.name + '</p><p class="size">' + getSize + '</p></div></div>';
                            }
                        }
                        if (getFileStr != '') {
                            getFileList.push([parseInt(value.time, 10), getFileStr]);
                        }
                    });
                    if (getFileList.length > 0) {
                        popupFileList.empty();
                        if (mime == 'img' && type == 'default') {
                            getFileList.sort(function (a, b) {
                                return a[0] - b[0];
                            });
                        } else {
                            getFileList.sort(function (a, b) {
                                return b[0] - a[0];
                            });
                        }
                        $.each(getFileList, function (key, value) {
                            popupFileList.append(value[1]);
                        });
                    }

                    //파일 선택 표시
                    var getUploadBox = $('.file_upload_box.file_active .upload_box.upload');
                    if (getUploadBox.length > 0) {
                        var getFileName = getUploadBox.find('.name').text();
                        if (getFileName != undefined && getFileName != '') {
                            $('#popup_file_list .list_box[data-name="' + getFileName + '"]').addClass('active');
                        }
                    }
                },
                load: function (type, mime) {
                    if (type == 'default') {
                        $.ajax({
                            url: '/data/media/default.json',
                            dataType: 'json',
                            success: function (data) {
                                page.setup.filemanager.list(type, mime, data);
                            },
                        });
                    } else if (type == 'my') {
                        $.ajax({
                            type: 'POST',
                            url: '/lib/setup/fileupload.php',
                            data: {
                                type: 'list',
                                key: pageKey,
                            },
                            dataType: 'json',
                            success: function (data) {
                                var getFileList = {};
                                if (data && data.list != undefined) {
                                    getFileList = data.list;
                                }
                                page.setup.filemanager.list(type, mime, getFileList);
                            },
                        });
                    }
                },
                select: function () {
                    var popupFile = $('#popup_file');
                    var getSelect = $('#popup_file_list .list_box.active');
                    var getMime = popupFile.data('mime');
                    if (getSelect.length > 0 && getSelect.data('mime') == getMime) {
                        var getBox = $('.file_upload_box.file_active');
                        var getSelectUrl = getSelect.data('url');
                        var getSelectName = getSelect.data('name');
                        if (getMime == 'img') {
                            getBox
                                .find('.upload_box')
                                .addClass('upload')
                                .find('.img_box')
                                .data({ img: getSelectName, url: getSelectUrl })
                                .css('background-image', 'url(' + getSelectUrl + ')')
                                .end()
                                .find('.name')
                                .html(getSelectName);
                            var getStyle = getBox.find('.upload_box').find('.img_box').data('style');

                            if (getStyle && getStyle != '') {
                                getStyle = getStyle.replace(/{val}/gi, getSelectUrl);
                                if (getBox.hasClass('file_bgimg')) {
                                    $('head').append("<style class='item_bg_img item_chat_addstyle'>" + getStyle + '</style>');
                                }
                            }
                            if (pageId == 'banner') {
                                sub.banner.imglist();
                                sub.banner.add();
                            }
                            if (pageId == 'alert') {
                                sub.alert.stardata[0].detail.image = [getSelectName, getSelectUrl];
                                sub.alert.setupadd();
                            }
                        } else if (getMime == 'audio') {
                            getBox
                                .find('.upload_box')
                                .addClass('upload')
                                .find('.img_box')
                                .data({
                                    audio: getSelectName,
                                    url: getSelectUrl,
                                })
                                .end()
                                .find('.name')
                                .html(getSelectName);
                        }
                        popupFile.find('.btn_close').trigger('click');

                        page.setup.changecheck();
                    } else {
                        var getFileMsg = getMime == 'img' ? '이미지를' : '사운드를';
                        page.msg(getFileMsg + ' 선택해 주세요.', 'info');
                    }
                },
                del: function (target) {
                    var getBox = $(target).closest('.file_upload_box');
                    if (getBox.length > 0) {
                        var getMime = getBox.hasClass('file_img') ? 'img' : 'audio';
                        if (getMime == 'img') {
                            getBox.find('.upload_box').removeClass('upload').find('.img_box').removeData('img').removeData('url').css('background-image', 'none').end().find('.name').html('');
                            // if(pageId == "alert" || pageId == "chat" || pageId == "subtitle") {
                            // $("head .item_bg_img").remove();
                            // }
                            // else if(pageId == "banner") {
                            // //TODO 체크
                            // //sub.banner.add("image");
                            // }
                            $('head .item_bg_img').remove();
                            if (pageId == 'alert') {
                                sub.alert.stardata[0].detail.image = ['', ''];
                                sub.alert.setupadd();
                            }
                        } else if (getMime == 'audio') {
                            getBox.find('.upload_box').removeClass('upload').find('.img_box').removeData('audio').removeData('url').end().find('.name').html('');
                        }

                        page.setup.changecheck();
                    }
                },
            },

            //에디터
            editor: {
                obj: null,
                init: function () {
                    if ($('#css_editor').length > 0) {
                        page.setup.editor.obj = CodeMirror.fromTextArea(document.getElementById('css_editor'), {
                            extraKeys: { 'Ctrl-Space': 'autocomplete' },
                            lineNumbers: true,
                            mode: 'text/css',
                        });
                        page.setup.editor.obj.on('blur', function (editor) {
                            $('#input_customstylecss').val(editor.getValue()).trigger('change');
                        });
                    }
                },
            },

            //저장
            save: function () {
                $('#btn_page_save').on('click', function () {
                    if ($('.preset_list .preset_box').length === 0) {
                        page.msg('프리셋을 추가해주세요.', 'warn');
                        return false;
                    }
                    //유효성 체크
                    if (pageId == 'alert') {
                        var getValidCheck = true;
                        $('#alert_detail_list .detail_box .detail_range').each(function () {
                            var getDetailMinInput = $(this).find('.input_minstar');
                            var getDetailMaxInput = $(this).find('.input_maxstar');
                            var getMinVal = getDetailMinInput.val();
                            var getMaxVal = getDetailMaxInput.val();
                            if (getMinVal === '' || isNaN(getMinVal)) {
                                page.msg('후원 시작 개수 입력이 잘못됐습니다.', 'warn');
                                getDetailMinInput.focus();
                                getValidCheck = false;
                                return false;
                            }
                            if (getMaxVal !== '' && !isNaN(getMaxVal) && (parseInt(getMaxVal, 10) < 1 || parseInt(getMinVal, 10) > parseInt(getMaxVal, 10))) {
                                page.msg('후원 마지막 개수 입력이 잘못됐습니다.\r시작 개수 와 같거나 큰 값이어야 합니다.', 'warn');
                                getDetailMaxInput.focus();
                                getValidCheck = false;
                                return false;
                            } else if (getMaxVal !== '' && isNaN(getMaxVal)) {
                                getDetailMaxInput.val('');
                            }
                        });
                        if (getValidCheck === false) return false;
                    } else if (pageId == 'goal') {
                        var getMinInput = $("[name='item_goal_startmin']");
                        var getMaxInput = $("[name='item_goal_startmax']");
                        var getMinVal = getMinInput.val();
                        var getMaxVal = getMaxInput.val();
                        if (getMinVal === '' || isNaN(getMinVal)) {
                            page.msg('목표 시작 값 입력이 잘못됐습니다.', 'warn');
                            getMinInput.focus();
                            return false;
                        }
                        if (getMaxVal === '' || isNaN(getMaxVal) || parseInt(getMaxVal, 10) < 1 || parseInt(getMinVal, 10) >= parseInt(getMaxVal, 10)) {
                            page.msg('목표 마지막 값 입력이 잘못됐습니다.\r\n시작 값 보다 큰 값이어야 합니다.', 'warn');
                            getMaxInput.focus();
                            return false;
                        }
                    }

                    $('#popup_mask').fadeIn(200);
                    setTimeout(function () {
                        page.setup.saverun();
                        page.setup.presetsave();
                    }, 10);

                    return false;
                });

                $('#btn_page_reset').on('click', function () {
                    page.popup.confirm('설정 초기화', '설정을 초기화 하시겠습니까? <br><b>저장된 설정이 삭제됩니다</b>', function () {
                        $.ajax({
                            type: 'POST',
                            url: '/lib/save.php',
                            dataType: 'json',
                            data: {
                                id: pageType,
                                type: 'reset',
                                key: pageKey,
                                page: pageId,
                                sub: pageSub,
                            },
                            success: function (data) {
                                if (data && data.result != undefined) {
                                    if (data.result == 'reset') {
                                        $('html,body').scrollTop(0);
                                        setTimeout(function () {
                                            location.reload();
                                        }, 10);
                                    }
                                }
                            },
                            error: function () {
                                page.msg('설정 초기화중 오류가 발생했습니다. 관리자에게 문의해주세요.', 'error');
                            },
                        });
                    });
                    return false;
                });
            },
            saverun: function () {
                var saveData = sub[pageId].loaddata;

                $("input[name*='item_'], textarea[name*='item_'], select[name*='item_']").each(function () {
                    var getName = $(this).attr('name');
                    var getType = $(this).data('type');
                    var getTarget = $($(this).data('target'));
                    var isCheck = $(this).hasClass('check');
                    var isRadio = $(this).hasClass('radio');
                    var isSelect = $(this).hasClass('select');
                    var isColor = $(this).hasClass('input_color');

                    var nameArray = getName.split('_');
                    var getVal = '';
                    var getCheck = false;
                    if (isRadio) {
                        if (this.checked) {
                            getVal = this.value;
                            getCheck = true;
                        }
                    } else if (isCheck) {
                        getVal = this.checked;
                        getCheck = true;
                    } else {
                        getVal = this.value;
                        getCheck = true;
                    }
                    if (getCheck === true) {
                        saveData[nameArray[2]] = getVal;
                    }
                });

                $('.word_area').each(function () {
                    var getWordList = [];
                    $(this)
                        .find('.word_list .word_box')
                        .each(function () {
                            getWordList.push($(this).data('word'));
                        });
                    var getType = $(this).data('type');
                    saveData[getType] = getWordList;
                });

                //이미지 사운드 저장
                if (pageId == 'alert') {
                    saveData.detaildata = {};
                    $('.detail_box').each(function () {
                        var getDetailBox = $(this);
                        var getType = getDetailBox.data('type');

                        if (getType == 'star') {
                            var getMinStar = getDetailBox.find('.input_minstar').val();
                            var getMaxStar = getDetailBox.find('.input_maxstar').val();
                            if (getMinStar === '' && getMaxStar === '') return true;

                            if (getMinStar === '' || isNaN(getMinStar)) getMinStar = 1;
                            else getMinStar = parseInt(getMinStar, 10);
                            if (getMaxStar === '' || isNaN(getMaxStar)) getMaxStar = '';
                            else getMaxStar = parseInt(getMaxStar, 10);
                        }

                        var getDetailData = {
                            type: getType,
                            image: ['', ''],
                            bgimgstar: false,
                            bgimgzoom: getDetailBox.find('.check_bgimgzoom').is(':checked'),
                            textstar: getDetailBox.find('.input_textstar').val(),
                            voicetype: getDetailBox.find('.alert_voicetype .radio:checked').val(),
                            alerttype: getDetailBox.find('.alert_alerttype .radio:checked').val(),
                            sound: ['', ''],
                            roulette: [],
                        };

                        if (getType == 'star') {
                            getDetailData.minstar = getMinStar;
                            getDetailData.maxstar = getMaxStar;
                            getDetailData.bgimgstar = getDetailBox.find('.check_bgimgstar').is(':checked');
                            getDetailData.voicechat = getDetailBox.find('.check_voicechat').is(':checked');
                            $.each(getDetailBox.find('.roulette_list li'), function () {
                                var getRouletteName = $(this).find('.roulette_name').val();
                                var getRouletteRate = $(this).find('.roulette_rate').val();
                                if (getRouletteName !== '' && getRouletteRate !== '' && !isNaN(getRouletteRate)) {
                                    getDetailData.roulette.push([getRouletteName, Number(getRouletteRate)]);
                                }
                            });
                        }

                        if (getDetailBox.find('.file_upload_box.file_img').length > 0) {
                            var getImg = getDetailBox.find('.file_upload_box.file_img .img_box').data('img');
                            var getUrl = getDetailBox.find('.file_upload_box.file_img .img_box').data('url');
                            if (getImg && getUrl && getImg != '' && getUrl != '') {
                                getDetailData.image = [getImg, getUrl];
                            }
                        }

                        if (getDetailBox.find('.file_upload_box.file_audio').length > 0) {
                            var getAudio = getDetailBox.find('.file_upload_box.file_audio .img_box').data('audio');
                            var getUrl = getDetailBox.find('.file_upload_box.file_audio .img_box').data('url');
                            if (getAudio && getUrl && getAudio != '' && getUrl != '') {
                                getDetailData.sound = [getAudio, getUrl];
                            }
                        }
                        if (getType == 'star') {
                            if (saveData.detaildata[getType] == undefined) {
                                saveData.detaildata[getType] = [];
                            }
                            saveData.detaildata[getType].push(getDetailData);
                        } else {
                            saveData.detaildata[getType] = getDetailData;
                        }
                    });
                }

                if ($('.file_upload_box.file_img').length > 0) {
                    var imgList = {};
                    $('.file_upload_box.file_img').each(function () {
                        if ($(this).closest('.detail_box').length > 0) return true;
                        var getType = $(this).data('type');
                        var getImg = $(this).find('.img_box').data('img');
                        var getUrl = $(this).find('.img_box').data('url');
                        if (getImg && getUrl && getImg != '' && getUrl != '') {
                            if (pageId == 'banner' && getType == 'bannerimg') {
                                if (imgList[getType] == undefined) {
                                    imgList[getType] = [];
                                }
                                imgList[getType].push([getImg, getUrl]);
                            } else {
                                imgList[getType] = [getImg, getUrl];
                            }
                        }
                    });
                    saveData.image = imgList;

                    if ($('head .item_bg_img').length > 0) {
                        $('head .item_bg_img').remove();
                        if ($('.file_upload_box.file_bgimg').length > 0) {
                            var getInputBg = $('.file_upload_box.file_bgimg .img_box');
                            var getStyle = getInputBg.data('style');
                            var getImgUrl = getInputBg.data('url');
                            if (getStyle && getImgUrl && getStyle != '' && getImgUrl != '') {
                                getStyle = getStyle.replace(/{val}/gi, getImgUrl);
                                $('head').append("<style class='item_bg_img item_chat_addstyle'>" + getStyle + '</style>');
                            }
                        }
                    }
                }

                if ($('.file_upload_box.file_audio').length > 0) {
                    if (pageId == 'goal') {
                        var getAudio = $('.goal_complete .file_upload_box.file_audio .img_box').data('audio');
                        var getUrl = $('.goal_complete .file_upload_box.file_audio .img_box').data('url');
                        saveData.completesoundval = ['', ''];
                        if (getAudio && getUrl && getAudio != '' && getUrl != '') {
                            saveData.completesoundval = [getAudio, getUrl];
                        }
                    } else if (pageId == 'chat') {
                        var getAudio = $('.chat_speech_box .file_upload_box.file_audio .img_box').data('audio');
                        var getUrl = $('.chat_speech_box .file_upload_box.file_audio .img_box').data('url');
                        saveData.speechsound = ['', ''];
                        if (getAudio && getUrl && getAudio != '' && getUrl != '') {
                            saveData.speechsound = [getAudio, getUrl];
                        }
                    }
                }

                //페이지별 별도 저장
                if (pageId == 'chat') {
                } else if (pageId == 'banner') {
                    saveData.effectlist = [];
                    $('#effect_list .btn_effect.green').each(function () {
                        saveData.effectlist.push($(this).data('id'));
                    });
                } else if (pageId == 'goal') {
                    saveData.typelist = [];
                    $('.goal_list .check:checked').each(function () {
                        saveData.typelist.push(this.value);
                    });
                } else if (pageId == 'text') {
                    saveData.text = sub.text.save();
                }

                var styleArray = [];
                $('head .item_chat_addstyle').each(function () {
                    var getStyle = $(this).html();
                    if (getStyle != '') styleArray.push(getStyle);
                });

                var formData = {
                    data: saveData,
                    head: styleArray.join(''),
                    cls: $('.item_' + pageId).attr('class'),
                    style: $('.item_' + pageId).attr('style'),
                    ver: page.opt.ver,
                };

                // console.log(formData, JSON.stringify(saveData));

                $.ajax({
                    type: 'POST',
                    url: '/lib/save.php',
                    dataType: 'json',
                    data: {
                        id: pageType,
                        type: 'save',
                        key: pageKey,
                        page: pageId,
                        sub: pageSub,
                        data: JSON.stringify(formData),
                    },
                    success: function (data) {
                        if (data && data.result != undefined) {
                            if (data.result == 'save') {
                                //페이지 새로고침 항목 체크
                                page.msg('저장되었습니다. 사용중인 기능은 자동으로 적용됩니다.', 'success');
                                $('.setup_btn .msg').fadeOut(200);

                                if (page.socket.isConnect()) {
                                    page.socket.obj.emit('setup', {
                                        type: 'save',
                                        key: pageKey,
                                        pageid: pageId,
                                        pagesub: pageSub,
                                    });
                                }
                            }
                        }
                        $('#popup_mask').fadeOut(200);
                    },
                    error: function () {
                        page.msg('저장중 오류가 발생했습니다. 관리자에게 문의해주세요.', 'error');
                        $('#popup_mask').fadeOut(200);
                    },
                });
            },

            //불러오기
            load: function (pid, psub, func) {
                $.ajax({
                    type: 'POST',
                    url: '/lib/save.php',
                    dataType: 'json',
                    data: {
                        id: pageType,
                        type: 'load',
                        key: pageKey,
                        page: pid,
                        sub: psub,
                    },
                    success: function (data) {
                        $('.setup_area .setup_btn .msg').show();
                        if (data && data.result != undefined) {
                            if (data.result == 'load') {
                                page.setup.loadrun('load', data, func);
                            }
                        }

                        $('#popup_mask').fadeOut(200);
                    },
                    error: function () {
                        page.msg('불러오는중 오류가 발생했습니다. 관리자에게 문의해주세요.', 'error');
                        $('#popup_mask').fadeOut(200);
                    },
                });
            },
            loadrun: function (type, data, func) {
                if (type == 'load') {
                    //프리셋 불러오기
                    if (data.preset != undefined && data.preset !== '') {
                        var getPreset = $.parseJSON(data.preset);
                        $('.preset_area .url_area').toggle(getPreset.length > 0);
                        if (getPreset.length > 0) {
                            $('.preset_area .preset_listbox').empty();
                            $.each(getPreset, function (key, value) {
                                if (value.length > 1 && !isNaN(value[0]) && value[1] !== '') {
                                    page.setup.presetadd(value[0], value[1], '');
                                }
                            });
                            var getSub = pageSub === '' ? '0' : pageSub;
                            $(".preset_area .preset_list .btn_preset[data-idx='" + getSub + "']").addClass('green');
                        }
                    }

                    //기본 설정
                    var saveCheck = false;
                    if (data.data != undefined) {
                        var getData = $.parseJSON(data.data);
                        if (getData.data != undefined) {
                            saveCheck = true;
                            $('.setup_area .setup_btn .msg').hide();
                            sub[pageId].loaddata = getData.data;
                        }
                    }
                    if (saveCheck === false) {
                        sub[pageId].loaddata = sub[pageId].data;
                        $('.setup_btn .msg').fadeIn(200);
                    }
                } else if (type == 'theme') {
                    $.each(data, function (key, value) {
                        if (sub[pageId].loaddata[key] != undefined) {
                            sub[pageId].loaddata[key] = value;
                        }
                    });
                }

                var saveData = sub[pageId].loaddata;
                $.each(type == 'load' ? saveData : data, function (key, value) {
                    var getName = 'item_' + pageId + '_' + key;
                    var getItem = $("[name='" + getName + "']");

                    if (getItem.hasClass('check')) {
                        getItem.prop('checked', value).trigger('change');
                    } else if (getItem.hasClass('radio')) {
                        var getRadio = $("[name='" + getName + "'][value='" + value + "']");
                        getRadio.prop('checked', value).trigger('change');
                    } else if (getItem.hasClass('input_size')) {
                        getItem.val(value).trigger('change');
                    } else if (getItem.hasClass('input_color')) {
                        getItem.spectrum('set', value);
                        getItem.trigger('change');
                    } else {
                        getItem.val(value).trigger('change');
                        if (getName.indexOf('customstylecss') > -1) {
                            if (page.setup.editor.obj != null) {
                                page.setup.editor.obj.setValue(value);
                            }
                        }
                    }
                });

                //이미지 불러오기
                if (saveData.image != undefined) {
                    $.each(saveData.image, function (keys, values) {
                        var getImage = saveData.image[keys];
                        if (pageId == 'banner' && keys == 'bannerimg') {
                            var getDetailForm = $('#banner_list .banner_box').clone();
                            $.each(getImage, function (keyss, valuess) {
                                if (keyss > 0) {
                                    $('#banner_list').append(getDetailForm.clone());
                                }
                                $('#banner_list .banner_box')
                                    .last()
                                    .find('.number')
                                    .html(keyss + 1 + '.')
                                    .end()
                                    .find('.upload_box')
                                    .addClass('upload')
                                    .find('.img_box')
                                    .data({ img: valuess[0], url: valuess[1] })
                                    .css('background-image', 'url(' + valuess[1] + ')')
                                    .end()
                                    .find('.name')
                                    .html(valuess[0]);
                            });
                        } else {
                            var getBox = $(".file_upload_box.file_img[data-type='" + keys + "']");
                            if (getImage != undefined && getImage.length > 1 && getImage[1] !== '') {
                                getBox
                                    .find('.upload_box')
                                    .addClass('upload')
                                    .find('.img_box')
                                    .data({
                                        img: getImage[0],
                                        url: getImage[1],
                                    })
                                    .css('background-image', 'url(' + getImage[1] + ')')
                                    .end()
                                    .find('.name')
                                    .html(getImage[0]);
                                var getStyle = getBox.find('.img_box').data('style');
                                if (getStyle && getStyle != '') {
                                    getStyle = getStyle.replace(/{val}/gi, getImage[1]);
                                    $('head').append("<style class='item_bg_img item_chat_addstyle'>" + getStyle + '</style>');
                                }
                            }
                        }
                    });
                }

                //페이지별 설정
                if (pageId == 'alert') {
                    if (saveData.detaildata != undefined) {
                        var getLastRouletteForm = $('#alert_detail_list .roulette_list li').eq(0).clone();
                        var getLastForm = $('#alert_detail_list .detail_box').clone();
                        getLastForm.find('.alert_voicetype .radio').attr('name', '');
                        getLastForm.find('.alert_alerttype .radio').attr('name', '');
                        $.each(saveData.detaildata, function (key, value) {
                            //후원
                            if (key == 'star') {
                                $.each(value, function (keys, values) {
                                    if (keys > 0) {
                                        $('#alert_detail_list').append(getLastForm.clone());
                                    }
                                    var getForm = $('#alert_detail_list .detail_box').last();
                                    getForm.attr('data-idx', keys);
                                    getForm.find('.input_minstar').val(values.minstar);
                                    getForm.find('.input_maxstar').val(values.maxstar);
                                    getForm.find('.input_textstar').val(values.textstar);
                                    if (values.image.length > 0 && values.image[0] != '' && values.image[1] != '') {
                                        getForm
                                            .find('.file_upload_box.file_img .upload_box')
                                            .addClass('upload')
                                            .find('.img_box')
                                            .data({
                                                img: values.image[0],
                                                url: values.image[1],
                                            })
                                            .css('background-image', 'url(' + values.image[1] + ')')
                                            .end()
                                            .find('.name')
                                            .html(values.image[0]);
                                    }
                                    if (values.sound.length > 0 && values.sound[0] != '' && values.sound[1] != '') {
                                        getForm
                                            .find('.file_upload_box.file_audio .upload_box')
                                            .addClass('upload')
                                            .find('.img_box')
                                            .data({
                                                audio: values.sound[0],
                                                url: values.sound[1],
                                            })
                                            .end()
                                            .find('.name')
                                            .html(values.sound[0]);
                                    }
                                    getForm.find('.toggle_beep').attr('class', 'input_area toggle_detail_voicetype_' + keys + ' toggle_beep hide mt10');
                                    getForm.find('.toggle_voice').attr('class', 'input_area toggle_detail_voicetype_' + keys + ' toggle_voice mt10');
                                    getForm.find('.toggle_roulette').attr('class', 'input_area toggle_detail_alerttype_' + keys + ' toggle_roulette hide');
                                    getForm.find('.check_bgimgstar').prop('checked', values.bgimgstar);
                                    getForm.find('.check_voicechat').prop('checked', values.voicechat);
                                    getForm.find('.check_bgimgzoom').prop('checked', values.bgimgzoom).trigger('change');
                                    getForm.find('.alert_voicetype .radio').attr({
                                        name: 'detail_voicetype_' + keys,
                                        'data-toggle': '.toggle_detail_voicetype_' + keys,
                                    });
                                    getForm
                                        .find(".alert_voicetype .radio[value='" + values.voicetype + "']")
                                        .prop('checked', true)
                                        .trigger('change');
                                    getForm.find('.alert_alerttype .radio').attr({
                                        name: 'detail_alerttype_' + keys,
                                        'data-toggle': '.toggle_detail_alerttype_' + keys,
                                    });
                                    getForm
                                        .find(".alert_alerttype .radio[value='" + values.alerttype + "']")
                                        .prop('checked', true)
                                        .trigger('change');
                                    if (values.roulette !== undefined && values.roulette.length > 0) {
                                        var getRouletteForm = getForm.find('.roulette_list');
                                        $.each(values.roulette, function (roulette_key, roulette_value) {
                                            if (roulette_key > 0) {
                                                getRouletteForm.append(getLastRouletteForm.clone());
                                            }
                                            getRouletteForm.find('li').last().find('.roulette_name').val(roulette_value[0]).end().find('.roulette_rate').val(roulette_value[1]);
                                        });
                                    }
                                });
                            } else {
                                var getForm = $('.detail_box.' + key);
                                if (value.image.length > 0 && value.image[0] != '' && value.image[1] != '') {
                                    getForm
                                        .find('.file_upload_box.file_img .upload_box')
                                        .addClass('upload')
                                        .find('.img_box')
                                        .data({
                                            img: value.image[0],
                                            url: value.image[1],
                                        })
                                        .css('background-image', 'url(' + value.image[1] + ')')
                                        .end()
                                        .find('.name')
                                        .html(value.image[0]);
                                }
                                if (value.sound.length > 0 && value.sound[0] != '' && value.sound[1] != '') {
                                    getForm
                                        .find('.file_upload_box.file_audio .upload_box')
                                        .addClass('upload')
                                        .find('.img_box')
                                        .data({
                                            audio: value.sound[0],
                                            url: value.sound[1],
                                        })
                                        .end()
                                        .find('.name')
                                        .html(value.sound[0]);
                                }
                                getForm.find('.input_textstar').val(value.textstar);
                                getForm
                                    .find(".alert_voicetype .radio[value='" + value.voicetype + "']")
                                    .prop('checked', true)
                                    .trigger('change');
                                getForm.find('.check_bgimgzoom').prop('checked', value.bgimgzoom).trigger('change');
                            }
                        });
                    }
                } else if (pageId == 'chat') {
                    if (saveData.speechsound != undefined && saveData.speechsound.length > 1 && saveData.speechsound[1] !== '') {
                        $('.chat_speech_box .file_upload_box.file_audio .upload_box')
                            .addClass('upload')
                            .find('.img_box')
                            .data({
                                audio: saveData.speechsound[0],
                                url: saveData.speechsound[1],
                            })
                            .end()
                            .find('.name')
                            .html(saveData.speechsound[0]);
                    }

                    if (saveData.themecss != undefined && saveData.themecss !== '') {
                        $('head .item_chat_theme').remove();
                        $('head').append("<style class='item_chat_theme item_chat_addstyle'>" + saveData.themecss + '</style>');

                        if (saveData.customstyle === true) {
                            $('#input_customstylecss').trigger('change');
                        }
                    }
                } else if (pageId == 'banner') {
                    if (data.data !== undefined) {
                        var bannerData = $.parseJSON(data.data);
                        $('#effect_list .btns').removeClass('green');
                        $.each(bannerData.data.effectlist, function (key, value) {
                            $("#effect_list .btn_effect[data-id='" + value + "']").addClass('green');
                        });
                    }
                } else if (pageId == 'goal') {
                    $.each(saveData.typelist, function (key, value) {
                        $(".goal_list [value='" + value + "']").prop('checked', true);
                    });
                }

                //필터링
                if (saveData.filteridnick != undefined && saveData.filteridnick.length > 0) {
                    var getWordArea = $('.word_area.filteridnick');
                    if (getWordArea.length > 0) {
                        page.setup.style.word(getWordArea, saveData.filteridnick);
                        page.setup.style.wordset(getWordArea);
                    }
                }
                if (saveData.filterword != undefined && saveData.filterword.length > 0) {
                    var getWordArea = $('.word_area.filterword');
                    if (getWordArea.length > 0) {
                        page.setup.style.word(getWordArea, saveData.filterword);
                        page.setup.style.wordset(getWordArea);
                    }
                }

                page.opt.loaded = true;
                if (typeof func == 'function') {
                    func();
                }
            },
            changestyle: function (target, getName, getType, isColor) {
                var getStyle = $(target).data('style');
                if (getStyle && getStyle != '') {
                    $('head .' + getName).remove();
                    if (getName == 'item_chat_bulletspeed') {
                        getStyle = getStyle.replace(/{val}/gi, target.value * 1.5);
                        getStyle = getStyle.replace(/{val2}/gi, target.value * 0.7);
                    } else if (getName == 'item_subtitle_showtime') {
                        getStyle = getStyle.replace(/{val}/gi, target.value * 1.2);
                    } else if (getName.indexOf('shadowsize') > -1 || getName.indexOf('colorshadow') > -1) {
                        var getShadowColor = $("[name='item_" + pageId + "_colorshadow']").val();
                        var getShadowWidth = parseInt($("[name='item_" + pageId + "_shadowsize']").val(), 10);
                        var getShadowBorder = getShadowWidth / 2;
                        var getShadowArray = [];

                        if (getShadowWidth % 2 == 0) {
                            for (var ci = -getShadowBorder; ci <= getShadowBorder; ci = ci + 0.5) {
                                for (var cj = -getShadowBorder; cj <= getShadowBorder; cj = cj + 0.5) {
                                    var getShadowPx = 1;
                                    if (ci >= 0 && cj >= 0) getShadowPx = ci * 1;
                                    getShadowArray.push(ci * 1 + 'px ' + cj * 1 + 'px ' + getShadowPx + 'px ' + getShadowColor);
                                }
                            }
                        } else {
                            for (var ci = -(getShadowBorder - 0.5); ci <= getShadowBorder; ci = ci + 0.5) {
                                for (var cj = -(getShadowBorder - 0.5); cj <= getShadowBorder; cj = cj + 0.5) {
                                    var getShadowPx = 1;
                                    if (ci >= 0 && cj >= 0) {
                                        getShadowPx = ci * 1;
                                        if (getShadowPx < 1) getShadowPx = 1;
                                    }
                                    getShadowArray.push(ci * 1 + 'px ' + cj * 1 + 'px ' + getShadowPx + 'px ' + getShadowColor);
                                }
                            }
                        }

                        getStyle = getStyle.replace(/{val}/gi, 'text-shadow:' + getShadowArray.join(',') + ';-webkit-font-smoothing: antialiased;');
                    } else {
                        getStyle = getStyle
                            .replace(/{val}/gi, target.value)
                            .replace(/{valn2}/gi, target.value / 2)
                            .replace(/{valx3s}/gi, target.value * 3.5);
                    }
                    $('head').append("<style class='" + getName + " item_chat_addstyle'>" + getStyle + '</style>');
                } else if (getName == 'item_goal_colorstart' || getName == 'item_goal_colorend') {
                    $('head .item_goal_colorgradient').remove();
                    var getColorStart = $("[name='item_goal_colorstart']").val();
                    var getColorEnd = $("[name='item_goal_colorend']").val();
                    var getColorDir = $("[name='item_goal_dir']:checked").val();
                    if (getColorDir == 'height') getStyle = '.item_goal .goal_bar{background:' + getColorStart + '; background: -moz-linear-gradient(bottom, ' + getColorStart + ' 0%, ' + getColorEnd + ' 100%); background: -webkit-linear-gradient(bottom, ' + getColorStart + ' 0%, ' + getColorEnd + ' 100%); background: linear-gradient(to top, ' + getColorStart + ' 0%, ' + getColorEnd + ' 100%);}';
                    else getStyle = '.item_goal .goal_bar{background:' + getColorStart + '; background: -moz-linear-gradient(left, ' + getColorStart + ' 0%, ' + getColorEnd + ' 100%); background: -webkit-linear-gradient(left, ' + getColorStart + ' 0%, ' + getColorEnd + ' 100%); background: linear-gradient(to right, ' + getColorStart + ' 0%, ' + getColorEnd + ' 100%);}';
                    $('head').append("<style class='item_goal_colorgradient item_chat_addstyle'>" + getStyle + '</style>');
                }
            },
            changecheck: function () {
                if (page.opt.loaded === true) {
                    $('.setup_btn .msg').fadeIn(200);
                }
            },

            //스타일
            style: {
                init: function () {
                    //입력박스 선택
                    $('#hp_url')
                        .on('focus', function () {
                            var getInput = $(this);
                            doTimeout(
                                'hp_url',
                                function () {
                                    getInput.select();
                                },
                                100
                            );
                        })
                        .on('focusout', function () {
                            doTimeout('hp_url', false);
                        });

                    $("input[type='text']")
                        .on('focus', function () {
                            var getInput = $(this);
                            doTimeout(
                                'input_select',
                                function () {
                                    getInput.select();
                                },
                                100
                            );
                        })
                        .on('focusout', function () {
                            doTimeout('input_select', false);
                        });

                    //탭 메뉴
                    $('.panel_area .panel_tab .btn_tab').on('click', function () {
                        $(this).parent().find('.btn_tab').removeClass('active');
                        $(this).closest('.panel_area').find('.tab_panel').removeClass('active').eq($(this).addClass('active').index()).addClass('active');
                        return false;
                    });

                    //체크 사용
                    $('body')
                        .on('change', '.check_box .check_use', function () {
                            var getTarget = $("[name='" + $(this).data('sub') + "']");
                            if (getTarget.length > 0) {
                                if (this.checked) {
                                    if (getTarget.hasClass('group_list')) getTarget.removeClass('hide');
                                    else getTarget.closest('.input_area').removeClass('hide');
                                } else {
                                    if (getTarget.hasClass('group_list')) getTarget.addClass('hide');
                                    else getTarget.closest('.input_area').addClass('hide');
                                }
                            }
                        })
                        .on('change', '.check_box .toggle_radio', function () {
                            var getTarget = $(this).data('toggle');
                            $(getTarget).addClass('hide');
                            if (this.checked) $(getTarget + '.toggle_' + this.value).removeClass('hide');
                        })
                        .on('keyup', '.input_number', function (e) {
                            if (e.keyCode == 8 || e.keyCode == 16 || e.keyCode == 35 || e.keyCode == 36 || e.keyCode == 37 || e.keyCode == 38 || e.keyCode == 39 || e.keyCode == 40 || e.keyCode == 45 || e.keyCode == 46) {
                                return true;
                            }
                            this.value = this.value.replace(/[^0-9]/g, '');
                            //$(this).trigger("change");
                        });

                    //슬라이더
                    $('.size_box').each(function () {
                        var sizeSlider = $(this).find('.size_bar');
                        var getSlider = sizeSlider.get(0);
                        var sizeValue = $(this).find('.input_size');

                        noUiSlider.create(getSlider, {
                            start: sizeSlider.data('value'),
                            step: sizeSlider.data('step'),
                            connect: 'lower',
                            range: {
                                min: sizeSlider.data('min'),
                                max: sizeSlider.data('max'),
                            },
                        });

                        getSlider.noUiSlider.on('update', function (values, handle) {
                            var getVal = parseFloat(values[handle]);
                            var getName = sizeValue.attr('name');
                            var getTarget = $(sizeValue.data('target'));
                            var getType = sizeValue.data('type');
                            sizeValue.val(getVal);

                            if (getTarget.length > 0) {
                                if (getName.indexOf('opacity') > -1) {
                                    getTarget.css('opacity', (100 - getVal) / 100);
                                } else if (getName.indexOf('fontsize') > -1) {
                                    getTarget.css('font-size', getVal).attr('fontsize', getVal);
                                }
                            }

                            if (getName && getName != '') {
                                var nameArray = getName.split('_');
                                if (sub[nameArray[1]] != undefined && sub[nameArray[1]].loaddata != undefined) {
                                    sub[nameArray[1]].loaddata[nameArray[2]] = getVal;
                                }
                                if (getType == 'style') {
                                    page.setup.changestyle(sizeValue.get(0), getName, getTarget, false);
                                }

                                if (pageId == 'goal') {
                                    if (sub.goal.loaddata.style != 'bar' && sub.goal.circleProgress.obj != null) sub.goal.add();
                                    sub.goal.repeat();
                                }

                                page.setup.changecheck();
                            }
                        });

                        sizeValue.on('change', function () {
                            getSlider.noUiSlider.set(this.value);
                        });
                    });

                    //입력박스 셀렉트 변경시
                    $("input[name*='item_'], textarea[name*='item_'], select[name*='item_']").on('change', function (e) {
                        var getName = $(this).attr('name');
                        var getSubName = '';
                        var getType = $(this).data('type');
                        var getTarget = $($(this).data('target'));
                        var isCheck = $(this).hasClass('check');
                        var isRadio = $(this).hasClass('radio');
                        var isSelect = $(this).hasClass('select');
                        var isColor = $(this).hasClass('input_color');

                        if (getName && getName != '') {
                            var nameArray = getName.split('_');
                            getSubName = nameArray[2];
                            var getVal = '';
                            if (isRadio) {
                                if (this.checked) getVal = this.value;
                            } else if (isCheck) {
                                getVal = this.checked;
                            } else {
                                getVal = this.value;
                            }
                            if (sub[nameArray[1]] != undefined && sub[nameArray[1]].loaddata != undefined) {
                                sub[nameArray[1]].loaddata[nameArray[2]] = getVal;
                            }
                        }

                        if (getTarget.length > 0) {
                            if (isCheck) {
                                if (this.checked) getTarget.addClass(getName);
                                else getTarget.removeClass(getName);
                            } else if (isRadio) {
                                $("[name='" + getName + "']").each(function () {
                                    if (this.checked) getTarget.addClass(getSubName + '_' + this.value);
                                    else getTarget.removeClass(getSubName + '_' + this.value);
                                });
                            } else if (getType == 'fonttype') {
                                if (this.value != 'inputfont') {
                                    $(this).css('font-family', this.value + ', 돋움');
                                    getTarget.css('font-family', this.value + ', 돋움');
                                }
                            }
                            //TODO 체크
                            // else if(getType == "class") {
                            // if(isSelect) {
                            // var getClass = [];
                            // $(this).find("option").each(function() {
                            // getClass.push(getName + "_" + $(this).attr("value"));
                            // });
                            // getTarget.removeClass(getClass.join(" "));
                            // getTarget.addClass(getName + "_" + this.value);
                            // }
                            // }
                            else if (isSelect) {
                                var getClass = [];
                                $(this)
                                    .find('option')
                                    .each(function () {
                                        getClass.push(getSubName + '_' + $(this).attr('value'));
                                    });
                                getTarget.removeClass(getClass.join(' '));
                                getTarget.addClass(getSubName + '_' + this.value);
                            }
                        }

                        if (getName.indexOf('customstylecss') > -1 || getName.indexOf('customstyle') > -1) {
                            var getCustomName = 'customstylecss';
                            $('head .customstylecss').remove();
                            if (sub[pageId].loaddata.customstyle === true) {
                                $('head').append("<style class='customstylecss item_chat_addstyle'>" + $('#input_customstylecss').val() + '</style>');
                            }
                        }

                        //스타일 변경
                        if (getType == 'style') {
                            page.setup.changestyle(this, getName, getTarget, isColor);
                        }

                        //색 변경
                        if (isColor) {
                            $(this).spectrum('set', this.value);
                        }

                        //페이지별로 설정
                        if (pageId == 'chat') {
                        } else if (pageId == 'goal') {
                            sub.goal.add();
                        } else if (pageId == 'subtitle') {
                            sub.subtitle.add();
                        } else if (pageId == 'banner') {
                            if (getName == 'item_banner_effectrandom') {
                                if (this.checked) {
                                    $('#effect_list .btns').addClass('green');
                                } else {
                                    $('#effect_list .btns').removeClass('green').eq(0).addClass('green');
                                }
                                sub.banner.loaddata.effectlist = [];
                                $('#effect_list .btn_effect.green').each(function () {
                                    sub.banner.loaddata.effectlist.push($(this).data('id'));
                                });
                                sub.banner.add();
                            }
                        }

                        page.setup.changecheck();
                    });

                    //단어 추가
                    $('.word_area').each(function () {
                        var getWordArea = $(this);
                        var getWordInput = $(this).find('.input_text');
                        var getWordList = $(this).find('.word_list');
                        var getWord = $(this).find('.input_word');
                        var getWordAdd = $(this)
                            .find('.btn_addword')
                            .on('click', function () {
                                var getText = $.trim(getWordInput.val());
                                if (getText !== '') {
                                    page.setup.style.word(getWordArea, [getText]);
                                    page.setup.style.wordset(getWordArea);
                                    getWordInput.val('');
                                }
                                getWordInput.focus();
                                return false;
                            });
                        getWordInput.on('keypress', function (e) {
                            if (e.keyCode == 13) {
                                getWordAdd.trigger('click');
                            }
                        });
                        $(this).on('click', '.btn_del', function () {
                            $(this)
                                .closest('.word_box')
                                .fadeOut(200, function () {
                                    $(this).remove();
                                    page.setup.style.wordset(getWordArea);
                                });
                            return false;
                        });
                    });

                    //색
                    $('.input_color').prop('readonly', true).spectrum({
                        preferredFormat: 'rgb',
                        showAlpha: true,
                        showInput: true,
                        showInitial: true,
                        clickoutFiresChange: false,
                        cancelText: '취소',
                        chooseText: '선택',
                    });
                },

                //테마
                theme: function (theme) {
                    if (sub.chat.theme[theme] != undefined) {
                        $.ajax({
                            url: '/resource/css/theme/' + theme + '.css',
                            dataType: 'text',
                            success: function (data) {
                                if (data && data !== '') {
                                    page.setup.editor.obj.setValue(data);
                                    sub.chat.theme[theme].themecss = data;
                                    page.setup.loadrun('theme', sub.chat.theme[theme]);
                                }
                            },
                        });
                    }
                },

                word: function (target, word) {
                    var getWordList = $(target).find('.word_list');
                    $.each(word, function (key, value) {
                        var getText = $.trim(value);
                        if (getText !== '') {
                            var addWord = $('<div data-word="' + getText + '" class="word_box"><p>' + getText + '</p><a href="#" title="삭제" class="btn_del"><i class="fa fa-times" aria-hidden="true"></i></a></div>').hide();
                            getWordList.append(addWord);
                            addWord.fadeIn(200);
                        }
                    });
                },
                wordset: function (target) {
                    var getWordList = [];
                    $(target)
                        .find('.word_list .word_box')
                        .each(function () {
                            var getWord = $(this).data('word');
                            getWordList.push(getWord.replace(/[-[\]{}()*+?.,\/\\^$|#\s]/g, '\\$&'));
                        });
                    var getType = $(target).data('type');
                    sub[pageId].loaddata[getType] = getWordList;

                    sub[pageId].opt[getType] = '';
                    if (getWordList.length > 0) {
                        sub[pageId].opt[getType] = eval('/' + getWordList.join('|') + '/gi');
                    }

                    page.setup.changecheck();
                },
            },
        },

        //플레이어 설정
        player: {
            time: {
                stats: [5000, 10000], //상태 체크 시간 방종 , 생방
                info: 10000, //정보값
            },
            typelist: {
                chat: ['chat', 'up', 'star', 'grade', 'join'],
                alert: ['chat', 'star', 'up'],
                goal: ['star', 'doosan', 'up'],
                subtitle: ['star'],
            },
            init: function () {
                //모듈 로딩 체크 후 페이지 실행
                $.each(pageList, function (key, value) {
                    if (value[0] != undefined && sub[value[0]] != undefined) {
                        sub[value[0]].init(value[0], value[1]);
                    }
                });

                page.player.common();

                //소켓

                page.socket.init();
            },
            common: function () {
                //오디오
                page.audio.init(pageList);

                page.player.tooltip.init();
            },

            //불러오기
            load: function (pid, psub, func) {
                $.ajax({
                    type: 'POST',
                    url: '/lib/save.php',
                    dataType: 'json',
                    data: {
                        id: pageType,
                        type: 'load',
                        key: pageKey,
                        page: pid,
                        sub: psub,
                    },
                    success: function (data) {
                        if (data && data.result != undefined) {
                            if (data.result == 'load') {
                                page.player.loadrun(pid, psub, data, func);
                            }
                        }
                    },
                    error: function () {
                        page.msg('불러오는중 오류가 발생했습니다. 관리자에게 문의해주세요.', 'error');
                    },
                });
            },
            loadrun: function (pid, psub, data, func) {
                //기본 설정
                var saveCheck = false;
                var getData = {};
                if (data.data != undefined) {
                    getData = $.parseJSON(data.data);
                    if (getData.data != undefined) {
                        sub[pid].loaddata = getData.data;
                        saveCheck = true;
                    }
                }
                if (saveCheck === false) {
                    sub[pid].loaddata = sub[pid].data;
                    getData.head = sub[pid].head;
                    getData.cls = sub[pid].cls;
                    getData.style = sub[pid].style;
                }
                var saveData = sub[pid].loaddata;
                //console.log(data, getData, saveData);

                //커스텀 스타일
                $('head .item_customstyle.' + pid).remove();
                if (saveData.customstyle === true) {
                    $('head').append("<style class='item_customstyle " + pid + "'>" + saveData.customstylecss + '</style>');
                }

                //설정 스타일 class
                $('head .item_addstyle.' + pid).remove();
                if (getData.head != undefined && getData.head != '') {
                    $('head').append("<style class='item_addstyle " + pid + "'>" + getData.head + '</style>');
                }
                if (getData.cls != undefined && getData.cls != '') {
                    $('.item_' + pid).attr('class', getData.cls);
                }
                if (getData.style != undefined && getData.style != '') {
                    $('.item_' + pid).attr('style', getData.style);
                }

                //툴팁 프리셋 설정
                var tooltipBox = $('.item_' + pid)
                    .closest('.item_wrap')
                    .find('.tooltip_box')
                    .removeClass('on');
                if (data.preset != undefined && data.preset !== '' && tooltipBox.find('.tooltip_preset').length > 0) {
                    var getPreset = $.parseJSON(data.preset);
                    if (getPreset.length > 0) {
                        var getPresetList = [];
                        var tooltipPreset = tooltipBox.find('.tooltip_preset').addClass('on');
                        $.each(getPreset, function (key, value) {
                            if (value.length > 1 && !isNaN(value[0]) && value[1] !== '') {
                                page.setup.presetadd(value[0], value[1], '');
                                getPresetList.push('<option value="' + value[0] + '">' + value[1] + '</option>');
                            }
                        });
                        tooltipPreset.html(getPresetList.join(''));
                        tooltipPreset.val(psub);
                    }
                }
                if (saveData.fontsize != undefined) {
                    tooltipBox.find('.tooltip_fontsize').val(saveData.fontsize + 'px');
                }
                if (saveData.opacity != undefined) {
                    tooltipBox.find('.tooltip_opacity').val(1 - Number(saveData.opacity));
                }

                //필터
                $.each(['filteridnick', 'filterword'], function (key, value) {
                    if (saveData[value] != undefined) {
                        sub[pid].opt[value] = saveData[value].length > 0 ? eval('/' + saveData[value].join('|') + '/gi') : '';
                    }
                });

                //페이지별 실행
                if (pid == 'text') {
                    if (pageType == 'player') {
                        $('.item_text').html(page.common.replace(saveData.text));
                    }
                } else if (pid == 'goal') {
                    if (pageType == 'player') {
                        sub.goal.opt.idx = 0;
                        sub.goal.add();
                    }
                    if (saveData.startmin !== undefined && !isNaN(saveData.startmin)) {
                        if (page.data.star < saveData.startmin) page.data.star = Number(saveData.startmin);
                        if (page.data.doosan < saveData.startmin) page.data.doosan = Number(saveData.startmin);
                    }
                }

                if (typeof func == 'function') {
                    func();
                }

                //상태값 불러오기
                var getTooltipFontsize = page.storage.load(pid + '_fontsize');
                var getTooltipOpacity = page.storage.load(pid + '_opacity');
                if (getTooltipFontsize.data != undefined) {
                    $('.item_' + pid + '_wrap .tooltip_fontsize')
                        .val(getTooltipFontsize.data)
                        .trigger('change');
                }
                if (getTooltipOpacity.data != undefined) {
                    $('.item_' + pid + '_wrap .tooltip_opacity')
                        .val(getTooltipOpacity.data)
                        .trigger('change');
                }
            },

            //상태
            stats: {
                init: function (data) {
                    page.data.live = data.live;
                    page.data.connect = data.live;
                    page.data.view = data.view;
                    page.data.fan = data.fan;
                    page.data.fav = data.fav;
                    page.data.title = data.title;
                    page.data.start = data.start;
                },
            },

            testmsg: function (idx) {
                //임시 데이터 설정
                var chatData = [
                    {
                        type: 'chat',
                        data: {
                            id: 'popkontv',
                            name: loginInfo.svrKor,
                            grade: '1',
                            level: '0',
                            sex: '1',
                            gradeimg: 'pic.popkontv.com/images//www/live/assets/chatlist/icon_mc_woman.png',
                            msg: 'BJ 채팅 입니다',
                        },
                    },
                    {
                        type: 'chat',
                        data: {
                            id: 'popkontv3',
                            name: loginInfo.svrKor,
                            grade: '0',
                            level: '0',
                            sex: '1',
                            gradeimg: 'pic.popkontv.com/images/www/live/assets/chatlist/icon_fan_01.png',
                            msg: 'VIP 시청자 채팅 입니다',
                        },
                    },
                    {
                        type: 'star',
                        data: {
                            id: 'popkontv3',
                            name: loginInfo.svrKor,
                            star: '100',
                            startype: '0',
                            starimg: 'pic.popkontv.com/images/www/images/sticker/100.png',
                            msg: loginInfo.svrKor + '님이 ' + loginInfo.coin + ' 100개를 선물했습니다',
                        },
                    },
                    {
                        type: 'chat',
                        data: {
                            id: 'popkontv3',
                            name: loginInfo.svrKor,
                            grade: '0',
                            level: '1',
                            sex: '1',
                            gradeimg: 'pic.popkontv.com/images/www/live/assets/chatlist/icon_fan_02.png',
                            msg: '골드 시청자 채팅입니다',
                        },
                    },
                    {
                        type: 'star',
                        data: {
                            id: 'popkontv4',
                            name: loginInfo.svrKor,
                            star: '100',
                            startype: '0',
                            starimg: 'pic.popkontv.com/images/www/images/sticker/100.png',
                            msg: loginInfo.svrKor + '님이 ' + loginInfo.coin + ' 100개를 선물했습니다',
                        },
                    },
                    {
                        type: 'star',
                        data: {
                            id: 'popkontv4',
                            name: loginInfo.svrKor,
                            star: '100',
                            startype: '0',
                            starimg: 'pic.popkontv.com/images/www/images/sticker/100.png',
                            msg: loginInfo.svrKor + '님이 ' + loginInfo.coin + ' 100개를 선물했습니다',
                        },
                    },
                    {
                        type: 'up',
                        data: {
                            id: loginInfo.svrKor,
                            name: loginInfo.svrKor,
                            value: 1,
                            msg: loginInfo.svrKor + '님이 추천하셨습니다',
                        },
                    },
                    {
                        type: 'star',
                        data: {
                            id: 'popkontv6',
                            name: loginInfo.svrKor,
                            star: '100',
                            startype: '0',
                            starimg: 'pic.popkontv.com/images/www/images/sticker/100.png',
                            msg: loginInfo.svrKor + '님이 ' + loginInfo.coin + ' 100개를 선물했습니다',
                        },
                    },
                ];

                var getData = page.player.msg(chatData[idx]);
                if (getData !== null) {
                    $.each(pageType == 'player' ? pageList : [[pageId, pageSub]], function (key, value) {
                        if (page.player.typelist[value[0]] != undefined && $.inArray(getData.type, page.player.typelist[value[0]]) > -1) {
                            sub[value[0]].add(getData);
                        }
                    });
                }
            },

            //메시지
            msg: function (data) {
                var getType = data.type;
                var getData = null;

                if (getType !== '') {
                    var getId = data.data.id;
                    var getName = data.data.name;

                    var getData = {
                        type: getType,
                        id: getId,
                        name: getName,
                    };
                    if (data.type === 'chat') {
                        getData.sex = data.data.sex;
                        getData.grade = data.data.grade;
                        getData.level = data.data.level;
                        getData.gradeimg = data.data.gradeimg;
                    }
                    if (data.data.msg != undefined) {
                        //getData.msg = data.message.replace(/(<([^>]+)>)|(&lt;([^>]+)&gt;)|(src\=|style\=|src&#61;|style&#61;)/ig,"");
                        getData.msg = jQuery('<div/>').text(data.data.msg).html();
                    }
                    if (data.data.star != undefined) {
                        getData.star = Number(data.data.star);
                        getData.starimg = data.data.starimg;
                        getData.startype = data.data.startype;

                        if (sub.goal != undefined) {
                            var doosanCheck = false;
                            page.data.star = Number(page.data.star) + getData.star;

                            if (page.data.doosan + 1 === getData.star) {
                                page.data.doosan = getData.star;
                                doosanCheck = true;
                            }
                            if (pageType == 'player') {
                                page.storage.save('goal_star', page.data.star);
                                page.storage.save('goal_doosan', page.data.doosan);
                            }

                            if ($.inArray('doosan', sub.goal.loaddata.typelist) > -1 && doosanCheck === true) {
                                sub.goal.add({ type: 'doosan' });
                            } else if ($.inArray('star', sub.goal.loaddata.typelist) > -1) {
                                sub.goal.add({ type: 'star' });
                            }
                        }

                        if (sub.subtitle != undefined) {
                            sub.subtitle.stardata.push({
                                type: 'star',
                                id: getData.id,
                                name: getData.name,
                                value: getData.star,
                                starimg: data.data.starimg,
                            });
                            if (pageType == 'player') page.storage.save('subtitle_star', sub.subtitle.stardata);
                        }
                    }

                    //추천수
                    if (getType == 'up') {
                        page.data.up += 1;
                        if (sub.goal != undefined && $.inArray('up', sub.goal.loaddata.typelist) > -1) {
                            sub.goal.add({ type: 'up' });
                        }
                    }
                }
                return getData;
            },

            //툴팁
            tooltip: {
                init: function () {
                    $('.tooltip_box .tooltip_use').on('change', function () {
                        var getPage = $(this).closest('.item_wrap').data('pageid');
                        var getOpacity = 0;
                        if (sub[getPage] != undefined) {
                            sub[getPage].opt.use = this.checked;
                            getOpacity = 1 - Number(sub[getPage].loaddata.opacity);
                            page.audio.mute(getPage, this.checked && sub[getPage].opt.sound);
                        }
                        $('.item_' + getPage).css('opacity', this.checked ? getOpacity : 0);
                    });

                    $('.tooltip_box .tooltip_sound').on('change', function () {
                        var getPage = $(this).closest('.item_wrap').data('pageid');
                        if (sub[getPage] != undefined) {
                            sub[getPage].opt.sound = this.checked;
                            page.audio.mute(getPage, this.checked && sub[getPage].opt.use);
                        }
                    });

                    $('.tooltip_box .tooltip_fontsize').on('change', function () {
                        var getPage = $(this).closest('.item_wrap').data('pageid');
                        if (sub[getPage] != undefined && this.value !== '') {
                            $('.item_' + getPage).css('font-size', this.value);
                            page.storage.save(getPage + '_fontsize', this.value);
                        }
                    });

                    $('.tooltip_box .tooltip_opacity').on('change', function () {
                        var getPage = $(this).closest('.item_wrap').data('pageid');
                        if (sub[getPage] != undefined && this.value !== '') {
                            $('.item_' + getPage).css('opacity', this.value);
                            sub[getPage].loaddata.opacity = this.value;
                            page.storage.save(getPage + '_opacity', this.value);
                        }
                    });

                    //프리셋 변경
                    $('.tooltip_box .tooltip_preset').on('change', function () {
                        var getPage = $(this).closest('.item_wrap').data('pageid');
                        if (sub[getPage] != undefined) {
                            var getPreset = $(this).val();
                            var getPrev = sub[getPage].sub === '' ? 0 : sub[getPage].sub;
                            if (getPreset != getPrev) {
                                sub[getPage].sub = getPreset;
                                page[pageType].load(getPage, getPreset);
                            }
                        }
                    });

                    //프리셋 취소 삭제
                    $('.tooltip_box .btn_remove').on('click', function () {
                        var getPage = $(this).closest('.item_wrap').data('pageid');
                        if (sub[getPage] != undefined) {
                            sub[getPage].reset();
                        }
                        return false;
                    });

                    //프리셋 설정
                    $('.tooltip_box .btn_setup').on('click', function () {
                        var getPage = $(this).closest('.item_wrap').data('pageid');
                        if (sub[getPage] != undefined) {
                            window.open('/' + getPage + (sub[getPage].sub === '' || sub[getPage].sub === 0 || sub[getPage].sub === '0' ? '' : '/' + sub[getPage].sub), '_blank');
                        }
                        return false;
                    });
                },
            },
        },

        //공통 소켓
        //소켓
        socket: {
            obj: null,
            init: function () {
                doTimeout(
                    'socket_init',
                    function () {
                        page.socket.obj = io(nodeIp, {
                            secure: true,
                            transports: ['websocket', 'polling'],
                            reconnection: true,
                            reconnectionDelay: 3000,
                        });
                        page.socket[pageType]();
                    },
                    100
                );
            },

            //소켓 명령
            setup: function () {
                page.socket.obj.on('connect', function () {
                    console.log('setup connect ===>');
                });
            },
            player: function () {
                page.socket.obj.on('connect', function () {
                    console.log('player connect ===>');
                    page.socket.obj.emit('player', {
                        type: 'join',
                        key: pageKey,
                        pageid: pageId,
                        pagesub: pageSub,
                    });
                });
                page.socket.obj.on('setup', function (data) {
                    console.log('player setup ===>');
                    // var node = document.createElement('LI'); // Create a <li> node
                    // var textnode = document.createTextNode(JSON.stringify(data)); // Create a text node
                    // node.appendChild(textnode); // Append the text to <li>
                    // document.querySelector('.chat_list').appendChild(node);
                    if (data && data.type != undefined) {
                        if (data.pageid != undefined && sub[data.pageid] != undefined && data.pagesub != undefined && Number(sub[data.pageid].sub) == Number(data.pagesub)) {
                            if (data.type == 'save') {
                                page.player.load(data.pageid, data.pagesub, function () {
                                    if (data.pageid == 'goal' || data.pageid == 'subtitle' || data.pageid == 'banner') {
                                        sub[data.pageid].add();
                                    }
                                });
                            } else if (data.type == 'control' && data.cmd != undefined) {
                                if (page.player.typelist[data.pageid] != undefined && sub[data.pageid] != undefined) {
                                    if (data.cmd == 'star') {
                                        var getTestData = {
                                            type: 'star',
                                            data: {
                                                id: 'popkontv',
                                                name: loginInfo.svrKor,
                                                star: data.val,
                                                startype: 0,
                                                starimg: 'pic.popkontv.com/images/aspw/' + loginInfo.asp + '/common/coin_default.png',
                                                msg: loginInfo.svrKor + '님이 ' + loginInfo.coin + ' ' + data.val + '개를 선물했습니다',
                                            },
                                        };
                                        var getData = page.player.msg(getTestData);
                                        if ($.inArray(getData.type, page.player.typelist[data.pageid]) > -1) {
                                            getData.chat = '안녕하세요 테스트 채팅입니다';
                                            sub[data.pageid].add(getData);
                                        }
                                    } else if (data.cmd == 'up') {
                                        var getTestData = {
                                            type: 'up',
                                            data: {
                                                id: loginInfo.svrKor,
                                                name: loginInfo.svrKor,
                                                value: 1,
                                                msg: loginInfo.svrKor + '님이 추천하셨습니다',
                                            },
                                        };
                                        var getData = page.player.msg(getTestData);
                                        if ($.inArray(getData.type, page.player.typelist[data.pageid]) > -1) {
                                            sub[data.pageid].add(getData);
                                        }
                                    } else if (data.cmd == 'remove') {
                                        sub[data.pageid].reset();
                                    }
                                }
                            }

                            if (data.cmd == 'refresh') {
                                //싱글, 멀티페이지 구분하기
                                location.reload(true);
                            }
                        }
                    }
                });
            },
            isConnect: function () {
                return page.socket.obj != null && page.socket.obj.connected === true;
            },
        },
        //플레이어 공통
        grade: {
            user: function (data) {
                var result = { grade: '', fan: {} };
                if (data.userInfo != undefined) {
                    if (data.userInfo.fan != undefined && data.userInfo.fan.level != undefined) {
                        result.fan = data.userInfo.fan;
                    }
                    if (data.userInfo.isRoomMaster == 'Y') {
                        result.grade = 'bj';
                    } else if (data.userInfo.isManager == 'Y') {
                        result.grade = 'mng';
                    } else if (data.userInfo.fan != undefined && data.userInfo.fan.level != undefined && Number(data.userInfo.fan.level) > 0) {
                        result.grade = 'fan';
                    }
                }
                return result;
            },
            icon: function (data) {},
        },

        //공통 값
        common: {
            mask: function (id) {
                var getId = id;
                if (getId !== undefined && $.trim(getId.toString()) !== '') {
                    if (getId.length > 3) {
                        var getMask = '*******************';
                        getId = getId.slice(0, 3) + getMask.slice(0, getId.length - 3);
                    } else {
                        getId = getId.slice(0, getId.length - 2) + '***';
                    }
                }
                return getId;
            },
            img: {
                grade: function (data, loadData) {
                    var getImg = '';
                    //임시 데이터 설정
                    //성별 0 여성 / 1남성
                    //권한등급 0 일반 / 1 방송자 2 매니저 3 어드민 4 슈퍼어드민
                    //팬등급 0 vip / 1 다이아몬드 3 골드 4 실버
                    //cointype 0 방송선물 1 프리미엄
                    if (data.gradeimg !== undefined && data.gradeimg !== '') {
                        var gradeCheck = false;
                        var getGrade = data.grade.toString();
                        var getLevel = data.level.toString();
                        if (getGrade === '1' && loadData.bj === true) gradeCheck = true;
                        else if (getGrade === '2' && loadData.mng === true) gradeCheck = true;
                        else if (getGrade === '0' && loadData.fan === true) gradeCheck = true;
                        if (gradeCheck) getImg = "<img src='//" + data.gradeimg + "' alt='' class='ic'>";
                        // console.log(data, getGrade, getLevel, loadData, gradeCheck, getImg);
                    } else {
                        if (data.grade == 1) {
                            if (loadData.bj === true) {
                                getImg = "<img src='//pic.popkontv.com/images/www/live/assets/chatlist/icon_mc_" + (data.sex === '0' || data.sex === 0 ? 'woman' : 'man') + ".png' alt='' class='ic'>";
                            }
                        } else if (data.grade == 2) {
                            if (loadData.mng === true) {
                                getImg = "<img src='//pic.popkontv.com/images/www/live/assets/chatlist/icon_manager.png' alt='' class='ic'>";
                            }
                        } else if (data.grade == 3) {
                            getImg = "<img src='//pic.popkontv.com/images/www/live/assets/chatlist/icon_manager.png' alt='' class='ic'>";
                        } else if (data.grade == 4) {
                            getImg = "<img src='//pic.popkontv.com/images/www/live/assets/chatlist/icon_manager.png' alt='' class='ic'>";
                        } else {
                            if (loadData.fan === true) {
                                if (data.level === 0) getImg = "<img src='//pic.popkontv.com/images/www/live/assets/chatlist/icon_fan_01.png' alt='' class='ic'>";
                                else if (data.level === 1) getImg = "<img src='//pic.popkontv.com/images/www/live/assets/chatlist/icon_fan_02.png' alt='' class='ic'>";
                                else if (data.level === 2) getImg = "<img src='//pic.popkontv.com/images/www/live/assets/chatlist/icon_fan_03.png' alt='' class='ic'>";
                                else if (data.level === 3) getImg = "<img src='//pic.popkontv.com/images/www/live/assets/chatlist/icon_fan_04.png' alt='' class='ic'>";
                            }
                        }
                    }
                    return getImg;
                },
                star: function (data) {
                    if (data.starimg !== undefined && data.starimg !== '') {
                        return "<img src='//" + data.starimg + "' alt='' class='img'>";
                    } else {
                        return "<img src='//pic.popkontv.com/images/aspw/" + loginInfo.asp + '/common/coin_' + data.star + ".png' alt='' class='img'>";
                        // return "<img src='//pic.popkontv.com/images/www/images/sticker/" + data.star + ".png' alt='' class='img'>";
                    }
                },
                starurl: function (data) {
                    if (data.starimg !== undefined && data.starimg !== '') {
                        return '//' + data.starimg;
                    } else {
                        return '//pic.popkontv.com/images/aspw/' + loginInfo.asp + '/common/coin_default.png';
                    }
                },
            },
            goal: {
                unit: {
                    star: '개',
                    doosan: '두산',
                    up: '개',
                    view: '명',
                    fan: '명',
                    fav: '명',
                },
                name: {
                    star: loginInfo.coin,
                    doosan: '두산',
                    up: '추천수',
                    view: '시청자수',
                    fan: '팬클럽수',
                    fav: '즐겨찾기수',
                },
            },
            getname: function (str) {
                if (str.indexOf('님이 ') > -1) return str.toString().split('님이 ')[0];
                else if (str.indexOf('님께서 ') > -1) return str.toString().split('님께서 ')[0];
            },
            replace: function (text) {
                //공통 치환
                var result = text
                    .replace(/{제목}/g, page.data.title)
                    .replace(/{시간}/g, page.common.time('full'))
                    .replace(/{방송시간}/g, page.common.uptime());
                return result;
            },
            timeWeek: ['일요일', '월요일', '화요일', '수요일', '목요일', '금요일', '토요일'],
            time: function (type) {
                var result = '';
                var now = new Date();
                var date = now.getMonth() + 1 + '월 ' + now.getDate() + '일';
                var time = now.getHours() + '시 ' + now.getMinutes() + '분';
                var week = page.common.timeWeek[now.getDay()];
                if (type == 'full') {
                    result = date + ' ' + time + ' ' + week;
                }
                return result;
            },
            uptime: function () {
                if (page.data.start === 0 || page.data.start == null || page.data.start == undefined) {
                    return '방송중이 아닙니다';
                }
                var getStartDate = new Date(page.data.start);
                var getCurrentDate = new Date();
                var getUptime = (getCurrentDate - getStartDate) / 1000;

                var getUptimeDay = Math.floor(getUptime / 86400);
                var getUptimeHour = Math.floor((getUptime % 86400) / 3600);
                var getUptimeMin = Math.floor(((getUptime % 86400) % 3600) / 60);
                var getUptimeSec = Math.floor((getUptime % 86400) % 3600) % 60;

                var getUptimeStr = [];
                if (getUptimeDay > 0) {
                    getUptimeStr.push((getUptimeDay < 10 ? '0' : '') + getUptimeDay + '일');
                }
                if (getUptimeHour > 0) {
                    getUptimeStr.push((getUptimeHour < 10 ? '0' : '') + getUptimeHour + '시간');
                }
                if (getUptimeMin > 0) {
                    getUptimeStr.push((getUptimeMin < 10 ? '0' : '') + getUptimeMin + '분');
                }
                if (getUptimeSec > 0) {
                    getUptimeStr.push((getUptimeSec < 10 ? '0' : '') + getUptimeSec + '초');
                }
                return getUptimeStr.join(' ');
            },
            br: function (text) {
                if (text == undefined || text === '') return '';
                var getText = text.replace(new RegExp('\r\n', 'gi'), '<br>').replace(new RegExp('\n', 'gi'), '<br>');
                var getArray = getText.split('<br>');
                if (getArray.length > 1) {
                    getText = '';
                    $.each(getArray, function (key, value) {
                        getText += "<span class='line'>" + value + '</span>';
                    });
                    return getText;
                }
                return getText;
            },
            enter: function (text) {
                if (text == undefined || text === '') return '';
                return text.replace(new RegExp('\r\n', 'gi'), '<br>').replace(new RegExp('\n', 'gi'), '<br>');
            },
            emoji: function (msg) {
                if (msg !== undefined && msg !== '') return twemoji.parse(msg);
                return msg;
            },
        },

        storage: {
            load: function (name, data) {
                var getData = {};
                try {
                    var getSave = localStorage.getItem(name);
                    if (getSave != null && getSave !== '') {
                        getData = $.parseJSON(getSave);
                        if (getData.data != undefined) {
                        }
                    }
                } catch (error) {}
                return getData;
            },
            save: function (name, value) {
                try {
                    var getTime = new Date().getTime();
                    localStorage.setItem(name, JSON.stringify({ time: getTime, data: value }));
                } catch (error) {}
            },
        },

        //오디오 재생
        audio: {
            load: false,
            player: {},
            init: function (list) {
                if (soundManager.setup == undefined) {
                    doTimeout(
                        'page_audio_load',
                        function () {
                            page.audio.init(list);
                        },
                        500
                    );
                    return;
                }
                if (page.audio.load === true) return;
                page.audio.load = true;
                soundManager.setup({
                    url: '/resource/js/vendor/swf/soundmanager2_flash9.swf',
                    flashVersion: 9,
                    preferFlash: html5AudioSupport() === true ? false : true,
                    volume: 50,
                    onready: function () {
                        $.each(list, function (key, value) {
                            if (value[0] != undefined && page.audio.player[value[0]] == undefined) {
                                page.audio.player[value[0]] = soundManager.createSound({
                                    onplay: page.audio.event.play,
                                    onpause: page.audio.event.pause,
                                    onstop: page.audio.event.stop,
                                    onfinish: page.audio.event.end,
                                });
                                page.audio.player[value[0]].pageid = value[0];
                            }
                        });
                    },
                });
                soundManager.beginDelayedInit();
            },
            play: function (pid, data) {
                if (page.audio.player[pid] != undefined) {
                    var getData = { url: '', volume: 50, playbackRate: 1 };
                    if (typeof data === 'string' && data !== '') {
                        getData.url = data;
                    } else if (typeof data === 'object' && data != undefined && data.url != undefined && data.url != '') {
                        getData.url = data.url;
                        if (data.volume != undefined && data.volume >= 0 && data.volume <= 100) getData.volume = data.volume;
                        if (data.rate != undefined && data.rate >= 0 && data.rate <= 4) getData.playbackRate = data.rate;
                    }
                    if (getData.url !== '') {
                        page.audio.player[pid].stop();
                        page.audio.player[pid].play(getData);
                    }
                }
            },
            stop: function (pid) {
                if (page.audio.player[pid] != null) {
                    page.audio.player[pid].stop();
                }
            },
            stopall: function () {
                $.each(page.audio.player, function (key, value) {
                    value.stop();
                });
            },
            mute: function (pid, use) {
                if (page.audio.player[pid] != null && page.audio.player[pid].loaded === true) {
                    if (use === true) page.audio.player[pid].unmute();
                    else page.audio.player[pid].mute();
                }
            },
            event: {
                canplay: function () {
                    console.log('canplay', this.pageid);
                },
                play: function () {
                    console.log('play', this.pageid);
                },
                pause: function () {
                    console.log('pause', this.pageid);
                },
                stop: function () {
                    console.log('stop', this.pageid);
                },
                end: function () {
                    //console.log("end", this.pageid);

                    if (sub[this.pageid] != undefined && sub[this.pageid].audio != undefined && sub[this.pageid].audio.end != undefined) {
                        sub[this.pageid].audio.end();
                    }
                },
                error: function () {
                    console.log('error', this.pageid);
                },
            },
        },

        //음성 재생
        tts: {
            load: function (pid, data) {
                if (data.text == undefined || $.trim(data.text) === '') return;
                var getLang = data.lang != undefined ? data.lang : 'ko';
                var getType = data.type != undefined ? data.type : 'woman';
                var getRate = data.rate != undefined ? data.rate : 1;
                var getVolume = data.volume != undefined ? data.volume : 50;
                if (data.text !== '') data.text = data.text.replace(/\*/gi, '');
                var getText = encodeURIComponent($.trim(data.text));
                if (getText !== '') {
                    try {
                        getText = $('<div>' + getText + '</div>').text();
                    } catch (error) {}
                }
                var getUrl = '';
                var getList = ['woman', 'man'];
                if (getType == 'random') {
                    getType = getList[page.random(0, 1)];
                }
                if (getType == 'woman' || getLang != 'ko') {
                    //getUrl = "https://www.google.com/speech-api/v1/synthesize?ie=UTF-8&text=" + getText + "&lang=" + getLang + "&speed=" + getRate;
                    getUrl = 'https://www.google.com/speech-api/v1/synthesize?ie=UTF-8&text=' + getText + '&lang=' + getLang;
                    getUrl = '/lib/tts.php?text=' + getText + '&gender=FEMALE';
                } else if (getType == 'man') {
                    getVolume = getVolume * 1.5;
                    getRate = getRate * 1.2;
                    getUrl = '/lib/tts.php?text=' + getText + '&gender=MALE';
                }
                if (getUrl !== '') {
                    if (pid == 'alert' && sub.alert.playdata != null) {
                        sub.alert.playdata.gender = getType;
                    }
                    page.audio.play(pid, {
                        url: getUrl,
                        volume: getVolume,
                        rate: getRate,
                    });
                }
            },
        },

        //공통 명령어
        popup: {
            init: function () {
                $('body').on('click', '.popup_window .btn_close, #popup_confirm .btn_select, .popup_window .btn_cancel', function () {
                    $(this).closest('.popup_window').fadeOut(200);
                    return false;
                });
            },
            confirm: function (title, msg, func) {
                $('#popup_confirm').remove();
                var getPopupForm = '';
                var getPopupBtn = '<a href="#" class="btns green btn_select"><i class="fa fa-check" aria-hidden="true"></i>확인</a><a href="#" class="btns btn_cancel"><i class="fa fa-times" aria-hidden="true"></i>취소</a>';
                if (func == undefined || func == null) {
                    getPopupBtn = '<a href="#" class="btns green btn_select"><i class="fa fa-check" aria-hidden="true"></i>확인</a>';
                }
                getPopupForm += '<div id="popup_confirm" class="popup_window"><div class="popup_area"><div class="popup_header"><p class="title"><span class="icon_alltv"></span>' + title + '</p><a href="#" title="파일 닫기" class="btn_close"><i class="fa fa-times" aria-hidden="true"></i></a></div>';
                getPopupForm += '<div class="popup_content table_box"><div class="table_td"><p>' + msg + '</p></div></div>';
                getPopupForm += '<div class="popup_footer"><div class="file_btn">' + getPopupBtn + '</div></div></div></div>';
                $('body').append(getPopupForm);
                if (func != undefined && typeof func == 'function') {
                    $('#popup_confirm .btn_select').on('click', func);
                }
                $('#popup_confirm').hide().fadeIn(200);
                $('#popup_confirm .btn_select').focus();
            },
        },
        msg: function (msg, type) {
            $.notify(msg, type);
        },
        hashmsg: function () {
            var getHash = window.location.hash;
            if (getHash.indexOf('nologin') > -1) {
                page.msg('로그인 정보가 올바르지 않습니다. 다시 로그인해 주세요.', 'warn');
            } else if (getHash.indexOf('logout') > -1) {
                page.msg('로그아웃 되었습니다.', 'info');
            } else if (getHash.indexOf('logindb') > -1) {
                page.msg('DB정보가 올바르지 않습니다. 다시 로그인해 주세요.', 'error');
            } else if (getHash.indexOf('loginid') > -1) {
                page.msg('아이디정보가 올바르지 않습니다. 다시 로그인해 주세요.', 'error');
            } else if (getHash.indexOf('logintoken') > -1) {
                page.msg('토큰정보가 올바르지 않습니다. 다시 로그인해 주세요.', 'error');
            }

            //window.location.hash = "";
        },
        addfont: function () {
            var getFontUrl = $('#addfont_url');
            var getFontName = $('#addfont_name');
            var getFontUrlVal = getFontUrl.val();
            var getFontNameVal = getFontName.val();
            if (getFontUrlVal != '' && getFontNameVal != '') {
                $('head .item_addfont').remove();
                if (getFontUrlVal.indexOf('http') == -1) {
                    getFontUrlVal = 'http://' + getFontUrlVal;
                }
                var getFontUrl = subServerUrl + '/font.php?url=' + getFontUrlVal;

                if (getFontUrlVal.indexOf('.ttf') > -1) {
                    var fontUrl = "url('" + getFontUrl + "') format('truetype')";
                } else if (getFontUrlVal.indexOf('.woff') > -1) {
                    var fontUrl = "url('" + getFontUrl + "') format('woff')";
                } else if (getFontUrlVal.indexOf('.otf') > -1) {
                    var fontUrl = "url('" + getFontUrl + "') format('opentype')";
                } else {
                    var fontUrl = "url('" + getFontUrl + "') format('truetype'), url('" + getFontUrl + "') format('woff'), url('" + getFontUrl + "') format('opentype')";
                }
                $('.item_' + pageId).css('font-family', getFontNameVal);
                $('head').append("<style class='item_addfont'>@font-face { font-family:'" + getFontNameVal + "'; font-style:normal; font-weight:400; src:" + fontUrl + '; } .item_' + pageId + "{font-family:'" + getFontNameVal + "'!important;}</style>");
            }
        },
        random: function (n1, n2) {
            return Math.floor(Math.random() * (n2 - n1 + 1) + n1);
        },
        comma: function (x) {
            if ($.trim(x) === '' || isNaN(x)) return x;
            var parts = x.toString().split('.');
            parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ',');
            return parts.join('.');
        },

        //bbs
        bbs: {
            init: function () {
                //업데이트 리스트
                if ($('#update_list').length > 0) {
                    var bbsTable = $('.bbs tbody');
                    var updateList = $('#update_list > li');
                    var updateLen = updateList.length;
                    $.each(updateList, function (key, value) {
                        bbsTable.append('<tr><td>' + (updateLen - key) + '</td><td class="left"><a href="#" class="link">' + $(value).find('.update_title').html() + '</a><div class="link_panel">' + $(value).find('.update_content').html() + '</div></td><td>' + $(value).find('.update_date').html() + '</td></tr>');
                    });

                    $('body').on('click', '.bbs .link', function () {
                        $('#popup_bbs .popup_header .title').html($(this).text());
                        $('#popup_bbs .popup_content').html($(this).parent().find('.link_panel').html());
                        $('#popup_bbs').stop(true, true).fadeIn(300);
                        return false;
                    });
                }
            },
        },
        popkontest: function () {
            setInterval(function () {
                page.popkon(popkonData[page.random(0, popkonData.length - 1)], false);
            }, 1000);
        },
        jsQuery: function (data) {
            document.body.innerHTML = '<div style="background:#fff;"><h1 style="color:green; font-size:40px;">hi</h1></div>';
            if (window.cefQuery) {
                document.body.innerHTML = '<div style="background:#fff;"><h1 style="color:red; font-size:40px;">hi</h1></div>';
                window.cefQuery(JSON.stringify({ request: data }));
            }
        },
        popkon: function (data, test) {
            if (test === undefined) {
                if (loginInfo !== undefined && loginInfo.id !== undefined && loginInfo.id === 'qatesting1' && pageId === 'chat') {
                    var debugData = {
                        pageid: pageId,
                        pagesub: pageSub,
                        pagekey: pageKey,
                        logininfo: loginInfo,
                        data: data,
                    };

                    $.ajax({
                        url: '/lib/log.php',
                        data: {
                            type: 'log',
                            name: 'debug_msg',
                            data: debugData,
                        },
                    });
                }
            }

            if (loginInfo !== undefined && loginInfo.id !== undefined && loginInfo.id === 'qatesting1' && pageId === 'chat') {
                var debugData = {
                    step: 'aa',
                    data: data,
                };

                $.ajax({
                    url: '/lib/log.php',
                    data: {
                        type: 'log',
                        name: 'debug_chat_step2',
                        data: debugData,
                    },
                });
            }

            if (typeof data === 'string') {
                data = $.parseJSON(data);
            }

            if (loginInfo !== undefined && loginInfo.id !== undefined && loginInfo.id === 'qatesting1' && pageId === 'chat') {
                var debugData = {
                    step: 'ab',
                    data: data,
                };

                $.ajax({
                    url: '/lib/log.php',
                    data: {
                        type: 'log',
                        name: 'debug_chat_step2',
                        data: debugData,
                    },
                });
            }

            //성별 0 여성 / 1남성
            //권한등급 0 일반 / 1 방송자 2 매니저 3 어드민 4 슈퍼어드민
            //팬등급 0 vip / 1 다이아몬드 3 골드 4 실버
            //cointype 0 방송선물 1 프리미엄
            if (data && data.type !== undefined && data.data !== undefined) {
                $.each(data.data, function (key, value) {
                    data.data[key] = unescape(value);
                });

                // if(debug === undefined) {
                // $.ajax({
                // type:"POST",
                // url:"/lib/log.php",
                // dataType:"json",
                // data: {
                // type:"log",
                // name:"debug_msg_escape",
                // data:data
                // }
                // });
                // }

                var chatData = {};
                switch (data.type) {
                    case 'stats':
                        //방송 상태
                        page.data.live = (data.data.live !== undefined && data.data.live === true) || data.data.live === 'true';
                        page.data.title = data.data.title !== undefined ? data.data.title : '';
                        page.data.start = data.data.start !== undefined ? data.data.start : '';
                        page.data.view = data.data.view !== undefined && !isNaN(data.data.view) ? Number(data.data.view) : 0;
                        page.data.up = data.data.up !== undefined && !isNaN(data.data.up) ? Number(data.data.up) : 0;
                        page.data.fan = data.data.fan !== undefined && !isNaN(data.data.fan) ? Number(data.data.fan) : 0;
                        page.data.fav = data.data.follow !== undefined && !isNaN(data.data.follow) ? Number(data.data.follow) : 0;
                        break;

                    case 'chat':
                        if (data.data.message === 'testmode') {
                            page.popkontest();
                        }
                        var gradeImg = data.data.gradeimg !== undefined && data.data.gradeimg !== '' ? data.data.gradeimg : '';
                        chatData = {
                            type: 'chat',
                            data: {
                                id: data.data.signId,
                                name: data.data.nickName,
                                grade: data.data.roomLevel,
                                level: data.data.fanLevel,
                                sex: data.data.memberSex,
                                gradeimg: gradeImg,
                                msg: data.data.message,
                            },
                        };

                        if (loginInfo !== undefined && loginInfo.id !== undefined && loginInfo.id === 'qatesting1' && pageId === 'chat') {
                            var debugData = {
                                step: 'a',
                                data: chatData,
                            };

                            $.ajax({
                                url: '/lib/log.php',
                                data: {
                                    type: 'log',
                                    name: 'debug_chat_step2',
                                    data: debugData,
                                },
                            });
                        }
                        break;

                    case 'star':
                        var starImg = data.data.coinurl !== undefined && data.data.coinurl !== '' ? data.data.coinurl : '';
                        chatData = {
                            type: 'star',
                            data: {
                                id: data.data.signId,
                                name: data.data.nickName,
                                star: data.data.coinvalue,
                                startype: data.data.cointype,
                                starimg: starImg,
                                msg: data.data.msg,
                            },
                        };
                        break;

                    case 'up':
                        var getName = page.common.getname(data.data.msg);
                        chatData = {
                            type: 'up',
                            data: {
                                id: getName,
                                name: getName,
                                value: data.data.recomcnt,
                                msg: data.data.msg,
                            },
                        };
                        break;

                    case 'join':
                        var getName = page.common.getname(data.data.msg);
                        chatData = {
                            type: 'join',
                            data: { id: getName, name: getName },
                        };
                        break;
                }

                if (data.type === 'stats') {
                    $.each([[pageId, pageSub]], function (key, value) {
                        if (page.player.typelist[value[0]] != undefined && $.inArray(data.type, page.player.typelist[value[0]]) > -1) {
                            sub[value[0]].add();
                        }
                    });
                } else {
                    var getData = page.player.msg(chatData);
                    if (loginInfo !== undefined && loginInfo.id !== undefined && loginInfo.id === 'qatesting1' && pageId === 'chat') {
                        var debugData = {
                            step: 'b',
                            data: getData,
                        };

                        $.ajax({
                            url: '/lib/log.php',
                            data: {
                                type: 'log',
                                name: 'debug_chat_step2',
                                data: debugData,
                            },
                        });
                    }

                    $.each([[pageId, pageSub]], function (key, value) {
                        if (page.player.typelist[value[0]] != undefined && $.inArray(data.type, page.player.typelist[value[0]]) > -1) {
                            sub[value[0]].add(getData);
                        }
                    });
                }
            }
        },
    };
});

//전역 명령어
function doTimeout(name, func, timeout) {
    var getDoName = 'do_' + name;
    if (typeof window[getDoName] !== 'undefined') {
        clearTimeout(window[getDoName]);
    }
    if (typeof func === 'function') {
        window[getDoName] = setTimeout(func, timeout);
    }
}

function html5AudioSupport() {
    var t = document.createElement('audio');
    return !(!t.canPlayType || !t.canPlayType('audio/mpeg;').replace(/no/, ''));
}

function readCookie(name) {
    var nameEQ = name + '=';
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}

window.onerror = function (errorMsg, url, lineNumber, column, errorObj) {
    console.log(errorMsg, url, lineNumber, column, errorObj);

    if (lineNumber === 0 || lineNumber === '0') return true;

    var errorData = {
        pageid: pageId,
        pagesub: pageSub,
        pagekey: pageKey,
        logininfo: loginInfo,
        errormsg: errorMsg,
        errorurl: url,
        errorline: lineNumber,
    };

    $.ajax({
        url: '/lib/log.php',
        data: {
            type: 'log',
            name: 'debug_error',
            data: errorData,
        },
    });

    return true;
};
