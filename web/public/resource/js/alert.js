define(['Sortable', 'lettering', 'textillate'], function (Sortable, lettering, textillate) {
    sub.alert = {
        opt: {
            main: null,
            use: true, //툴팁설정
            sound: true,

            target: null,
            typelist: {
                star: ['star'],
                normal: ['up'],
                voice: ['star'],
            },
            time: {
                next: 1000, //다음 팝업 딜레이
                voicenext: 1000, //보이스 끝난후 다음 팝업 딜레이
                roulettedelay: 3000, //룰렛 사용시 팝업 딜레이
            },
            starmax: 100, //stardata 최대 개수
            filteridnick: '',
            filterword: '',
        },
        sub: '',
        data: {
            texteffectin: 'tada',
            popupeffectin: 'tada',
            popupeffectout: 'bounceOut',
            showtime: '10',
            font: 'Jeju Gothic',
            fontbold: false,
            color: '#00ac00',
            colornick: '#ffc247',
            colornum: '#ffc247',
            opacity: '0',
            layout: 'top',
            align: 'center',
            customstyle: false,
            customstylecss: '',
            up: false,
            texteffectintype: 'none',
            voicetime: '15',
            volume: '50',
            voicerate: '1',
            voicegender: 'woman',
            voicelen: '',
            filtersp: false,
            addfonturl: '',
            addfontname: '',
            fontsize: '16',
            shadowsize: '2',
            colorshadow: 'rgba(0,0,0,0.8)',
            bgtrans: 'none',
            colorbg: '#000000',
            bgimgzoom: false,
            filteridnick: [],
            filterword: [],
            detaildata: {
                star: [
                    {
                        type: 'star',
                        image: ['cat9.gif', '//pic.popkontv.com/images/aspw/P-00117/common/coin_default.png'],
                        bgimgstar: true,
                        bgimgzoom: false,
                        textstar: '{닉네임}님 ' + loginInfo.coin + ' {개수}개 감사합니다!',
                        voicetype: 'voice',
                        alerttype: 'normal',
                        sound: ['', ''],
                        roulette: [
                            ['꽝', 200],
                            ['리액션', 100],
                            ['노래', 50],
                            ['퀵뷰', 20],
                            ['매니저', 10],
                            ['24시간방송', 5],
                            ['방종', 1],
                        ],
                        minstar: 1,
                        maxstar: '',
                        voicechat: true,
                    },
                ],
                up: { type: 'up', image: ['cat5.gif', '/data/media/img/cat5.gif'], bgimgstar: false, bgimgzoom: false, textstar: '{닉네임}님 추천 감사합니다!', voicetype: 'voice', sound: ['', ''], roulette: [] },
            },
            image: {},
        },
        head:
            '.item_alert .alert{-webkit-transition:transform 10s linear;-moz-transition:transform 10s linear;-ms-transition:transform 10s linear;-o-transition:transform 10s linear;transition:transform 10s linear}.item_alert .text_box{color:#20c9b0!important}.text_preview .item_alert{color:#20c9b0!important}.item_alert .text_box .nick{color:#ffc247!important}.text_preview .item_alert .nick{color:#ffc247!important}.item_alert .text_box .value{color:#ffc247!important}.text_preview .item_alert .value{color:#ffc247!important}.item_alert .alert_box .text,.text_preview .item_alert{text-shadow:-1px -1px 0px rgba(0,0,0,0.8),-1px -0.5px 0px rgba(0,0,0,0.8),-1px 0px 0px rgba(0,0,0,0.8),-1px 0.5px 0px rgba(0,0,0,0.8),-1px 1px 0px rgba(0,0,0,0.8),-0.5px -1px 0px rgba(0,0,0,0.8),-0.5px -0.5px 0px rgba(0,0,0,0.8),-0.5px 0px 0px rgba(0,0,0,0.8),-0.5px 0.5px 0px rgba(0,0,0,0.8),-0.5px 1px 0px rgba(0,0,0,0.8),0px -1px 0px rgba(0,0,0,0.8),0px -0.5px 0px rgba(0,0,0,0.8),0px 0px 0px rgba(0,0,0,0.8),0px 0.5px 0px rgba(0,0,0,0.8),0px 1px 0px rgba(0,0,0,0.8),0.5px -1px 0px rgba(0,0,0,0.8),0.5px -0.5px 0px rgba(0,0,0,0.8),0.5px 0px 0.5px rgba(0,0,0,0.8),0.5px 0.5px 0.5px rgba(0,0,0,0.8),0.5px 1px 0.5px rgba(0,0,0,0.8),1px -1px 0px rgba(0,0,0,0.8),1px -0.5px 0px rgba(0,0,0,0.8),1px 0px 1px rgba(0,0,0,0.8),1px 0.5px 1px rgba(0,0,0,0.8),1px 1px 1px rgba(0,0,0,0.8)}.item_alert .alert_box .text,.text_preview .item_alert{text-shadow:-1px -1px 0px rgba(0, 0, 0, 0.8),-1px -0.5px 0px rgba(0, 0, 0, 0.8),-1px 0px 0px rgba(0, 0, 0, 0.8),-1px 0.5px 0px rgba(0, 0, 0, 0.8),-1px 1px 0px rgba(0, 0, 0, 0.8),-0.5px -1px 0px rgba(0, 0, 0, 0.8),-0.5px -0.5px 0px rgba(0, 0, 0, 0.8),-0.5px 0px 0px rgba(0, 0, 0, 0.8),-0.5px 0.5px 0px rgba(0, 0, 0, 0.8),-0.5px 1px 0px rgba(0, 0, 0, 0.8),0px -1px 0px rgba(0, 0, 0, 0.8),0px -0.5px 0px rgba(0, 0, 0, 0.8),0px 0px 0px rgba(0, 0, 0, 0.8),0px 0.5px 0px rgba(0, 0, 0, 0.8),0px 1px 0px rgba(0, 0, 0, 0.8),0.5px -1px 0px rgba(0, 0, 0, 0.8),0.5px -0.5px 0px rgba(0, 0, 0, 0.8),0.5px 0px 0.5px rgba(0, 0, 0, 0.8),0.5px 0.5px 0.5px rgba(0, 0, 0, 0.8),0.5px 1px 0.5px rgba(0, 0, 0, 0.8),1px -1px 0px rgba(0, 0, 0, 0.8),1px -0.5px 0px rgba(0, 0, 0, 0.8),1px 0px 1px rgba(0, 0, 0, 0.8),1px 0.5px 1px rgba(0, 0, 0, 0.8),1px 1px 1px rgba(0, 0, 0, 0.8)}.item_alert.bgtrans_color{background-color:#000000}.item_alert.item_alert_bgimgzoom{background-size:100% 100%!important}',
        cls: 'item_box item_alert layout_top align_center voicegender_woman bgtrans_none',
        style: 'opacity: 1; font-size: 16px; font-family: "Jeju Gothic", 돋움;',
        loaddata: {},
        stardata: [],
        playdata: null,
        voiceid: {},
        staridx: 0,
        init: function (pid, psub) {
            sub.alert.opt.main = $('.item_alert');
            sub.alert.opt.target = sub.alert.opt.main.find('.item_alert_area .alert_box');
            sub.alert.sub = psub;

            if (pageType == 'setup') {
                //룰렛
                var getRouletteForm = $('#alert_detail_list .roulette_list li').eq(0).clone();
                $('#alert_detail_list')
                    .on('click', '.roulette_list .btn_listadd', function () {
                        var getForm = getRouletteForm.clone();
                        $(this).closest('.roulette_list').append(getForm);
                        return false;
                    })
                    .on('click', '.roulette_list .btn_listdel', function () {
                        var getRouletteList = $(this).closest('.roulette_list');
                        if (getRouletteList.find('li').length > 1) {
                            $(this).closest('li').remove();
                        }
                        return false;
                    });

                $('#alert_detail_list').on('click', '.btn_roulette_simulation', function () {
                    sub.alert.roulette.simulation(this);
                    return false;
                });
                $('#alert_detail_list').on('focusout', '.roulette_list .input_text', function () {
                    sub.alert.roulette.simulation(this);
                });

                //후원 프리셋
                var getDetailForm = $('#alert_detail_list .detail_box').clone();
                getDetailForm.find('.alert_voicetype .radio').attr('name', '');
                getDetailForm.find('.alert_alerttype .radio').attr('name', '');
                $('#alert_detail_list')
                    .on('click', '.alert_detailbox .btn_listadd', function () {
                        var getForm = getDetailForm.clone();
                        var getLastForm = $('#alert_detail_list .detail_box').last();
                        var getIdx = parseInt(getLastForm.data('idx'), 10) + 1;
                        var getLastVal = getLastForm.find('.input_maxstar').val();
                        var getVal = 1;
                        if (getLastVal !== '' && !isNaN(getLastVal)) {
                            getVal = parseInt(getLastVal, 10) + 1;
                        }
                        getForm.attr('data-idx', getIdx);
                        getForm.find('.input_minstar').val(getVal);
                        getForm.find('.alert_voicetype .radio').attr({ name: 'detail_voicetype_' + getIdx, 'data-toggle': '.toggle_detail_voicetype_' + getIdx });
                        getForm.find('.alert_alerttype .radio').attr({ name: 'detail_alerttype_' + getIdx, 'data-toggle': '.toggle_detail_alerttype_' + getIdx });
                        getForm.find('.toggle_beep').attr('class', 'input_area toggle_detail_voicetype_' + getIdx + ' toggle_beep hide mt10');
                        getForm.find('.toggle_voice').attr('class', 'input_area toggle_detail_voicetype_' + getIdx + ' toggle_voice mt10');
                        getForm.find('.toggle_roulette').attr('class', 'input_area toggle_detail_alerttype_' + getIdx + ' toggle_roulette hide');
                        $('#alert_detail_list').append(getForm);
                        var getTop = getForm.offset().top;
                        $('html, body')
                            .stop(true, true)
                            .animate({ scrollTop: getTop - 110 }, 200);
                        return false;
                    })
                    .on('click', '.alert_detailbox .btn_listdel', function () {
                        if ($('#alert_detail_list .detail_box').length > 1) {
                            $(this).closest('.detail_box').remove();
                        }
                        return false;
                    });

                //이미지 넓이 맞추기
                $('body')
                    .on('change', '.detail_box .check_bgimgzoom', function () {
                        if (this.checked) $(this).closest('.file_upload_box').addClass('bgimgzoom');
                        else $(this).closest('.file_upload_box').removeClass('bgimgzoom');
                        if (sub.alert.stardata.length > 0) {
                            sub.alert.stardata[0].detail.bgimgzoom = this.checked;
                            sub.alert.setupadd();
                        }
                    })
                    .on('change', '#alert_detail_list .detail_box .check_bgimgstar', function () {
                        if (this.checked) $(this).closest('.file_upload_box').addClass('bgimgdefault');
                        else $(this).closest('.file_upload_box').removeClass('bgimgdefault');
                        if (sub.alert.stardata.length > 0) {
                            sub.alert.stardata[0].detail.bgimgstar = this.checked;
                            sub.alert.setupadd();
                        }
                    })
                    .on('change', '#alert_detail_list .detail_box .input_textstar', function () {
                        if (sub.alert.stardata.length > 0) {
                            sub.alert.stardata[0].detail.textstar = this.value;
                            sub.alert.setupadd();
                        }
                    });

                //표시 효과
                $("[name='item_alert_texteffectin'], [name='item_alert_texteffectintype']").on('change', function () {
                    var texteffectin = $("[name='item_alert_texteffectin']").val();
                    var texteffectintype = $("[name='item_alert_texteffectintype']").val();
                    $('#effect_preview .alert').textillate('stop');
                    doTimeout(
                        'alert_texteffectin',
                        function () {
                            sub.alert.texteffect($('#effect_preview .alert'), texteffectin, texteffectintype);
                        },
                        100
                    );
                });

                var popupEffectInterval = null;
                function popupEffectOut() {
                    var popupeffectin = $("[name='item_alert_popupeffectin']").val();
                    var popupeffectout = $("[name='item_alert_popupeffectout']").val();
                    doTimeout(
                        'alert_popupeffect',
                        function () {
                            $('.item_alert_popup .alert_box').attr('class', 'alert_box').css('effect');
                            $('.item_alert_popup .alert_box').addClass(popupeffectin + ' on animated');
                            doTimeout(
                                'alert_popupeffectout',
                                function () {
                                    $('.item_alert_popup .alert_box').finish().removeClass(popupeffectin).addClass(popupeffectout);
                                },
                                2000
                            );
                            doTimeout(
                                'alert_popupeffectin',
                                function () {
                                    popupEffectOut();
                                },
                                4000
                            );
                        },
                        100
                    );
                }
                $("[name='item_alert_popupeffectin'], [name='item_alert_popupeffectout']").on('change', function () {
                    popupEffectOut();
                });
                popupEffectOut();

                //음성 재생
                $('.btn_voice_play').on('click', function () {
                    if (sub.alert.stardata.length <= 0) {
                        $btn = $('.lnb_area .test_area .btn_test');
                        if ($btn.length > 0) {
                            $btn.each(function () {
                                $this = $(this);
                                if ($this.data('type') === 'star') {
                                    $this.click();
                                    return false;
                                }
                            });
                        }
                    }
                    if (sub.alert.stardata.length > 0) {
                        page.tts.load(pid, { text: sub.alert.stardata[sub.alert.stardata.length - 1].detail.textstar, volume: sub.alert.loaddata.volume, rate: sub.alert.loaddata.voicerate, type: sub.alert.loaddata.voicegender });
                    }
                    return false;
                });

                setTimeout(function () {
                    $('.btn_test').trigger('click');
                }, 1000);
            } else {
                //클릭시 닫기
                sub.alert.opt.main.on('click', function () {
                    if (sub.alert.playdata != null) {
                        sub.alert.popupclose(true, false, sub.alert.playdata);
                    }
                    return false;
                });
            }

            //설정 불러오기
            $('#popup_mask').fadeIn(200);
            setTimeout(function () {
                page[pageType].load(pid, psub, function () {
                    if (pageType == 'setup') {
                        $('#alert_detail_list .detail_box .check_bgimgzoom, #alert_detail_list .detail_box .check_bgimgstar').trigger('change');
                        page.player.testmsg(0);
                    }
                });
            }, 10);
        },
        setupadd: function () {
            doTimeout(
                'alert_setupadd',
                function () {
                    if (sub.alert.stardata.length > 0) {
                        sub.alert.popup(true, sub.alert.stardata[0]);
                    }
                },
                100
            );
        },
        add: function (data) {
            //TODO 룰렛 추천 안됨
            var chatOpt = sub.alert.opt;
            var loadData = sub.alert.loaddata;
            // console.log(chatOpt, loadData, data);

            var starData = data;
            var starType = data.type;
            var starId = data.id;
            var starName = data.name;
            var starMsg = data.msg != undefined ? data.msg : '';
            var starVal = data.star != undefined ? Number(data.star) : '';
            var starChat = data.chat != undefined ? data.chat : '';
            var starImg = data.starimg != undefined ? data.starimg : '';
            var starCoinType = data.startype != undefined ? data.startype : 0;
            var getCheck = true;

            if (starType == 'chat') {
                sub.alert.chat(data);
                return;
            }

            //필터링
            chatOpt.filteridnick.lastIndex = 0;
            if (chatOpt.filteridnick !== '' && starName !== '' && chatOpt.filteridnick.test(starName)) {
                getCheck = false;
            }
            chatOpt.filteridnick.lastIndex = 0;
            if (getCheck === true && chatOpt.filteridnick !== '' && starId !== '' && chatOpt.filteridnick.test(starId)) {
                getCheck = false;
            }
            chatOpt.filterword.lastIndex = 0;
            if (getCheck === true && chatOpt.filterword !== '' && starChat !== '' && chatOpt.filterword.test(starChat)) {
                starChat = '';
            }
            if (getCheck === false) return;

            if (loadData.filtersp === true) {
                starChat = starChat.toString().replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '');
            }

            //proc 1 시작 / 2 팝업오픈 / 3 알림음 / 4 음성채팅 / 5 끝
            var getDetail = { idx: 0, proc: 1, type: starType, id: starId, name: starName, msg: starMsg, chat: starChat, gender: '', popup: { showinterval: null, endinterval: null, chatinterval: null }, time: new Date().getTime(), detail: {} };
            if ($.inArray(starType, chatOpt.typelist.star) > -1) {
                //후원
                getDetail.value = starVal;
                getDetail.starimg = starImg;
                getDetail.startype = starCoinType;
                $.each(loadData.detaildata.star, function (key, value) {
                    var getMin = !isNaN(value.minstar) && Number(value.minstar) > 0 ? Number(value.minstar) : 1;
                    var getMax = !isNaN(value.maxstar) && Number(value.maxstar) > 0 ? Number(value.maxstar) : 99999;
                    if (starVal >= getMin && starVal <= getMax) {
                        getDetail.detail = value;
                    }
                });

                if (pageType == 'setup') {
                    getDetail.proc = 4;
                    getDetail.chat = '안녕하세요 채팅메시지 입니다';
                } else {
                    if (getDetail.chat !== '') {
                        getDetail.proc = 4;
                    }
                }
            } else if ($.inArray(starType, chatOpt.typelist.normal) > -1) {
                //추천 기타
                if (loadData[starType] != undefined && loadData[starType] === true) {
                    getDetail.detail = loadData.detaildata[starType];
                }

                getDetail.proc = 4;
            }

            if (getDetail.detail != undefined && getDetail.detail.type != undefined) {
                getDetail.idx = sub.alert.staridx;
                sub.alert.staridx += 1;
                sub.alert.stardata.push(getDetail);

                if (getDetail.type == 'star' && getDetail.detail.voicechat === true && getDetail.proc === 1) {
                    sub.alert.voiceid[getDetail.idx] = getDetail;
                    var getDelay = sub.alert.delay('voice', getDetail);
                    if (getDetail.detail.alerttype !== undefined && getDetail.detail.alerttype === 'roulette') {
                        getDelay += chatOpt.time.roulettedelay;
                    }
                    getDetail.popup.chatinterval = setTimeout(function () {
                        sub.alert.chatremove(getDetail);
                    }, getDelay);
                } else {
                    getDetail.proc = 4;
                }

                if (getDetail.type == 'star' && getDetail.detail.alerttype !== undefined && getDetail.detail.alerttype === 'roulette' && getDetail.detail.roulette !== undefined && getDetail.detail.roulette.length > 0) {
                    var getSortList = getDetail.detail.roulette.sort(function (a, b) {
                        if (a[1] < b[1]) return -1;
                        if (a[1] > b[1]) return 1;
                        return 0;
                    });
                    var rouletteList = $.parseJSON(JSON.stringify(getSortList));
                    getDetail.roulettelist = [];
                    $.each(rouletteList, function (key, value) {
                        if (key > 0) {
                            value[1] = Number(value[1]) + Number(rouletteList[key - 1][1]);
                        }
                        getDetail.roulettelist.push(value[0]);
                    });

                    var rouletteTotal = rouletteList[rouletteList.length - 1][1];
                    var getResult = sub.alert.roulette.select(rouletteList, rouletteTotal);
                    if (getResult !== undefined && getResult !== null && getResult.length > 2) {
                        getDetail.roulette = getResult;
                    }
                }

                sub.alert.popup(true, getDetail);
            }
        },

        //채팅메시지
        chat: function (data) {
            var chatOpt = sub.alert.opt;
            var loadData = sub.alert.loaddata;
            var getIdx = sub.alert.playdata != null ? sub.alert.playdata.idx : -1;
            $.each(sub.alert.voiceid, function (key, value) {
                if (sub.alert.playdata != null)
                    if (value != undefined && value.id == data.id) {
                        //필터링
                        chatOpt.filterword.lastIndex = 0;
                        if (chatOpt.filterword !== '' && data.msg !== '' && chatOpt.filterword.test(data.msg)) {
                            return;
                        }

                        value.chat = data.msg;
                        if (getIdx == value.idx) {
                            if (loadData.filtersp === true) {
                                data.msg = data.msg.toString().replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '');
                            }

                            sub.alert.chatremove(sub.alert.playdata);
                            sub.alert.playdata.chat = data.msg;
                            sub.alert.opt.target.find('.td_text').append('<span class="text chat">' + data.msg + '</span>');
                            if (sub.alert.playdata.proc === 1) {
                                value.proc = 2;
                                sub.alert.playdata.proc = 2;
                            }
                            // else if(sub.alert.playdata.proc === 3) {
                            // }
                            else {
                                value.proc = 4;
                                sub.alert.playdata.proc = 4;
                                sub.alert.popup(false, sub.alert.playdata);
                            }
                        } else {
                            value.proc = 4;
                        }

                        return false;
                    }
            });
        },
        chatremove: function (data) {
            //console.log("chatremove", data, sub.alert.voiceid[data.idx], data.idx);

            clearTimeout(data.popup.chatinterval);
            if (sub.alert.voiceid[data.idx] != undefined) {
                sub.alert.voiceid[data.idx] = undefined;
                delete sub.alert.voiceid[data.idx];
            }
        },

        //팝업
        popup: function (type, data) {
            var chatOpt = sub.alert.opt;
            var loadData = sub.alert.loaddata;
            //console.log(chatOpt, loadData, type, data, sub.alert.playdata, data.proc, data.chat);

            if (pageType == 'setup') {
                sub.alert.popupclose(true, false, sub.alert.playdata);
            }

            if ((type === true && sub.alert.playdata == null) || (type === false && data != null)) {
                if (type === true) {
                    //이전 팝업 및 오디오 취소
                    page.audio.stop('alert');

                    sub.alert.playdata = data;
                    var getMsg = data.msg;
                    if (data.detail.textstar != undefined && data.detail.textstar != '' && (data.startype === undefined || data.startype.toString() === '0')) {
                        getMsg = page.common.br(sub.alert.replace(data.detail.textstar, data));
                    }
                    var getChat = '';
                    if (data.proc === 4 && data.chat !== '') {
                        getChat = '<span class="text chat">' + data.chat + '</span>';
                    }

                    sub.alert.opt.target
                        .data('idx', data.idx)
                        .empty()
                        .append(sub.alert.bg(data) + '<div class="text_box"><div class="td_text"><span class="text">' + getMsg + '</span>' + getChat + '</div></div>');
                    sub.alert.texteffect(chatOpt.main.find('.alert_box .nick, .alert_box .value'), loadData.texteffectin, loadData.texteffectintype, data);
                }
                sub.alert.popupeffect();

                //tts
                if (pageType != 'setup') {
                    var getVoiceText = data.detail.voicechat === true ? data.chat : '';
                    var getGender = loadData.voicegender;
                    if (loadData.voicelen !== '' && loadData.voicelen > 0) getVoiceText = getVoiceText.slice(0, loadData.voicelen);
                    if (type === true) {
                        var getRouletteText = '';
                        if (data.type === 'star' && data.roulette !== undefined && data.roulette[0] !== undefined && data.roulette[0] !== '') {
                            getRouletteText = ' ' + data.roulette[0];
                        }

                        if (data.detail.voicetype == 'voice') {
                            if (data.chat !== '') {
                                data.proc = 4;
                            }
                            var getMsg = sub.alert.replace(data.detail.textstar, data, false);
                            if (data.startype !== undefined && data.startype.toString() === '1') {
                                getMsg = data.msg.replace(/ *\([^)]*개\) */g, '을');
                            }
                            getVoiceText = getMsg + ' ' + getVoiceText;
                            page.tts.load('alert', { text: getVoiceText + getRouletteText, volume: loadData.volume, rate: loadData.voicerate, type: loadData.voicegender });
                        } else if (data.detail.voicetype == 'beep') {
                            data.proc = data.detail.voicechat === true ? 3 : 4;
                            page.audio.play('alert', data.detail.sound[1]);
                        } else {
                            data.proc = 4;
                            page.tts.load('alert', { text: getVoiceText + getRouletteText, volume: loadData.volume, rate: loadData.voicerate, type: loadData.voicegender });
                        }
                    } else {
                        //채팅 추가된 경우

                        page.tts.load('alert', { text: getVoiceText, volume: loadData.volume, rate: loadData.voicerate, type: loadData.voicegender });
                    }
                }

                //팝업 딜레이
                var getShowInterval = sub.alert.delay('show', data);
                var getEndInterval = sub.alert.delay('end', data);
                clearTimeout(data.popup.showinterval);
                clearTimeout(data.popup.endinterval);

                if (data.type === 'star' && data.roulette !== undefined) {
                    getShowInterval += chatOpt.time.roulettedelay;
                    getEndInterval += chatOpt.time.roulettedelay;
                }

                if (type === true && getShowInterval < getEndInterval) {
                    data.popup.showinterval = setTimeout(function () {
                        var getPopupIdx = chatOpt.target.data('idx') != null && !isNaN(chatOpt.target.data('idx')) ? Number(chatOpt.target.data('idx')) : -1;
                        if (getPopupIdx === data.idx) {
                            sub.alert.popupclose(false, true, data);
                        }
                    }, getShowInterval);
                }
                if ((type === true && getEndInterval < getShowInterval) || (type === false && getEndInterval > getShowInterval)) getEndInterval = getShowInterval;
                data.popup.endinterval = setTimeout(function () {
                    var getPopupIdx = chatOpt.target.data('idx') != null && !isNaN(chatOpt.target.data('idx')) ? Number(chatOpt.target.data('idx')) : -1;
                    if (getPopupIdx === data.idx) {
                        sub.alert.popupclose(false, false, data);
                        if (pageType == 'setup') {
                            doTimeout(
                                'alert_popup',
                                function () {
                                    sub.alert.setupadd();
                                },
                                chatOpt.time.next
                            );
                        }
                    }
                }, getEndInterval);
            }
        },

        //다음 팝업
        popupnext: function (idx) {
            var chatOpt = sub.alert.opt;
            var loadData = sub.alert.loaddata;
            var getDataLen = sub.alert.stardata.length;
            var getIdx = idx + 1;

            //console.log("popupnext", getDataLen, getIdx, sub.alert.stardata[getIdx]);

            if (sub.alert.stardata[getIdx] != undefined) {
                //다음 팝업
                sub.alert.popup(true, sub.alert.stardata[getIdx]);
            } else {
                sub.alert.voiceid = {};
                if (getDataLen > chatOpt.starmax) {
                    var getStarMax = getDataLen - chatOpt.starmax;
                    for (var i = 0; i < getStarMax; i++) {
                        sub.alert.stardata[i] = {};
                    }
                }
            }
        },

        //팝업 효과
        popupeffect: function () {
            var chatOpt = sub.alert.opt;
            var loadData = sub.alert.loaddata;

            doTimeout('alert_popup', false);
            var popupeffectin = loadData.popupeffectin;
            chatOpt.target.attr('class', 'alert_box').css('effect');
            chatOpt.target.addClass(popupeffectin + ' on animated');
        },
        texteffect: function (target, effect, type, data) {
            var chatOpt = sub.alert.opt;
            var loadData = sub.alert.loaddata;

            if (effect != 'none') {
                var getTextOption = {
                    loop: true,
                    initialDelay: 100,
                    in: { effect: effect, reverse: false, shuffle: false, sync: false, delay: 70 },
                    out: { effect: effect, reverse: false, shuffle: false, sync: false, delay: 70 },
                };
                getTextOption['in'][type] = true;
                getTextOption['out'][type] = true;
                target.textillate(getTextOption);
                target.textillate('start');
            }

            if (data !== undefined && data.type !== undefined && data.type === 'star' && data.roulette !== undefined && data.roulette.length === 3 && data.roulette[0] !== '') {
                var rouletteList = [];
                $.each(data.roulettelist, function (key, value) {
                    rouletteList.push('<span class="roulette_text text nick">' + value + '</span>');
                });
                $.each(data.roulettelist.slice(0, Number(data.roulette[2]) + 1), function (key, value) {
                    rouletteList.push('<span class="roulette_text text nick">' + value + '</span>');
                });

                var rouletteListAll = [];
                for (var i = 0; i < 50; i++) {
                    $.each(rouletteList, function (key, value) {
                        rouletteListAll.push(value);
                    });
                    if (rouletteListAll.length > 50) break;
                }

                sub.alert.opt.target.find('.td_text').append('<span id="roulette_area" class="roulette_list"><span class="roulette_text text nick blank">' + data.roulette[0] + '</span><span id="roulette_list" class="roulette_box on blur">' + rouletteListAll.join('') + '</span></span>');
                $('#roulette_list').css('opacity');

                doTimeout(
                    'alert_roulette_blur',
                    function () {
                        $('#roulette_list').removeClass('blur');
                    },
                    chatOpt.time.roulettedelay
                );
                $('#roulette_list')
                    .css({ transform: 'translateY(-100%)' })
                    .one('transitionend webkitTransitionEnd oTransitionEnd otransitionend', function () {
                        ///sub.alert.roulette.stop(data,"animation end");
                    });
            }
        },

        //팝업 닫기
        popupclose: function (type, hide, data) {
            var chatOpt = sub.alert.opt;
            var loadData = sub.alert.loaddata;
            //console.log("popupclose", type, hide, sub.alert.playdata, data);

            if (data == null || data == undefined) return;

            var getIdx = data.idx;
            var popupeffectin = loadData.popupeffectin;
            var popupeffectout = loadData.popupeffectout;
            var popupNextDelay = type == true ? 0 : chatOpt.time.next;

            //완전 취소
            if (hide === false) {
                if (data != null && data.popup != undefined) {
                    clearTimeout(data.popup.showinterval);
                    clearTimeout(data.popup.endinterval);
                    clearTimeout(data.popup.chatinterval);
                    sub.alert.chatremove(data);
                }

                if (getIdx != undefined && !isNaN(getIdx)) {
                    doTimeout(
                        'alert_popup_next',
                        function () {
                            sub.alert.playdata = null;
                            sub.alert.popupnext(getIdx);
                        },
                        popupNextDelay
                    );
                } else {
                    sub.alert.playdata = null;
                }
            }

            //즉시취소
            if (type == true) {
                page.audio.stop('alert');
                chatOpt.target.finish().removeClass('on animated').removeClass(popupeffectin);
            } else {
                chatOpt.target.finish().removeClass('on').removeClass(popupeffectin).addClass(popupeffectout);
            }
        },

        //오디오
        audio: {
            end: function () {
                if (pageType == 'setup') return;

                var chatOpt = sub.alert.opt;
                var loadData = sub.alert.loaddata;
                //console.log("audio end", loadData, sub.alert.playdata, sub.alert.stardata);

                if (sub.alert.playdata != null) {
                    var getData = sub.alert.playdata;
                    if (getData.proc === 4) {
                        //모든 음성 재생 끝
                        clearTimeout(sub.alert.playdata.showinterval);
                        clearTimeout(sub.alert.playdata.endinterval);
                        sub.alert.playdata.endinterval = setTimeout(function () {
                            var getPopupIdx = chatOpt.target.data('idx') != null && !isNaN(chatOpt.target.data('idx')) ? Number(chatOpt.target.data('idx')) : -1;
                            if (getPopupIdx === getData.idx) {
                                sub.alert.popupclose(false, false, getData);
                            }
                        }, chatOpt.time.voicenext);
                    } else if (getData.proc === 3) {
                        //알림음
                        if (getData.chat !== '') {
                            getData.proc = 4;
                            page.tts.load('alert', { text: getData.chat, volume: loadData.volume, rate: loadData.voicerate, type: loadData.voicegender });
                        }
                    } else if (getData.proc === 2) {
                        //채팅 추가된 경우
                        getData.proc = 4;
                        sub.alert.popup(false, getData);
                    } else if (getData.proc === 1) {
                        getData.proc = 2;
                    }
                }
            },
        },

        roulette: {
            index: 6,
            simulation: function (target) {
                var getRouletteList = $(target).closest('.toggle_roulette').find('.roulette_list li');
                if (getRouletteList.length === 0) return false;

                var nameArray = {};
                var rouletteArray = [];
                $.each(getRouletteList, function (key, value) {
                    var getKey = $(this).find('.roulette_name').val();
                    var getVal = $(this).find('.roulette_rate').val();
                    if (getKey !== '' && getVal !== '' && !isNaN(getVal)) {
                        getKey = ' ' + getKey;
                        rouletteArray.push([getKey, parseInt(getVal, 10)]);
                        nameArray[getKey] = $(this);
                        $(this).find('.roulette_percent').val('0% 확률');
                    }
                });
                if (rouletteArray.length === 0) return false;

                var getSortList = rouletteArray.sort(function (a, b) {
                    if (a[1] < b[1]) return -1;
                    if (a[1] > b[1]) return 1;
                    return 0;
                });
                var rouletteList = $.parseJSON(JSON.stringify(getSortList));
                $.each(rouletteList, function (key, value) {
                    if (key > 0) {
                        value[1] = Number(value[1]) + Number(rouletteList[key - 1][1]);
                    }
                });
                var rouletteTotal = rouletteList[rouletteList.length - 1][1];
                var resultList = {};
                var countTotal = 10000;

                for (var i = 0; i < countTotal; i++) {
                    var getResult = sub.alert.roulette.select(rouletteList, rouletteTotal);
                    if (getResult !== undefined && getResult !== null && getResult.length > 1) {
                        if (resultList[getResult[0]] === undefined) resultList[getResult[0]] = 1;
                        else resultList[getResult[0]] += 1;
                    }
                }

                $.each(resultList, function (key, value) {
                    var getPercent = ((value / countTotal) * 100).toFixed(2);
                    if (nameArray[key].length > 0) {
                        nameArray[key].find('.roulette_percent').val(getPercent + '% 확률');
                    }
                });
            },
            select: function (list, total) {
                var resultList = {};
                var getIndex = list.length > sub.alert.roulette.index ? sub.alert.roulette.index : list.length;
                for (var i = 0; i < getIndex; i++) {
                    var getKey = sub.alert.roulette.check(list, total);
                    var getName = list[getKey];
                    if (getName !== undefined && getName.length > 0) {
                        if (resultList[getName[0]] === undefined) resultList[getName[0]] = [1, getKey];
                        else resultList[getName[0]][0] += 1;
                    }
                }

                var sortList = [];
                $.each(resultList, function (key, value) {
                    sortList.push([key, value[0], value[1]]);
                });

                var resultSort = sortList.sort(function (a, b) {
                    if (a[1] < b[1]) return -1;
                    if (a[1] > b[1]) return 1;
                    // if (a[2] < b[2]) return -1;
                    // if (a[2] > b[2]) return 1;
                    return 0;
                });
                return resultSort[resultSort.length - 1];
            },
            check: function (list, total) {
                var rouletteRand = page.random(1, total);
                var getKey = -1;
                $.each(list, function (key, value) {
                    var getMin = 0;
                    var getMax = Number(value[1]);
                    if (key > 0) {
                        getMin = Number(list[key - 1][1]);
                    }
                    if (getMin < rouletteRand && rouletteRand <= getMax) {
                        getKey = key;
                        return false;
                    }
                });
                return getKey;
            },
        },

        //공통 명령
        bg: function (data) {
            var chatOpt = sub.alert.opt;
            var loadData = sub.alert.loaddata;
            var getBox = '';
            var getImg = data.detail.bgimgstar === true || data.detail.image[1] === '' ? page.common.img.starurl(data) : data.detail.image[1];
            var getFit = data.detail.bgimgzoom === true ? ';background-size:100% 100%!important' : '';
            getBox = '<div class="img_box"><div class="td_img" style="background-image:url(' + getImg + ')' + getFit + '"></div></div>';
            return getBox;
        },
        delay: function (type, data) {
            var chatOpt = sub.alert.opt;
            var loadData = sub.alert.loaddata;
            var getShowTime = !isNaN(loadData.showtime) ? Number(loadData.showtime) : 10;
            if ($.inArray(data.type, chatOpt.typelist.voice) > -1) {
                var getVoiceTime = !isNaN(loadData.voicetime) ? Number(loadData.voicetime) : 10;
            } else {
                var getVoiceTime = 0;
            }
            var getPopupTime = data.detail.voicechat === false || getShowTime > getVoiceTime ? getShowTime : getVoiceTime;
            if (type == 'show') return getShowTime * 1000;
            else if (type == 'voice') return getVoiceTime * 1000;
            else if (type == 'end') return getPopupTime * 1000;
        },
        replace: function (text, data, type) {
            if (type == undefined || type === true) {
                return text
                    .replace(/{닉네임}/gi, "<span class='nick'>" + data.name + '</span>')
                    .replace(/{아이디}/gi, "<span class='nick'>" + page.common.mask(data.id) + '</span>')
                    .replace(/{개수}/gi, "<span class='value'>" + page.comma(data.value) + '</span>');
            } else {
                return text.replace(/{닉네임}/gi, data.name).replace(/{아이디}/gi, page.common.mask(data.id)).replace(/{개수}/gi, data.value);
            }
        },
        reset: function () {
            var chatOpt = sub.alert.opt;

            sub.alert.stardata = [];
            sub.alert.voiceid = {};
            sub.alert.staridx = 0;
            if (sub.alert.playdata != null) {
                sub.alert.popupclose(true, false, sub.alert.playdata);
            }
            sub.alert.playdata = null;
        },
    };
});
