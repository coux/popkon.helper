sub.chat = {
    opt: {
        main: null,
        use: true, //툴팁설정
        sound: true,

        target: null,
        scroll: null,
        scrollLock: false,
        scrollCheck: false,
        effect: '',
        chatidx: 0,
        coloridx: 0,
        colordata: {},
        filteridnick: '',
        filterword: '',
        voiceplay: true,
    },
    sub: '',
    data: { opacity: '0', customstyle: false, customstylecss: '', theme: 'normal', themecss: '', effect: 'fadeIn', speed: '0.2', chatalign: 'enter', chatdirect: 'left', scroll: false, id: true, random: false, fade: true, autohide: false, autotime: '30', giftshow: 'image', joinvoice: 'none', jointext: '{닉네임}님 어서오세요', speechtype: 'none', speechtext: '{닉네임}님이 {채팅}', colorbj: '#00ade0', colorbjchat: '#ffffff', colormng: '#00ade0', colormngchat: '#ffffff', colorvip: '#00ade0', colorvipchat: '#ffffff', colorfan: '#00ade0', colorfanchat: '#ffffff', colornon: '#00ade0', colornonchat: '#ffffff', font: 'Jeju Gothic', fontbold: false, addfonturl: '', addfontname: '', fontsize: '18', shadowsize: '2', colorshadow: 'rgba(0, 0, 0, 0.8)', bgtrans: 'none', colorbg: '#000000', bgimgzoom: false, filteridnick: [], filterword: [], image: {}, speechsound: ['', ''], colorjoin: '#ffc81f', colorgift: '#00dc5d', colorfanchat: '#ffffff', bj: true, mng: true, fan: true },
    head:
        '.item_chat .chat_list li{-webkit-animation-duration:0.2s;-moz-animation-duration:0.2s;-o-animation-duration:0.2s;-ms-animation-duration:0.2s;animation-duration:0.2s}.item_chat .chat_list li{-webkit-transition-duration:0.2s;-moz-transition-duration:0.2s;-o-transition-duration:0.2s;-ms-transition-duration:0.2s;transition-duration:0.2s}.item_chat li.grade1 p.nick{color:#00ade0}.item_chat li.grade1 p.text{color:#ffffff}.item_chat li.grade2 p.nick{color:#00ade0}.item_chat li.grade2 p.text{color:#ffffff}.item_chat li.level0:not(.grade1):not(.grade2) p.nick{color:#00ade0}.item_chat li.level0:not(.grade1):not(.grade2) p.text{color:#ffffff}.item_chat li.level1:not(.grade1):not(.grade2) .nick{color:#00ade0}.item_chat li.level2:not(.grade1):not(.grade2) .nick{color:#00ade0}.item_chat li.level3:not(.grade1):not(.grade2) .nick{color:#00ade0}.item_chat li.level4:not(.grade1):not(.grade2) .nick{color:#00ade0}.item_chat li .nick{color:#00ade0}.item_chat li .text{color:#ffffff}.item_chat .chat_list p,.text_preview .item_chat{text-shadow:-1px -1px 1px rgba(0,0,0,0.8),-1px -0.5px 1px rgba(0,0,0,0.8),-1px 0px 1px rgba(0,0,0,0.8),-1px 0.5px 1px rgba(0,0,0,0.8),-1px 1px 1px rgba(0,0,0,0.8),-0.5px -1px 1px rgba(0,0,0,0.8),-0.5px -0.5px 1px rgba(0,0,0,0.8),-0.5px 0px 1px rgba(0,0,0,0.8),-0.5px 0.5px 1px rgba(0,0,0,0.8),-0.5px 1px 1px rgba(0,0,0,0.8),0px -1px 1px rgba(0,0,0,0.8),0px -0.5px 1px rgba(0,0,0,0.8),0px 0px 0px rgba(0,0,0,0.8),0px 0.5px 0px rgba(0,0,0,0.8),0px 1px 0px rgba(0,0,0,0.8),0.5px -1px 1px rgba(0,0,0,0.8),0.5px -0.5px 1px rgba(0,0,0,0.8),0.5px 0px 0.5px rgba(0,0,0,0.8),0.5px 0.5px 0.5px rgba(0,0,0,0.8),0.5px 1px 0.5px rgba(0,0,0,0.8),1px -1px 1px rgba(0,0,0,0.8),1px -0.5px 1px rgba(0,0,0,0.8),1px 0px 1px rgba(0,0,0,0.8),1px 0.5px 1px rgba(0,0,0,0.8),1px 1px 1px rgba(0,0,0,0.8);-webkit-font-smoothing: antialiased;}.item_chat .chat_list p,.text_preview .item_chat{text-shadow:-1px -1px 1px rgba(0, 0, 0, 0.8),-1px -0.5px 1px rgba(0, 0, 0, 0.8),-1px 0px 1px rgba(0, 0, 0, 0.8),-1px 0.5px 1px rgba(0, 0, 0, 0.8),-1px 1px 1px rgba(0, 0, 0, 0.8),-0.5px -1px 1px rgba(0, 0, 0, 0.8),-0.5px -0.5px 1px rgba(0, 0, 0, 0.8),-0.5px 0px 1px rgba(0, 0, 0, 0.8),-0.5px 0.5px 1px rgba(0, 0, 0, 0.8),-0.5px 1px 1px rgba(0, 0, 0, 0.8),0px -1px 1px rgba(0, 0, 0, 0.8),0px -0.5px 1px rgba(0, 0, 0, 0.8),0px 0px 0px rgba(0, 0, 0, 0.8),0px 0.5px 0px rgba(0, 0, 0, 0.8),0px 1px 0px rgba(0, 0, 0, 0.8),0.5px -1px 1px rgba(0, 0, 0, 0.8),0.5px -0.5px 1px rgba(0, 0, 0, 0.8),0.5px 0px 0.5px rgba(0, 0, 0, 0.8),0.5px 0.5px 0.5px rgba(0, 0, 0, 0.8),0.5px 1px 0.5px rgba(0, 0, 0, 0.8),1px -1px 1px rgba(0, 0, 0, 0.8),1px -0.5px 1px rgba(0, 0, 0, 0.8),1px 0px 1px rgba(0, 0, 0, 0.8),1px 0.5px 1px rgba(0, 0, 0, 0.8),1px 1px 1px rgba(0, 0, 0, 0.8);-webkit-font-smoothing: antialiased;}.item_chat.bgtrans_color{background-color:#000000}.item_chat.item_chat_bgimgzoom{background-size:100% 100%!important}.item_chat .chat_list li.up .text{color:#ffc81f!important}.item_chat .chat_list li.join .text{color:#ffc81f!important}.item_chat .chat_list li.star .text{color:#00dc5d!important}.item_chat li.level1:not(.grade1):not(.grade2) .text{color:#ffffff}.item_chat li.level2:not(.grade1):not(.grade2) .text{color:#ffffff}.item_chat li.level3:not(.grade1):not(.grade2) .text{color:#ffffff}.item_chat li.level4:not(.grade1):not(.grade2) .text{color:#ffffff}',
    cls: 'item_box item_chat theme_normal effect_fadeIn chatalign_enter chatdirect_left item_chat_id item_chat_fade giftshow_image joinvoice_none speechtype_none bgtrans_none item_chat_bj item_chat_mng item_chat_fan',
    style: 'opacity: 1; font-size: 18px; font-family: "Jeju Gothic", 돋움;',
    loaddata: {},
    theme: {
        normal: {
            effect: 'fadeIn',
            speed: '0.2',
            chatalign: 'enter',
            random: false,
            colorbj: '#00ade0',
            colorbjchat: '#ffffff',
            colormng: '#00ade0',
            colormngchat: '#ffffff',
            colorvip: '#00ade0',
            colorvipchat: '#ffffff',
            colorfan: '#00ade0',
            colorfanchat: '#ffffff',
            colornon: '#00ade0',
            colornonchat: '#ffffff',
            colorgift: '00dc5d',
            colorjoin: 'ffc81f',
            shadowsize: '2',
            colorshadow: 'rgba(0, 0, 0, 0.8)',
            bgtrans: 'none',
            image: {},
            bgimgzoom: false,
        },
        kakaotalk: {
            effect: 'fadeIn',
            speed: '0.2',
            chatalign: 'normal',
            random: false,
            colorbj: '#000000',
            colorbjchat: '#000000',
            colormng: '#000000',
            colormngchat: '#000000',
            colorvip: '#000000',
            colorvipchat: '#000000',
            colorfan: '#000000',
            colorfanchat: '#000000',
            colornon: '#000000',
            colornonchat: '#000000',
            colorgift: '00dc5d',
            colorjoin: 'ffc81f',
            shadowsize: '2',
            colorshadow: 'rgba(255, 255, 255, 0.15)',
            bgtrans: 'none',
            image: {},
            bgimgzoom: false,
        },
        board: {
            effect: 'fadeIn',
            speed: '0.2',
            chatalign: 'enter',
            random: true,
            colorbj: '#00ade0',
            colorbjchat: '#ffffff',
            colormng: '#00ade0',
            colormngchat: '#ffffff',
            colorvip: '#00ade0',
            colorvipchat: '#ffffff',
            colorfan: '#00ade0',
            colorfanchat: '#ffffff',
            colornon: '#00ade0',
            colornonchat: '#ffffff',
            colorgift: '00dc5d',
            colorjoin: 'ffc81f',
            shadowsize: '2',
            colorshadow: 'rgba(0, 0, 0, 0.8)',
            bgtrans: 'image',
            image: { bgimg: ['board.png', '/data/media/img/board.png'] },
            bgimgzoom: true,
        },
        bubble: {
            effect: 'none',
            speed: '0.4',
            chatalign: 'enter',
            random: true,
            colorbj: '#00ade0',
            colorbjchat: '#ffffff',
            colormng: '#00ade0',
            colormngchat: '#ffffff',
            colorvip: '#00ade0',
            colorvipchat: '#ffffff',
            colorfan: '#00ade0',
            colorfanchat: '#ffffff',
            colornon: '#00ade0',
            colornonchat: '#ffffff',
            colorgift: '00dc5d',
            colorjoin: 'ffc81f',
            shadowsize: '2',
            colorshadow: 'rgba(0, 0, 0, 0.8)',
            bgtrans: 'none',
            image: {},
            bgimgzoom: false,
        },
        neon: {
            effect: 'fadeIn',
            speed: '0.2',
            chatalign: 'enter',
            random: true,
            colorbj: '#00ade0',
            colorbjchat: '#ffffff',
            colormng: '#00ade0',
            colormngchat: '#ffffff',
            colorvip: '#00ade0',
            colorvipchat: '#ffffff',
            colorfan: '#00ade0',
            colorfanchat: '#ffffff',
            colornon: '#00ade0',
            colornonchat: '#ffffff',
            colorgift: '00dc5d',
            colorjoin: 'ffc81f',
            shadowsize: '2',
            colorshadow: 'rgba(0, 0, 0, 0.8)',
            bgtrans: 'none',
            image: {},
            bgimgzoom: false,
        },
    },
    voicedata: [],
    init: function (pid, psub) {
        sub.chat.opt.main = $('.item_chat');
        sub.chat.opt.target = sub.chat.opt.main.find('.chat_list');
        sub.chat.opt.scroll = sub.chat.opt.main.find('.item_area').get(0);
        sub.chat.sub = psub;

        if (pageType == 'setup') {
            //음성 재생
            $('.btn_voice_play').on('click', function () {
                var getTarget = $("[name='" + $(this).data('target') + "']");
                page.tts.load(pid, { text: getTarget.val() });
                return false;
            });
        }

        //스크롤
        sub.chat.opt.main
            .on('mouseenter', function () {
                if (sub.chat.loaddata.scroll === true) {
                    sub.chat.opt.scrollCheck = true;
                    sub.chat.opt.scroll.scrollTop = sub.chat.opt.scroll.scrollHeight + 100;
                }
            })
            .on('mouseleave', function () {
                if (sub.chat.loaddata.scroll === true) {
                    sub.chat.opt.scrollCheck = false;
                }
            });

        $(sub.chat.opt.scroll).on('scroll', function () {
            if (sub.chat.loaddata.scroll === true) {
                var getScrollTop = sub.chat.opt.scroll.scrollTop;
                var getAreaHeight = $(sub.chat.opt.scroll).outerHeight();
                var getScrollHeight = sub.chat.opt.scroll.scrollHeight;
                if (sub.chat.opt.scrollCheck === true && getScrollTop + getAreaHeight < getScrollHeight - 40) {
                    sub.chat.opt.scrollLock = true;
                    doTimeout(
                        'scroll_lock',
                        function () {
                            sub.chat.opt.scrollLock = false;
                            sub.chat.opt.scroll.scrollTop = sub.chat.opt.scroll.scrollHeight + 100;
                        },
                        10000
                    );
                } else {
                    sub.chat.opt.scrollLock = false;
                    doTimeout('scroll_lock', false);
                }
            }
        });

        //채팅 최대수 삭제
        setInterval(function () {
            var getChatList = sub.chat.opt.target.find('li');
            var chatLen = getChatList.length;
            var chatMax = sub.chat.loaddata.scroll === true ? 300 : 100;
            if (chatLen > chatMax) {
                sub.chat.opt.target
                    .find('li')
                    .slice(0, chatLen - chatMax)
                    .remove();
            }
        }, 5000);

        //설정 불러오기
        $('#popup_mask').fadeIn(200);
        setTimeout(function () {
            page[pageType].load(pid, psub, function () {
                if (pageType == 'setup') {
                    //테마설정
                    $("[name='item_chat_theme']").on('change', function () {
                        page.setup.style.theme(this.value);
                    });

                    sub.chat.test();
                }
            });
        }, 10);
    },
    test: function () {
        //임시 데이터 설정
        var chatData = popkonData;
        var idx = 0;
        setInterval(function () {
            if (chatData[idx] == undefined) idx = 0;
            page.popkon(chatData[idx], false);
            idx += 1;
        }, 1000);
    },
    add: function (data) {
        if (pageType == 'player' && loginInfo !== undefined && loginInfo.id !== undefined && loginInfo.id === 'qatesting1') {
            var debugData = {
                step: 'c',
                data: data,
            };

            $.ajax({
                url: '/lib/log.php',
                data: {
                    type: 'log',
                    name: 'debug_chat_step2',
                    data: debugData,
                },
            });
        }

        var chatOpt = sub.chat.opt;
        var loadData = sub.chat.loaddata;
        var chatData = data;
        var chatType = data.type;
        var chatId = data.id;
        var chatName = data.name;
        var originalChatMsg = data.msg != undefined ? data.msg : '';
        var chatGrade = data.grade;
        var chatLevel = data.level;
        var chatSex = data.sex;
        var chatGradeImg = data.gradeimg;
        var getCheck = true;

        if (chatType == 'join' && (loadData.joinvoice == 'none' || loadData.jointext === '')) {
            getCheck = false;
        }
        if (chatType == 'star' && loadData.giftshow == 'none') {
            getCheck = false;
        }

        //필터링
        if (getCheck === true) {
            chatOpt.filteridnick.lastIndex = 0;
            if (chatOpt.filteridnick !== '' && chatName !== '' && chatOpt.filteridnick.test(chatName)) {
                getCheck = false;
            }
            chatOpt.filteridnick.lastIndex = 0;
            if (getCheck === true && chatOpt.filteridnick !== '' && chatId !== '' && chatOpt.filteridnick.test(chatId)) {
                getCheck = false;
            }
            chatOpt.filterword.lastIndex = 0;
            if (getCheck === true && chatOpt.filterword !== '' && originalChatMsg !== '' && chatOpt.filterword.test(originalChatMsg)) {
                getCheck = false;
            }
        }
        if (getCheck === false) return;

        //등급 아이콘
        var getGradeImg = page.common.img.grade(chatData, loadData);
        var getNick = '<p class="nick">' + getGradeImg + '<span class="name">' + chatName + '</span><span class="id">(' + page.common.mask(chatId) + ')</span></p>';
        var getText = '';
        var getUniqId = chatId != '' ? chatId : chatName;
        var getRandom = '';

        var chatMsg = originalChatMsg;

        if (pageType == 'player' && loginInfo !== undefined && loginInfo.id !== undefined && loginInfo.id === 'qatesting1') {
            var debugData = {
                step: 'd',
                data: chatMsg,
            };

            $.ajax({
                url: '/lib/log.php',
                data: {
                    type: 'log',
                    name: 'debug_chat_step2',
                    data: debugData,
                },
            });
        }

        // chatMsg = chatMsg.split(" ").join("&nbsp;");
        chatMsg = chatMsg.replace(/\n/gi, '<br />');
        chatMsg = page.common.emoji(chatMsg);

        if (chatType == 'star') {
            if (loadData.giftshow == 'text') {
                getText = '<p class="text">' + chatMsg + '</p>';
            } else {
                getText = '<div class="gift_img">' + page.common.img.star(chatData) + '</div><p class="text">' + chatMsg + '</p>';
            }
        } else {
            if (chatType == 'chat') {
                getText = getNick + '<p class="text">' + chatMsg + '</p>';

                //채팅 랜덤 색
                if (loadData.random === true) {
                    if (chatOpt.colordata[getUniqId] == undefined) {
                        getRandom = 'color_' + chatOpt.coloridx;
                        chatOpt.colordata[getUniqId] = chatOpt.coloridx;
                        chatOpt.coloridx += 1;
                        if (chatOpt.coloridx > 6) chatOpt.coloridx = 0;
                    } else {
                        getRandom = 'color_' + chatOpt.colordata[getUniqId];
                    }
                }

                //채팅 음성
                if (pageType == 'player' && chatMsg !== '' && (loadData.speechtype == 'voice' || loadData.speechtype == 'sound')) {
                    if (loadData.speechtype == 'voice') {
                        var getVoiceText = loadData.speechtext.replace(/{닉네임}/g, chatName).replace(/{채팅}/g, chatMsg);
                        sub.chat.voicedata.push(getVoiceText);
                        sub.chat.audio.voice();
                    } else if (loadData.speechtype == 'sound' && loadData.speechsound[1] !== '') {
                        page.audio.play('chat', loadData.speechsound[1]);
                    }
                } else if (loadData.speechtype == 'none') {
                    sub.chat.voicedata = [];
                    sub.chat.voiceplay = true;
                }
            } else if (chatType == 'join') {
                chatMsg = loadData.jointext.replace(/{닉네임}/g, chatName).replace(/{아이디}/g, chatId);
                getText = '<p class="text">' + chatMsg + '</p>';

                //채팅 음성
                if (pageType == 'player' && chatMsg !== '' && loadData.joinvoice == 'voice') {
                    page.tts.load('chat', { text: chatMsg });
                }
            } else {
                getText = '<p class="text">' + chatMsg + '</p>';
            }
        }

        if (pageType == 'player' && loginInfo !== undefined && loginInfo.id !== undefined && loginInfo.id === 'qatesting1') {
            var debugData = {
                step: 'e',
                data: getText,
            };

            $.ajax({
                url: '/lib/log.php',
                data: {
                    type: 'log',
                    name: 'debug_chat_step2',
                    data: debugData,
                },
            });
        }

        //채팅 추가
        var getChat = $('<li data-id="' + chatId + '" data-name="' + chatName + '" class="' + chatType + ' grade' + chatGrade + ' level' + chatLevel + ' ' + chatSex + ' ' + getRandom + '">' + getText + '</li>');
        chatOpt.target.append(getChat);

        if (pageType == 'player' && loginInfo !== undefined && loginInfo.id !== undefined && loginInfo.id === 'qatesting1') {
            var debugData = {
                step: 'f',
                data: getChat.html(),
            };

            $.ajax({
                url: '/lib/log.php',
                data: {
                    type: 'log',
                    name: 'debug_chat_step2',
                    data: debugData,
                },
            });
        }

        //랜덤 채팅 번호 매기기
        getChat.addClass('ran_' + page.random(0, 1));

        if (loadData.theme == 'bubble') {
            getChat
                .wrapInner("<div class='cloud_box'></div")
                .addClass('cloudcolor_' + (chatOpt.chatidx % 2))
                .addClass('cloudanimate_' + page.random(0, 2));
            chatOpt.chatidx += 1;
            getChat.css('opacity');
            getChat.addClass('cloudactive');
        }

        //스크롤
        if (loadData.scroll && chatOpt.scrollLock === false) {
            chatOpt.scroll.scrollTop = chatOpt.scroll.scrollHeight + 100;
        }

        //채팅 표시 효과
        if (loadData.effect != 'none') {
            getChat.css('effect');
            getChat.addClass(loadData.effect + ' animated');
        }

        //채팅 자동 숨김
        if (loadData.autohide === true) {
            setTimeout(function () {
                if (loadData.effect != 'none') {
                    getChat.removeClass(loadData.effect);
                    getChat.removeClass('animated');
                    getChat.removeClass('cloudactive');
                }

                setTimeout(function () {
                    getChat.one('transitionend webkitTransitionEnd oTransitionEnd otransitionend MSTransitionEnd webkitAnimationEnd oanimationend msAnimationEnd animationend', function () {
                        getChat.remove();
                        getChat = null;
                    });

                    getChat.addClass('autohide');
                    getChat.css('effect');
                    getChat.addClass('autohiderun');
                }, 0);
            }, Number(loadData.autotime) * 1000);
        }
    },
    reset: function () {
        var chatOpt = sub.chat.opt;
        chatOpt.target.empty();
    },

    //오디오
    audio: {
        voice: function () {
            var chatOpt = sub.chat.opt;
            var loadData = sub.chat.loaddata;
            if (chatOpt.voiceplay === true && loadData.speechtype == 'voice' && sub.chat.voicedata.length > 0) {
                chatOpt.voiceplay = false;
                page.tts.load('chat', { text: sub.chat.voicedata[0] });
            }
        },
        end: function () {
            if (pageType == 'setup') return;
            var chatOpt = sub.chat.opt;
            var loadData = sub.chat.loaddata;

            if (sub.chat.voicedata.length > 0) {
                chatOpt.voiceplay = true;
                sub.chat.voicedata.shift();
                sub.chat.audio.voice();
            }
        },
    },
};
