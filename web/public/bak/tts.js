//tts 설정
const fs = require('fs');
const projectId='atomic-quasar-271509';
const keyfile='./popkontts-de658d3013d8.json';
const textToSpeech = require('@google-cloud/text-to-speech');
const client = new textToSpeech.TextToSpeechClient({
    projectId:projectId,
    keyFilename:keyfile
});

//서버
var express = require('express'),
    fileSystem = require('fs'),
    path = require('path');

var app = express();
app.get("/", function(req, res) {
    console.log(req.query);
    var result = "";
    
    console.log("a");
    if(req.query !== undefined) {
        if(req.query.type === "tts") {
            console.log("b");
            result = tts(req.query.text, "");
        }
    }
    console.log("e");
    res.send(result);
});
app.listen(8002);

async function tts(text, gender) {
    const request = {
        input: {text: text},
        voice: {languageCode: 'ko-KR', ssmlGender: "FEMALE"},
        audioConfig: {speaking_rate:1, audioEncoding: 'MP3'},
    };
    var [audio] = await client.synthesizeSpeech(request);
    console.log(audio.audioContent);
    return audio.audioContent;
}
