<?php
	include "/home/fullhp/public_html/lib/common.php";
	
	$get_code = isset($_GET["code"]) ? $_GET["code"] : "";
	$get_type = isset($_GET["type"]) ? $_GET["type"] : "";
	$get_state = isset($_GET["state"]) ? $_GET["state"] : "full";
	
	if($get_code != "") {
		$user_data = new loginInfo();
		$get_data = $user_data->accesstoken($get_code, $fullhp_clientId, $fullhp_clientSecret, $get_state);
	
		if(isset($get_data->access_token)) {
			$get_user = $user_data->userinfo($get_data->access_token, $get_state);
			if(isset($get_user->id)) {
				$sql_data = new stdClass;
				$sql_data->id = $get_user->id;
				$sql_data->name = $get_user->nick;
				$sql_data->profile = $get_user->imgProfile;
				$sql_data->token = $get_data->access_token;
				$sql_data->refresh = $get_data->refresh_token;
				$sql_data->expires_in = $get_data->expires_in;
				$sql_data->tokenEndDateTime = $get_data->tokenEndDateTime;
				$sql_data->state = $get_state;
				$get_idx = $user_data->dbsave($sql_data);
				if(isset($get_idx->result)) {
					$get_user->expTime = time() + $memtime->userinfo;
					$get_user->token = $get_data->access_token;
					$get_user->refresh = $get_data->refresh_token;
					$get_user->expires_in = $get_data->expires_in;
					$get_user->tokenEndDateTime = $get_data->tokenEndDateTime;
					$get_user->state = $get_state;

					$mem = new Memcached();
					$mem->addServer($mem_ip, $mem_port);
					$mem->set("userinfo_".$get_user->id, json_encode($get_user), $get_user->expTime);

					$user_key = new userKey($fullhp_keycode);
					$page_key = $user_key->encrypt($get_user->id);
					$_SESSION["page_key"] = $page_key;
					header("Location: /chat");  //기본 페이지 나중에 대쉬보드 등 설정
				}
				else {
					//echo "error db";
					header("Location: /#logindb");
				}
			}
			else {
				header("Location: /#loginid");
				// echo "error 아이디";
				//print_r($get_result);
				// print_r(curl_getinfo($ch));
				// print_r(curl_errno($ch));
			}
		}
		else {
			 header("Location: /#logintoken");
			 //echo "error 토큰";
			 //print_r($get_result);
			 //print_r(curl_getinfo($ch));
			 //print_r(curl_errno($ch));
		}
		die();
	} else if($get_type == "logout") {
		if(isset($_SESSION["page_key"])) {
			$_SESSION["page_key"] = "";
		}
		session_unset();
		session_destroy();
		header("Location: /#logout");
		die();
	}