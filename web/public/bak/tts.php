<?php
    $get_q = isset($_GET["q"]) ? trim($_GET["q"]) : "";
    if($get_q == "") {
        header("HTTP/1.0 404 Not Found");
        exit;
    }
	
	if(empty($_SERVER['HTTP_REFERER'])) {
        header("HTTP/1.0 404 Not Found");
        exit;
	}
	$parsedUrlHost = parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST);
	if($parsedUrlHost === false || !(stripos($parsedUrlHost, $_SERVER['HTTP_HOST']) === 0 || stripos($parsedUrlHost, 'www.'.$_SERVER['HTTP_HOST']) === 0)) {
        header("HTTP/1.0 404 Not Found");
        exit;
	}

    header("Access-Control-Allow-Origin: *");
    header('Content-Type: audio/mpeg');

    $get_q = urlencode($get_q);
    $url = "http://translate.google.com/translate_tts?ie=UTF-8&total=1&idx=0&textlen=0&client=tw-ob&tl=ko&q=".$get_q;
    
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 3);
    $result = curl_exec($curl);
    curl_close ($curl);
    echo $result;