<?php
    header("Access-Control-Allow-Origin: *");
	header("Access-Control-Allow-Credentials: true");
	header("Access-Control-Allow-Methods: GET, POST");
	header("Access-Control-Allow-Headers: X-Requested-With");
	header("Content-Type:application/json; charset=utf-8");
	
	include_once "../../lib/function.php";
	require 'vendor/autoload.php';
			
	use Aws\S3\S3Client;
	use Aws\Exception\AwsException;
	
	$result = new stdClass();
	$result->result = "";
	
	if(isset($_POST["type"])) {
		$get_type = isset($_POST["type"]) ? $_POST["type"] : "";
		$get_key = isset($_POST["key"]) ? $_POST["key"] : "";
		$get_idx = isset($_POST["idx"]) ? $_POST["idx"] : "";
	}
	else {
		$get_type = isset($_GET["type"]) ? $_GET["type"] : "";
		$get_key = isset($_GET["key"]) ? $_GET["key"] : "";
		$get_idx = isset($_GET["idx"]) ? $_GET["idx"] : "";
	}
		
	if($get_type === "upload") {
		if(!empty($get_idx) && isset($_FILES["popup_file"])) {
			$extimg_array = array("gif", "png", "jpg", "jpeg", "webm");
			$extaudio_array = array("mp3", "mp4", "wav", "ogg", "aac");
			$file_maxsize = 10;
			$real_idx = "";
			if(!empty($get_idx)) {
	            $real_idx = decrypt($get_idx, $base64_keycode);
	        }
			$filename = $_FILES["popup_file"]["tmp_name"];
			$upload_check = false;
			
			if(isset($filename) && !empty($filename)) {
				$files = $_FILES["popup_file"]["name"];
				$filesize = $_FILES["popup_file"]["size"];
				$filepath = strtolower(pathinfo($files, PATHINFO_EXTENSION));
				
				if(array_search($filepath, $extimg_array) > -1 || array_search($filepath, $extaudio_array) > -1) {
					$file_size = intval($filesize) / 1024 / 1024;
	                if($file_size > $file_maxsize) {
	                    $result->result = "nosize";
	                }
					else {
						$save_id = uniqid();
						$file_mime = array_search($filepath, $extimg_array) > -1 ? "img" : "audio";
						$upload_folder = $s3_config->media->$file_mime.$real_idx;
						
						$fullpath = $upload_folder."/".$file_mime."/".$save_id.".".$filepath;
	
						try {
							$s3client = S3Client::factory(
								array(
									'credentials' => array(
										'key' => $s3_config->accesskey,
										'secret' => $s3_config->secretkey
									),
									'version' => 'latest',
									'region'  => 'us-east-1',
									'endpoint' => $s3_config->endpoint,
									'bucket_name' => $s3_config->name
								)
							);
			
							$upload_result = $s3client->putObject([
						        'Bucket' => $s3_config->name,
						        'Key'     => $fullpath,
						        'SourceFile' => $filename,
						        //'Body'   => $s3_data,
						        'ACL'    => "public-read"
						    ]);
							
							$result->mime = $file_mime;
							$result->url = $fullpath;
							//$result->link = $s3client->getObjectUrl($s3_config->name, $fullpath);
							//$result->url2 = $s3_config->url."/".$fullpath;
							//$result->url3 = $upload_result["ObjectURL"];
		                    $result->name = $files;
		                    $result->size = $_FILES["popup_file"]["size"];
							$result->id = $save_id;
		                    $result->key = $fullpath;
		                    $result->deletehash = "";
							$result->ver = "s3";
		                    $upload_check = true;
						}
						catch(Exception $error) {
							$result->result = "noupload";
				            $result->error = $error;
				        }
	                }
				}
				else {
					$result->result = "noext";
				}
			}
			else {
				$result->result = "nofile";
			}
		}
		else {
			$result->result = "nofile";
		}

		if($upload_check === true) {
			if(isset($result->url) && isset($result->name)) {
				//save
				$save_id = $result->id;
				$save_file = "../../data/file/".$get_key.".json";
				$save_list = new stdClass;
				if(file_exists($save_file)) {
	                $save_data = file_get_contents($save_file);
	                if($save_data != "") {
	                    $save_list = json_decode($save_data);
	                }
	            }
				
				if(!isset($save_list->$save_id)) {
	                $save_list->$save_id = new stdClass;
	            }
				
				$save_list->$save_id->mime = $result->mime;
	            $save_list->$save_id->url = $result->url;
	            $save_list->$save_id->name = $result->name;
	            $save_list->$save_id->size = $result->size;
				$save_list->$save_id->key = $result->key;
	            $save_list->$save_id->deletehash = $result->deletehash;
				$save_list->$save_id->ver = $result->ver;
	            $save_list->$save_id->time = date('YmdHis');
	            
	            file_put_contents($save_file, json_encode($save_list));
	            $result->list = $save_list;
			}
			else {
				$result->result = "nourl";
			}
		}
	}
	else if($get_type === "list") {
		if(!empty($get_key)) {
			if($get_key === "default") {
				$save_file = "default.json";
			}
			else {
				$save_file = "../../data/file/".$get_key.".json";
			}
			$result->list = new stdClass();
	        if(file_exists($save_file)) {
	            $result->result = "list";
	            $result->list = json_decode(file_get_contents($save_file));
	        }
		}
	}
	else if($get_type === "delete") {
		if(!empty($get_key)) {
			$save_file = "../../data/file/".$get_key.".json";
	        $get_id = isset($_POST["id"]) ? $_POST["id"] : "";
	        $result->list = new stdClass();
	        if(file_exists($save_file)) {
	        	$result->debug = 1;
	            $result->list = json_decode(file_get_contents($save_file));
				$result->debug = $get_id;
	            if(isset($result->list->$get_id)) {
	            	$result->debug = 2;
	            	$delete_file = $result->list->$get_id;
					$get_ver = $delete_file->ver;
					$get_mime = $delete_file->mime;
					$get_url = $delete_file->url;
					$delete_check = false;
					
					if(isset($get_ver) && $get_ver === "s3") {
						$result->debug = 3;
						try{
							if(isset($delete_file->key) && !empty($delete_file->key)) {
								$result->debug = 4;
								$s3client = S3Client::factory(
									array(
										'credentials' => array(
											'key' => $s3_config->accesskey,
											'secret' => $s3_config->secretkey
										),
										'version' => 'latest',
										'region'  => 'us-east-1',
										'endpoint' => $s3_config->endpoint,
										'bucket_name' => $s3_config->name
									)
								);
							
								$delete_result = $s3client->deleteObject([
							        'Bucket' => $s3_config->name,
							        'Key'     => $delete_file->key
								]);
								$delete_check = true;
							}
						}
						catch(Exception $error) {
							$result->result = "nodelete";
				            $result->error = $error;
				        }
					}
					else {
						$file_path = "";
						if(strpos($get_url, "voice/upload/") !== false) {
							$file_path = $get_url;
						}
						else {
							$file_path = "voice/upload/".$get_mime."/".$get_key."/".$get_name;
						}
						if(!empty($file_path) && file_exists($file_path)) {
							unlink($file_path);
							$delete_check = true;
						}
					}
					
					if($delete_check === true) {
						$result->debug = 6;
						$result->result = "delete";
		                unset($result->list->$get_id);
		                file_put_contents($save_file, json_encode($result->list));
					}
	            }
	        }
		}
	}

	echo json_encode($result);
?>