<div id="reHeader">
    <!-- header inner wrap -->
    <div class="header_inner">
        <!-- logo gnb search -->
        <div class="clfix gnb_main_area">
            <!-- 로고 -->
            <h1 class="re_logo">
                
                <a href="https://www.popkontv.com/" class="popkon_logo"><img src="https://pic.popkontv.com/images/www/images/popkon/renewal/popkon_logo.png" alt="POPKON TV"></a>
            </h1>
            <!-- // 로고 -->
            <!-- 네비게이션 -->
            <div class="re_gnb">
                <ul class="clfix">
                    <li><a href="https://www.popkontv.com/live/">Live</a></li>
                    <li><a href="https://www.popkontv.com/vod/vodContent.asp">VOD</a></li>
                    <li><a href="https://www.popkontv.com/store/">P Store</a></li>
                    <!--<li><a href="https://www.popkontv.com/Magazine/">Magazine</a></li>-->
                    <li><a href="https://www.popkontv.com/news/">News</a></li>
                    <li><a href="https://www.popkontv.com/vipmall/">VIP MALL</a></li>
                </ul>
            </div>
            <!-- // 네비게이션 -->

            <div class="login_group">
                <div class="user_btn_group">
                    <a href="https://www.popkontv.com/coin/" class="user_btn_item btn_link_popkon" title="팝콘충전">팝콘충전</a>
                    <a href="https://www.popkontv.com/item/item_fullView.asp" class="user_btn_item btn_link_item" title="아이템 구매">아이템구매</a>
                </div>
            </div>

            <!-- 검색 -->
            <div id="searchBox" class="re_search">
                <form id="popkonSearch" name="popkonSearch" action="https://www.popkontv.com/search/search_all.asp" method="post" onsubmit="return fnsearch();">
                <input type="hidden" name="d" value="123456789123">
                    <fieldset>
                        <legend class="blind">검색어 입력폼</legend>
                        <div class="box_searchbar">
                            <input type="text" class="tf_keyword js_search_string imeHan" id="qTop" name="q" value="" title="검색어 입력" placeholder="검색어를 입력하세요" maxlength="20">
                            <button id="popkonBtnSearch" class="ico_search btn_search" type="submit"><i class="fa fa-search" aria-hidden="true"></i><span class="ir_wa">검색</span></button>
                        </div>
                    </fieldset>
                </form>
                <script type="text/javascript">
                    function fnsearch(){
                        if($.trim($(".js_search_string").val()).length<2){
                            alert("검색어를 2자 이상 입력하세요");
                            $(".js_search_string").focus();
                            return false;
                        }
                        return true;
                    }
                </script>
            </div>
            <!-- // 검색 -->
        </div>
    </div>
    <!-- //header inner wrap -->
</div>


<div id="header">
    <div class="header_bottom">
        <div class="contains">
            <ul class="lnb help">
                <li><a href="https://www.popkontv.com/news/" class="menu"><i class="fa fa-bullhorn" aria-hidden="true"></i>업데이트</a></li>
                <li><a href="https://www.popkontv.com/news/" class="menu"><i class="fa fa-book" aria-hidden="true"></i>도움말</a></li>
            </ul>
            <ul class="lnb menu">
                <li><a href="/chat" class="menu <?=$page_id == "chat" ? "active" : ""?>">채팅</a></li>
                <li><a href="/alert" class="menu <?=$page_id == "alert" ? "active" : ""?>">알림</a></li>
                <li><a href="/goal" class="menu <?=$page_id == "goal" ? "active" : ""?>">목표치</a></li>
                <li><a href="/subtitle" class="menu <?=$page_id == "subtitle" ? "active" : ""?>">후원자막</a></li>
                <li><a href="/text" class="menu <?=$page_id == "text" ? "active" : ""?>">텍스트자막</a></li>
                <li><a href="/banner" class="menu <?=$page_id == "banner" ? "active" : ""?>">배너</a></li>
                <li><a href="/timer" class="menu <?=$page_id == "timer" ? "active" : ""?>">시계</a></li>
            </ul>
        </div>
    </div>
</div>