<?php
    $root_path = $_SERVER["DOCUMENT_ROOT"]."/";
	include $root_path."lib/common.php";
	
	$page_key = isset($_GET["key"]) ? $_GET["key"] : "";
	
	$user_id = isset($_GET["userid"]) ? $_GET["userid"] : "";
	$user_asp = isset($_GET["pcode"]) ? $_GET["pcode"] : "";
	$crypt = new Crypt();
	$login_id;
	if(empty($page_key)){
		if(isset($user_id)){
			$page_key = $crypt->encrypt($user_id."/".$user_asp);
			if(empty($user_asp)){
				echo "ERROR:: 잘못된 페이지 요청 입니다. [NULL PCODE]";
				die();
			}
		}
	}
	
	$user_info = new loginInfo();
	$login_data = $user_info->userInfo($page_key);
	if(isset($login_data->data->id)) {
		$login_id = $login_data->data->id;
	}
	echo $login_data->data->nickName;
	echo "<br>";
	echo $login_id;
	echo "<br>";
	echo "<br>";
	if(empty($login_id)) {
		echo "ERROR:: 아이디를 찾을수 없습니다. [NULL USER]";
		die();
	}
	$user_key = new userKey($popkon_keycode);
	$get_key = $user_key->encrypt($login_id);
	$page_array = array("chat", "alert", "alertlist", "goal", "subtitle", "text", "banner", "timer");
	foreach ($page_array as $key => $value) {
		$preset = "./data/save/".$get_key."/".$value."/"."preset.json";
		if(file_exists($preset)) {
			$preset = file_get_contents($preset);
			echo $value.":".$preset;
		}else{
			continue;
		}
		echo "<br>";
	}
	
?>

<!DOCTYPE html>
<html lang="ko" >
<head>
    <title>방송도우미</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=0.5, minimum-scale=0.5, maximum-scale=1, user-scalable=yes">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    
	<style>
	</style>
    <!--[if lt IE 10]>
        <script src="/resource/js/vendor/typedarray.js"></script>
    <![endif]-->
</head>
<body>
    
</body>
</html>    
