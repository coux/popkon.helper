<div id="header">
	<div class="header_top">
        <div class="contains">
            <h1 class="logo">
				<a href="https://<?=$login_info->web?>" target="_blank">
					<img src="https://pic.popkontv.com/images/aspw/<?=$login_info->asp?>/common/siteLogo_190.png" alt="POPKON TV">
				</a>
			</h1>
			<? 
				if($page_id == "main") { ?>
                <div class="login_area login_box">
                    <input type="text" placeholder="아이디" id="login_id" class="input_text input_id">
                    <a href="#" id="btn_login" class="btns green btn_login email">로그인</a>
                </div>
            <? } else { ?>
                <ul class="gnb menu">
                    <li><a href="/chat" class="menu <?=$page_id == "chat" ? "active" : ""?>">채팅</a></li>
                    <li><a href="/alert" class="menu <?=$page_id == "alert" ? "active" : ""?>">알림</a></li>
                    <li><a href="/goal" class="menu <?=$page_id == "goal" ? "active" : ""?>">목표치</a></li>
                    <li><a href="/subtitle" class="menu <?=$page_id == "subtitle" ? "active" : ""?>">후원자막</a></li>
                    <li><a href="/text" class="menu <?=$page_id == "text" ? "active" : ""?>">텍스트자막</a></li>
                    <li><a href="/banner" class="menu <?=$page_id == "banner" ? "active" : ""?>">배너</a></li>
                    <li><a href="/timer" class="menu <?=$page_id == "timer" ? "active" : ""?>">시계</a></li>
                </ul>
            <? } ?>
        </div>
    </div>
    <div class="header_bottom">
        <div class="contains">
            <ul class="lnb help">
                <li><a href="https://www.popkontv.com/userGuide/helper_guide.asp?pcode=<?=$login_info->asp?>" target="_blank" class="menu"><i class="fa fa-book" aria-hidden="true"></i>도움말</a></li>
            </ul>
            <? if($page_id == "chat") { ?>
                <ul class="lnb menu">
                    <li><a href="#" class="menu">프리셋</a></li>
                    <li><a href="#" class="menu">기본</a></li>
                    <li><a href="#" class="menu">스타일</a></li>
                    <li><a href="#" class="menu">정렬</a></li>
                    <li><a href="#" class="menu">채팅</a></li>
                    <li><a href="#" class="menu">알림</a></li>
                    <li><a href="#" class="menu">필터링</a></li>
                    <li><a href="#" class="menu">채팅색</a></li>
                    <li><a href="#" class="menu">폰트 및 배경</a></li>
                </ul>
            <? } else if($page_id == "alert") { ?>
                <ul class="lnb menu">
                    <li><a href="#" class="menu">프리셋</a></li>
                    <li><a href="#" class="menu">기본</a></li>
                    <li><a href="#" class="menu">알림</a></li>
                    <li><a href="#" class="menu">팝업</a></li>
                    <li><a href="#" class="menu">사운드</a></li>
                    <li><a href="#" class="menu">필터링</a></li>
                    <li><a href="#" class="menu">텍스트색</a></li>
                    <li><a href="#" class="menu">폰트 및 배경</a></li>
                </ul>
            <? } else if($page_id == "goal") { ?>
                <ul class="lnb menu">
                    <li><a href="#" class="menu">프리셋</a></li>
                    <li><a href="#" class="menu">기본</a></li>
                    <li><a href="#" class="menu">스타일</a></li>
                    <li><a href="#" class="menu">목표치</a></li>
                    <li><a href="#" class="menu">그래프색</a></li>
                    <li><a href="#" class="menu">폰트 및 배경</a></li>
                </ul>
            <? } else if($page_id == "subtitle") { ?>
                <ul class="lnb menu">
                    <li><a href="#" class="menu">프리셋</a></li>
                    <li><a href="#" class="menu">기본</a></li>
                    <li><a href="#" class="menu">자막</a></li>
                    <li><a href="#" class="menu">정렬</a></li>
                    <li><a href="#" class="menu">필터링</a></li>
                    <li><a href="#" class="menu">자막색</a></li>
                    <li><a href="#" class="menu">폰트 및 배경</a></li>
                </ul>
            <? } else if($page_id == "text") { ?>
                <ul class="lnb menu">
                    <li><a href="#" class="menu">프리셋</a></li>
                    <li><a href="#" class="menu">기본</a></li>
                    <li><a href="#" class="menu">에디터</a></li>
                    <li><a href="#" class="menu">배경</a></li>
                </ul>
            <? } else if($page_id == "banner") { ?>
                <ul class="lnb menu">
                    <li><a href="#" class="menu">프리셋</a></li>
                    <li><a href="#" class="menu">기본</a></li>
                    <li><a href="#" class="menu">배너</a></li>
                </ul>
            <? } else if($page_id == "timer") { ?>
                <ul class="lnb menu">
                    <li><a href="#" class="menu">프리셋</a></li>
                    <li><a href="#" class="menu">기본</a></li>
                    <li><a href="#" class="menu">시계</a></li>
                </ul>
            <? } ?>
            <a href="#" title="위로" class="btn_top"><i class="fa fa-chevron-up" aria-hidden="true"></i></a>
        </div>
    </div>
</div>