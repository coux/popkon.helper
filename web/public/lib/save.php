<?
    header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Credentials: true");
    header("Access-Control-Allow-Methods: GET, POST");
    header("Access-Control-Allow-Headers: X-Requested-With");
    header("Content-Type:application/json; charset=utf-8");
    
    $root_path = $_SERVER["DOCUMENT_ROOT"]."/";
    // $root_path = "../";
    include_once $root_path."lib/common.php";
    
    // $user_key = new userKey($popkon_keycode);
    
    $result = new stdClass();
    $result->result = "";
    
    $get_id = isset($_REQUEST["id"]) ? $_REQUEST["id"] : "";
	$get_type = isset($_REQUEST["type"]) ? $_REQUEST["type"] : "";
	$get_asp = isset($_GET["pcode"]) && !empty($_GET["pcode"]) ? $_GET["pcode"] : "";
    $get_key = isset($_REQUEST["key"]) ? $_REQUEST["key"] : "";
    $get_page = isset($_REQUEST["page"]) ? $_REQUEST["page"] : "";
    $get_sub = isset($_REQUEST["sub"]) && !empty($_REQUEST["sub"])? $_REQUEST["sub"] : "0";
    $get_data = isset($_REQUEST["data"]) ? $_REQUEST["data"] : "";
    $get_time = isset($_REQUEST["time"]) ? $_REQUEST["time"] : "";
    $get_list = isset($_REQUEST["list"]) ? $_REQUEST["list"] : array();

    //중요한 읽고 쓰기 할때는 key 값과 로그인 세션값 id 변환키로 로그인 db와 체크해서 accesskey 비교해서 보안 체크하기
    
    if($get_key != "" && $get_type != "" && $get_page != "") {
        // $get_key = $user_key->decrypt($get_key);
        if($get_type === "presetload") {
            $user_key = new userKey($popkon_keycode);
            $get_key = $user_key->encrypt($get_key);
        }
        
        $get_rootpath = $root_path."data/save/".$get_key;
        $get_pagedir = $get_rootpath."/".$get_page;
        $get_savefile = $get_pagedir."/".$get_sub.".json";
        $get_presetfile = $get_pagedir."/preset.json";
        
        if($get_type == "save") {
            if(!file_exists($get_rootpath)) {
                mkdir($get_rootpath);
            }
            if(!file_exists($get_pagedir)) {
                mkdir($get_pagedir);
            }
            if($get_data != "") {
                file_put_contents($get_savefile, $get_data);
                $result->result = "save";
                //file_put_contents("../log/debug_save.json", $_SERVER["REMOTE_ADDR"]."-".$get_id."-".$get_type."-".$get_key."-".$get_page."-".$get_sub."-".$get_data."\r\n", FILE_APPEND);
            }
        }
        else if($get_type == "preset") {
            if(!file_exists($get_rootpath)) {
                mkdir($get_rootpath);
            }
            if(!file_exists($get_pagedir)) {
                mkdir($get_pagedir);
            }
            if($get_data != "") {
                file_put_contents($get_presetfile, $get_data);
                $result->result = "save";
                //file_put_contents("../log/debug_preset.json", $_SERVER["REMOTE_ADDR"]."-".$get_id."-".$get_type."-".$get_key."-".$get_page."-".$get_sub."-".$get_data."\r\n", FILE_APPEND);
            }
            
            $get_idx = isset($_REQUEST["idx"]) ? $_REQUEST["idx"] : "";
            $get_preset = isset($_REQUEST["preset"]) ? $_REQUEST["preset"] : "";
            if($get_preset != "" && $get_idx != "") {
                $get_presetload = $root_path."data/preset/".$get_page."/".$get_preset.".json";
                if(file_exists($get_presetload)) {
                    $get_presetsave = $get_pagedir."/".$get_idx.".json";
                    if(copy($get_presetload, $get_presetsave)) {
                        $result->preset = "save";
                    }
                }
            }
        }
        else if($get_type == "presetload") {
            $result->data = array();
            $get_presetfile = $get_rootpath."/".$get_page."/preset.json";
            if(file_exists($get_presetfile)) {
                $result->data = json_decode(file_get_contents($get_presetfile));
            }
        }
        else if($get_type == "load") {
            if(file_exists($get_presetfile)) {
                $result->preset = file_get_contents($get_presetfile);
            }
            if(file_exists($get_savefile)) {
                $result->data = file_get_contents($get_savefile);
            }
            $result->result = "load";
        }
        else if($get_type == "reset") {
            if(file_exists($get_savefile)) {
                unlink($get_savefile);
            }
            $result->result = "reset";
        }
    }

    echo json_encode($result);
?>
