<?
    $root_path = $_SERVER["DOCUMENT_ROOT"]."/";
    // $root_path = "../";
    include_once $root_path."lib/common.php";
    
    putenv('GOOGLE_APPLICATION_CREDENTIALS=/svc/nginx/html/lib/popkontts-de658d3013d8.json');
    
    $get_text = isset($_REQUEST["text"]) ? $_REQUEST["text"] : "";
    $get_gender = isset($_REQUEST["gender"]) ? $_REQUEST["gender"] : "FEMALE";
    $get_lang = "ko-KR";
    
    require __DIR__ . '/vendor/autoload.php';
    $googleId = 'atomic-quasar-271509';
    $client = new GuzzleHttp\Client(); 
    
    $requestData = [
        'input' =>[
            'text' => $get_text
        ], 'voice' => [
            'languageCode' => $get_lang,
            'ssmlGender' => $get_gender
        ], 'audioConfig' => [ 
            'audioEncoding' => 'MP3',
            'speakingRate' => 1.00
        ]
    ]; 
    try {
        header("Content-Type: audio/mpeg");
        $response = $client->request('POST', 'https://texttospeech.googleapis.com/v1beta1/text:synthesize?key=' . $google_api, [ 'json' => $requestData ]); 
    } catch (Exception $e) {
        echo $e->getMessage();
    } 
    $fileData = json_decode($response->getBody()->getContents(), true);
    echo base64_decode($fileData['audioContent']);
?>