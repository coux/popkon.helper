<?php
	
    session_start();

	//sql
	class sql {
		public $con;
		private $host, $id, $pw;
		
		function sql() {
			global $sql_host, $sql_id, $sql_pw;
			$this->host = $sql_host;
			$this->id = $sql_id;
			$this->pw = $sql_pw;
		}
		function con() {
			$this->con = mysqli_connect($this->host, $this->id, $this->pw);
			mysqli_query($this->con,"set names utf8");
			return $this->con;
		}
		function close() {
			mysqli_close($this->con);
		}
	}
	
	//키 암호화 복호화
	class userKey {
		private $key;
		
		function userKey($val) {
		   $this->key = $val;
		}
		function encrypt($string) {
			$result = '';
			for($i=0; $i<strlen($string); $i++) {
				$char = substr($string, $i, 1);
				$keychar = substr($this->key, ($i % strlen($this->key))-1, 1);
				$char = chr(ord($char)+ord($keychar));
				$result.=$char;
			}
			return rtrim(strtr(base64_encode($result), '+/', '-_'), '=');
		}
		function decrypt($string) {
			$result = '';
			$string = base64_decode(str_pad(strtr($string, '-_', '+/'), strlen($string) % 4, '=', STR_PAD_RIGHT));
			for($i=0; $i<strlen($string); $i++) {
				$char = substr($string, $i, 1);
				$keychar = substr($this->key, ($i % strlen($this->key))-1, 1);
				$char = chr(ord($char)-ord($keychar));
				$result.=$char;
			}
			return $result;
		}
	}

	//api 보안
	class crypt {
		function __construct($key = 0, $cipher = 'AES-256-CBC'){
			$this->table = file($root_path."lib/setup/keys.inc", FILE_IGNORE_NEW_LINES);
			$this->secret = 't[{]PUSrh-YY2JgH!R*OhwY$CZRR0mq-';
			$this->cipher = $cipher;
		}

		public function safe_b64encode($string) {
			$data = base64_encode($string);
			$data = str_replace(array('='),array(''), $data);
			return $data;
		}

		public function safe_b64decode($string) {
			$data = str_replace(array('-','_'),array('+','/'),$string);
			$mod4 = strlen($data) % 4;
			if ($mod4) {
				$data .= substr('====', $mod4);
			}
			return base64_decode($data);
		}

		public function encrypt($value, $key = NULL) {
			$iv = str_repeat(chr(0), 16);
			$secret = $key ? $this->table[$key - 1] : $this->secret;
			$value = openssl_encrypt($value, $this->cipher, $secret, true, $iv);
			return $this->safe_b64encode($value); 
		}
		
		public function decrypt($value, $key = NULL){
			$iv = str_repeat(chr(0), 16);
			$secret = $key ? $this->table[$key - 1] : $this->secret;
			$value = $this->safe_b64decode($value);
			return openssl_decrypt($value, $this->cipher, $secret, true, $iv);
		}
		
		public function pkcs5_pad($text, $blocksize = 16) {
			$pad = $blocksize - (strlen($text) % $blocksize);
			return $text . str_repeat(chr($pad), $pad);
		}

		public function pkcs5_unpad($text) {
			$pad = ord($text{strlen($text)-1});
			if ($pad > strlen($text)) {
				return $text;
			}
			if (!strspn($text, chr($pad), strlen($text) - $pad)) {
				return $text;
			}
			return substr($text, 0, -1 * $pad);
		}
	}


	//유저 정보
	class loginInfo {
		function userInfo($data) {
			$rand = rand(1,1000);
			$crypt = new crypt($rand);
			$data = $crypt->decrypt($data);
			$info = explode('/', $data);
			if(isset($data[0]) && isset($data[1])){
				$data = (object) array (
						'SC_SI' => $info[0],
						'SC_PC' => $info[1]
				);
				$data = json_encode($data);
				$data = $crypt->encrypt($data, $rand);
				$data = array(
					'data' => $data,
					'key' => (string) $rand,
					'v' => '2'
				);
				$res = post("http://sys1.popkontv.kr:9001/AS/helper/helperinfo.asp", $data);
				$res = json_decode($res);
				$res = $crypt->decrypt($res->data, $res->key);
				$res = json_decode($res, true);
			
				if($res['rstCode'] == 0){
					$res['userInfo']['id'] = $info[0];
					$res['partnerInfo']['pcode'] = $info[1];
					$obj = new stdClass();
					$obj->data = (object) array_merge($res["userInfo"], $res["partnerInfo"]);
					return $obj;
				}
			}
		}
		
		function presetload($key, $page) {
			$result = array();
			$get_presetfile = $_SERVER["DOCUMENT_ROOT"]."/data/save/".$key."/".$page."/preset.json";
			if(file_exists($get_presetfile)) {
                $result = json_decode(file_get_contents($get_presetfile));
            }
			return $result;
		}
	    
		//로그인 저장
		function dbsave($data) {
			$result = new stdClass;
            $result->id = "";
			$result->result = "";
			if(!empty($data->id)) {
				$sql = new sql();
				$con = $sql->con();
				if($con) {
					$sql_select = "SELECT idx FROM popkon.user WHERE id='".mysqli_real_escape_string($con, $data->id)."' AND uses='Y' ORDER BY uptime DESC LIMIT 0,1";
					$result_select = mysqli_query($con, $sql_select);
					if($result_select) {
						while($row = mysqli_fetch_assoc($result_select)) {
							$result->id = $row["idx"];
						}
					}
					if(!empty($result->id) && is_numeric($result->id)) {
						$sql_update = "UPDATE popkon.user SET uptime=now() WHERE idx=".intval($result->id)." AND uses='Y'";
						if(mysqli_query($con, $sql_update)) {
							$result->result = "update";
						}
					}
					else {
						$sql_insert = "INSERT INTO popkon.user (idkey, id, name, profile, instime, uptime) VALUES ('".$data->idkey."', '".$data->id."', '".$data->name."', '".$data->profile."', NOW(), NOW())";
						if(mysqli_query($con, $sql_insert)) {
							$result->id = mysqli_insert_id($con);
                            $result->result = "insert";
						}
					}
					$sql->close();
				}
			}
			return $result;
		}
		
		//로그인 불러오기
		function dbload($id) {
			$result = new stdClass;
			$result->result = "";
			if(!empty($id)) {
				$sql = new sql();
				$con = $sql->con();
				if($con) {
					$sql_select = "SELECT * FROM popkon.user WHERE id='".mysqli_real_escape_string($con, $id)."' AND uses='Y' ORDER BY modifytime DESC LIMIT 0,1";
					$result_select = mysqli_query($con, $sql_select);
					if($result_select) {
						while($row = mysqli_fetch_assoc($result_select)) {
							$result->idx = $row["idx"];
							$result->name = $row["name"];
							$result->profile = $row["profile"];
						}
						$result->result = "load";
					}
					$sql->close();
				}
			}
			return $result;
		}
		
		//로그 저장
		function dblog($data) {
			$result = new stdClass;
            $result->id = "";
			$result->result = "";
			if(!empty($data->id)) {
				$sql = new sql();
				$con = $sql->con();
				if($con) {
					$sql_insert = "INSERT INTO popkon.log (idkey, id, name, type, ip, agent, referer, time) VALUES ('".$data->idkey."', '".$data->id."', '".$data->name."', '".$data->type."', '".$data->ip."', '".$data->agent."', '".$data->referer."', NOW())";
					if(mysqli_query($con, $sql_insert)) {
						$result->id = mysqli_insert_id($con);
                        $result->result = "insert";
					}
					$sql->close();
				}
			}
			return $result;
		}
	}

	// POST 방식 함수
	function post($url, $fields)
	{
		$post_field_string = http_build_query($fields, '', '&');
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post_field_string);
		curl_setopt($ch, CURLOPT_POST, true);
		$response = curl_exec($ch);
		curl_close ($ch);
		return $response;
	}

	// GET 방식 함수
	function get($url, $params=array()) 
	{ 
		$url = $url.'?'.http_build_query($params, '', '&');
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		$response = curl_exec($ch);
		curl_close($ch);
		return $response;
	}



?>