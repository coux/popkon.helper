<!-- 기본설정 -->
<div class="setup_title info_box">
    <p class="title">기본</p>
</div>
<div class="setup_group menu_panel">
    <ul class="group_list">
        <li class="input_area">
            <p class="input_label tooltip_box">창 투명도<span class="tooltip"><i class="fa fa-info-circle info" aria-hidden="true"></i><span class="tooltip_text">창 투명도를 설정합니다. (0% 보임, 100% 투명)</span></span></p>
            <div class="size_box">
                <div data-min="0" data-max="100" data-step="10" data-value="0" class="size_bar"></div>
                <input type="text" name="item_text_opacity" data-target=".item_text" readonly="readonly" class="input_size input_text value">
                <p class="unit">%</p>
            </div>
        </li>
        <?
            if($page_id != "text") include $root_path."lib/setup/comm_customcss.php";
        ?>
    </ul>
</div>

<!-- 텍스트설정 -->
<div class="setup_title info_box">
    <p class="title tooltip_box">에디터<span class="tooltip"><i class="fa fa-info-circle info" aria-hidden="true"></i><span class="tooltip_text">텍스트를 다양한 스타일로 편집할 수 있습니다.<br><br><span class="title"><i class="fa fa-info-circle info" aria-hidden="true"></i>대체어</span><br>{제목} : 방송 제목<br>{시간} : 현재 시간<br>{방송시간} : 방송한 시간</span></span></p>
</div>
<div class="setup_group menu_panel">
    <div class="item_text">
        <div id="text_editor"></div>
        <div class="text_selectlist">
            <?
                include $root_path."lib/setup/comm_fontlist.php";
            ?>        
        </div>
    </div>
</div>

<div class="setup_title info_box">
    <p class="title">배경</p>
</div>
<div class="setup_group menu_panel">
    <ul class="group_list">
        <li class="input_area mt0">
            <p class="input_label tooltip_box">배경색<span class="tooltip"><i class="fa fa-info-circle info" aria-hidden="true"></i><span class="tooltip_text">배경색 또는 이미지를 설정합니다.</span></span></p>
            <label class="check_box">
                <input type="radio" name="item_<?=$page_id?>_bgtrans" value="none" data-target=".item_<?=$page_id?>" data-toggle=".toggle_bgcolor" checked="checked" class="radio toggle_radio">
                <span class="icon"><i class="fa fa-check" aria-hidden="true"></i></span>
                <span class="text">사용 안함</span>
            </label>
            <label class="check_box">
                <input type="radio" name="item_<?=$page_id?>_bgtrans" value="color" data-target=".item_<?=$page_id?>" data-toggle=".toggle_bgcolor" class="radio toggle_radio">
                <span class="icon"><i class="fa fa-check" aria-hidden="true"></i></span>
                <span class="text">배경색</span>
            </label>
            <label class="check_box">
                <input type="radio" name="item_<?=$page_id?>_bgtrans" value="image" data-target=".item_<?=$page_id?>" data-toggle=".toggle_bgcolor" class="radio toggle_radio">
                <span class="icon"><i class="fa fa-check" aria-hidden="true"></i></span>
                <span class="text">이미지</span>
            </label>
            <ul class="group_list">
                <li class="input_area toggle_bgcolor toggle_color hide mt10">
                    <input type="text" name="item_<?=$page_id?>_colorbg" data-type="style" data-style=".item_<?=$page_id?>.bgtrans_color,.item_text.bgtrans_color .note-editing-area .note-editable{background-color:{val}!important}" value="#000000" class="input_text input_color">
                </li>
                <li class="input_area toggle_bgcolor toggle_image hide mt10">
                    <div class="file_upload_box file_img file_bgimg" data-type="bgimg">
                        <div class="upload_box">
                            <p class="name"></p>
                            <div data-style=".item_<?=$page_id?>.bgtrans_image,.item_text.bgtrans_image .note-editing-area .note-editable{background-image:url({val})!important;background-position:center top;background-repeat:no-repeat;background-size:contain}" class="img_box">
                                <i class="fa fa-picture-o icon_file" aria-hidden="true"></i>
                                <p class="text">이미지를 선택해주세요</p>
                            </div>
                            <div class="upload_tooltip">
                                <a href="#" title="파일 선택" class="btns green btn_file_upload"><i class="fa fa-file-image-o" aria-hidden="true"></i>파일 선택</a>
                                <a href="#" title="외부 링크 설정" class="btns green btn_file_link"><i class="fa fa-link" aria-hidden="true"></i>외부 링크</a>
                                <a href="#" title="파일 삭제" class="btns gray btn_file_delete"><i class="fa fa-trash-o" aria-hidden="true"></i>파일 삭제</a>
                            </div>
                        </div>
                        <label class="check_box">
                            <input type="checkbox" name="item_<?=$page_id?>_bgimgzoom" data-type="style" data-target=".item_<?=$page_id?>" data-style=".item_<?=$page_id?>.item_<?=$page_id?>_bgimgzoom,.item_text.item_text_bgimgzoom .note-editing-area .note-editable{background-size:100% 100%!important}" class="check">
                            <span class="icon"><i class="fa fa-check" aria-hidden="true"></i></span>
                            <span class="text">이미지 넓이 맞추기</span>
                        </label>
                    </div>
                </li>
            </ul>
        </li>
    </ul>
</div>