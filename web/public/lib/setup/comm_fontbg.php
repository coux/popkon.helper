<div class="setup_title info_box">
    <p class="title">폰트 및 배경</p>
</div>
<div class="setup_group menu_panel">
    <ul class="group_list">
        <li class="input_area">
            <p class="input_label">폰트 미리보기</p>
            <div class="text_preview">
                <? if($page_id == "alert" || $page_id == "subtitle") { ?>
                    <p id="text_preview" class="item_<?=$page_id?>">안녕하세요. 감사합니다! <span class="nick">닉네임색</span> <span class="value">개수색</span> 됏 햏 Hello! 你好. こんにちは。</p>
                <? } else { ?>
                    <p id="text_preview" class="item_<?=$page_id?>">안녕하세요. 감사합니다! 됏 햏 Hello! 你好. こんにちは。</p>
                <? } ?>
            </div>
        </li>
        <li class="input_area">
            <p class="input_label tooltip_box">폰트 선택<span class="tooltip"><i class="fa fa-info-circle info" aria-hidden="true"></i><span class="tooltip_text">폰트이름 뒤에 (완)은 <b>됏 햏</b> 같은 비표준단어까지 모두 표시되는 폰트입니다.</span></span></p>
            <?
                include $root_path."lib/setup/comm_fontlist.php";
            ?>
            <label class="check_box">
                <input type="checkbox" name="item_<?=$page_id?>_fontbold" data-target=".item_<?=$page_id?>" class="check">
                <span class="icon"><i class="fa fa-check" aria-hidden="true"></i></span>
                <span class="text">폰트 굵게 표시</span>
            </label>
        </li>
        <li class="input_area addfont_area">
            <p class="input_label tooltip_box">웹폰트 URL<span class="tooltip"><i class="fa fa-info-circle info" aria-hidden="true"></i><span class="tooltip_text">사용할 ttf, wof, otf 웹폰트 URL을 입력해주세요.</span></span></p>
            <input type="text" id="addfont_url"name="item_<?=$page_id?>_addfonturl" class="input_text sizem" placeholder="http://example.com/font.ttf" value="">
        </li>
        <li class="input_area addfont_area">
            <p class="input_label tooltip_box">웹폰트 이름<span class="tooltip"><i class="fa fa-info-circle info" aria-hidden="true"></i><span class="tooltip_text">사용할 웹폰트 이름을 입력해주세요.</span></span></p>
            <input type="text" id="addfont_name" name="item_<?=$page_id?>_addfontname" class="input_text" placeholder="폰트 이름" value="">
            <a href="#" id="btn_fontselect" class="btns green btn_fontselect">웹폰트 추가</a>
        </li>
        <li class="input_area">
            <p class="input_label">폰트 크기</p>
            <div class="size_box">
                <div data-min="6" data-max="60" data-step="2" data-value="16" class="size_bar"></div>
                <input type="text" name="item_<?=$page_id?>_fontsize" data-target=".item_<?=$page_id?>" readonly="readonly" class="input_size input_text value">
                <p class="unit">픽셀</p>
            </div>
        </li>
        <li class="input_area">
            <p class="input_label">폰트 외곽선</p>
            <div class="size_box mb10">
            <div data-min="0" data-max="20" data-step="1" data-value="2" class="size_bar"></div>
            <? if($page_id == "chat") { ?>
                    <input type="text" name="item_chat_shadowsize" data-type="style" data-target=".item_chat" data-style=".item_chat .chat_list p,.text_preview .item_chat{{val}}" readonly="readonly" class="input_size input_text value">
                    <p class="unit">픽셀</p>
                </div>
                <input type="text" name="item_chat_colorshadow" data-type="style" data-style=".item_chat .chat_list p,.text_preview .item_chat{{val}}" value="rgba(0,0,0,0.8)" class="input_text input_color">
            <? } else if($page_id == "alert") { ?>
                    <input type="text" name="item_alert_shadowsize" data-type="style" data-target=".item_alert" data-style=".item_alert .alert_box .text,.text_preview .item_alert{{val}}" readonly="readonly" class="input_size input_text value">
                    <p class="unit">픽셀</p>
                </div>
                <input type="text" name="item_alert_colorshadow" data-type="style" data-style=".item_alert .alert_box .text,.text_preview .item_alert{{val}}" value="rgba(0,0,0,0.8)" class="input_text input_color">
            <? } else if($page_id == "goal") { ?>
                    <!-- <input type="text" name="item_goal_shadowsize" data-type="style" data-target=".item_goal" data-style=".item_goal .text,.text_preview .item_goal,.item_goal.item_goal_textonly .goal_box .goal_textin,#goal_circle .goal_textin{{val}}" readonly="readonly" class="input_size input_text value">
                    <p class="unit">픽셀</p>
                </div>
                <input type="text" name="item_goal_colorshadow" data-type="style" data-style=".item_goal .text,.text_preview .item_goal,.item_goal.item_goal_textonly .goal_box .goal_textin,#goal_circle .goal_textin{{val}}" value="rgba(0,0,0,0.8)" class="input_text input_color"> -->
                    <input type="text" name="item_goal_shadowsize" data-type="style" data-target=".item_goal" data-style=".item_goal .text,.text_preview .item_goal{{val}}" readonly="readonly" class="input_size input_text value">
                    <p class="unit">픽셀</p>
                </div>
                <input type="text" name="item_goal_colorshadow" data-type="style" data-style=".item_goal .text,.text_preview .item_goal{{val}}" value="rgba(0,0,0,0.8)" class="input_text input_color">
            <? } else if($page_id == "subtitle") { ?>
                    <input type="text" name="item_subtitle_shadowsize" data-type="style" data-target=".item_subtitle" data-style=".item_subtitle .subtitle,.text_preview .item_subtitle{{val}}" readonly="readonly" class="input_size input_text value">
                    <p class="unit">픽셀</p>
                </div>
                <input type="text" name="item_subtitle_colorshadow" data-type="style" data-style=".item_subtitle .subtitle,.text_preview .item_subtitle{{val}}" value="rgba(0,0,0,0.8)" class="input_text input_color">
            <? } ?>
        </li>
        <? if($page_id != "banner") { ?>
        <li class="input_area">
            <p class="input_label tooltip_box">배경색<span class="tooltip"><i class="fa fa-info-circle info" aria-hidden="true"></i><span class="tooltip_text">배경색 또는 이미지를 설정합니다.</span></span></p>
            <label class="check_box">
                <input type="radio" name="item_<?=$page_id?>_bgtrans" value="none" data-target=".item_<?=$page_id?>" data-toggle=".toggle_bgcolor" checked="checked" class="radio toggle_radio">
                <span class="icon"><i class="fa fa-check" aria-hidden="true"></i></span>
                <span class="text">사용 안함</span>
            </label>
            <label class="check_box">
                <input type="radio" name="item_<?=$page_id?>_bgtrans" value="color" data-target=".item_<?=$page_id?>" data-toggle=".toggle_bgcolor" class="radio toggle_radio">
                <span class="icon"><i class="fa fa-check" aria-hidden="true"></i></span>
                <span class="text">배경색</span>
            </label>
            <label class="check_box">
                <input type="radio" name="item_<?=$page_id?>_bgtrans" value="image" data-target=".item_<?=$page_id?>" data-toggle=".toggle_bgcolor" class="radio toggle_radio">
                <span class="icon"><i class="fa fa-check" aria-hidden="true"></i></span>
                <span class="text">이미지</span>
            </label>
            <ul class="group_list">
                <li class="input_area toggle_bgcolor toggle_color hide mt10">
                    <input type="text" name="item_<?=$page_id?>_colorbg" data-type="style" data-style=".item_<?=$page_id?>.bgtrans_color{background-color:{val}}" value="#000000" class="input_text input_color">
                </li>
                <li class="input_area toggle_bgcolor toggle_image hide mt10">
                    <div class="file_upload_box file_img file_bgimg" data-type="bgimg">
                        <div class="upload_box">
                            <p class="name"></p>
                            <div data-style=".item_<?=$page_id?>.bgtrans_image{background-image:url({val});background-position:center top;background-repeat:no-repeat;background-size:contain}" class="img_box">
                                <i class="fa fa-picture-o icon_file" aria-hidden="true"></i>
                                <p class="text">이미지를 선택해주세요</p>
                            </div>
                            <div class="upload_tooltip">
                                <a href="#" title="파일 선택" class="btns green btn_file_upload"><i class="fa fa-file-image-o" aria-hidden="true"></i>파일 선택</a>
                                <a href="#" title="외부 링크 설정" class="btns green btn_file_link"><i class="fa fa-link" aria-hidden="true"></i>외부 링크</a>
                                <a href="#" title="파일 삭제" class="btns gray btn_file_delete"><i class="fa fa-trash-o" aria-hidden="true"></i>파일 삭제</a>
                            </div>
                        </div>
                        <label class="check_box">
                            <input type="checkbox" name="item_<?=$page_id?>_bgimgzoom" data-type="style" data-target=".item_<?=$page_id?>" data-style=".item_<?=$page_id?>.item_<?=$page_id?>_bgimgzoom{background-size:100% 100%!important}" class="check">
                            <span class="icon"><i class="fa fa-check" aria-hidden="true"></i></span>
                            <span class="text">이미지 넓이 맞추기</span>
                        </label>
                    </div>
                </li>
            </ul>
        </li>
        <? } ?>
    </ul>
</div>