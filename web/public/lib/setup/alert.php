<!-- 기본설정 -->
<div class="setup_title info_box">
    <p class="title">기본</p>
</div>
<div class="setup_group menu_panel">
    <ul class="group_list">
        <li class="input_area">
            <p class="input_label tooltip_box">창 투명도<span class="tooltip"><i class="fa fa-info-circle info" aria-hidden="true"></i><span class="tooltip_text">창 투명도를 설정합니다. (0% 보임, 100% 투명)</span></span></p>
            <div class="size_box">
                <div data-min="0" data-max="100" data-step="10" data-value="0" class="size_bar"></div>
                <input type="text" name="item_alert_opacity" data-target=".item_alert" readonly="readonly" class="input_size input_text value">
                <p class="unit">%</p>
            </div>
        </li>
        <li class="input_area layout_box">
            <p class="input_label tooltip_box">레이아웃<span class="tooltip"><i class="fa fa-info-circle info" aria-hidden="true"></i><span class="tooltip_text">알림 팝업의 이미지 및 텍스트 정렬 방식을 설정합니다.</span></span></p>
            <label class="check_box">
                <input type="radio" name="item_alert_layout" value="top" data-target=".item_alert" checked="checked" class="radio">
                <span class="box top">
                    <span class="img_box"><i class="fa fa-picture-o" aria-hidden="true"></i></span>
                    <span class="text_box">텍스트</span>
                </span>
            </label>
            <label class="check_box">
                <input type="radio" name="item_alert_layout" value="center" data-target=".item_alert" class="radio ">
                <span class="box center">
                    <span class="img_box"><i class="fa fa-picture-o" aria-hidden="true"></i></span>
                    <span class="text_box">텍스트</span>
                </span>
            </label>
            <label class="check_box">
                <input type="radio" name="item_alert_layout" value="left" data-target=".item_alert" class="radio">
                <span class="box left">
                    <span class="img_box"><i class="fa fa-picture-o" aria-hidden="true"></i></span>
                    <span class="text_box">텍스트</span>
                </span>
            </label>
        </li>
        <li class="input_area">
            <p class="input_label">텍스트 정렬</p>
            <label class="check_box">
                <input type="radio" name="item_alert_align" value="left" data-target=".item_alert" class="radio">
                <span class="icon"><i class="fa fa-check" aria-hidden="true"></i></span>
                <span class="text"><i class="fa fa-align-left" aria-hidden="true"></i> 왼쪽</span>
            </label>
            <label class="check_box">
                <input type="radio" name="item_alert_align" value="center" data-target=".item_alert" checked="checked" class="radio">
                <span class="icon"><i class="fa fa-check" aria-hidden="true"></i></span>
                <span class="text"><i class="fa fa-align-justify" aria-hidden="true"></i> 가운데</span>
            </label>
            <label class="check_box">
                <input type="radio" name="item_alert_align" value="right" data-target=".item_alert" class="radio">
                <span class="icon"><i class="fa fa-check" aria-hidden="true"></i></span>
                <span class="text"><i class="fa fa-align-right" aria-hidden="true"></i> 오른쪽</span>
            </label>
        </li>
        <?
            include $root_path."lib/setup/comm_customcss.php";
        ?>
    </ul>
</div>

<!-- 알림설정 -->
<div class="setup_title info_box">
    <p class="title">알림</p>
</div>
<div class="panel_area menu_panel">
    <div class="panel_tab">
        <div class="tab_menu">
            <a href="#" class="btn_tab active">후원</a>
            <a href="#" class="btn_tab">추천</a>
        </div>
    </div>
    <div class="setup_group tab_panel active">
        <div id="alert_detail_list" class="detail_list">
            <div data-idx="0" data-type="star" class="detail_box">
                <div class="btn_listbox alert_detailbox">
                    <a href="#" class="btns btn_listdel"><i class="fa fa-minus" aria-hidden="true"></i></a>
                    <a href="#" class="btns btn_listadd"><i class="fa fa-plus" aria-hidden="true"></i></a>
                </div>
                <ul class="group_list">
                    <li class="input_area alert_alerttype">
                        <p class="input_label">후원 방식</p>
                        <label class="check_box">
                            <input type="radio" name="detail_alerttype_0" value="normal" checked="checked" data-toggle=".toggle_detail_alerttype_0" class="radio toggle_radio">
                            <span class="icon"><i class="fa fa-check" aria-hidden="true"></i></span>
                            <span class="text">일반</span>
                        </label>
                        <label class="check_box">
                            <input type="radio" name="detail_alerttype_0" value="roulette" data-toggle=".toggle_detail_alerttype_0" class="radio toggle_radio">
                            <span class="icon"><i class="fa fa-check" aria-hidden="true"></i></span>
                            <span class="text">룰렛</span>
                        </label>
                    </li>
                    <li class="input_area alert_roulette toggle_detail_alerttype_0 toggle_roulette hide">
                        <ul class="roulette_header">
                            <li class="name">룰렛이름</li>
                            <li class="rate">당첨비율</li>
                            <li class="percent">룰렛확률</li>
                            <li></li>
                        </ul>
                        <ul class="roulette_list">
                            <li>
                                <input type="text" placeholder="룰렛이름" class="input_text roulette_name">
                                <input type="text" placeholder="당첨비율" class="input_text input_number roulette_rate">
                                <input type="text" readonly="readonly" tabindex="-1" class="input_text roulette_percent">
                                <div class="btn_listbox">
                                    <a href="#" class="btns btn_listdel"><i class="fa fa-minus" aria-hidden="true"></i></a>
                                    <a href="#" class="btns btn_listadd"><i class="fa fa-plus" aria-hidden="true"></i></a>
                                </div>
                            </li>
                        </ul>
                        <a href="#" class="btns green btn_roulette_simulation">룰렛확률 시뮬레이션</a>
                    </li>
                    <li class="input_area">
                        <p class="input_label tooltip_box">후원 개수<span class="tooltip"><i class="fa fa-info-circle info" aria-hidden="true"></i><span class="tooltip_text">후원 개수별로 알림 설정이 가능합니다. <br>100, 1004 등 단일 개수에 대해 설정시 시작과 마지막 개수를 동일하게 입력해주세요.<br>※ 개수가 중복될 경우 아래쪽 설정이 우선순위가 높습니다.</span></span></p>
                        <div class="detail_range">
                            <input type="text" placeholder="시작" value="1" class="input_text input_minstar input_number"><span class="unit">개 부터</span>
                            <input type="text" placeholder="마지막" class="input_text input_maxstar input_number"><span class="unit">개 까지</span>
                        </div>
                    </li>
                    <li class="input_area">
                        <p class="input_label">알림 이미지</p>
                        <div class="file_upload_box file_img">
                            <div class="upload_box">
                                <p class="name"></p>
                                <div class="img_box">
                                    <i class="fa fa-picture-o icon_file" aria-hidden="true"></i>
                                    <p class="text">이미지를 선택해주세요</p>
                                </div>
                                <div class="upload_tooltip">
                                    <a href="#" title="파일 선택" class="btns green btn_file_upload"><i class="fa fa-file-image-o" aria-hidden="true"></i>파일 선택</a>
                                    <a href="#" title="외부 링크 설정" class="btns green btn_file_link"><i class="fa fa-link" aria-hidden="true"></i>외부 링크</a>
                                    <a href="#" title="파일 삭제" class="btns gray btn_file_delete"><i class="fa fa-trash-o" aria-hidden="true"></i>파일 삭제</a>
                                </div>
                            </div>
                            <label class="check_box">
                                <input type="checkbox" checked="checked" class="check check_bgimgstar">
                                <span class="icon"><i class="fa fa-check" aria-hidden="true"></i></span>
                                <span class="text">후원 이미지 사용</span>
                            </label>
                            <label class="check_box">
                                <input type="checkbox" class="check check_bgimgzoom">
                                <span class="icon"><i class="fa fa-check" aria-hidden="true"></i></span>
                                <span class="text">이미지 넓이 맞추기</span>
                            </label>
                        </div>
                    </li>
                    <li class="input_area">
                        <p class="input_label tooltip_box">알림 텍스트<span class="tooltip"><i class="fa fa-info-circle info" aria-hidden="true"></i><span class="tooltip_text">입력한 알림 텍스트 내용을 알림 팝업에 표시하고, 음성으로 읽어줍니다.<br><br><span class="title"><i class="fa fa-info-circle info" aria-hidden="true"></i>대체어</span><br>{닉네임} : 후원한 시청자의 닉네임<br>{아이디} : 후원한 시청자의 아이디<br>{개수} : 후원 개수<br>예 : {닉네임}님 <?=$login_info->coin?> {개수}개 감사합니다!</span></span></p>
                        <textarea placeholder="{닉네임}님 <?=$login_info->coin?> {개수}개 감사합니다!" class="input_text input_textstar">{닉네임}님 <?=$login_info->coin?> {개수}개 감사합니다!</textarea>
                    </li>
                    <li class="input_area alert_voicetype">
                        <p class="input_label">알림 방식</p>
                        <label class="check_box">
                            <input type="radio" name="detail_voicetype_0" value="none" data-toggle=".toggle_detail_voicetype_0" class="radio toggle_radio">
                            <span class="icon"><i class="fa fa-check" aria-hidden="true"></i></span>
                            <span class="text">사용 안함</span>
                        </label>
                        <label class="check_box">
                            <input type="radio" name="detail_voicetype_0" value="beep" data-toggle=".toggle_detail_voicetype_0" class="radio toggle_radio">
                            <span class="icon"><i class="fa fa-check" aria-hidden="true"></i></span>
                            <span class="text">알림음</span>
                        </label>
                        <label class="check_box active">
                            <input type="radio" name="detail_voicetype_0" value="voice" checked="checked" data-toggle=".toggle_detail_voicetype_0" class="radio toggle_radio">
                            <span class="icon"><i class="fa fa-check" aria-hidden="true"></i></span>
                            <span class="text">알림 음성</span>
                        </label>
                    </li>
                    <li class="input_area toggle_detail_voicetype_0 toggle_beep hide mt10">
                        <div class="file_upload_box file_audio">
                            <div class="upload_box">
                                <p class="name"></p>
                                <div class="img_box">
                                    <i class="fa fa-music icon_file" aria-hidden="true"></i>
                                    <p class="text">알림음을 선택해주세요</p>
                                    <a href="#" class="btn_play"><i class="fa fa-play-circle-o" aria-hidden="true"></i></a>
                                </div>
                                <div class="upload_tooltip">
                                    <a href="#" title="파일 선택" class="btns green btn_file_upload"><i class="fa fa-file-image-o" aria-hidden="true"></i>파일 선택</a>
                                    <a href="#" title="외부 링크 설정" class="btns green btn_file_link"><i class="fa fa-link" aria-hidden="true"></i>외부 링크</a>
                                    <a href="#" title="파일 삭제" class="btns gray btn_file_delete"><i class="fa fa-trash-o" aria-hidden="true"></i>파일 삭제</a>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="input_area toggle_detail_voicetype_0 toggle_voice mt10">
                        <p class="info_txt"><i class="fa fa-info-circle icon_info" aria-hidden="true"></i>알림 텍스트 내용을 음성으로 읽어줍니다.</p>
                    </li>
                    <li class="input_area">
                        <p class="input_label tooltip_box">채팅 음성<span class="tooltip"><i class="fa fa-info-circle info" aria-hidden="true"></i><span class="tooltip_text">후원한 시청자의 채팅을 음성으로 읽어줍니다.</span></span></p>
                        <label class="check_box">
                            <input type="checkbox" checked="checked" class="check check_voicechat">
                            <span class="icon"><i class="fa fa-check" aria-hidden="true"></i></span>
                            <span class="text">채팅 음성 사용</span>
                        </label>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    
    <!-- 추천 -->
    <div class="setup_group tab_panel">
        <div data-type="up" class="detail_box up">
            <label class="check_box detailsub_type">
                <input type="checkbox" data-sub="item_alert_upsub" name="item_alert_up" value="up" class="check check_use">
                <span class="icon"><i class="fa fa-check" aria-hidden="true"></i></span>
                <span class="text">추천 알림 사용</span>
            </label>
            <ul class="group_list hide mt10" name="item_alert_upsub">
                <li class="input_area">
                    <p class="input_label">알림 이미지</p>
                    <div class="file_upload_box file_img">
                        <div class="upload_box">
                            <p class="name"></p>
                            <div class="img_box">
                                <i class="fa fa-picture-o icon_file" aria-hidden="true"></i>
                                <p class="text">이미지를 선택해주세요</p>
                            </div>
                            <div class="upload_tooltip">
                                <a href="#" title="파일 선택" class="btns green btn_file_upload"><i class="fa fa-file-image-o" aria-hidden="true"></i>파일 선택</a>
                                <a href="#" title="외부 링크 설정" class="btns green btn_file_link"><i class="fa fa-link" aria-hidden="true"></i>외부 링크</a>
                                <a href="#" title="파일 삭제" class="btns gray btn_file_delete"><i class="fa fa-trash-o" aria-hidden="true"></i>파일 삭제</a>
                            </div>
                        </div>
                        <label class="check_box">
                            <input type="checkbox" class="check check_bgimgzoom">
                            <span class="icon"><i class="fa fa-check" aria-hidden="true"></i></span>
                            <span class="text">이미지 넓이 맞추기</span>
                        </label>
                    </div>
                </li>
                <li class="input_area">
                    <p class="input_label tooltip_box">알림 텍스트<span class="tooltip"><i class="fa fa-info-circle info" aria-hidden="true"></i><span class="tooltip_text">입력한 알림 텍스트 내용을 알림 팝업에 표시하고, 음성으로 읽어줍니다.<br><br><span class="title"><i class="fa fa-info-circle info" aria-hidden="true"></i>대체어</span><br>{닉네임} : 후원한 시청자의 닉네임<br>{아이디} : 후원한 시청자의 아이디<br>예 : {닉네임}님 추천 감사합니다!</span></span></p>
                    <textarea placeholder="{닉네임}님 추천 감사합니다!" class="input_text input_textstar">{닉네임}님 추천 감사합니다!</textarea>
                </li>
                <li class="input_area alert_voicetype">
                    <p class="input_label">알림 방식</p>
                    <label class="check_box">
                        <input type="radio" name="up_voicetype" value="none" data-toggle=".toggle_up_voicetype" class="radio toggle_radio">
                        <span class="icon"><i class="fa fa-check" aria-hidden="true"></i></span>
                        <span class="text">사용 안함</span>
                    </label>
                    <label class="check_box">
                        <input type="radio" name="up_voicetype" value="beep" data-toggle=".toggle_up_voicetype" class="radio toggle_radio">
                        <span class="icon"><i class="fa fa-check" aria-hidden="true"></i></span>
                        <span class="text">알림음</span>
                    </label>
                    <label class="check_box active">
                        <input type="radio" name="up_voicetype" value="voice" checked="checked" data-toggle=".toggle_up_voicetype" class="radio toggle_radio">
                        <span class="icon"><i class="fa fa-check" aria-hidden="true"></i></span>
                        <span class="text">알림 음성</span>
                    </label>
                </li>
                <li class="input_area toggle_up_voicetype toggle_beep hide mt10">
                    <div class="file_upload_box file_audio">
                        <div class="upload_box">
                            <p class="name"></p>
                            <div class="img_box">
                                <i class="fa fa-music icon_file" aria-hidden="true"></i>
                                <p class="text">알림음을 선택해주세요</p>
                                <a href="#" class="btn_play"><i class="fa fa-play-circle-o" aria-hidden="true"></i></a>
                            </div>
                            <div class="upload_tooltip">
                                <a href="#" title="파일 선택" class="btns green btn_file_upload"><i class="fa fa-file-image-o" aria-hidden="true"></i>파일 선택</a>
                                <a href="#" title="외부 링크 설정" class="btns green btn_file_link"><i class="fa fa-link" aria-hidden="true"></i>외부 링크</a>
                                <a href="#" title="파일 삭제" class="btns gray btn_file_delete"><i class="fa fa-trash-o" aria-hidden="true"></i>파일 삭제</a>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="input_area toggle_up_voicetype toggle_voice mt10">
                    <p class="info_txt"><i class="fa fa-info-circle icon_info" aria-hidden="true"></i>알림 텍스트 내용을 음성으로 읽어줍니다.</p>
                </li>
            </ul>
        </div>
    </div>
</div>

<!-- 팝업설정 -->
<div class="setup_title info_box">
    <p class="title">팝업</p>
</div>
<div class="setup_group menu_panel">
    <ul class="group_list alert_preview">
        <li class="input_area">
            <p class="input_label">텍스트 표시 효과</p>
            <select name="item_alert_texteffectin" class="select">
                <option value="none">없음</option>
                <optgroup label="강조">
                    <option value="bounce">bounce</option>
                    <option value="flash">flash</option>
                    <option value="pulse">pulse</option>
                    <option value="rubberBand">rubberBand</option>
                    <option value="shake">shake</option>
                    <option value="swing">swing</option>
                    <option value="tada">tada</option>
                    <option value="wobble">wobble</option>
                    <option value="jello">jello</option>
                    <option value="hinge">hinge</option>
                    <option value="jackInTheBox">jackInTheBox</option>
                    <option value="rollIn">rollIn</option>
                    <option value="rollOut">rollOut</option>
                </optgroup>
                <optgroup label="바운스">
                    <option value="bounceIn">bounceIn</option>
                    <option value="bounceInDown">bounceInDown</option>
                    <option value="bounceInLeft">bounceInLeft</option>
                    <option value="bounceInRight">bounceInRight</option>
                    <option value="bounceInUp">bounceInUp</option>
                    <option value="bounceOut">bounceOut</option>
                    <option value="bounceOutDown">bounceOutDown</option>
                    <option value="bounceOutLeft">bounceOutLeft</option>
                    <option value="bounceOutRight">bounceOutRight</option>
                    <option value="bounceOutUp">bounceOutUp</option>
                </optgroup>
                <optgroup label="페이드">
                    <option value="fadeIn">fadeIn</option>
                    <option value="fadeInDown">fadeInDown</option>
                    <option value="fadeInLeft">fadeInLeft</option>
                    <option value="fadeInRight">fadeInRight</option>
                    <option value="fadeInUp">fadeInUp</option>
                    <option value="fadeOut">fadeOut</option>
                    <option value="fadeOutDown">fadeOutDown</option>
                    <option value="fadeOutLeft">fadeOutLeft</option>
                    <option value="fadeOutRight">fadeOutRight</option>
                    <option value="fadeOutUp">fadeOutUp</option>
                </optgroup>
                <optgroup label="플립">
                    <option value="flip">flip</option>
                    <option value="flipInX">flipInX</option>
                    <option value="flipInY">flipInY</option>
                    <option value="flipOutX">flipOutX</option>
                    <option value="flipOutY">flipOutY</option>
                </optgroup>
                <optgroup label="회전">
                    <option value="rotateIn">rotateIn</option>
                    <option value="rotateInDownLeft">rotateInDownLeft</option>
                    <option value="rotateInDownRight">rotateInDownRight</option>
                    <option value="rotateInUpLeft">rotateInUpLeft</option>
                    <option value="rotateInUpRight">rotateInUpRight</option>
                    <option value="rotateOut">rotateOut</option>
                    <option value="rotateOutDownLeft">rotateOutDownLeft</option>
                    <option value="rotateOutDownRight">rotateOutDownRight</option>
                    <option value="rotateOutUpLeft">rotateOutUpLeft</option>
                    <option value="rotateOutUpRight">rotateOutUpRight</option>
                </optgroup>
                <optgroup label="슬라이드">
                    <option value="slideInUp">slideInUp</option>
                    <option value="slideInDown">slideInDown</option>
                    <option value="slideInLeft">slideInLeft</option>
                    <option value="slideInRight">slideInRight</option>
                    <option value="lightSpeedIn">lightSpeedIn</option>
                    <option value="slideOutUp">slideOutUp</option>
                    <option value="slideOutDown">slideOutDown</option>
                    <option value="slideOutLeft">slideOutLeft</option>
                    <option value="slideOutRight">slideOutRight</option>
                    <option value="lightSpeedOut">lightSpeedOut</option>
                </optgroup>
                <optgroup label="확대">
                    <option value="zoomIn">zoomIn</option>
                    <option value="zoomInDown">zoomInDown</option>
                    <option value="zoomInLeft">zoomInLeft</option>
                    <option value="zoomInRight">zoomInRight</option>
                    <option value="zoomInUp">zoomInUp</option>
                    <option value="zoomOut">zoomOut</option>
                    <option value="zoomOutDown">zoomOutDown</option>
                    <option value="zoomOutLeft">zoomOutLeft</option>
                    <option value="zoomOutRight">zoomOutRight</option>
                    <option value="zoomOutUp">zoomOutUp</option>
                </optgroup>
            </select>
            <select name="item_alert_texteffectintype" class="select sizes">
                <option value="none">정방향</option>
                <option value="reverse">역방향</option>
                <option value="shuffle">랜덤</option>
            </select>
            <div id="effect_preview" class="effect_preview"><p data-placeholder="안녕하세요. 알림 텍스트 입니다" class="alert">안녕하세요. 알림 텍스트 입니다</p></div>
        </li>
        <li class="input_area pb30">
            <p class="input_label">팝업 표시 효과</p>
            <select name="item_alert_popupeffectin" class="select">
                <optgroup label="페이드">
                    <option value="fadeIn">fadeIn</option>
                    <option value="fadeInDown">fadeInDown</option>
                    <option value="fadeInLeft">fadeInLeft</option>
                    <option value="fadeInRight">fadeInRight</option>
                    <option value="fadeInUp">fadeInUp</option>
                </optgroup>
                <optgroup label="강조">
                    <option value="bounce">bounce</option>
                    <option value="flash">flash</option>
                    <option value="pulse">pulse</option>
                    <option value="rubberBand">rubberBand</option>
                    <option value="shake">shake</option>
                    <option value="swing">swing</option>
                    <option value="tada">tada</option>
                    <option value="wobble">wobble</option>
                    <option value="jello">jello</option>
                    <option value="hinge">hinge</option>
                    <option value="jackInTheBox">jackInTheBox</option>
                    <option value="rollIn">rollIn</option>
                </optgroup>
                <optgroup label="확대">
                    <option value="zoomIn">zoomIn</option>
                    <option value="zoomInDown">zoomInDown</option>
                    <option value="zoomInLeft">zoomInLeft</option>
                    <option value="zoomInRight">zoomInRight</option>
                    <option value="zoomInUp">zoomInUp</option>
                </optgroup>
                <optgroup label="플립">
                    <option value="flip">flip</option>
                    <option value="flipInX">flipInX</option>
                    <option value="flipInY">flipInY</option>
                </optgroup>
                <optgroup label="회전">
                    <option value="rotateIn">rotateIn</option>
                    <option value="rotateInDownLeft">rotateInDownLeft</option>
                    <option value="rotateInDownRight">rotateInDownRight</option>
                    <option value="rotateInUpLeft">rotateInUpLeft</option>
                    <option value="rotateInUpRight">rotateInUpRight</option>
                </optgroup>
                <optgroup label="슬라이드">
                    <option value="slideInUp">slideInUp</option>
                    <option value="slideInDown">slideInDown</option>
                    <option value="slideInLeft">slideInLeft</option>
                    <option value="slideInRight">slideInRight</option>
                    <option value="lightSpeedIn">lightSpeedIn</option>
                </optgroup>
                <optgroup label="바운스">
                    <option value="bounceIn">bounceIn</option>
                    <option value="bounceInDown">bounceInDown</option>
                    <option value="bounceInLeft">bounceInLeft</option>
                    <option value="bounceInRight">bounceInRight</option>
                    <option value="bounceInUp">bounceInUp</option>
                </optgroup>
            </select>
            <select name="item_alert_popupeffectout" class="select">
                <optgroup label="페이드">
                    <option value="fadeOut">fadeOut</option>
                    <option value="fadeOutDown">fadeOutDown</option>
                    <option value="fadeOutLeft">fadeOutLeft</option>
                    <option value="fadeOutRight">fadeOutRight</option>
                    <option value="fadeOutUp">fadeOutUp</option>
                </optgroup>
                <optgroup label="강조">
                    <option value="bounce">bounce</option>
                    <option value="flash">flash</option>
                    <option value="pulse">pulse</option>
                    <option value="rubberBand">rubberBand</option>
                    <option value="shake">shake</option>
                    <option value="swing">swing</option>
                    <option value="tada">tada</option>
                    <option value="wobble">wobble</option>
                    <option value="jello">jello</option>
                    <option value="hinge">hinge</option>
                    <option value="jackInTheBox">jackInTheBox</option>
                    <option value="rollOut">rollOut</option>
                    <option value="hinge">hinge</option>
                </optgroup>
                <optgroup label="확대">
                    <option value="zoomOut">zoomOut</option>
                    <option value="zoomOutDown">zoomOutDown</option>
                    <option value="zoomOutLeft">zoomOutLeft</option>
                    <option value="zoomOutRight">zoomOutRight</option>
                    <option value="zoomOutUp">zoomOutUp</option>
                </optgroup>
                <optgroup label="플립">
                    <option value="flip">flip</option>
                    <option value="flipOutX">flipOutX</option>
                    <option value="flipOutY">flipOutY</option>
                </optgroup>
                <optgroup label="회전">
                    <option value="rotateOut">rotateOut</option>
                    <option value="rotateOutDownLeft">rotateOutDownLeft</option>
                    <option value="rotateOutDownRight">rotateOutDownRight</option>
                    <option value="rotateOutUpLeft">rotateOutUpLeft</option>
                    <option value="rotateOutUpRight">rotateOutUpRight</option>
                </optgroup>
                <optgroup label="슬라이드">
                    <option value="slideOutUp">slideOutUp</option>
                    <option value="slideOutDown">slideOutDown</option>
                    <option value="slideOutLeft">slideOutLeft</option>
                    <option value="slideOutRight">slideOutRight</option>
                    <option value="lightSpeedOut">lightSpeedOut</option>
                </optgroup>
                <optgroup label="바운스">
                    <option value="bounceOut">bounceOut</option>
                    <option value="bounceOutDown">bounceOutDown</option>
                    <option value="bounceOutLeft">bounceOutLeft</option>
                    <option value="bounceOutRight">bounceOutRight</option>
                    <option value="bounceOutUp">bounceOutUp</option>
                </optgroup>
            </select>
            
            <div class="preview_popup">
                <div class="item_alert_popup">
                    <div class="alert_box">
                        <i class="fa fa-picture-o" aria-hidden="true"></i>
                        <p class="text">팝업 내용</p>
                    </div>
                </div>
            </div>
        </li>
        <li class="input_area">
            <p class="input_label">팝업 표시 시간</p>
            <div class="size_box">
                <div data-min="1" data-max="100" data-step="1" data-value="10" class="size_bar"></div>
                <input type="text" name="item_alert_showtime" data-type="style" data-style=".item_alert .alert{-webkit-transition:transform {val}s linear;-moz-transition:transform {val}s linear;-ms-transition:transform {val}s linear;-o-transition:transform {val}s linear;transition:transform {val}s linear}" readonly="readonly" class="input_size input_text value">
                <p class="unit">초</p>
            </div>
        </li>
        <li class="input_area">
            <p class="input_label tooltip_box">채팅 입력 시간<span class="tooltip"><i class="fa fa-info-circle info" aria-hidden="true"></i><span class="tooltip_text">후원 후 채팅 입력 시간내 입력한 채팅을 음성으로 읽어줍니다.</span></span></p>
            <div class="size_box">
                <div data-min="0" data-max="30" data-step="1" data-value="15" class="size_bar"></div>
                <input type="text" name="item_alert_voicetime" value="15" readonly="readonly" class="input_size input_text value">
                <p class="unit">초</p>
            </div>
        </li>
    </ul>
</div>

<!-- 사운드설정 -->
<div class="setup_title info_box">
    <p class="title">사운드</p>
</div>
<div class="setup_group menu_panel">
    <ul class="group_list">
        <li class="input_area">
            <p class="input_label">알림음 볼륨</p>
            <div class="size_box voice_box">
                <div data-min="0" data-max="100" data-step="1" data-value="50" class="size_bar"></div>
                <input type="text" name="item_alert_volume" value="50" readonly="readonly" class="input_size input_text value">
                <p class="unit">%</p>
                <a href="#" title="음성 재생" class="btns btn_voice_play"><i class="fa fa-play" aria-hidden="true"></i></a>
            </div>
        </li>
        <li class="input_area">
            <p class="input_label tooltip_box">음성 재생 속도<span class="tooltip"><i class="fa fa-info-circle info" aria-hidden="true"></i><span class="tooltip_text">음성을 느리게 혹은 빠르게 읽어줍니다.<br>1배속 : 기본</span></span></p>
            <div class="size_box">
                <div data-min="0.1" data-max="4" data-step="0.1" data-value="1" class="size_bar"></div>
                <input type="text" name="item_alert_voicerate" value="1" readonly="readonly" class="input_size input_text value">
                <p class="unit">배속</p>
            </div>
        </li>
        <li class="input_area">
            <p class="input_label">음성 성별</p>
            <label class="check_box">
                <input type="radio" name="item_alert_voicegender" value="woman" data-target=".item_alert" checked="checked" class="radio">
                <span class="icon"><i class="fa fa-check" aria-hidden="true"></i></span>
                <span class="text">여성</span>
            </label>
            <label class="check_box">
                <input type="radio" name="item_alert_voicegender" value="man" data-target=".item_alert" class="radio">
                <span class="icon"><i class="fa fa-check" aria-hidden="true"></i></span>
                <span class="text">남성</span>
            </label>
            <label class="check_box">
                <input type="radio" name="item_alert_voicegender" value="random" data-target=".item_alert" class="radio">
                <span class="icon"><i class="fa fa-check" aria-hidden="true"></i></span>
                <span class="text">랜덤</span>
            </label>
        </li>
    </ul>
</div>

<!-- 필터링설정 -->
<div class="setup_title info_box">
    <p class="title">필터링</p>
</div>
<div class="setup_group menu_panel">
    <ul class="group_list">
        <li class="input_area">
            <p class="input_label tooltip_box">음성 글자수 제한<span class="tooltip"><i class="fa fa-info-circle info" aria-hidden="true"></i><span class="tooltip_text">입력한 글자수 까지 음성으로 읽어줍니다.<br>빈칸일 경우 제한 없이 모두 읽어줍니다.</span></span></p>
            <input type="text" placeholder="" name="item_alert_voicelen" value="" class="input_text input_number sizes">
            <p class="unit">글자</p>
        </li>
        <li class="input_area">
            <p class="input_label tooltip_box">특수문자 제거<span class="tooltip"><i class="fa fa-info-circle info" aria-hidden="true"></i><span class="tooltip_text"><b>바@!_보</b> 같은 채팅의 특수문자를 제거하여 <b>바보</b>로 인식합니다.</span></span></p>
            <label class="check_box">
                <input type="checkbox" name="item_alert_filtersp" class="check">
                <span class="icon"><i class="fa fa-check" aria-hidden="true"></i></span>
            </label>
        </li>
        <li class="input_area">
            <p class="input_label tooltip_box">필터링 ID, 닉네임<span class="tooltip"><i class="fa fa-info-circle info" aria-hidden="true"></i><span class="tooltip_text">필터링에 포함된 ID또는 닉네임의 채팅은 음성에서 제외됩니다.</span></span></p>
            <div class="word_area filteridnick" data-type="filteridnick">
                <input type="text" placeholder="필터링 ID, 닉네임 입력" class="input_text sizem">
                <a href="#" class="btns green btn_addword"><i class="fa fa-plus" aria-hidden="true"></i>추가</a>
                <div class="word_list"></div>
            </div>
        </li>
        <li class="input_area">
            <p class="input_label tooltip_box">필터링 단어<span class="tooltip"><i class="fa fa-info-circle info" aria-hidden="true"></i><span class="tooltip_text">필터링 단어에 포함된 채팅은 음성에서 제외됩니다.</span></span></p>
            <div class="word_area filterword" data-type="filterword">
                <input type="text" placeholder="필터링 단어 입력" class="input_text sizem">
                <a href="#" class="btns green btn_addword"><i class="fa fa-plus" aria-hidden="true"></i>추가</a>
                <div class="word_list"></div>
            </div>
        </li>
    </ul>
</div>

<!-- 채팅 색 -->
<div class="setup_title info_box">
    <p class="title">텍스트색</p>
</div>
<div class="setup_group menu_panel">
    <ul class="group_list">
        <li class="input_area">
            <p class="input_label">폰트색</p>
            <input type="text" name="item_alert_color" data-type="style" data-style=".item_alert .text_box{color:{val}!important}.text_preview .item_alert{color:{val}!important}" value="#00ac00" class="input_text input_color">
        </li>
        <li class="input_area">
            <p class="input_label">닉네임색</p>
            <input type="text" name="item_alert_colornick" data-type="style" data-style=".item_alert .text_box .nick{color:{val}!important}.text_preview .item_alert .nick{color:{val}!important}" value="#ffc247" class="input_text input_color">
        </li>
        <li class="input_area">
            <p class="input_label">개수색</p>
            <input type="text" name="item_alert_colornum" data-type="style" data-style=".item_alert .text_box .value{color:{val}!important}.text_preview .item_alert .value{color:{val}!important}" value="#ffc247" class="input_text input_color">
        </li>
    </ul>
</div>
