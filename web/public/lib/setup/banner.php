<!-- 기본설정 -->
<div class="setup_title info_box">
    <p class="title">기본</p>
</div>
<div class="setup_group menu_panel">
    <ul class="group_list">
        <li class="input_area">
            <p class="input_label tooltip_box">창 투명도<span class="tooltip"><i class="fa fa-info-circle info" aria-hidden="true"></i><span class="tooltip_text">창 투명도를 설정합니다. (0% 보임, 100% 투명)</span></span></p>
            <div class="size_box">
                <div data-min="0" data-max="100" data-step="10" data-value="0" class="size_bar"></div>
                <input type="text" name="item_banner_opacity" data-target=".item_banner" readonly="readonly" class="input_size input_text value">
                <p class="unit">%</p>
            </div>
        </li>
        <?
            include $root_path."lib/setup/comm_customcss.php";
        ?>
    </ul>
</div>

<!-- 배너설정 -->
<div class="setup_title info_box">
    <p class="title">배너</p>
</div>
<div class="setup_group menu_panel">
    <ul class="group_list">
        <li class="input_area">
            <p class="input_label tooltip_box">배너 전환 효과<span class="tooltip"><i class="fa fa-info-circle info" aria-hidden="true"></i><span class="tooltip_text">배너 전환시 선택한 효과로 전환됩니다. <br>여러개 선택 가능</span></span></p>
            <ul id="effect_list" class="effect_list"></ul>
        </li>
        <li class="input_area">
            <p class="input_label">전환 효과 랜덤</p>
            <label class="check_box">
                <input type="checkbox" data-target=".item_banner" name="item_banner_effectrandom" class="check">
                <span class="icon"><i class="fa fa-check" aria-hidden="true"></i></span>
            </label>
        </li>
        <li class="input_area">
            <p class="input_label">배너 표시 시간</p>
            <div class="size_box">
                <div data-min="1" data-max="300" data-step="1" data-value="10" class="size_bar"></div>
                <input type="text" name="item_banner_showtime" data-target=".item_banner" readonly="readonly" class="input_size input_text value">
                <p class="unit">초</p>
            </div>
        </li>
        <li class="input_area">
            <p class="input_label">배너 전환 시간</p>
            <div class="size_box">
                <div data-min="0" data-max="5" data-step="0.1" data-value="0.5" class="size_bar"></div>
                <input type="text" name="item_banner_duration" data-target=".item_banner" readonly="readonly" class="input_size input_text value">
                <p class="unit">초</p>
            </div>
        </li>
        <li class="input_area">
            <p class="input_label tooltip_box">배너 이미지<span class="tooltip"><i class="fa fa-info-circle info" aria-hidden="true"></i><span class="tooltip_text">배너 이미지를 추가 삭제가 가능합니다.<br>숫자를 마우스로 드래그 드롭시 순서를 변경할 수 있습니다.</span></span></p>
            <ul id="banner_list" class="banner_list">
                <li class="banner_box">
                    <p class="number">1.</p>
                    <div class="btn_listbox">
                        <a href="#" class="btns btn_listdel"><i class="fa fa-minus" aria-hidden="true"></i></a>
                        <a href="#" class="btns btn_listadd"><i class="fa fa-plus" aria-hidden="true"></i></a>
                    </div>
                    <div class="file_upload_box file_img" data-type="bannerimg">
                        <div class="upload_box">
                            <p class="name"></p>
                            <div class="img_box">
                                <i class="fa fa-picture-o icon_file" aria-hidden="true"></i>
                                <p class="text">이미지를 선택해주세요</p>
                            </div>
                            <div class="upload_tooltip">
                                <a href="#" title="파일 선택" class="btns green btn_file_upload"><i class="fa fa-file-image-o" aria-hidden="true"></i>파일 선택</a>
                                <a href="#" title="외부 링크 설정" class="btns green btn_file_link"><i class="fa fa-link" aria-hidden="true"></i>외부 링크</a>
                                <a href="#" title="파일 삭제" class="btns gray btn_file_delete"><i class="fa fa-trash-o" aria-hidden="true"></i>파일 삭제</a>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </li>
    </ul>
</div>

