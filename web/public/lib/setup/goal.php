<!-- 기본설정 -->
<div class="setup_title info_box">
    <p class="title">기본</p>
</div>
<div class="setup_group menu_panel">
    <ul class="group_list">
        <li class="input_area">
            <p class="input_label tooltip_box">창 투명도<span class="tooltip"><i class="fa fa-info-circle info" aria-hidden="true"></i><span class="tooltip_text">창 투명도를 설정합니다. (0% 보임, 100% 투명)</span></span></p>
            <div class="size_box">
                <div data-min="0" data-max="100" data-step="10" data-value="0" class="size_bar"></div>
                <input type="text" name="item_goal_opacity" data-target=".item_goal" readonly="readonly" class="input_size input_text value">
                <p class="unit">%</p>
            </div>
        </li>
        <?
            include $root_path."lib/setup/comm_customcss.php";
        ?>
    </ul>
</div>

<!-- 그래프설정 -->
<div class="setup_title info_box">
    <p class="title">스타일</p>
</div>
<div class="setup_group menu_panel">
    <ul class="group_list">
        <li class="input_area goal_area">
            <p class="input_label tooltip_box">그래프 종류<span class="tooltip"><i class="fa fa-info-circle info" aria-hidden="true"></i><span class="tooltip_text">목표치 그래프 모양을 선택할 수 있습니다.</span></span></p>
            <label class="check_box">
                <input type="radio" name="item_goal_style" value="bar" data-target=".item_goal" data-toggle=".toggle_style" checked="checked" class="radio toggle_radio">
                <span class="icon"><i class="fa fa-check" aria-hidden="true"></i></span>
                <span class="text"><span class="bar">바</span></span>
            </label>
            <label class="check_box">
                <input type="radio" name="item_goal_style" value="circle" data-target=".item_goal" data-toggle=".toggle_style" class="radio toggle_radio">
                <span class="icon"><i class="fa fa-check" aria-hidden="true"></i></span>
                <span class="text"><span class="circle">원</span></span>
            </label>
            <label class="check_box">
                <input type="radio" name="item_goal_style" value="heart" data-target=".item_goal" data-toggle=".toggle_style" class="radio toggle_radio">
                <span class="icon"><i class="fa fa-check" aria-hidden="true"></i></span>
                <span class="text"><span class="box"><span class="heart">♡</span><span class="msg">하트</span></span></span>
            </label>
            <label class="check_box">
                <input type="radio" name="item_goal_style" value="stars" data-target=".item_goal" data-toggle=".toggle_style" class="radio toggle_radio">
                <span class="icon"><i class="fa fa-check" aria-hidden="true"></i></span>
                <span class="text"><span class="box"><span class="stars">☆</span><span class="msg">별</span></span></span>
            </label>
            <label class="check_box">
                <input type="radio" name="item_goal_style" value="arc" data-target=".item_goal" data-toggle=".toggle_style" class="radio toggle_radio">
                <span class="icon"><i class="fa fa-check" aria-hidden="true"></i></span>
                <span class="text"><span class="box"><span class="half">○</span><span class="msg">반원</span></span></span>
            </label>
            <label class="check_box">
                <input type="radio" name="item_goal_style" value="arcr" data-target=".item_goal" data-toggle=".toggle_style" class="radio toggle_radio">
                <span class="icon"><i class="fa fa-check" aria-hidden="true"></i></span>
                <span class="text"><span class="box"><span class="half halfr"><span>○</span></span><span class="msg">반원</span></span></span>
            </label>
        </li>
        <li class="input_area toggle_style toggle_bar">
            <p class="input_label">그래프 방향</p>
            <label class="check_box">
                <input type="radio" name="item_goal_dir" value="width" data-target=".item_goal" checked="checked" class="radio goal_width">
                <span class="icon"><i class="fa fa-check" aria-hidden="true"></i></span>
                <span class="text">가로</span>
            </label>
            <label class="check_box">
                <input type="radio" name="item_goal_dir" value="height" data-target=".item_goal" class="radio">
                <span class="icon"><i class="fa fa-check" aria-hidden="true"></i></span>
                <span class="text">세로</span>
            </label>
        </li>
        <li class="input_area">
            <p class="input_label">그래프 굵기</p>
            <div class="size_box">
                <div data-min="1" data-max="100" data-step="1" data-value="40" class="size_bar"></div>
                <input type="text" name="item_goal_border" data-type="style" data-style=".item_goal .goal_box{margin-left:-{valn2}px;margin-top:-{valn2}px;width:{val}px;height:{val}px}" readonly="readonly" class="input_size input_text value">
                <p class="unit">픽셀</p>
            </div>
        </li>
        <li class="input_area">
            <p class="input_label">텍스트만 표시</p>
            <label class="check_box">
                <input type="checkbox" name="item_goal_textonly" data-target=".item_goal" class="check goal_textonly">
                <span class="icon"><i class="fa fa-check" aria-hidden="true"></i></span>
            </label>
        </li>
    </ul>
</div>


<!-- 목표치설정 -->
<div class="setup_title info_box">
    <p class="title">목표치</p>
</div>
<div class="setup_group menu_panel">
    <ul class="group_list">
        <li class="input_area goal_list">
            <p class="input_label tooltip_box">목표치 종류<span class="tooltip"><i class="fa fa-info-circle info" aria-hidden="true"></i><span class="tooltip_text">목표치 종류를 설정할 수 있습니다. <br>여러개의 목표치 선택시 반복 시간마다 돌아가며 표시합니다.</span></span></p>
            <label class="check_box">
                <input type="checkbox" value="star" class="check">
                <span class="icon"><i class="fa fa-check" aria-hidden="true"></i></span>
                <span class="text"><?=$login_info->coin?></span>
            </label>
            <label class="check_box">
                <input type="checkbox" value="doosan" class="check">
                <span class="icon"><i class="fa fa-check" aria-hidden="true"></i></span>
                <span class="text">백두산</span>
            </label>
            <label class="check_box">
                <input type="checkbox" value="up" class="check">
                <span class="icon"><i class="fa fa-check" aria-hidden="true"></i></span>
                <span class="text">추천수</span>
            </label>
            <label class="check_box">
                <input type="checkbox" value="view" class="check">
                <span class="icon"><i class="fa fa-check" aria-hidden="true"></i></span>
                <span class="text">시청자수</span>
            </label>
            <label class="check_box">
                <input type="checkbox" value="fav" class="check">
                <span class="icon"><i class="fa fa-check" aria-hidden="true"></i></span>
                <span class="text">즐겨찾기수</span>
            </label>
        </li>
        <li class="input_area">
            <p class="input_label">시작 값</p>
            <input type="text" name="item_goal_startmin" data-min="0" value="0" class="input_text input_number">
        </li>
        <li class="input_area">
            <p class="input_label">목표 값</p>
            <input type="text" name="item_goal_startmax" data-min="1" value="100" class="input_text input_number">
        </li>
        <li class="input_area">
            <p class="input_label tooltip_box">목표치 텍스트<span class="tooltip"><i class="fa fa-info-circle info" aria-hidden="true"></i><span class="tooltip_text">입력한 목표치 텍스트 내용을 목표치 그래프 위에 표시합니다.<br><br><span class="title"><i class="fa fa-info-circle info" aria-hidden="true"></i>대체어</span><br>{목표종류} : 목표치 이름<br>예 : 오늘의 {목표종류} 목표치</span></span></p>
            <textarea name="item_goal_text" placeholder="오늘의 {목표종류} 목표치" class="input_text">오늘의 {목표종류} 목표치</textarea>
        </li>
        <li class="input_area">
            <p class="input_label tooltip_box">내부 텍스트<span class="tooltip"><i class="fa fa-info-circle info" aria-hidden="true"></i><span class="tooltip_text">입력한 내부 텍스트 내용을 목표치 그래프 안에 표시합니다.<br><br><span class="title"><i class="fa fa-info-circle info" aria-hidden="true"></i>대체어</span><br>{목표값} : 목표치 현재 값<br>{개} : 목표치 단위 (개, 명)<br>{목표퍼센트} : 목표치 현재 퍼센트<br>예 : {목표값}{개} ({목표퍼센트})</span></span></p>
            <textarea name="item_goal_textin" placeholder="{목표값}{개} ({목표퍼센트})" class="input_text">{목표값}{개} ({목표퍼센트})</textarea>
        </li>
        <li class="input_area">
            <p class="input_label tooltip_box">목표치 반복 시간<span class="tooltip"><i class="fa fa-info-circle info" aria-hidden="true"></i><span class="tooltip_text">반복 시간마다 목표치 그래프를 반복해서 표시합니다.</span></span></p>
            <div class="size_box">
                <div data-min="0" data-max="100" data-step="1" data-value="10" class="size_bar"></div>
                <input type="text" name="item_goal_repeat" data-target=".item_goal" readonly="readonly" class="input_size input_text value">
                <p class="unit">초</p>
            </div>
        </li>
        <li class="input_area">
            <p class="input_label tooltip_box">목표치 자동 증가<span class="tooltip"><i class="fa fa-info-circle info" aria-hidden="true"></i><span class="tooltip_text">목표치 달성시 목표 값이 자동으로 증가합니다.</span></span></p>
            <label class="check_box">
                <input type="checkbox" name="item_goal_completeauto" data-target=".item_goal" data-sub="item_goal_completeautoval" class="check check_use">
                <span class="icon"><i class="fa fa-check" aria-hidden="true"></i></span>
            </label>
        </li>
        <li class="input_area mt10 hide">
            <input type="text" name="item_goal_completeautoval" data-min="1" value="100" class="input_text input_number">
            <p class="unit">증가</p>
        </li>
    </ul>
</div>

<!-- 배경색설정 -->
<div class="setup_title info_box">
    <p class="title">그래프색</p>
</div>
<div class="setup_group menu_panel">
    <ul class="group_list">
        <li class="input_area">
            <p class="input_label">그래프 배경색</p>
            <div class="color_gradient">
                <a href="#" data-color="#cb2d3e,#ef473a" class="btn_color"></a>
                <a href="#" data-color="#FFB75E,#ED8F03" class="btn_color"></a>
                <a href="#" data-color="#F09819,#EDDE5D" class="btn_color"></a>
                <a href="#" data-color="#39ba51,#46e65a" class="btn_color"></a>
                <a href="#" data-color="#005C97,#363795" class="btn_color"></a>
                <a href="#" data-color="#6441A5,#2a0845" class="btn_color"></a>
                <a href="#" data-color="#bdc3c7,#2c3e50" class="btn_color"></a>
                <a href="#" data-color="#2C3E50,#4CA1AF" class="btn_color"></a>
                <a href="#" data-color="#616161,#9bc5c3" class="btn_color"></a>
                <a href="#" data-color="#7474BF,#348AC7" class="btn_color"></a>
                <a href="#" data-color="#ED4264,#FFEDBC" class="btn_color"></a>
                <a href="#" data-color="#1D976C,#93F9B9" class="btn_color"></a>
                <a href="#" data-color="#c21500,#ffc500" class="btn_color"></a>
            </div>
            <ul class="group_list sub_list mt10">
                <li class="input_area">
                    <p class="input_label">시작 배경색</p>
                    <input type="text" name="item_goal_colorstart" data-type="style" data-target=".item goal .goal_box" value="#1d976c" class="input_text input_color">
                </li>
                <li class="input_area">
                    <p class="input_label">끝 배경색</p>
                    <input type="text" name="item_goal_colorend" data-type="style" data-target=".item goal .goal_box" value="#93f9b9" class="input_text input_color">
                </li>
                <li class="input_area">
                    <p class="input_label">내부 배경색</p>
                    <input type="text" name="item_goal_colorinner" data-type="style" data-style=".item_goal .goal_inner{background-color:{val}}" value="#dddddd" class="input_text input_color">
                </li>
            </ul>
        </li>
        <li class="input_area">
            <p class="input_label">텍스트색</p>
            <ul class="group_list sub_list">
                <li class="input_area">
                    <p class="input_label">폰트색</p>
                    <input type="text" name="item_goal_colortext" data-type="style" data-style=".item_goal .text{color:{val}}" value="#FFFFFF" class="input_text input_color">
                </li>
                <li class="input_area">
                    <p class="input_label">내부 폰트색</p>
                    <input type="text" name="item_goal_colortextin" data-type="style" data-style=".item_goal .goal_textin{color:{val}}" value="#000000" class="input_text input_color">
                </li>
            </ul>
        </li>
        <li class="input_area">
            <p class="input_label tooltip_box">배경 에니메이션<span class="tooltip"><i class="fa fa-info-circle info" aria-hidden="true"></i><span class="tooltip_text">바 그래프 사용시 배경에 에니메이션 효과를 사용합니다.</span></span></p>
            <label class="check_box">
                <input type="checkbox" name="item_goal_progressani" data-target=".item_goal" checked="checked" class="check">
                <span class="icon"><i class="fa fa-check" aria-hidden="true"></i></span>
            </label>
        </li>
    </ul>
</div>