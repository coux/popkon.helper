<!-- 기본설정 -->
<div class="setup_title info_box">
    <p class="title">기본</p>
</div>
<div class="setup_group menu_panel">
    <ul class="group_list">
        <li class="input_area">
            <p class="input_label tooltip_box">창 투명도<span class="tooltip"><i class="fa fa-info-circle info" aria-hidden="true"></i><span class="tooltip_text">창 투명도를 설정합니다. (0% 보임, 100% 투명)</span></span></p>
            <div class="size_box">
                <div data-min="0" data-max="100" data-step="10" data-value="0" class="size_bar"></div>
                <input type="text" name="item_chat_opacity" data-target=".item_chat" readonly="readonly" class="input_size input_text value">
                <p class="unit">%</p>
            </div>
        </li>
        <?
            include $root_path."lib/setup/comm_customcss.php";
        ?>
    </ul>
</div>

<!-- 스타일설정 -->
<div class="setup_title info_box">
    <p class="title">스타일</p>
</div>
<div class="setup_group menu_panel">
    <ul class="group_list">
        <li class="input_area">
            <p class="input_label">채팅 테마</p>
            <div class="chat_theme_box">
                <label class="check_box">
                    <input type="radio" name="item_chat_theme" value="normal" data-target=".item_chat" checked="checked" class="radio">
                    <span class="icon"><i class="fa fa-check" aria-hidden="true"></i></span>
                    <span class="img_box"><img src="/resource/img/theme/normal.png" alt=""></span>
                    <span class="text"><?=$login_info->svrKor?></span>
                </label>
                <label class="check_box">
                    <input type="radio" name="item_chat_theme" value="kakaotalk" data-target=".item_chat" class="radio">
                    <span class="icon"><i class="fa fa-check" aria-hidden="true"></i></span>
                    <span class="img_box"><img src="/resource/img/theme/kakaotalk.png" alt=""></span>
                    <span class="text">카카오톡</span>
                </label>
                <label class="check_box">
                    <input type="radio" name="item_chat_theme" value="board" data-target=".item_chat" class="radio">
                    <span class="icon"><i class="fa fa-check" aria-hidden="true"></i></span>
                    <span class="img_box"><img src="/resource/img/theme/board.png" alt=""></span>
                    <span class="text">칠판</span>
                </label>
                <label class="check_box">
                    <input type="radio" name="item_chat_theme" value="bubble" data-target=".item_chat" class="radio">
                    <span class="icon"><i class="fa fa-check" aria-hidden="true"></i></span>
                    <span class="img_box"><img src="/resource/img/theme/bubble.png" alt=""></span>
                    <span class="text">풍선</span>
                </label>
                <label class="check_box">
                    <input type="radio" name="item_chat_theme" value="neon" data-target=".item_chat" class="radio">
                    <span class="icon"><i class="fa fa-check" aria-hidden="true"></i></span>
                    <span class="img_box"><img src="/resource/img/theme/neon.png" alt=""></span>
                    <span class="text">네온사인</span>
                </label>
            </div>
            <input type="hidden" name="item_chat_themecss" value="">
        </li>
        <li class="input_area">
            <p class="input_label tooltip_box">채팅 표시 효과<span class="tooltip"><i class="fa fa-info-circle info" aria-hidden="true"></i><span class="tooltip_text">채팅이 올라올때 표시되는 에니메이션 효과입니다.</span></span></p>
            <select name="item_chat_effect" data-target=".item_chat" class="select">
                <option value="none">없음</option>
                <optgroup label="슬라이드">
                    <option value="slideInUp">slideInUp</option>
                    <option value="slideInDown">slideInDown</option>
                    <option value="slideInLeft">slideInLeft</option>
                    <option value="slideInRight">slideInRight</option>
                    <option value="lightSpeedIn">lightSpeedIn</option>
                </optgroup>
                <optgroup label="페이드">
                    <option value="fadeIn">fadeIn</option>
                    <option value="fadeInDown">fadeInDown</option>
                    <option value="fadeInLeft">fadeInLeft</option>
                    <option value="fadeInRight">fadeInRight</option>
                    <option value="fadeInUp">fadeInUp</option>
                </optgroup>
                <optgroup label="효과">
                    <option value="bounce">bounce</option>
                    <option value="flash">flash</option>
                    <option value="pulse">pulse</option>
                    <option value="rubberBand">rubberBand</option>
                    <option value="shake">shake</option>
                    <option value="swing">swing</option>
                    <option value="tada">tada</option>
                    <option value="wobble">wobble</option>
                    <option value="jello">jello</option>
                    <option value="jackInTheBox">jackInTheBox</option>
                    <option value="rollIn">rollIn</option>
                </optgroup>
                <optgroup label="바운스">
                    <option value="bounceIn">bounceIn</option>
                    <option value="bounceInDown">bounceInDown</option>
                    <option value="bounceInLeft">bounceInLeft</option>
                    <option value="bounceInRight">bounceInRight</option>
                    <option value="bounceInUp">bounceInUp</option>
                </optgroup>
                <optgroup label="플립">
                    <option value="flip">flip</option>
                    <option value="flipInX">flipInX</option>
                    <option value="flipInY">flipInY</option>
                </optgroup>
                <optgroup label="회전">
                    <option value="rotateIn">rotateIn</option>
                    <option value="rotateInDownLeft">rotateInDownLeft</option>
                    <option value="rotateInDownRight">rotateInDownRight</option>
                    <option value="rotateInUpLeft">rotateInUpLeft</option>
                    <option value="rotateInUpRight">rotateInUpRight</option>
                </optgroup>
                <optgroup label="확대">
                    <option value="zoomIn">zoomIn</option>
                    <option value="zoomInDown">zoomInDown</option>
                    <option value="zoomInLeft">zoomInLeft</option>
                    <option value="zoomInRight">zoomInRight</option>
                    <option value="zoomInUp">zoomInUp</option>
                </optgroup>
            </select>
        </li>
        <li class="input_area">
            <p class="input_label">표시 효과 속도</p>
            <div class="size_box">
                <div data-min="0" data-max="5" data-step="0.1" data-value="0.2" class="size_bar"></div>
                <input type="text" name="item_chat_speed" data-type="style" data-style=".item_chat .chat_list li{-webkit-animation-duration:{val}s;-moz-animation-duration:{val}s;-o-animation-duration:{val}s;-ms-animation-duration:{val}s;animation-duration:{val}s}.item_chat .chat_list li{-webkit-transition-duration:{val}s;-moz-transition-duration:{val}s;-o-transition-duration:{val}s;-ms-transition-duration:{val}s;transition-duration:{val}s}" readonly="readonly" class="input_size input_text value">
                <p class="unit">초</p>
            </div>
        </li>
    </ul>
</div>

<!-- 스타일설정 -->
<div class="setup_title info_box">
    <p class="title">정렬</p>
</div>
<div class="setup_group menu_panel">
    <ul class="group_list">
        <li class="input_area">
            <p class="input_label">채팅 정렬</p>
            <label class="check_box">
                <input type="radio" name="item_chat_chatalign" value="normal" data-target=".item_chat" class="radio">
                <span class="icon"><i class="fa fa-check" aria-hidden="true"></i></span>
                <span class="text">기본</span>
            </label>
            <label class="check_box">
                <input type="radio" name="item_chat_chatalign" value="enter" data-target=".item_chat" checked="checked" class="radio">
                <span class="icon"><i class="fa fa-check" aria-hidden="true"></i></span>
                <span class="text">채팅 줄바꿈</span>
            </label>
            <label class="check_box">
                <input type="radio" name="item_chat_chatalign" value="align" data-target=".item_chat" class="radio">
                <span class="icon"><i class="fa fa-check" aria-hidden="true"></i></span>
                <span class="text">채팅 들여쓰기</span>
            </label>
            <label class="check_box">
                <input type="radio" name="item_chat_chatalign" value="alignright" data-target=".item_chat" class="radio">
                <span class="icon"><i class="fa fa-check" aria-hidden="true"></i></span>
                <span class="text">닉네임 우측 정렬</span>
            </label>
        </li>
        <li class="input_area">
            <p class="input_label">채팅 표시 방향</p>
            <!-- //TODO item_chat_bulletalign 에서 item_chat_chatdirect로 변경 -->
            <label class="check_box">
                <input type="radio" name="item_chat_chatdirect" value="left" data-target=".item_chat" checked="checked" class="radio">
                <span class="icon"><i class="fa fa-check" aria-hidden="true"></i></span>
                <span class="text">왼쪽으로</span>
            </label>
            <label class="check_box">
                <input type="radio" name="item_chat_chatdirect" value="right" data-target=".item_chat" class="radio">
                <span class="icon"><i class="fa fa-check" aria-hidden="true"></i></span>
                <span class="text">오른쪽으로</span>
            </label>
        </li>
    </ul>
</div>

<!-- 채팅설정 -->
<div class="setup_title info_box">
    <p class="title">채팅</p>
</div>
<div class="setup_group menu_panel">
    <ul class="group_list">
        <li class="input_area">
            <p class="input_label">아이콘 표시</p>
            <label class="check_box">
                <input type="checkbox" data-target=".item_chat" name="item_chat_bj" checked="checked" class="check">
                <span class="icon"><i class="fa fa-check" aria-hidden="true"></i></span>
                <span class="text"><span class="icon_bj"></span>BJ</span>
            </label>
            <label class="check_box">
                <input type="checkbox" data-target=".item_chat" name="item_chat_mng" checked="checked" class="check">
                <span class="icon"><i class="fa fa-check" aria-hidden="true"></i></span>
                <span class="text"><span class="icon_mng"></span>매니저</span>
            </label>
            <label class="check_box">
                <input type="checkbox" data-target=".item_chat" name="item_chat_fan" checked="checked" class="check">
                <span class="icon"><i class="fa fa-check" aria-hidden="true"></i></span>
                <span class="text"><span class="icon_fan"></span>팬</span>
            </label>
        </li>
        <li class="input_area">
            <p class="input_label">채팅 설정</p>
            <ul class="group_list">
				<li class="input_area">
                    <label class="check_box">
                        <input type="checkbox" data-target=".item_chat" name="item_chat_id" checked="checked" class="check">
                        <span class="icon"><i class="fa fa-check" aria-hidden="true"></i></span>
                        <span class="text">아이디 표시</span>
                    </label>
                </li>
                <li class="input_area">
                    <label class="check_box">
                        <input type="checkbox" data-target=".item_chat" name="item_chat_random" class="check">
                        <span class="icon"><i class="fa fa-check" aria-hidden="true"></i></span>
                        <span class="text">닉네임 랜덤 색 사용</span>
                    </label>
                </li>
                <li class="input_area">
                    <label class="check_box">
                        <input type="checkbox" data-target=".item_chat" name="item_chat_fade" class="check">
                        <span class="icon"><i class="fa fa-check" aria-hidden="true"></i></span>
                        <span class="text">채팅 페이드 아웃</span>
                    </label>
                </li>
            </ul>
        </li>
        <li class="input_area">
            <p class="input_label tooltip_box">채팅 자동 숨김<span class="tooltip"><i class="fa fa-info-circle info" aria-hidden="true"></i><span class="tooltip_text">설정된 시간이 지나면 자동으로 채팅이 사라집니다.</span></span></p>
            <ul class="group_list">
                <li class="input_area">
                    <label class="check_box">
                        <input type="checkbox" data-target=".item_chat" data-sub="item_chat_autotime" name="item_chat_autohide" class="check check_use">
                        <span class="icon"><i class="fa fa-check" aria-hidden="true"></i></span>
                        <span class="text">채팅 자동 숨김 사용</span>
                    </label>
                </li>
                <li class="input_area hide">
                    <div class="size_box">
                        <div data-min="1" data-max="120" data-step="1" data-value="30" class="size_bar"></div>
                        <input type="text" name="item_chat_autotime" data-target=".item_chat" readonly="readonly" class="input_size input_text value">
                        <p class="unit">초</p>
                    </div>
                </li>
            </ul>
        </li>
        <li class="input_area">
            <p class="input_label tooltip_box">후원 표시<span class="tooltip"><i class="fa fa-info-circle info" aria-hidden="true"></i><span class="tooltip_text">후원내용을 텍스트 또는 이미지로 표시합니다.</span></span></p>
            <ul class="group_list">
                <li class="input_area">
                    <label class="check_box">
                        <input type="radio" name="item_chat_giftshow" value="none" data-target=".item_chat" class="radio">
                        <span class="icon"><i class="fa fa-check" aria-hidden="true"></i></span>
                        <span class="text">사용 안함</span>
                    </label>
                    <label class="check_box">
                        <input type="radio" name="item_chat_giftshow" value="text" data-target=".item_chat" class="radio">
                        <span class="icon"><i class="fa fa-check" aria-hidden="true"></i></span>
                        <span class="text">텍스트</span>
                    </label>
                    <label class="check_box">
                        <input type="radio" name="item_chat_giftshow" value="image" data-target=".item_chat" checked="checked" class="radio">
                        <span class="icon"><i class="fa fa-check" aria-hidden="true"></i></span>
                        <span class="text">이미지</span>
                    </label>
                </li>
            </ul>
        </li>
    </ul>
</div>

<!-- 채팅알림설정 -->
<div class="setup_title info_box">
    <p class="title">알림</p>
</div>
<div class="setup_group menu_panel">
    <ul class="group_list">
        <li class="input_area">
            <p class="input_label tooltip_box">열혈팬 입장인사<span class="tooltip"><i class="fa fa-info-circle info" aria-hidden="true"></i><span class="tooltip_text">열혈팬 입장시 텍스트 또는 음성으로 표시합니다.</span></span></p>
            <label class="check_box">
                <input type="radio" name="item_chat_joinvoice" value="none" data-target=".item_chat" data-toggle=".toggle_joinvoice" checked="checked" class="radio toggle_radio">
                <span class="icon"><i class="fa fa-check" aria-hidden="true"></i></span>
                <span class="text">사용 안함</span>
            </label>
            <label class="check_box">
                <input type="radio" name="item_chat_joinvoice" value="text" data-target=".item_chat" data-toggle=".toggle_joinvoice" class="radio toggle_radio">
                <span class="icon"><i class="fa fa-check" aria-hidden="true"></i></span>
                <span class="text">텍스트</span>
            </label>
            <label class="check_box">
                <input type="radio" name="item_chat_joinvoice" value="voice" data-target=".item_chat" data-toggle=".toggle_joinvoice" class="radio toggle_radio">
                <span class="icon"><i class="fa fa-check" aria-hidden="true"></i></span>
                <span class="text">음성</span>
            </label>
            <ul class="group_list sub_list">
                <li class="input_area toggle_joinvoice toggle_text toggle_voice hide mt10">
                    <p class="input_label toggle_joinvoice toggle_text hide">인사 텍스트</p>
                    <p class="input_label toggle_joinvoice toggle_voice hide">인사 음성</p>
                    <input type="text" name="item_chat_jointext" class="input_text sizem" placeholder="{닉네임}님 어서오세요" value="{닉네임}님 어서오세요">
                    <a href="#" title="음성 재생" data-target="item_chat_jointext" class="btns btn_voice_play toggle_joinvoice toggle_voice hide"><i class="fa fa-play" aria-hidden="true"></i></a>
                </li>
            </ul>
        </li>
        <li class="input_area">
            <p class="input_label tooltip_box">채팅 알림<span class="tooltip"><i class="fa fa-info-circle info" aria-hidden="true"></i><span class="tooltip_text">새로운 채팅 내용을 음성으로 읽어주거나 알림음으로 알려줍니다.</span></span></p>
            <label class="check_box">
                <input type="radio" name="item_chat_speechtype" value="none" data-target=".item_chat" data-toggle=".toggle_speechtype" checked="checked" class="radio toggle_radio">
                <span class="icon"><i class="fa fa-check" aria-hidden="true"></i></span>
                <span class="text">사용 안함</span>
            </label>
            <label class="check_box">
                <input type="radio" name="item_chat_speechtype" value="voice" data-target=".item_chat" data-toggle=".toggle_speechtype" class="radio toggle_radio">
                <span class="icon"><i class="fa fa-check" aria-hidden="true"></i></span>
                <span class="text">음성</span>
            </label>
            <label class="check_box">
                <input type="radio" name="item_chat_speechtype" value="sound" data-target=".item_chat" data-toggle=".toggle_speechtype" class="radio toggle_radio">
                <span class="icon"><i class="fa fa-check" aria-hidden="true"></i></span>
                <span class="text">알림음</span>
            </label>
            <ul class="group_list sub_list">
                <li class="input_area toggle_speechtype toggle_voice chat_voice hide mt10">
                    <p class="input_label">알림 음성</p>
                    <input type="text" name="item_chat_speechtext" class="input_text sizem" placeholder="{닉네임}님이 {채팅}" value="{닉네임}님이 {채팅}">
                    <a href="#" title="음성 재생" data-target="item_chat_speechtext" class="btns btn_voice_play"><i class="fa fa-play" aria-hidden="true"></i></a>
                </li>
                <li class="input_area toggle_speechtype toggle_sound chat_speech_box hide mt10">
                    <p class="input_label">알림음</p>
                    <div class="file_upload_box file_audio" data-type="alarm">
                        <div class="upload_box">
                            <p class="name"></p>
                            <div class="img_box">
                                <i class="fa fa-music icon_file" aria-hidden="true"></i>
                                <p class="text">알림음을 선택해주세요</p>
                                <a href="#" class="btn_play"><i class="fa fa-play-circle-o" aria-hidden="true"></i></a>
                            </div>
                            <div class="upload_tooltip">
                                <a href="#" title="파일 선택" class="btns green btn_file_upload"><i class="fa fa-file-image-o" aria-hidden="true"></i>파일 선택</a>
                                <a href="#" title="외부 링크 설정" class="btns green btn_file_link"><i class="fa fa-link" aria-hidden="true"></i>외부 링크</a>
                                <a href="#" title="파일 삭제" class="btns gray btn_file_delete"><i class="fa fa-trash-o" aria-hidden="true"></i>파일 삭제</a>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </li>
    </ul>
</div>

<!-- 필터링 대체어 설정 -->
<div class="setup_title info_box">
    <p class="title">필터링</p>
</div>
<div class="setup_group menu_panel">
    <ul class="group_list">
        <li class="input_area">
            <p class="input_label tooltip_box">필터링 ID, 닉네임<span class="tooltip"><i class="fa fa-info-circle info" aria-hidden="true"></i><span class="tooltip_text">필터링에 포함된 ID또는 닉네임의 채팅은 제외됩니다.</span></span></p>
            <div class="word_area filteridnick" data-type="filteridnick">
                <input type="text" placeholder="필터링 ID, 닉네임 입력" class="input_text sizem">
                <a href="#" class="btns green btn_addword"><i class="fa fa-plus" aria-hidden="true"></i>추가</a>
                <div class="word_list"></div>
            </div>
        </li>
        <li class="input_area">
            <p class="input_label tooltip_box">필터링 단어<span class="tooltip"><i class="fa fa-info-circle info" aria-hidden="true"></i><span class="tooltip_text">필터링 단어에 포함된 채팅은 제외됩니다.</span></span></p>
            <div class="word_area filterword" data-type="filterword">
                <input type="text" placeholder="필터링 단어 입력" class="input_text sizem">
                <a href="#" class="btns green btn_addword"><i class="fa fa-plus" aria-hidden="true"></i>추가</a>
                <div class="word_list"></div>
            </div>
        </li>
    </ul>
</div>

<!-- 채팅 색 -->
<div class="setup_title info_box">
    <p class="title">채팅색</p>
</div>
<div class="setup_group column_02 menu_panel">
    <ul class="group_list">
        <li class="input_area">
            <p class="input_label"><img src="//pic.popkontv.com/images/www/live/assets/chatlist/icon_mc_man.png" alt="" class="ic">BJ 닉네임</p>
            <input type="text" name="item_chat_colorbj" data-type="style" data-style=".item_chat li.grade1 p.nick{color:{val}}" value="#00ade0" class="input_text input_color">
        </li>
        <li class="input_area">
            <p class="input_label"><img src="//pic.popkontv.com/images/www/live/assets/chatlist/icon_mc_man.png" alt="" class="ic">BJ 채팅</p>
            <input type="text" name="item_chat_colorbjchat" data-type="style" data-style=".item_chat li.grade1 p.text{color:{val}}" value="#2a2a2a" class="input_text input_color">
        </li>
        <li class="input_area">
            <p class="input_label"><img src="//pic.popkontv.com/images/www/live/assets/chatlist/icon_manager.png" alt="" class="ic">매니저 닉네임</p>
            <input type="text" name="item_chat_colormng" data-type="style" data-style=".item_chat li.grade2 p.nick{color:{val}}" value="#00ade0" class="input_text input_color">
        </li>
        <li class="input_area">
            <p class="input_label"><img src="//pic.popkontv.com/images/www/live/assets/chatlist/icon_manager.png" alt="" class="ic">매니저 채팅</p>
            <input type="text" name="item_chat_colormngchat" data-type="style" data-style=".item_chat li.grade2 p.text{color:{val}}" value="#2a2a2a" class="input_text input_color">
        </li>
        <li class="input_area">
            <p class="input_label"><img src="//pic.popkontv.com/images/www/live/assets/chatlist/icon_fan_01.png" alt="" class="ic">VIP 닉네임</p>
            <input type="text" name="item_chat_colorvip" data-type="style" data-style=".item_chat li.level0:not(.grade1):not(.grade2) p.nick{color:{val}}" value="#00ade0" class="input_text input_color">
        </li>
        <li class="input_area">
            <p class="input_label"><img src="//pic.popkontv.com/images/www/live/assets/chatlist/icon_fan_01.png" alt="" class="ic">VIP 채팅</p>
            <input type="text" name="item_chat_colorvipchat" data-type="style" data-style=".item_chat li.level0:not(.grade1):not(.grade2) p.text{color:{val}}" value="#2a2a2a" class="input_text input_color">
        </li>
        <li class="input_area">
            <p class="input_label"><img src="//pic.popkontv.com/images/www/live/assets/chatlist/icon_fan_02.png" alt="" class="ic">팬 닉네임</p>
            <input type="text" name="item_chat_colorfan" data-type="style" data-style=".item_chat li.level1:not(.grade1):not(.grade2) .nick{color:{val}}.item_chat li.level2:not(.grade1):not(.grade2) .nick{color:{val}}.item_chat li.level3:not(.grade1):not(.grade2) .nick{color:{val}}.item_chat li.level4:not(.grade1):not(.grade2) .nick{color:{val}}" value="#00ade0" class="input_text input_color">
        </li>
        <li class="input_area">
            <p class="input_label"><img src="//pic.popkontv.com/images/www/live/assets/chatlist/icon_fan_02.png" alt="" class="ic">팬 채팅</p>
            <input type="text" name="item_chat_colorfanchat" data-type="style" data-style=".item_chat li.level1:not(.grade1):not(.grade2) .text{color:{val}}.item_chat li.level2:not(.grade1):not(.grade2) .text{color:{val}}.item_chat li.level3:not(.grade1):not(.grade2) .text{color:{val}}.item_chat li.level4:not(.grade1):not(.grade2) .text{color:{val}}" value="#2a2a2a" class="input_text input_color">
        </li>
        <li class="input_area">
            <p class="input_label">일반 닉네임</p>
            <input type="text" name="item_chat_colornon" data-type="style" data-style=".item_chat li .nick{color:{val}}" value="#00ade0" class="input_text input_color">
        </li>
        <li class="input_area">
            <p class="input_label">일반 채팅</p>
            <input type="text" name="item_chat_colornonchat" data-type="style" data-style=".item_chat li .text{color:{val}}" value="#2a2a2a" class="input_text input_color">
        </li>
        <li class="input_area">
            <p class="input_label">후원</p>
            <input type="text" name="item_chat_colorgift" data-type="style" data-style=".item_chat .chat_list li.star .text{color:{val}!important}" value="#00dc5d" class="input_text input_color">
        </li>
        <li class="input_area">
            <p class="input_label">추천</p>
            <input type="text" name="item_chat_colorjoin" data-type="style" data-style=".item_chat .chat_list li.up .text{color:{val}!important}.item_chat .chat_list li.join .text{color:{val}!important}" value="#ffc81f" class="input_text input_color">
        </li>         
    </ul>
</div>