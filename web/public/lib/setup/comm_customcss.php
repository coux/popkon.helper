<li class="input_area">
    <p class="input_label tooltip_box">추가 스타일<span class="tooltip"><i class="fa fa-info-circle info" aria-hidden="true"></i><span class="tooltip_text">CSS를 사용하여 스타일을 수정 및 추가할 수 있습니다.<br>※ 잘못된 CSS 사용시 레이아웃이 깨질 수 있으니 주의해주세요.</span></span></p>
    <label class="check_box">
        <input type="checkbox" name="item_<?=$page_id?>_customstyle" data-target=".item_<?=$page_id?>" data-sub="item_<?=$page_id?>_customstylecss" class="check check_use">
        <span class="icon"><i class="fa fa-check" aria-hidden="true"></i></span>
        <span class="text">추가 스타일 사용</span>
    </label>
</li>
<li class="input_area csseditor_box hide mt0">
    <div class="customstyle_box mt10">
        <textarea name="item_<?=$page_id?>_customstylecss" tabindex="-1" data-target=".item_<?=$page_id?>" disabled="disabled" id="input_customstylecss" class="input_text"></textarea>
    </div>
    <div class="css_editor_box">
        <textarea mode="css" id="css_editor"></textarea>
    </div>
</li>