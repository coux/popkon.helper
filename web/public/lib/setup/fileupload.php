<?
    header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Credentials: true");
    header("Access-Control-Allow-Methods: GET, POST");
    header("Access-Control-Allow-Headers: X-Requested-With");
    header("Content-Type:application/json; charset=utf-8");
    
    $root_path = $_SERVER["DOCUMENT_ROOT"]."/";
    include_once $root_path."lib/common.php";
    $user_key = new userKey($popkon_keycode);
    
    $result = new stdClass();
    $result->result = "";
    
    $get_type = isset($_POST["type"]) ? $_POST["type"] : "";
    $get_key = isset($_POST["key"]) ? $_POST["key"] : "";
    
    if($get_key != "" && $get_type != "") {
        $get_rootpath = $root_path."data/save/".$get_key;
        
        if($get_type == "upload") {
            if(isset($_FILES["popup_file"])) {
                $filename = $_FILES["popup_file"]["tmp_name"];
                if(isset($filename) && !empty($filename)) {
                    $files = $_FILES["popup_file"]["name"];
                    $filepath = strtolower(pathinfo($files, PATHINFO_EXTENSION));
                    $get_mime = "";
                    if($filepath == "gif" || $filepath == "png" || $filepath == "jpg" || $filepath == "jpeg") {
                        $get_mime = "img";
                    }
                    else if($filepath == "mp3" || $filepath == "wav" || $filepath == "ogg") {
                        $get_mime = "audio";
                    }
                    else {
                        $result->result = "wrong";
                    }
                    
                    if($get_mime != "") {
                        $get_id = uniqid();
                        if(!file_exists($get_rootpath)) {
                            mkdir($get_rootpath);
                        }
                        $get_path = $root_path."data/save/".$get_key."/".$get_mime;
                        if(!file_exists($get_path)) {
                            mkdir($get_path);
                        }
                        
                        $get_imgname = $get_id.".".$filepath;
                        $get_name = $get_path."/".$get_imgname;
                        $get_url = "/data/save/".$get_key."/".$get_mime."/".$get_imgname;
                        
                        if(move_uploaded_file($filename, $get_name)) {
                            //파일리스트 저장
                            $get_list = new stdClass();
                            $get_filelist = $get_rootpath."/filelist.json";
                            if(file_exists($get_filelist)) {
                                $get_filedata = file_get_contents($get_filelist);
                                if($get_filedata != "") {
                                    $get_list = json_decode($get_filedata);
                                }
                            }
                            
                            if(!isset($get_list->$get_id)) {
                                $get_list->$get_id = new stdClass;
                            }
                            
                            $get_list->$get_id->mime = $get_mime;
                            $get_list->$get_id->url = $get_url;
                            $get_list->$get_id->name = $files;
                            $get_list->$get_id->size = $_FILES["popup_file"]["size"];
                            $get_list->$get_id->time = date('YmdHis');
                            
                            file_put_contents($get_filelist, json_encode($get_list));
                            $result->result = "save";
                            $result->list = $get_list;
                            $result->mime = $get_mime;
                        }
                        else {
                            $result->error = $out;
                        }
                    }
                }
            }
        }
        else if($get_type == "list") {
            $get_list = new stdClass();
            $get_filelist = $get_rootpath."/filelist.json";
            if(file_exists($get_filelist)) {
                $get_filedata = file_get_contents($get_filelist);
                if($get_filedata != "") {
                    $get_list = json_decode($get_filedata);
                }
                $result->result = "load";
                $result->list = $get_list;
            }
        }
        else if($get_type == "delete") {
            $get_url = isset($_POST["url"]) ? $_POST["url"] : "";
            $get_id = isset($_POST["id"]) ? $_POST["id"] : "";
            if($get_url != "") {
                $get_path = $root_path.$get_url;
                if(file_exists($get_path)) {
                    unlink($get_path);
                    
                    $get_list = new stdClass();
                    $get_filelist = $get_rootpath."/filelist.json";
                    if(file_exists($get_filelist)) {
                        $get_filedata = file_get_contents($get_filelist);
                        if($get_filedata != "") {
                            $get_list = json_decode($get_filedata);
                        }
                    }
                    
                    if(isset($get_list->$get_id)) {
                        unset($get_list->$get_id);
                        file_put_contents($get_filelist, json_encode($get_list));
                        $result->result = "del";
                        $result->list = $get_list;
                    }
                }
            }
        }
    }

    echo json_encode($result);
?>
