<!-- 업데이트 내용 (위쪽이 최신) -->
<ul id="update_list" class="update_list">
    <!-- <li class="update_box">
        <p class="update_title">목표치의 종류에 백두산이 추가됐습니다.</p>
        <p class="update_date">2019-01-21</p>
        <div class="update_content">
            <p>목표치 페이지의 목표치 종류에 백두산이 추가됐습니다.</p>
            <p>1개부터 순차적으로 선물을 받을 경우 두산 목표치가 증가합니다.</p>
        </div>
    </li> -->
</ul>


<?
    include $root_path."lib/header.php";
?>

<div id="content">
    <div class="bbs_area">
        <div class="contains">
            <div class="setup_desc">
                <h3 class="title">업데이트</h3>
            </div>
            
            <div class="bbs_wrap">
                <table class="bbs">
                    <colgroup>
                        <col style="width:90px">
                        <col>
                        <col style="width:140px">
                    </colgroup>
                    <thead>
                        <tr>
                            <th>번호</th>
                            <th>제목</th>
                            <th>작성일</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
            
            <!-- <div class="bbs_page">
                <a href="#" class="page active">1</a>
                <a href="#" class="page">2</a>
                <a href="#" class="page">3</a>
                <a href="#" class="page">4</a>
                <a href="#" class="page">5</a>
            </div> -->
            
        </div>
    </div>
</div>

<div id="popup_bbs" class="popup_window">
    <div class="popup_area">
        <div class="popup_header">
            <p class="title"></p>
            <a href="#" title="파일 닫기" class="btn_close"><i class="fa fa-times" aria-hidden="true"></i></a>
        </div>
        <div class="popup_content">
        </div>
        <div class="popup_footer">
            <div class="file_btn">
                <a href="#" class="btns btn_close">닫기</a>
            </div>
        </div>
    </div>
</div>

<?
    include $root_path."lib/footer.php";
?>
