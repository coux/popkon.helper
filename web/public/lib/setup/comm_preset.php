<div class="preset_area">
    <div class="setup_title info_box">
        <p class="title tooltip_box">프리셋<span class="tooltip"><i class="fa fa-info-circle info" aria-hidden="true"></i><span class="tooltip_text">다양한 형태의 설정내용을 프리셋으로 저장 및 불러오기가 가능합니다.<br>프리셋에 마우스 오버시 이름 변경 및 삭제가 가능하며, 마우스로 드래그드롭시 프리셋 위치 변경이 가능합니다.<br>우측 콤보박스에서 미리정의된 프리셋을 선택 후 추가할 수 있습니다.<br>※ 프리셋마다 저장되는 URL이 다릅니다.</span></span></p>
    </div>
    <div class="setup_group menu_panel">
        <div class="preset_list">
            <ul class="preset_listbox"></ul>
            <div class="btn_box">
                <!-- <select class="select select_preset">
                    <option value="">새프리셋</option>
                </select> -->
                <a href="#" title="추가" class="btns green btn_add"><i class="fa fa-plus" aria-hidden="true"></i>프리셋추가</a>
            </div>
        </div>
        
        <div class="url_area">
            <input type="text" id="hp_url" value="<?=$login_info->url?>?pcode=<?=$login_info->asp?>" class="input_text">
            <div class="btn_box">
                <button data-clipboard-action="copy" data-clipboard-target="#hp_url" class="btns btn_urlcopy"><i class="fa fa-files-o" aria-hidden="true"></i>URL 복사</button>
                <a href="#" class="btns btn_urlview"><i class="fa fa-television" aria-hidden="true"></i>화면 보기</a>
            </div>
        </div>
        <p class="warn_txt"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>URL은 보안을 위해 공유 및 화면에 노출을 금지합니다.</p>
    </div>
</div>