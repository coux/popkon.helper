<?
    include $root_path."lib/header.php";
?>

<div id="content">
    <div class="contains">
        <div class="content_area content_<?=$page_id?>">
            <?
                include $root_path."lib/setup/comm_lnb.php";
            ?>
            
            <div class="setup_area">
                <!-- 설명 -->
                <div class="setup_desc">
                    <? if($page_id == "chat") { ?>
                        <h3 class="title">채팅</h3>
                        <p>채팅창을 다양한 테마와 스타일로 화면에 표시할 수 있습니다.</p>
                    <? } else if($page_id == "alert") { ?>
                        <h3 class="title">알림</h3>
                        <p>후원 및 추천 내용을 알림으로 표시해주며, 후원한 시청자의 채팅 내용을 음성으로 읽어줍니다.</p>
                    <? } else if($page_id == "goal") { ?>
                        <h3 class="title">목표치</h3>
                        <p>후원 및 시청자수 등의 정보를 다양한 형태의 그래프로 표시할 수 있습니다.</p>
                    <? } else if($page_id == "subtitle") { ?>
                        <h3 class="title">후원자막</h3>
                        <p>후원 내용을 개수별, 최대개수, 순위, 최근 후원 등의 다양한 형태로 표시할 수 있습니다.</p>
                    <? } else if($page_id == "text") { ?>
                        <h3 class="title">텍스트자막</h3>
                        <p>화면에 표시할 텍스트를 편집할 수 있습니다.</p>
                    <? } else if($page_id == "banner") { ?>
                        <h3 class="title">배너</h3>
                        <p>프로필 및 광고등의 이미지를 다양한 효과로 표시할 수 있습니다.</p>
                    <? } else if($page_id == "timer") { ?>
                        <h3 class="title">시계</h3>
                        <p>화면에 표시할 시계를 설정할 수 있습니다.</p>
                    <? } ?>
                    
                </div>
                
                <!-- 프리셋 -->
                <?
                    include $root_path."lib/setup/comm_preset.php";
                ?>
                
                <!-- 페이지 설정 -->
                <?
                    include $root_path."lib/setup/".$page_id.".php";
                ?>
                
                <!-- 폰트 배경 -->
                <?
                    if($page_id != "text" && $page_id != "banner" && $page_id != "timer") {
                        include $root_path."lib/setup/comm_fontbg.php";
                    }
                ?>
                
                <div class="setup_btn">
                    <div class="btn_wrap">
                        <a href="#" title="위로" class="btn_top"><i class="fa fa-chevron-up" aria-hidden="true"></i></a>
                        <p class="msg">설정을 저장해주세요.</p>
                        <!-- <a href="#" id="btn_page_url" class="btns btn_urlcopy"><i class="fa fa-files-o" aria-hidden="true"></i>URL 복사</a> -->
                        <a href="#" id="btn_page_save" class="btns green btn_save"><i class="fa fa-check" aria-hidden="true"></i>설정 저장</a>
                        <a href="#" id="btn_page_reset" class="btns gray btn_reset"><i class="fa fa-refresh" aria-hidden="true"></i>설정 초기화</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?
    include $root_path."lib/footer.php";
    include $root_path."lib/setup/comm_filemanager.php";
?>

<div id="popup_mask">
    <div class="loader"></div>
</div>

<div id="popup_external" class="popup_window">
    <div class="popup_area">
        <div class="popup_header">
            <p class="title"><i class="fa fa-link" aria-hidden="true"></i>외부 링크</p>
            <a href="#" title="파일 닫기" class="btn_close"><i class="fa fa-times" aria-hidden="true"></i></a>
        </div>
        <div class="popup_content">
            <p class="info_txt img"><i class="fa fa-info-circle icon_info" aria-hidden="true"></i>이미지 외부 링크 URL을 입력해주세요. 예: http://example.com/img.png</p>
            <p class="info_txt audio"><i class="fa fa-info-circle icon_info" aria-hidden="true"></i>사운드 외부 링크 URL을 입력해주세요. 예: http://example.com/audio.mp3</p>
            <div class="input_box">
                <input type="text" placeholder="외부 링크 URL을 입력해주세요." class="input_text input_external">
            </div>
        </div>
        <div class="popup_footer">
            <div class="file_btn">
                <a href="#" class="btns green btn_select"><i class="fa fa-check" aria-hidden="true"></i>선택</a>
                <a href="#" class="btns btn_cancel"><i class="fa fa-times" aria-hidden="true"></i>취소</a>                
            </div>
        </div>
    </div>
</div>
