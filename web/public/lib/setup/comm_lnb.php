<div class="lnb_area">
    <div class="menu_area">
        <div class="user_box">
            <div class="user">
			 <? if($login_info->profile) { ?>
				<img src="<?=$login_info->profile?>" alt="" class="profile">
			<? } ?>
			</div>
			<div><?=$login_info->name."(".$login_info->id.")"?></div>
        </div>
		
    </div>
    <div class="preview_area">
        <div class="preview_box">
            <? if($page_id == "chat") { ?>
                <div class="item_box item_chat">
                    <div class="item_area">
                        <ul class="chat_list"></ul>
                    </div>
                </div>
            <? } else if($page_id == "alert") { ?>
                <div class="item_box item_alert">
                    <div class="item_area item_alert_area">
                        <div class="alert_box"></div>
                    </div>
                </div>
            <? } else if($page_id == "goal") { ?>
                <div class="item_box item_goal">
                    <div class="item_area">
                        <div class="goal_bar_area">
                            <div class="goal_box">
                                <div class="goal_text text"></div>
                                <div class="goal_bar"><div class="goal_inner animate"></div></div>
                                <div class="goal_start text"><span class="value"></span><span class="val"></span></div>
                                <div class="goal_end text"><span class="value"></span><span class="val"></span></div>
                                <div class="goal_textin textin"></div>
                            </div>
                        </div>
                        <div class="goal_circle_area">
                            <div class="goal_box">
                                <div class="text_box">
                                    <div class="goal_text text"></div>
                                    <div class="textin_box"><div class="goal_textin textin"></div></div>
                                </div>
                            </div>
                            <div class="goal_progress"></div>
                        </div>
                    </div>
                </div>
            <? } else if($page_id == "subtitle") { ?>
                <div class="item_box item_subtitle">
                    <div class="subtitle"></div>
                </div>
            <? } else if($page_id == "text") { ?>
                <div class="item_box item_text"></div>
            <? } else if($page_id == "banner") { ?>
                <div class="item_box item_banner">
                    <div class="img_box"><i class="fa fa-picture-o" aria-hidden="true"></i><span class="text">배너 이미지를 선택해주세요.</span></div>
                </div>
            <? } else if($page_id == "timer") { ?>
                <div class="item_box item_timer"></div>
            <? } ?>
        </div>
    </div>
    
    <div class="test_area">
        <p class="lnb_title">원격 도구</p>
        <div class="test_box">
            <p class="input_label tooltip_box">테스트<span class="tooltip"><i class="fa fa-info-circle info" aria-hidden="true"></i><span class="tooltip_text">기능 확인을 위해 테스트 내용을 전송합니다.</span></span></p>
            <input type="text" id="test_star" value="100" class="input_text">
            <a href="#" data-type="star" class="btns btn_test"><?=$login_info->coin?></a>
            <a href="#" data-type="up" class="btns btn_test">추천</a>
        </div>
        <div class="test_box">
            <p class="input_label tooltip_box">제어<span class="tooltip"><i class="fa fa-info-circle info" aria-hidden="true"></i><span class="tooltip_text">사용중인 기능을 제어합니다. <br>초기화시 채팅 및 알림의 내용이 삭제되고, 목표치 및 후원자막의 후원 내역이 초기화됩니다.</span></span></p>
            <a href="#" data-type="remove" class="btns btn_test">초기화</a>
            <a href="#" data-type="refresh" class="btns btn_test">새로고침</a>
        </div>
    </div>
</div>