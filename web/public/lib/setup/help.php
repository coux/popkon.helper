<?
    include $root_path."lib/header.php";
?>

<div id="content">
    <div class="contains">
        <div class="content_area help_area">
            <div class="lnb_area">
                <ul class="help_menu">
                    <li><a href="#" class="btn_menu active">도우미기능</a></li>
                    <li><a href="#" class="btn_menu">PC설정</a></li>
                    <li><a href="#" class="btn_menu">모바일설정</a></li>
                </ul>
            </div>
            <div class="setup_area">
                <!-- 도우미기능 -->
                <div class="help_panel active">
                    <div class="setup_desc">
                        <h3 class="title">도우미기능</h3>
                        <p>도우미기능 설정을 도와드립니다.</p>
                    </div>
                    
                    <div class="help_tab">
                        <a href="#" class="btn_tab active">채팅 설정</a>
                        <a href="#" class="btn_tab">알림 설정</a>
                        <a href="#" class="btn_tab">목표치 설정</a>
                        <a href="#" class="btn_tab">후원자막 설정</a>
                        <a href="#" class="btn_tab">텍스트자막 설정</a>
                        <a href="#" class="btn_tab">배너 설정</a>
                    </div>
                    
                    <div class="tab_panel active">
                        <img src="/resource/img/help/chat.png" alt="">
                    </div>
                    <div class="tab_panel">
                        <img src="/resource/img/help/alert.png" alt="">
                    </div>
                    <div class="tab_panel">
                        <img src="/resource/img/help/goal.png" alt="">
                    </div>
                    <div class="tab_panel">
                        <img src="/resource/img/help/subtitle.png" alt="">
                    </div>
                    <div class="tab_panel">
                        <img src="/resource/img/help/text.png" alt="">
                    </div>
                    <div class="tab_panel">
                        <img src="/resource/img/help/banner.png" alt="">
                    </div>
                </div>
                
                <!-- pc설정 -->
                <div class="help_panel">
                    <div class="setup_desc">
                        <h3 class="title">PC설정</h3>
                        <p>PC 설정을 도와드립니다.</p>
                    </div>
                    
                    <img src="/resource/img/help/pc.png?ver=20181209" alt="">
                </div>
                
                <!-- 모바일설정 -->
                <div class="help_panel">
                    <div class="setup_desc">
                        <h3 class="title">모바일설정</h3>
                        <p>모바일 설정을 도와드립니다.</p>
                    </div>
                    
                    <div class="help_tab">
                        <a href="#" class="btn_tab active">카메라 방송</a>
                        <a href="#" class="btn_tab">라이브캠 방송</a>
                    </div>
                    
                    <div class="tab_panel active">
                        <img src="/resource/img/help/mobile_camera.png" alt="">
                    </div>
                    <div class="tab_panel">
                        <img src="/resource/img/help/mobile_livecam.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?
    include $root_path."lib/footer.php";
?>
