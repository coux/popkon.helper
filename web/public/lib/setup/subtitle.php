<!-- 기본설정 -->
<div class="setup_title info_box">
    <p class="title">기본</p>
</div>
<div class="setup_group menu_panel">
    <ul class="group_list">
        <li class="input_area">
            <p class="input_label tooltip_box">창 투명도<span class="tooltip"><i class="fa fa-info-circle info" aria-hidden="true"></i><span class="tooltip_text">창 투명도를 설정합니다. (0% 보임, 100% 투명)</span></span></p>
            <div class="size_box">
                <div data-min="0" data-max="100" data-step="10" data-value="0" class="size_bar"></div>
                <input type="text" name="item_subtitle_opacity" data-target=".item_subtitle" readonly="readonly" class="input_size input_text value">
                <p class="unit">%</p>
            </div>
        </li>
        <?
            include $root_path."lib/setup/comm_customcss.php";
        ?>
    </ul>
</div>

<!-- 자막설정 -->
<div class="setup_title info_box">
    <p class="title">자막</p>
</div>
<div class="setup_group menu_panel">
    <ul class="group_list">
        <li class="input_area">
            <p class="input_label tooltip_box">후원 자막 종류<span class="tooltip"><i class="fa fa-info-circle info" aria-hidden="true"></i><span class="tooltip_text">후원 내용을 다양한 형태로 표시할 수 있습니다.</span></span></p>
            <label class="check_box">
                <input type="radio" data-target=".item_subtitle" name="item_subtitle_startype" data-toggle=".toggle_startype" value="list" checked="checked" class="radio toggle_radio">
                <span class="icon"><i class="fa fa-check" aria-hidden="true"></i></span>
                <span class="text">개수별</span>
            </label>
            <label class="check_box">
                <input type="radio" data-target=".item_subtitle" name="item_subtitle_startype" data-toggle=".toggle_startype" value="add" class="radio toggle_radio">
                <span class="icon"><i class="fa fa-check" aria-hidden="true"></i></span>
                <span class="text">합산</span>
            </label>
            <label class="check_box">
                <input type="radio" data-target=".item_subtitle" name="item_subtitle_startype" data-toggle=".toggle_startype" value="total" class="radio toggle_radio">
                <span class="icon"><i class="fa fa-check" aria-hidden="true"></i></span>
                <span class="text">최대개수</span>
            </label>
            <label class="check_box">
                <input type="radio" data-target=".item_subtitle" name="item_subtitle_startype" data-toggle=".toggle_startype" value="rank" class="radio toggle_radio">
                <span class="icon"><i class="fa fa-check" aria-hidden="true"></i></span>
                <span class="text">순위</span>
            </label>
            <label class="check_box">
                <input type="radio" data-target=".item_subtitle" name="item_subtitle_startype" data-toggle=".toggle_startype" value="recent" class="radio toggle_radio">
                <span class="icon"><i class="fa fa-check" aria-hidden="true"></i></span>
                <span class="text">최근 후원 시청자</span>
            </label>
            <label class="check_box">
                <input type="radio" data-target=".item_subtitle" name="item_subtitle_startype" data-toggle=".toggle_startype" value="giftimg" class="radio toggle_radio">
                <span class="icon"><i class="fa fa-check" aria-hidden="true"></i></span>
                <span class="text">후원 이미지</span>
            </label>
        </li>
        <li class="input_area toggle_startype toggle_list toggle_add toggle_rank toggle_giftimg">
            <ul class="group_list subtitle_area">
                <li class="input_area toggle_startype toggle_list toggle_add toggle_giftimg">
                    <label class="check_box">
                        <input type="checkbox" data-target=".item_subtitle" name="item_subtitle_staralign" checked="checked" class="check">
                        <span class="icon"><i class="fa fa-check" aria-hidden="true"></i></span>
                        <span class="text">개수 순으로 정렬</span>
                    </label>
                </li>
                <li class="input_area toggle_startype toggle_list">
                    <label class="check_box">
                        <input type="checkbox" data-target=".item_subtitle" name="item_subtitle_listunit" checked="checked" class="check">
                        <span class="icon"><i class="fa fa-check" aria-hidden="true"></i></span>
                        <span class="text">개수별 + 기호 구분 사용</span>
                    </label>
                </li>
                <li class="input_area toggle_startype toggle_rank hide">
                    <label class="check_box">
                        <input type="checkbox" data-target=".item_subtitle" name="item_subtitle_rankmedal" checked="checked" class="check">
                        <span class="icon"><i class="fa fa-check" aria-hidden="true"></i></span>
                        <span class="text">메달 표시</span>
                    </label>
                </li>
                <li class="input_area toggle_startype toggle_giftimg giftimg_width hide">
                    <p class="input_label">후원 이미지 넓이</p>
                    <input type="text" data-target=".item_subtitle" name="item_subtitle_stargiftwidth" placeholder="" value="150" class="input_text input_number sizes">
                    <p class="unit">픽셀</p>
                </li>
            </ul>
        </li>
        <!-- <li class="input_area">
            <p class="input_label tooltip_box">자동 초기화<span class="tooltip"><i class="fa fa-info-circle info" aria-hidden="true"></i><span class="tooltip_text">설명</span></span></p>
            <label class="check_box">
                <input type="checkbox" name="item_subtitle_autoreset" data-target=".item_subtitle" class="check">
                <span class="icon"><i class="fa fa-check" aria-hidden="true"></i></span>
            </label>
        </li> -->
        <li class="input_area">
            <p class="input_label tooltip_box">후원 최소 개수<span class="tooltip"><i class="fa fa-info-circle info" aria-hidden="true"></i><span class="tooltip_text">최소 개수 이상의 후원만 자막에 표시합니다.</span></span></p>
            <input type="text" name="item_subtitle_starmin" data-min="1" value="1" class="input_text input_number sizes">
            <p class="unit">개</p>
        </li>
        <li class="input_area toggle_startype toggle_list toggle_add toggle_total toggle_recent toggle_giftimg">
            <p class="input_label tooltip_box">후원 자막 텍스트<span class="tooltip"><i class="fa fa-info-circle info" aria-hidden="true"></i><span class="tooltip_text">입력한 텍스트를 후원 자막으로 표시합니다.<br><br><span class="title"><i class="fa fa-info-circle info" aria-hidden="true"></i>대체어</span><br>{닉네임} : 후원한 시청자의 닉네임<br>{아이디} : 후원한 시청자의 아이디<br>{개수} : 후원 개수<br>예 : {닉네임}님 {개수}개</span></span></p>
            <textarea name="item_subtitle_startext" data-target=".item_subtitle" placeholder="{닉네임}님 {개수}개" class="input_text">{닉네임}님 {개수}개</textarea>
        </li>
        <li class="input_area toggle_startype toggle_rank hide">
            <p class="input_label tooltip_box">후원 자막 텍스트<span class="tooltip"><i class="fa fa-info-circle info" aria-hidden="true"></i><span class="tooltip_text">입력한 텍스트를 순위 자막 제목으로 표시합니다.<br>{개수} : 후원 개수<br>예 : 오늘의 후원 순위</span></span></p>
            <textarea name="item_subtitle_ranktext" data-target=".item_subtitle" placeholder="오늘의 후원 순위" class="input_text">오늘의 후원 순위</textarea>
        </li>
    </ul>
</div>

<!-- 정렬설정 -->
<div class="setup_title info_box">
    <p class="title">정렬</p>
</div>
<div class="setup_group menu_panel">
    <ul class="group_list">
        <li class="input_area">
            <p class="input_label">가로 정렬</p>
            <label class="check_box">
                <input type="radio" name="item_subtitle_align" value="left" data-target=".item_subtitle" checked="checked" class="radio">
                <span class="icon"><i class="fa fa-check" aria-hidden="true"></i></span>
                <span class="text"><i class="fa fa-align-left" aria-hidden="true"></i> 왼쪽</span>
            </label>
            <label class="check_box">
                <input type="radio" name="item_subtitle_align" value="center" data-target=".item_subtitle" class="radio">
                <span class="icon"><i class="fa fa-check" aria-hidden="true"></i></span>
                <span class="text"><i class="fa fa-align-justify" aria-hidden="true"></i> 가운데</span>
            </label>
            <label class="check_box">
                <input type="radio" name="item_subtitle_align" value="right" data-target=".item_subtitle" class="radio">
                <span class="icon"><i class="fa fa-check" aria-hidden="true"></i></span>
                <span class="text"><i class="fa fa-align-right" aria-hidden="true"></i> 오른쪽</span>
            </label>
        </li>
        <li class="input_area">
            <p class="input_label">세로 정렬</p>
            <label class="check_box">
                <input type="radio" name="item_subtitle_halign" value="top" data-target=".item_subtitle" class="radio">
                <span class="icon"><i class="fa fa-check" aria-hidden="true"></i></span>
                <span class="text"><i class="fa fa-align-up" aria-hidden="true"></i> 위</span>
            </label>
            <label class="check_box">
                <input type="radio" name="item_subtitle_halign" value="bottom" data-target=".item_subtitle" checked="checked" class="radio">
                <span class="icon"><i class="fa fa-check" aria-hidden="true"></i></span>
                <span class="text"><i class="fa fa-align-down" aria-hidden="true"></i> 아래</span>
            </label>
        </li>
    </ul>
</div>

<!-- 필터링설정 -->
<div class="setup_title info_box">
    <p class="title">필터링</p>
</div>
<div class="setup_group menu_panel">
    <ul class="group_list">
        <li class="input_area">
            <p class="input_label tooltip_box">필터링 ID, 닉네임<span class="tooltip"><i class="fa fa-info-circle info" aria-hidden="true"></i><span class="tooltip_text">필터링에 포함된 ID또는 닉네임의 후원은 제외됩니다.</span></span></p>
            <div class="word_area filteridnick" data-type="filteridnick">
                <input type="text" placeholder="필터링 ID, 닉네임 입력" class="input_text sizem">
                <a href="#" class="btns green btn_addword"><i class="fa fa-plus" aria-hidden="true"></i>추가</a>
                <div class="word_list"></div>
            </div>
        </li>
    </ul>
</div>

<!-- 색설정 -->
<div class="setup_title info_box">
    <p class="title">자막색</p>
</div>
<div class="setup_group menu_panel">
    <ul class="group_list">
        <li class="input_area">
            <p class="input_label">폰트색</p>
            <input type="text" name="item_subtitle_color" data-type="style" data-style=".item_subtitle .subtitle,.item_subtitle .star_title{color:{val}!important}.item_subtitle .subtitle .item_gift .gift_text p{color:{val}!important}.text_preview .item_subtitle{color:{val}!important}" value="#FFFFFF" class="input_text input_color">
        </li>
        <li class="input_area">
            <p class="input_label">닉네임색</p>
            <input type="text" name="item_subtitle_colornick" data-type="style" data-style=".item_subtitle .subtitle .nick{color:{val}!important}.item_subtitle .subtitle .item_gift .gift_text .nick{color:{val}!important}.text_preview .item_subtitle .nick{color:{val}!important}" value="#ffc247" class="input_text input_color">
        </li>
        <li class="input_area">
            <p class="input_label">개수색</p>
            <input type="text" name="item_subtitle_colornum" data-type="style" data-style=".item_subtitle .subtitle .value{color:{val}!important}.item_subtitle .subtitle .item_gift .gift_text .gift{color:{val}!important}.text_preview .item_subtitle .value{color:{val}!important}" value="#ffc247" class="input_text input_color">
        </li>
    </ul>
</div>