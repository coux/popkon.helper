<div id="popup_file" class="popup_window">
    <div class="popup_area">
        <div class="popup_header">
            <p class="title"><i class="fa fa-file-image-o" aria-hidden="true"></i>미디어 파일</p>
            <a href="#" title="파일 닫기" class="btn_close"><i class="fa fa-times" aria-hidden="true"></i></a>
        </div>
        <div class="popup_content">
            <div class="file_tooltip">
                <div class="arrow"></div>
                <p>좌측 상자의 파일선택하기 또는 <br>파일을 드래그해서 업로드해보세요.</p>
            </div>
            <div class="file_left">
                <div class="upload_box">
                    <form id="popup_file_form" action="<?=$root_path;?>lib/setup/fileupload.php" enctype="multipart/form-data" method="POST">
                        <input type="file" name="popup_file" class="input_upload">
                        <input type="hidden" name="key" value="<?=$get_key;?>">
                        <input type="hidden" name="type" value="upload">
                    </form>
                    <div class="upload_text"><i class="fa fa-cloud-upload" aria-hidden="true"></i><br>파일 선택 <span class="msg">또는 <br>여기에 드래그&amp;드롭</span></div>
                    <div class="upload_text upload_on"><i class="fa fa-cloud-upload" aria-hidden="true"></i><br>파일을 놓으시면<br>업로드됩니다.</div>
                </div>
                <p class="info_txt"><i class="fa fa-info-circle icon_info" aria-hidden="true"></i>8MB이하의 이미지와<br>4MB이하의 사운드만<br>업로드 가능합니다.</p>
                <p class="file_title">내 파일</p>
                <ul class="file_list">
                    <li><a href="#" data-location="my" data-type="img" class="btn_list my img"><i class="fa fa-file-image-o" aria-hidden="true"></i>이미지</a></li>
                    <? if($page_id != 'banner') {  ?>
						<li><a href="#" data-location="my" data-type="audio" class="btn_list my audio"><i class="fa fa-file-audio-o" aria-hidden="true"></i>사운드</a></li>
					<? } ?>
                </ul>
                <p class="file_title">기본 파일</p>
                <ul class="file_list">
                    <li><a href="#" data-location="default" data-type="img" class="btn_list default img"><i class="fa fa-file-image-o" aria-hidden="true"></i>이미지</a></li>
					<? if($page_id != 'banner') {  ?>
						<li><a href="#" data-location="default" data-type="audio" class="btn_list default audio"><i class="fa fa-file-audio-o" aria-hidden="true"></i>사운드</a></li> 
					<? } ?>
                </ul>
            </div>
            <div class="file_right">
                <div id="popup_file_list"></div>
            </div>
        </div>
        <div class="popup_footer">
            <div class="file_btn">
                <a href="#" class="btns green btn_select"><i class="fa fa-check" aria-hidden="true"></i>선택</a>
                <a href="#" class="btns btn_cancel"><i class="fa fa-times" aria-hidden="true"></i>취소</a>                
            </div>
        </div>
        
        <div class="popup_mask"><div class="loader"></div></div>
        <div class="audio_box"></div>
    </div>
</div>