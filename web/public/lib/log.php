<?
    header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Credentials: true");
    header("Access-Control-Allow-Methods: GET, POST");
    header("Access-Control-Allow-Headers: X-Requested-With");
    header("Content-Type:application/json; charset=utf-8");
	
	$get_broad = isset($_REQUEST["broad"]) ? $_REQUEST["broad"] : "";
    $get_type = isset($_REQUEST["type"]) ? $_REQUEST["type"] : "";
	$get_id = isset($_REQUEST["id"]) ? $_REQUEST["id"] : "";
	$get_name = isset($_REQUEST["name"]) ? $_REQUEST["name"] : "";
	$get_data = isset($_REQUEST["data"]) ? $_REQUEST["data"] : "";
	
	$current_date = date('Y/m/d H:i:s', time());
    $result = new stdClass;
    $result->result = -1;
	
	if($get_type === "log") {
		file_put_contents("./../log/".$get_name.".json", "[" . $current_date . "] : " . json_encode($get_data, JSON_UNESCAPED_UNICODE)."\r\n", FILE_APPEND);
        //file_put_contents("./../log/".$get_name."2.json", "[" . $current_date . "] : " . json_encode($get_data)."\r\n", FILE_APPEND);
	}
	
	echo json_encode($result, JSON_UNESCAPED_UNICODE);
?>