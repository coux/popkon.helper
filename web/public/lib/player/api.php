<?
	header("Access-Control-Allow-Origin: *");
	header("Access-Control-Allow-Credentials: true");
	header("Access-Control-Allow-Methods: GET, POST");
	header("Access-Control-Allow-Headers: X-Requested-With");
	header("Content-Type:application/json; charset=utf-8");
	
	include "/home/fullhp/public_html/lib/common.php";
	
	$mem = new Memcached();
	$mem->addServer($mem_ip, $mem_port);
	
	$get_key = isset($_GET["key"]) ? $_GET["key"] : "";
	$get_type = isset($_GET["type"]) ? $_GET["type"] : "";
	$get_id = isset($_GET["id"]) ? $_GET["id"] : "";
	$get_token = isset($_GET["token"]) ? $_GET["token"] : "";
	$get_refresh = isset($_GET["refresh"]) ? $_GET["refresh"] : "";
	$result = new stdClass;
	$result->data = $get_mem_data = new stdClass;
	$result->result = "";

	if(isset($get_key)) {
		$user_key = new userKey($fullhp_keycode);
		$page_key = "";
		$login_info = new stdClass;
		$login_info->id = "";
		$login_info->state = "";
		$login_info->refresh = "";
		$login_info->token = "";
		$login_info->tokenEndDateTime = "";
		$login_info->expires_in = 0;
		
		if(!empty($get_key)) {
			$player_array = explode("/", $get_key);
			$get_page = $player_array[0];
			$get_page = $user_key->decrypt($get_page);
			$get_array = explode("/", $get_page);
			if(count($get_array) > 1) {
				$page_id = $get_array[0];
				$page_key = $user_key->encrypt($get_array[1]);
				$page_sub = isset($get_array[2]) ? $get_array[2] : 0;
				$page_list = array(array($page_id, $page_sub));
			}
		}

		if(!empty($page_key) && $user_key->decrypt($page_key) === $get_id) {
			$get_mem = $mem->get("userinfo_".$get_id);
			if($get_mem) {
				$get_mem_data = @json_decode($get_mem);
				if($get_mem_data && property_exists($get_mem_data, "expTime") && (time() - $get_mem_data->expTime) < 0) {
					$login_info->id = $get_mem_data->id;
					$login_info->state = isset($get_mem_data->state) ? $get_mem_data->state : "full";
					$login_info->token = $get_mem_data->token;
					$login_info->refresh = $get_mem_data->refresh;
					$login_info->expires_in = $get_mem_data->expires_in;
					$login_info->tokenEndDateTime = $get_mem_data->tokenEndDateTime;
					if($get_type == "userinfo") {
						$result->result = "cache";
					}
				}
			}
			
			if(empty($login_info->id) || empty($login_info->token)) {
				$user_data = new loginInfo();
				$get_user = $user_data->dbload($get_id);
				if(property_exists($get_user, "idx") && !empty($get_user->idx)) {
					$login_info->id = $get_user->id;
					$login_info->state = isset($get_user->state) ? $get_user->state : "full";
					if($get_type == "userinfo") {
						$tokencheckd = $user_data->tokencheck($get_user->token, $get_user->refresh, $get_user->idx, $get_user->tokenEndDateTime, $get_user->state);

						if($tokencheckd != null && property_exists($tokencheckd, "access_token") && !empty($tokencheckd->access_token)) {
							$login_info->refresh = $tokencheckd->refresh;
							$login_info->token = $tokencheckd->access_token;
							$login_info->expires_in = $tokencheckd->expires_in;
							$login_info->tokenEndDateTime = $tokencheckd->tokenEndDateTime;
						} else {
							$login_info->refresh = $get_user->refresh;
							$login_info->token = $get_user->token;
							$login_info->expires_in = $get_user->expires_in;
							$login_info->tokenEndDateTime = $get_user->tokenEndDateTime;
						}

						$user_data->rawMessage = null;
						$get_mem_data = $user_data->userinfo($login_info->token, $login_info->state);

						if(stripos($user_data->rawMessage, 'access_token expire') !== false) {
							$tokencheckd = $user_data->tokencheck($get_user->token, $get_user->refresh, $get_user->idx, '0000-00-00 00:00:00', $get_user->state);
							if($tokencheckd != null && property_exists($tokencheckd, "access_token") && !empty($tokencheckd->access_token)) {
								$login_info->refresh = $tokencheckd->refresh;
								$login_info->token = $tokencheckd->access_token;
								$login_info->expires_in = $tokencheckd->expires_in;
								$login_info->tokenEndDateTime = $tokencheckd->tokenEndDateTime;

								$get_mem_data = $user_data->userinfo($login_info->token, $login_info->state);
							}
						}

						if($get_mem_data && property_exists($get_mem_data, "id")) {
							$get_mem_data->expTime = time() + $memtime->userinfo;
							$get_mem_data->token = $login_info->token;
							$get_mem_data->refresh = $login_info->refresh;
							$get_mem_data->state = $login_info->state;
							$get_mem_data->expires_in = $login_info->expires_in;
							$get_mem_data->tokenEndDateTime = $login_info->tokenEndDateTime;
							$mem->set("userinfo_".$get_id, json_encode($get_mem_data), $get_mem_data->expTime);
						}

						$result->result = "user";
					} else {
						$login_info->token = $get_user->token;
						$login_info->refresh = $get_user->refresh;
					}
				}
			}

			if($get_type == "userinfo") {
				if(property_exists($get_mem_data, "id") && !empty($get_mem_data->id)) {
					$result->data = $get_mem_data;
					if($result->data) {
						if(property_exists($result->data, "refresh")) {
							if(!empty($result->data->refresh) && $get_refresh !== $result->data->refresh) {
								if(!property_exists($result, 'token')) {
									$result->token = new stdClass;
								}
								$result->token->refresh = $result->data->refresh;
							}
							unset($result->data->refresh);
						}
						if(property_exists($result->data, "token")) {
							if(!empty($result->data->token) && $get_token !== $result->data->token) {
								if(!property_exists($result, 'token')) {
									$result->token = new stdClass;
								}
								$result->token->access_token = $result->data->token;
							}
							unset($result->data->token);
						}

						if(property_exists($result->data, "expTime"))
							unset($result->data->expTime);
						if(property_exists($result->data, "sessKey"))
							unset($result->data->sessKey);
					}
				} else {
					$result->error_message = '로그인 정보가 올바르지 않습니다. 다시 로그인해 주세요.';
					if($user_data != null &&$_SERVER['REMOTE_ADDR'] === '106.255.227.90') {
						$result->rawMessage = $user_data->rawMessage;
					}
				}
			} else if($get_type == "liveplay") {
				$get_mem = $mem->get("liveplay_".$login_info->id);
				if($get_mem) {
					$result->data = @json_decode($get_mem);
					if($result->data && property_exists($result->data, "media") && property_exists($result->data, "expTime") && (time() - $result->data->expTime) < 0) {
						$result->result = "cache";
						unset($result->data->expTime);
					} else {
						$result->data = new stdClass;
					}
				}

				if(!$result->data || !property_exists($result->data, "media")) {
					$user_data = new loginInfo();
					$mediaData = $user_data->liveinfo($login_info->token, $login_info->state);
					
					if($mediaData && property_exists($mediaData, "media")) {
						$result->data = $mediaData;
						$result->result = "live";
						if(property_exists($result->data, "sessKey"))
							unset($result->data->sessKey);
						$result->data->expTime = time() + $memtime->liveplay;
						$mem->set("liveplay_".$login_info->id, json_encode($result->data), $result->data->expTime);
						unset($result->data->expTime);
					}
				}
			}
		}
	}
	/*
	else {
		$get_idx = isset($_GET["idx"]) ? $_GET["idx"] : "";
		$get_state = isset($_GET["state"]) ? $_GET["state"] : "full";
		if($get_type == "userinfo") {
			if($get_token != "") {
				$get_mem = $mem->get("userinfo_".$get_id);
				if($get_mem) {
					$get_mem_data = @json_decode($get_mem);
					$result->data = $get_mem_data;
					if($result->data && property_exists($result->data, "expTime") && (time() - $result->data->expTime) < 0) {
						$result->result = "cache";
						unset($result->data->expTime);
					} else {
						$result->data = new stdClass;
					}
				}
				
				if(!$result->data || !property_exists($result->data, "id")) {
					$user_data = new loginInfo();
					$result->data = $user_data->userinfo($get_token, $get_state);
					$result->result = "user";
					if(!isset($result->data->id)) {
						$result->token = $user_data->tokenrefresh($get_refresh, $get_idx, $get_state);
						$result->result = "";
						if(isset($result->token->access_token)) {
							$result->data = $user_data->userinfo($result->token->access_token, $get_state);
							$result->result = "token";
						}
					}
					
					if($result->data && property_exists($result->data, "id")) {
						if(property_exists($result->data, "sessKey"))
							unset($result->data->sessKey);
						$result->data->expTime = time() + $memtime->userinfo;
						$mem->set("userinfo_".$get_id, json_encode($result->data), $result->data->expTime);
						unset($result->data->expTime);
					}
				}
			}
		} else if($get_type == "liveplay") {
			if($get_token != "") {
				$get_mem = $mem->get("liveplay_".$get_id);
				if($get_mem) {
					$get_mem_data = @json_decode($get_mem);
					$result->data = $get_mem_data;
					if($result->data && property_exists($result->data, "expTime") && (time() - $result->data->expTime) < 0) {
						$result->result = "cache";
						unset($result->data->expTime);
					} else {
						$result->data = new stdClass;
					}
				}

				if(!$result->data || !property_exists($result->data, "media")) {
					$user_data = new loginInfo();
					$result->data = $user_data->liveinfo($get_token, $get_state);
					$result->result = "live";
					
					if($result->data && property_exists($result->data, "media")) {
						if(property_exists($result->data, "sessKey"))
							unset($result->data->sessKey);
						$result->data->expTime = time() + $memtime->liveplay;
						$mem->set("liveplay_".$get_id, json_encode($result->data), $result->data->expTime);
						unset($result->data->expTime);
					}
				}
			}
		}
	}
	*/
	
	echo json_encode($result);