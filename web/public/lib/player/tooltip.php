<div class="tooltip_box">
    <div class="tooltip_left">
        <label title="사용 ON/OFF" class="toggle_box">
            <input type="checkbox" checked="chekced" class="check tooltip_use">
            <span class="label"></span>
        </label>
        <!-- <select title="프리셋" class="select tooltip_preset">
        </select> -->
        <select title="폰트크기" class="select tooltip_fontsize">
            <option value="">폰트크기</option>
            <option value="6px">6픽셀</option>
            <option value="8px">8픽셀</option>
            <option value="10px">10픽셀</option>
            <option value="12px">12픽셀</option>
            <option value="14px">14픽셀</option>
            <option value="16px">16픽셀</option>
            <option value="18px">18픽셀</option>
            <option value="20px">20픽셀</option>
            <option value="22px">22픽셀</option>
            <option value="24px">24픽셀</option>
            <option value="26px">26픽셀</option>
            <option value="28px">28픽셀</option>
            <option value="30px">30픽셀</option>
            <option value="34px">34픽셀</option>
            <option value="38px">38픽셀</option>
            <option value="42px">42픽셀</option>
            <option value="48px">48픽셀</option>
            <option value="54px">54픽셀</option>
            <option value="60px">60픽셀</option>
        </select>
        <select title="창투명도" class="select tooltip_opacity">
            <option value="">창투명도</option>
            <option value="1">0%(보임)</option>
            <option value="0.9">10%</option>
            <option value="0.8">20%</option>
            <option value="0.7">30%</option>
            <option value="0.6">40%</option>
            <option value="0.5">50%</option>
            <option value="0.4">60%</option>
            <option value="0.3">70%</option>
            <option value="0.2">80%</option>
            <option value="0.1">90%</option>
            <option value="0">100%(투명)</option>
        </select>
        <label title="사운드 ON/OFF" class="toggle_box tooltip_icon">
            <input type="checkbox" checked="chekced" class="check tooltip_sound">
            <span class="label">
                <i class="fa fa-volume-up on" aria-hidden="true"></i>
                <i class="fa fa-volume-off off" aria-hidden="true"></i>
            </span>
        </label>
    </div>
    <div class="tooltip_right">
        <a href="#" title="초기화" class="btns btn_remove"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
        <a href="#" onclick="location.reload(); return false;" title="새로고침" class="btns btn_refresh"><i class="fa fa-refresh" aria-hidden="true"></i></a>
        <a href="#" target="_blank" title="설정페이지" class="btns btn_setup"><i class="fa fa-cog" aria-hidden="true"></i></a>
    </div>
</div>