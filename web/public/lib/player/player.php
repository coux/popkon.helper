
<? if($page_id == "chat") { ?>
<div data-pageid="chat" class="item_wrap item_<?=$page_id?>_wrap <?=$page_id == "chat" ? "on": ""?>">
    <? include $root_path."lib/player/tooltip.php"; ?>
    <div class="item_box item_chat">
        <div class="item_area">
            <ul class="chat_list"></ul>
        </div>
    </div>
</div>
<? } else if($page_id == "alert") { ?>
<div data-pageid="alert" class="item_wrap item_<?=$page_id?>_wrap <?=$page_id == "alert" ? "on": ""?>">
    <? include $root_path."lib/player/tooltip.php"; ?>
    <!-- <a href="#" data-type="star" class="btns btn_alert">하트</a>
    <a href="#" data-type="up" class="btns btn_alert">추천</a>
    <a href="#" data-type="chat" class="btns btn_alert">채팅</a> -->
        
    <div class="item_box item_alert">
        <div class="item_area item_alert_area">
            <div class="alert_box"></div>
        </div>
    </div>
</div>
<? } else if($page_id == "goal") { ?>
<div data-pageid="goal" class="item_wrap item_<?=$page_id?>_wrap <?=$page_id == "goal" ? "on": ""?>">
    <? include $root_path."lib/player/tooltip.php"; ?>
    <div class="item_box item_goal">
        <div class="item_area">
            <div class="goal_bar_area">
                <div class="goal_box">
                    <div class="goal_text text"></div>
                    <div class="goal_bar"><div class="goal_inner animate"></div></div>
                    <div class="goal_start text"><span class="value"></span><span class="val"></span></div>
                    <div class="goal_end text"><span class="value"></span><span class="val"></span></div>
                    <div class="goal_textin textin"></div>
                </div>
            </div>
            <div class="goal_circle_area">
                <div class="goal_box">
                    <div class="text_box">
                        <div class="goal_text text"></div>
                        <div class="textin_box"><div class="goal_textin textin"></div></div>
                    </div>
                </div>
                <div class="goal_progress"></div>
            </div>
        </div>
    </div>
</div>
<? } else if($page_id == "subtitle") { ?>
<div data-pageid="subtitle" class="item_wrap item_<?=$page_id?>_wrap <?=$page_id == "subtitle" ? "on": ""?>">
    <? include $root_path."lib/player/tooltip.php"; ?>
    <div class="item_box item_subtitle">
        <div class="subtitle"></div>
    </div>
</div>
<? } else if($page_id == "text") { ?>
<div data-pageid="text" class="item_wrap item_<?=$page_id?>_wrap <?=$page_id == "text" ? "on": ""?>">
    <? include $root_path."lib/player/tooltip.php"; ?>
    <div class="item_box item_text"></div>
</div>
<? } else if($page_id == "banner") { ?>
<div data-pageid="banner" class="item_wrap item_<?=$page_id?>_wrap <?=$page_id == "banner" ? "on": ""?>">
    <? include $root_path."lib/player/tooltip.php"; ?>
    <div class="item_box item_banner"></div>
</div>
<? } else if($page_id == "timer") { ?>
<div data-pageid="timer" class="item_wrap item_<?=$page_id?>_wrap <?=$page_id == "timer" ? "on": ""?>">
    <? include $root_path."lib/player/tooltip.php"; ?>
    <div class="item_box item_timer"></div>
</div>
<? } ?>